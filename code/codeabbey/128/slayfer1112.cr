#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 3.78 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(Float64)
  data.each_line do |x|
    inter = [] of Float64
    (x.split).each do |y|
      y = y.is_a?(String) ? y.try &.to_f : y
      inter << y
    end
    values << inter
  end
  values[1..]
end

def fact(n)
  return 1 if n == 0
  n * fact(n - 1)
end

def solution(array)
  n = array[0]
  k = array[1]
  nk = n-k

  nf = fact(n)
  kf = fact(k)
  nkf = fact(nk)

  c = Int64.new((nf/(kf*nkf)).round(0))

  print "#{c} "
end

data = data_entry()
data.each do |x|
  solution(x)
end
puts

# $ ./slayfer1112
# 53060358690 11050084695 10639125640 28987537150
# 10235867928 35607051480 22760723700 65033528560
