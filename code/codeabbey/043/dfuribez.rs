/*
$ clippy-driver dfuribez.rs -D warnings
$ rustc dfuribez.rs
*/

use std::io::{self, BufRead};

fn main() {
  let stdin = io::stdin();
  let lines = stdin.lock().lines();

  for (index, line) in lines.enumerate() {
    let l = line.unwrap();
    if index > 0 {
      let value: f64 = l.parse().unwrap_or(0.0);
      print!("{} ", (value * 6f64).floor() as u8 + 1);
    }
  }
  println!();
}

/*
$ cat DATA.lst | ./dfuribez
3 1 4 6 4 5 2 3 3 3 6 2 2 2 5 6 1 5 1 3 6 6 3
*/
