;; $ clj-kondo --lint slayfer1112.clj
;; linting took 177ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        head (str/split (data 0) #" ")
        body (subvec data 1 (alength (to-array-2d data)))]
    [head
     body]))


(defn average
  [numbers]
  (if (empty? numbers)
    0
    (/ (reduce + numbers) (- (count numbers) 1))))

(defn solution [_ body]
  (doseq [x body]
    (let [vals (str/split x #" ")
          vals-i (map read-string vals)
          avg (Math/round (double (average vals-i)))]
      (print (str avg " ")))))

(defn main []
  (let [[head body] (get-data)]
    (solution head body)
    (println)))
(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; 277 611 10977 517 1098 5051 135 138 947 4499 7933 4607 471
