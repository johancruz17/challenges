open System

[<EntryPoint>]
let main argv =

    printfn "ingrese la cantidad de pruebas:"
    let c = stdin.ReadLine() |> int
    let mutable out = Array.zeroCreate c
    for i in 0..(c-1) do
        let mutable sum = 0
        let mutable prom = 0.0
        printfn "(%i) ingrese los datos separados por un espacio y con un cero al final" (i+1)
        let arrayData = stdin.ReadLine().Split ' '
        for j in 0..(arrayData.Length-2) do
            sum <- sum + (int arrayData.[j])
        prom <- (float sum) / (float (arrayData.Length-1))
        let result = (prom- Math.Floor(prom))*10.0 //obtener parte decimal
        if (result >= 5.0) then (out.[i] <- int (Math.Ceiling(prom))) else out.[i] <- Convert.ToInt32(prom)

    for i in 0..(out.Length-1) do
        printf "%i " out.[i]
    Console.ReadKey() |> ignore
    0 // devolver un codigo de salida entero
