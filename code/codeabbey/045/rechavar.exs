# Checking 1 source file ...

# Please report incorrect results: https://github.com/rrrene/credo/issues

# Analysis took 0.1 seconds (0.00s to load, 0.1s running 54 checks on 1 file)
# 13 mods/funs, found no issues.

# iex(1)> c("rechavar.exs")

defmodule CardsShuffling do
@moduledoc "
This is module solves Codeabbey exercise number 045 (Cards Shuffling)
"

  def main do
    list = IO.read(:stdio, :line)
    string_values = Enum.at(String.split(list, "\n"), 0)
    list = String.split(string_values, " ")
    int_values = Enum.map(list, &String.to_integer/1)
    fixed_values = fix_values(int_values, [])
    int_deck = Enum.to_list 0..51
    deck = card_names(int_deck, [])
    swaped_deck = swap_values(fixed_values, deck)

    Enum.each swaped_deck, fn item ->
      IO.puts "#{item} "
    end
  end

  def fix_values([h|t], list) do
    if h > 52 do
      list = append_values(list, [rem(h, 52)])
      fix_values(t, list)
    else
      list = append_values(list, [rem(h, 52)])
      fix_values(t, list)
    end
  end

  def fix_values([], list) do
    list
  end

  def append_values(list1, list2) do
    list1 ++ list2
  end

  def card_names([h|t], list) do
    number_rank = rem(h, 13)
    rank = get_rank(number_rank - 1)
    number_suit = floor(h / 13)
    suit = get_suit(number_suit - 1)

    card = suit <> rank

    list = append_values(list, [card])

    card_names(t, list)
  end

  def card_names([], list) do
    list
  end

  def get_suit(number) do
    suit = ["D", "H", "S", "C"]
    Enum.at(suit, number)
  end

  def get_rank(number) do
    rank = ["2", "3", "4", "5", "6", "7", "8", "9", "T", "J", "Q", "K", "A"]
    Enum.at(rank, number)
  end

  def swap_values([h|t], deck2) do
    pos2 = h
    pos1 = length(deck2) - length(t) - 1
    value1 = Enum.at(deck2, pos1)
    value2 = Enum.at(deck2, pos2)

    deck2 = insert_values(deck2, value1, value2, pos1, pos2)

    swap_values(t, deck2)
  end

  def swap_values([], deck2) do
    deck2
  end

  def insert_values(deck, value1, value2, pos1, pos2) do

    cond do
      pos1 > pos2 -> swap(deck, value2, value1, pos2, pos1)

      pos2 > pos1 -> swap(deck, value1, value2, pos1, pos2)

      pos2 == pos1 -> deck
    end

  end

  def swap(deck, value_upper, value_lower, upper, lower) do
    deck
    |> List.insert_at(lower, value_upper)
    |> List.delete_at(lower + 1)
    |> List.insert_at(upper, value_lower)
    |> List.delete_at(upper + 1)
  end

end

# cat DATA.lst | elixir -r rechavar.exs -e CardsShuffling.main
# H7 HT D6 DT SA C4 C7 C2 CJ C3 DQ S9 D3 H5 CT H4 H9 D4 H6 S4 D2 DK H8 CK SQ S8
# H3 C5 C8 D8 HK C9 DJ DA S5 CA ST S6 D7 SK D5 HQ H2 D9 S2 C6 S7 CQ SJ HA HJ S3
