#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.24 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

RANKS = "A23456789TJQK"
SUITS = "CDHS"

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args.map {|x| x.to_i}

deck = [] of String

SUITS.each_char do |suit|
  RANKS.each_char do |rank|
    deck << "#{suit}#{rank}"
  end
end

(0...deck.size).each do |x|
  n = args[x] % deck.size
  temp = deck[n]
  deck[n] = deck[x]
  deck[x] = temp
end

deck.each do |card|
  print "#{card} "
end

puts

# $ ./richardalmanza.cr
# H7 HT D6 DT SA C4 C7 C2 CJ C3 DQ S9 D3 H5 CT H4 H9 D4 H6 S4 D2 DK H8 CK SQ
# S8 H3 C5 C8 D8 HK C9 DJ DA S5 CA ST S6 D7 SK D5 HQ H2 D9 S2 C6 S7 CQ SJ HA
# HJ S3
