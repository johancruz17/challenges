/*
linter:
cppcheck \
  --error-exitcode=1 \
  slayfer1112.c \
&&  splint \
  -strict \
  -internalglobs \
  -modfilesys \
  -boundsread \
  slayfer1112.c
Checking slayfer1112.c ...
Splint 3.1.2 --- 05 Sep 2017

Finished checking --- no warnings

compilation:
gcc \
  -Wall \
  -Wextra \
  -Winit-self \
  -Wuninitialized \
  -Wmissing-declarations \
  -Winit-self \
  -ansi \
  -pedantic \
  -Werror \
  slayfer1112.c \
  -o slayfer1112 \
  -lm
*/
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#define MAX_SIZE 1024

static int shuffling (/*@in@*/ char cards[][3],
  /*@in@*/ int nums[]) /*@modifies cards[][3]@*/ {
  char temp[3];
  int j = 0;
  int mod_num = 0;
  while (j < 52) {
    mod_num = nums[j] % 52;
    temp[0] = cards[mod_num][0], temp[1] = cards[mod_num][1];
    cards[mod_num][0] = cards[j][0], cards[mod_num][1] = cards[j][1];
    cards[j][0] = temp[0], cards[j][1] = temp[1];
    j++;
  }
  for (j = 0; j < 52; j++) { printf("%s ", cards[j]); }
  printf("\n");
  return 1;
}

int main(void) {
  static char cards[52][3];
  static int vals[52];
  char temp[] = {'0', '0', '\0'}, kinds[] = {'C', 'D', 'H', 'S'};
  char special[] = {'A', 'T', 'J', 'Q', 'K'}, numbers[MAX_SIZE];
  char values[MAX_SIZE];
  int i, j;
  int z = 0;
  int k = 0;
  for (j = 0; j < 4; j++) {
    for (i = 0; i < 13; i++) {
      temp[0] = kinds[j];
      if (i == 0 || i >= 9) { temp[1] = special[k]; k++; }
      else { temp[1] = (char) (49 + i); }
      cards[z][0] = temp[0], cards[z][1] = temp[1], cards[z][2] = '\0';
      z++;
    } k = 0;
  }
  j = 0, i = 0;
  if (fgets(values, MAX_SIZE, stdin) == 0) { return 0; }
  while (true) {
    if (values[i] != '\n') {
      if (values[i] != ' ') { numbers[k] = values[i]; }
      else { numbers[k] = '\0', vals[j] = atoi(numbers), j++, k = -1; }
      k++, i++;
    } else { numbers[k] = '\0', vals[j] = atoi(numbers); break; }
  }
  i = shuffling(cards, vals);
  return i;
}

/*
$ cat DATA.lst | ./slayfer1112
ST C2 CK HA C7 DQ DK S6 SK D9 D7 DT D5 CQ DJ H8 D2 C5 DA H9 D3 C6 H3 H6 HJ S5
CA CJ SJ S3 HT HK C9 D6 S9 S8 S2 SQ C4 H2 D4 CT H7 S4 S7 H4 D8 H5 C3 C8 HQ SA
*/
