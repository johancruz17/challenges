-- luacheck oscardjuribe.lua
-- Checking oscardjuribe.lua                         OK
-- Total: 0 warnings / 0 errors in 1 file

--[[
Code to solve the problem Fibonacci Divisibility Advanced
from codeabbey in lua
]]

local function find_divisor(mod)
  --[[
  method to find first divisor using modular arithmetic
  using modular arithmetic addition
  ( a + b ) % c == (( a % c ) + ( b % c)) % c
  ]]
  -- var to store F(n-2) term of Fibonacci sequence module value to search
  local a = 0 % mod
  -- var to store F(n-1) term of Fibonacci sequence module value to search
  local b = 1 % mod
  -- current Fibonacci term
  local fibo = ( a + b ) % mod
  -- var to count current Fibonacci position
  local count = 1

 -- iterate until the current fibo=0, that means the current number is divisible
  while fibo ~= 0 do
    -- calculate current Fibonacci number
    fibo = (( a % mod ) + ( b % mod ))%mod
    -- update F(n-2)
    a = b
    -- update F(n-1)
    b = fibo
    -- update counter
    count =  count + 1
  end

  -- return value
  return count
end

-- read data from file
local file = io.open("DATA.lst", "r")
-- file as input
io.input(file)

-- read firts number
local test_cases = io.read("*n")

-- var to concat solutions
local solution = ""

-- iterate over number of test_cases
for _ in 1, test_cases do
  -- read value
  local number = io.read("*n")
  -- call function an concat to solution
  solution = solution .. find_divisor(number) .. " "
end

-- print solution
print(solution)

-- lua oscardjuribe.lua
-- 71100 123000 1400 28290 60414 233172 3480 857744 54420 320630
-- 29160 1015890 118608 73308 55836 12654 101610 172533 68100
