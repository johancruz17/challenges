/*
$ clippy-driver dfuribez.rs -D warnings
$ rustc dfuribez.rs
*/

use std::io::{self, BufRead};

fn gcd(aa: u32, bb: u32) -> u32 {
  let mut a = aa;
  let mut b = bb;
  if a == b {
    a
  } else {
    if a < b {
      b -= a;
    } else {
      a -= b;
    }
    gcd(a, b)
  }
}

fn egcd(n1: u32, n2: u32) -> (u32, i32, i32) {
  let mut n1 = n1;
  let mut n2 = n2;
  let n_gcd = gcd(n1, n2);

  let mut t: [i32; 2] = [0, 1];
  let mut s: [i32; 2] = [1, 0];

  let mut q;
  let mut r = 1;

  loop {
    if r == 0 {
      break;
    } else {
      q = (n1 / n2) as u32;
      r = (n1 % n2) as u32;
      let s_next = s[0] - q as i32 * s[1];
      let t_next = t[0] - q as i32 * t[1];
      n1 = n2;
      n2 = r;
      t[0] = t[1];
      t[1] = t_next;
      s[0] = s[1];
      s[1] = s_next;
    }
  }

  (n_gcd, s[0], t[0])
}

fn main() {
  let stdin = io::stdin();
  let lines = stdin.lock().lines();
  let mut first_line = true;
  let mut switched;
  for line in lines {
    let line = line.unwrap();
    if !first_line {
      let params: Vec<&str> = line.split_whitespace().collect();
      let n1: u32 = params[0].parse().unwrap_or(0);
      let n2: u32 = params[1].parse().unwrap_or(0);
      let result;
      if n1 > n2 {
        result = egcd(n1, n2);
        switched = false;
      } else {
        result = egcd(n2, n1);
        switched = true;
      }
      let (x0, y0, z0) = result;
      if switched {
        println!("{} {} {}", x0, z0, y0);
      } else {
        println!("{} {} {}", x0, y0, z0);
      }
    } else {
      first_line = false;
    }
  }
}

/*
$ cat DATA.lst | ./dfuribez
3 -1383 941
1 -21296 12741
5 905 -808
2 5935 -2641
1 -24691 21609
1 -18862 4499
6 -4782 2155
1 42207 -41467
1 16485 -21523
30 -1307 1066
3 3727 -5321
232 -41 114
1 -19079 23388
1 1118 -1853
4 -103 265
1 -1908 1249
1 -44032 40517
1 42723 -25244
2 -7811 1749
5 3217 -3025
1 -22833 12377
32 -472 259
4 2488 -5211
1 -17068 22667
*/
