/*
$ cargo clippy
Checking sullenestd1 v0.1.0 (/.../sullenestd1)
Finished dev [unoptimized + debuginfo] target(s) in 0.10s
$ cargo build
Compiling sullenestd1 v0.1.0 (/.../sullenestd1)
Finished dev [unoptimized + debuginfo] target(s) in 0.28s
$ rustrc sullenestd1.rs
*/

use std::io;
fn generator(aux: &mut i64) {
  *aux = *aux * *aux;
  *aux /= 100;
  *aux %= 10000;
}

fn main() {
  let mut line: String = String::new();
  io::stdin()
    .read_line(&mut line)
    .expect("Failed to read line");
  line.clear();
  io::stdin()
    .read_line(&mut line)
    .expect("Failed to read line");
  let inputs: Vec<i64> = line
    .trim()
    .split(' ')
    .map(|x| x.parse().expect("Not an integer!"))
    .collect();
  let mut seq: Vec<i64> = Vec::new();
  for y in inputs {
    let mut aux: i64 = y;
    'outer: loop {
      generator(&mut aux);
      if aux == y {
        break;
      }
      if seq.len() > 1 {
        for x in &seq {
          if x == &aux {
            seq.push(aux);
            break 'outer;
          }
        }
      }
      seq.push(aux);
    }
    println!("{}", seq.len());
    seq.clear();
  }
}

/*
$ cat DATA.lst | ./sullenestd1
106
111
97
101
98
98
105
98
110
100
101
*/
