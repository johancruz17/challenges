# $ lintr::lint('dantivar.r')
input <- readLines("stdin")
i <- 1

result <- c()

objects <- as.integer(input[1])

while (i <= objects) {
    value <- as.numeric(input[i + 1])

    if (value <= 1) {
        result <- append(result, value)
    } else {
        index <- round(2.078087 * log(value) + 1.672276)
        result <- append(result, index)
    }
    i <- i + 1
}

cat(paste(result, sep = " "))
# $ cat DATA.lst | Rscript dantivar.r
# 496 440 209 334 450 230 822 939 986 819 618 224 242 19
