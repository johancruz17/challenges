{- Linting:
$ hlint jdanilo7.hs
No hints

Compilation:
$ ghc -Wall -Wmissing-local-signatures -Wmissing-signatures jdanilo7.hs
[1 of 1] Compiling Main             ( jdanilo7.hs, jdanilo7.o )
Linking jdanilo7.exe ...
-}

calcYears :: Float -> Float -> Float -> Int -> Int
calcYears mon target rate year
  | mon >= target = year
  | otherwise = calcYears (mon * (1.0 + rate /100.0)) target rate (year + 1)

calcAll :: [[Float]] -> String
calcAll [] = ""
calcAll (x:xs) = show years ++ " " ++ calcAll xs
        where
        years :: Int
        years = calcYears (head x) (head (tail x)) (head (tail (tail x))) 0

splitLines :: [String] -> [[Float]]
splitLines = map (map (read :: String -> Float) . words)

main :: IO ()
main = do
    src <- readFile "DATA.lst"
    let lins = lines src
    putStr $ calcAll $ splitLines $ tail lins

{- $ runhaskell jdanilo7.hs
9 31 8 8 10 26 7 43 8 66 7 100 37 48 6 17 10 8
-}
