;;$ clisp -c frank1030.lsp
;; Compiling file /Users/Frank/Desktop/frank/052/frank1030.lsp ...
;; Wrote file /Users/Frank/Desktop/frank/052/frank1030.fas
;;0 errors, 0 warnings
;;Bye.

(defun getIndex()
  (let ((dat))
  (setq dat(read))
  )
)

(defun Theorem()
  (let ((result "") (sidea 0) (sideb 0) (sidec 0) (rc 0) (rab 0) (index 0))
    (setq index(getIndex))
    (loop for a from 0 to (- index 1)
      do (setq sidea(read))
      do (setq sideb(read))
      do (setq sidec(read))
      do (setq rc(* sidec sidec))
      do (setq rab(+ (* sidea sidea) (* sideb sideb)))
        (if (< rc rab)
          (setq result (concatenate 'string result "A ")))
        (if (= rc rab)
          (setq result (concatenate 'string result "R ")))
        (if (> rc rab)
          (setq result (concatenate 'string result "O ")))
    )
    (format t "~a" result)
  )
)
(Theorem)

;;$ cat DATA.lst | clisp frank1030.lsp
;;A A R O A R R O O O O R R O O R O O O R R R
