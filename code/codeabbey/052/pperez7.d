/*
dscanner.exe --styleCheck pperez7.d
dscanner.exe --syntaxCheck pperez7.d
dscanner.exe --report pperez7.d
"issues": [];
*/

import std.stdio;
import std.file;
import std.array;
import std.conv;
import std.math;
import std.format;

void main(){
  File file = File("DATA.lst", "r");
  string testcases = file.readln();
  const int triangles = parse!int(testcases);
  string [] answers = new string[](triangles);

  for(int i = 0; i < triangles; i++){
    string input = file.readln();
    string [] sides = input.split(" ");
    const int a = parse!int(sides[0]);
    const int b = parse!int(sides[1]);
    const int c = parse!int(sides[2]);
    if(pow(a,2) + pow(b,2) == pow(c,2)){
      answers[i]="R";
    }
    if(pow(a,2) + pow(b,2) > pow(c,2)){
      answers[i]="A";
    }
    if(pow(a,2) + pow(b,2) < pow(c,2)){
      answers[i]="O";
    }
  }
  writefln("%-(%s %)",answers[]);
}

/*
dmd -run pperez7.d
O A A R R R R R O O A A O A R R R R O A
*/
