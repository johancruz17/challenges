/*
$ clippy-driver dfuribez.rs -D warnings
$ rustc dfuribez.rs
*/

use std::io::{self, BufRead};

fn calculate(min: u32, med: u32, max: u32) {
  let hipotenuse: u32 =
    (min.pow(2u32) as f64 + med.pow(2u32) as f64).sqrt() as u32;

  if min.pow(2u32) + med.pow(2u32) == max.pow(2u32) {
    print!("R ");
  } else if max > hipotenuse {
    print!("O ");
  } else {
    print!("A ");
  }
}

fn main() {
  let stdin = io::stdin();
  let lines = stdin.lock().lines();

  for (index, line) in lines.enumerate() {
    let l = line.unwrap();
    if index > 0 {
      let params: Vec<&str> = l.split_whitespace().collect();
      let min: u32 = params[0].parse().unwrap_or(0);
      let med: u32 = params[1].parse().unwrap_or(0);
      let max: u32 = params[2].parse().unwrap_or(0);

      calculate(min, med, max);
    }
  }
  println!();
}

/*
$ cat DATA.lst | ./dfuribez
A A R O A R R O O O O R R O O R O O O R R R
*/
