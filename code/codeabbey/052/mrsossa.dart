/**
 * $ dartanalyzer mrsossa.dart
 * Analyzing mrsossa.dart
 * No issues found!
 */

import 'dart:io';

void main() {
  new File('DATA.lst').readAsLines().then((var data) {
    var c = int.parse(data[0]);
    for (var i = 1; i <= c; i++) {
      var cas = data[i];
      var cas2 = cas.split(" ");
      var n1 = int.parse(cas2[0]);
      var n2 = int.parse(cas2[1]);
      var n3 = int.parse(cas2[2]);
      var f1 = n1*n1+n2*n2;
      var f2 = n3*n3;
      if (f1 > f2) {
        print("A");
      }
      else if (f1==f2){
        print("R");
      }
      else {
        print("O");
      }
    }
  }
  );
}

/* $ dart mrsossa.dart
 * O A A R R R R R O O A A O A R R R R O A
*/
