(*
> fsharplint -sf yachtdicepoker.fs #linting
Finished.
> fsc yachtdicepoker.fs #compilation
Microsoft (R) F# Compiler version 14.0.23413.0
Copyright (c) Microsoft Corporation. All Rights Reserved.
*)
namespace Codeabbey.YachtOrDicePoker

module MainProgram =

  open System
  open System.IO


  ///Setting 'validateFile' to 'true' if the file exists and has the lst format
  let private validateFile (path : string[]) =

    let isNullPath () =
      if Array.isEmpty path then
        ""
      else
        path.[0]

    let file = isNullPath ()
    if not <| File.Exists(file) then
        printfn "File doesn't exist %s" file
        printfn "\n<file.lst>  Expected argument"
        false
    else
      if (file.Split('.') |> (fun x -> x.[1]<>"lst")) then
        printfn "Invalid format \"%s\"" file
        printfn "\n<file.lst>  Expected argument"
        false
      else
        true

  ///reading the lines of the input file
  let private getListInputData (argv : String[]) =
    let tmpFile = Path.Combine(__SOURCE_DIRECTORY__, argv.[0])
    let linesFile = File.ReadAllLines(tmpFile) |> Array.toList;
    linesFile

  ///Printing the input list
  let private printListInput (list : String list) =
    printfn "\nInput:"
    list |> List.iter (printfn "%s")
    printfn ""
    list.Tail

  ///Split a string into sort array of integers
  let private diceRollStringToSortDiceRollArray(diceRollString : String) =
    diceRollString.Split(' ')
    |> Array.map System.Int32.Parse
    |> Array.sortBy id

  ///Converting a string list to a array list
  let private stringListToSortDiceRollList (listString : String list) =
    List.collect (fun x -> [diceRollStringToSortDiceRollArray x]) listString

  ///Validating dice roll points
  let private comparisonDiceRoll (listSort : int[]) =

    let outputOtherCases (arr : int[]) =
      match arr with
      | [|2;1;_;_;_;_|] -> ["pair"]
      | [|3;1;_;_;_;_|] -> ["three"]
      | [|4;1;_;_;_;_|] -> ["four"]
      | [|5;0;_;_;_;_|] -> ["yacht"]
      | [|2;2;_;_;_;_|] -> ["two-pairs"]
      | [|3;2;_;_;_;_|] -> ["full-house"]
      | _ -> ["none"]

    let otherCase arr =
      let countArray = [|0;0;0;0;0;0|]
      Array.iter (fun x -> countArray.[x-1] <- countArray.[x-1]+1) arr
      outputOtherCases <| Array.sortBy (~-) countArray

    match listSort with
    | [|1;2;3;4;5|] -> ["small-straight"]
    | [|2;3;4;5;6|] -> ["big-straight"]
    | case -> otherCase case

  ///printing result
  let private result list =
    printfn "Ouput:"
    list
    |> List.collect comparisonDiceRoll
    |> List.iter (printf "%s ")
    printfn ""

  [<EntryPoint>]
  let main argv =

    ///validating argument
    let validatedFile = argv |> validateFile

    ///if the argument is valid print result for the input file
    if validatedFile then
      getListInputData argv
      |> printListInput
      |> stringListToSortDiceRollList
      |> result

    0
(*
>yachtdicepoker.exe DATA.lst

Input:
26
2 3 4 5 6
1 2 1 4 5
3 3 3 3 3
2 3 4 5 6
4 5 6 4 3
...

Ouput:
big-straight pair yacht big-straight pair ...

*)
