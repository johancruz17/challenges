/*
$ golint slayfer1112.go
*/

package main

import (
  "bufio"
  "fmt"
  "os"
  "strconv"
  "strings"
)

func dataEntry() [][]string {

  dataIn, _ := os.Open("Data.lst")
  dataScan := bufio.NewScanner(dataIn)
  var data [][]string
  for dataScan.Scan() {
    line := strings.Split(dataScan.Text(), " ")
    if len(line) > 1 {
      data = append(data, line)
    }
  }

  return data
}

func main() {
  data := dataEntry()

  var Aprilfools int

  for i := 0; i < len(data); i++ {
    for j := 0; j < len(data[i]); j++ {
      x, _ := strconv.Atoi(data[i][j])
      Aprilfools += (x * x)
    }
    fmt.Print(Aprilfools, " ")
    Aprilfools = 0
  }
}

/*
93 224 1760 219 1791 179 973 583 311 568 26 276 37 924 322 1090 1192
*/
