/* $ npx eslint paolagiraldo.ts */

process.stdin.setEncoding('utf8');
process.stdin.on('readable', () => {
  const inputData = process.stdin.read();
  if (inputData !== null) {
    const rowsData = inputData.split('\n').slice(1);
    rowsData.forEach((row: string) => {
      const nums = row.split(' ').map((numStr: string) => Number(numStr));
      process.stdout.write(`${Math.round(nums[0] / nums[1])}`);
      return 'Output';
    });
  }
  return 'Data loaded';
});

/* $ tsc paolagiraldo.ts
$ cat DATA.lst | node paolagiraldo.js
12 14782 11 17 19 0 14 6 -2 12 21 19110 2 21 -1 11506 -2 13 16 6 1 6 4 12 3
 */
