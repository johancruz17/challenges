#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.94 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

def mcd_eucl(a : Int32, b : Int32)
  a, b = b, a if b > a
  until b == 0
    a, b = b, a % b
  end
  a
end

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args[1..].map {|x| x.to_i}

(0...args.size).step(2).each  do |x|
  a = args[x]
  b = args[x + 1]
  mcd = mcd_eucl(a, b)
  mcm = a * b // mcd
  print "(#{mcd} #{mcm}) "
end

puts

# $ ./richardalmanza.cr
# (30 164250) (3 13485) (80 28160) (2 2346) (22 7788) (1 45) (100 77500)
# (1 12920) (39 30537) (3 26163) (1 20844) (2 14) (1 4730) (79 211641) (1 10)
# (30 48180) (160 184800) (2 40)
