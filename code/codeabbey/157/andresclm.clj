;; $ clj-kondo --lint andresclm.clj
;; linting took 63ms, errors: 0, warnings: 0

(ns andresclm
  (:require [clojure.string :as str]))

(defn get-data-from [file]
  (let [data (slurp file)
        data' (str/split data #"\n")]
    (vec (rest data'))))

(defn is-prime? [n]
  (.isProbablePrime n (biginteger 4)))

(defn next-prime [n]
  (.nextProbablePrime n))

(defn reverse-bigint [n]
  (let [temp (str n)
        temp' (str/reverse temp)]
    (biginteger temp')))

(defn is-emirp? [prime]
  (let [prime' (reverse-bigint prime)]
    (if (is-prime? prime')
      true
      false)))

(defn skip-gaps [n]
  (let [temp (bigint n)]
    (cond
      (and (> temp 20000000000000000000000) (< temp 30000000000000000000000))
      (biginteger 30000000000000000000000)
      (and (> temp 40000000000000000000000) (< temp 70000000000000000000000))
      (biginteger 70000000000000000000000)
      (and (> temp 80000000000000000000000) (< temp 90000000000000000000000))
      (biginteger 90000000000000000000000)
      :else (biginteger temp))))

(defn next-emirp [n]
  (let [temp (skip-gaps n)]
    (loop [prime (next-prime temp)]
      (if (is-emirp? prime)
        prime
        (recur (next-prime prime))))))

(defn -main "Entry point" []
  (let [data (get-data-from "DATA.lst")
        result (map (fn [n] (let [temp (biginteger n)]
                              (next-emirp temp)))
                    data)]
    (apply print result)))

;; $ clj -m andresclm
;; 30939269335276755753029 70542276912119908564307 30542990501070864739151
;; 30180029436013545780049 90668339652399947893099 90377870022727936643971
;; 10554112870693480545563 90723707961999868564111 70000000000000000000859
;; 90000000000000000000001 30000000000000000000047 70000000000000000000859
;; 70891076823639578114099 70344452305513385070083 10782355894393623321557
;; 70000000000000000000859 10132869121360915557049 30718898702116166538083
