       IDENTIFICATION DIVISION.
       PROGRAM-ID.


       ENVIRONMENT INITIAL.


       DATA DIVISION.
       WORKING-STORAGE SECTION.
       77 A PIC 9999.
       77 B PIC 9999.
       77 ANS PIC 999v99.


       PROCEDURE DIVISION.
       MAIN-PARA.
         DISPLAY " ENTER A".
         ACCEPT A.
         DISPLAY " ENTER B".
         ACCEPT B.

       ADD-PARA.
         ADD A B GIVING ANS.

       DISP-PARA.
         DISPLAY A "+" B "=" ANS.
         STOP RUN.
