% $ erlc -Werror andresclm.erl
% andresclm.beam

-module(andresclm).

-export([start/0]).

-compile(andresclm).

start() ->
  Data = read_file("DATA.lst"),
  Result = sum_list(Data),
  io:format("~B\n", [Result]).

sum_list(List) ->
  lists:foldl(fun (Item, Accumulator) ->
    {Number, _} = string:to_integer(Item),
    Number + Accumulator end,
    0, List).

read_file(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  string:split(erlang:binary_to_list(Binary), " ").

% $ erl -noshell -s andresclm -s init stop
% 18772
