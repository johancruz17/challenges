#! /usr/bin/crystal

# $ ameba \
#   --all \
#   --fail-level Convention \
#   slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 59.08 milliseconds
# $ crystal build slayfer1112.cr

def solution(array)
  result = 0
  seed = 113
  limit = 10_000_007
  array.each do |x|
    x = x.is_a?(String) ? x.try &.to_f : x
    result = (result+x)*seed
    if result >= limit
      a = result.divmod(limit)
      result = a[1]
    end
  end
  result = result.is_a?(Float64) ? result.try &.to_i : result
  print "#{result.round(0)} "
end

cases = gets

if cases
  cases = cases.is_a?(String) ? cases.try &.to_i : cases
  0.step(to: cases,by:1) do |_|
    args = gets
    if args
      args = args.split
      solution(args)
    end
  end
end

puts

# $ ./slayfer1112
# 6389830
