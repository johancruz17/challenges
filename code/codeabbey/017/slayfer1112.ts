/*
$ eslint slayfer1112.ts
$ tsc slayfer1112.ts
*/

function checksum(val: number): number {
  const seed = 113;
  const limit = 10000007;
  const res = val * seed;
  if (res >= limit) {
    const value = res % limit;
    return value;
  }
  return res;
}

function solution(entry: string[]): number {
  const entryF: number[] = entry.map(parseFloat);
  const check: number = entryF.reduce((acc, val) => checksum(acc + val), 0);
  process.stdout.write(`${check} `);
  return 0;
}

function main(): number {
  process.stdin.resume();
  process.stdin.setEncoding('utf-8');
  process.stdin.on('data', (datEntry) => {
    const entry: string = datEntry.toString().trim();
    const entryArray: string[] = entry.trim().split('\n');
    const dat: string[] = entryArray.slice(1, entryArray.length);
    dat.map((val) => solution(val.split(' ')));
    process.stdout.write('\n');
    return 0;
  });
  return 0;
}

main();
/*
$ cat DATA.lst | node slayfer1112.js
6389830
*/
