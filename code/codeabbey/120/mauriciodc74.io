#!/usr/local/bin/io
/*
 Scripts are executed without compilation.
 No linter available for IO
*/

listsizestring := File with("DATA.lst")
listsizestring openForReading
listsizeinit := listsizestring readLine asNumber

listsize := listsizeinit - 1
results := list clone

while (line := listsizestring readLine, array := line split)
for(i, 0, listsize, array atPut(i, array at(i) asNumber))

max := method(listsize,
  index := 0
  for(i, 0, listsize,
    maximum := array max
    if(array at(i) == maximum, index := i)
  )
  results append(index)
  return index
)

swap := method(listsize,
  i := max(listsize)
  temp := array at(listsize)
  array atPut(listsize, array at(i))
  array atPut(i, temp)
  array removeLast
  listsize := listsize - 1
  if(listsize > 0, swap(listsize))
)

swap(listsize)
for(i, 0, results size - 1, write(results at(i)," "))

listsizestring close

/*
 $ io mauriciodc74.io
 111 119 19 31 39 58 52 51 89 11 85 38 69 92 28 61 32 4 4 12 94
 49 92 21 42 74 15 62 79 85 38 68 28 40 77 50 16 36 5 79 22 48
 75 2 9 6 5 34 51 14 33 48 27 2 66 42 6 55 43 19 27 20 21 18 34
 27 31 15 19 39 7 27 13 17 7 32 41 5 40 11 3 25 21 23 12 35 12
 10 12 29 11 21 16 17 26 10 4 17 3 20 15 17 1 10 3 8 4 2 10 10
 0 5 2 7 0 1 2 4 2 0 1
*/
