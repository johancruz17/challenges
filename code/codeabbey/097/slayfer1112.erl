% $ erl -compile slayfer1112.erl
% $ erl -noshell -s erlint slayfer1112 -s init stop
% ok

-module(slayfer1112).

-import(lists, [nth/2]).

-export([start/0]).

-compile(slayfer1112).

% Read file and convert in binary list
readfile(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  string:tokens(erlang:binary_to_list(Binary), "\r\n").

% Take the number of inputs
inputs(Array) ->
  [H | T] = Array,
  {Inputs, _} = string:to_integer(H),
  Values = for(1, Inputs,
    fun (X) ->
      Q = string:tokens(nth(X, T), " "),
      {A, _} = string:to_integer(nth(1, Q)),
      {B, _} = string:to_integer(nth(2, Q)),
      [{A, B}]
    end),
  {Inputs, Values}.

% For loop with I<N
for(N, N, F) -> F(N);
for(I, N, F) -> F(I) ++ for(I + 1, N, F).

% Print the result
result(F, List) -> [F(X) || X <- List].

% Say the number of solutions of the exercise
solutions(Tuple) ->
  {Legs, Breast} = Tuple,
  N = trunc(math:floor((Legs - 2) / 4)),
  Sols = string:len(for(1, N,
    fun (X) ->
      Pigs = X,
      Girls = (Legs - Pigs * 4) / 2,
      Girls_B = Girls * 2,
      Pigs_B = Breast - Girls_B,
      Condition1 = math:fmod(Pigs_B, Pigs),
      Condition2 = math:fmod(Pigs_B / Pigs, 2),
      case solution of
      _
      when (Condition1 == 0) and
         (Condition2 == 0) ->
      [Pigs];
      _ -> []
      end
    end)),
  Sols.

% Run the program
start() ->
  Data = readfile("DATA.lst"),
  {N, Values} = inputs(Data),
  Sols = for(1, N,
    fun (X) -> [solutions(nth(X, Values))] end),
  Print = fun (X) -> io:fwrite("~w ", [X]) end,
  result(Print, Sols),
  io:fwrite("~n").

% $ erl -noshell -s slayfer1112 -s init stop
% 7 5 28 5 5 6 3 5 10 4 2 27 29 18 5 8
