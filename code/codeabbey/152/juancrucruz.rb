#!/usr/bin/ruby
# frozen_string_literal: false

# $ rubocop juancrucruz.rb #linting
# Inspecting 1 file
# .
#
# 1 file inspected, no offenses detected
# $

# rubocop:disable Metrics/AbcSize
def exteuclidian(x_x: 0, y_y: 0) # rubocop:disable Metrics/MethodLength
  # This form of calculating the modular inverse was my implementation of the
  # Modular inverse task so i'm recycling it.
  sprev = 1
  scur = 0

  tprev = 0
  tcur  = 1
  r = x_x % y_y
  while r != 0
    q = x_x / y_y
    r = x_x % y_y
    snext = sprev - q * scur
    tnext = tprev - q * tcur
    x_x = y_y
    y_y = r
    sprev = scur
    tprev = tcur
    scur = snext
    tcur = tnext
    r = x_x % y_y
  end
  a = scur
  a
end

def rsadecryptor(cipher: 0, d_key: 0, n_n: 0)
  m = cipher.pow(d_key, n_n)
  msg = ''
  m = m.to_s
  (0..m.length).step(2).each do |i|
    a = m[i..i + 1]
    break if a == '00'

    msg << (m[i] + m[i + 1]).to_i.chr
  end
  # This is the message decrypted and decoded
  puts msg
end

# Please feed the script with the values of DATA.lst
print 'Please feed me with the p: '
p = gets.chomp.to_i
print 'Please feed me with the q: '
q = gets.chomp.to_i
print 'Please feed me with the c: '
c = gets.chomp.to_i

puts 'Remember, we are assuming that e = 65537'
e = 65_537
n = p * q
phi_n = (p - 1) * (q - 1)
d = exteuclidian(x_x: e, y_y: phi_n)
d = d % phi_n if d.negative?
rsadecryptor(cipher: c, d_key: d, n_n: n)
# rubocop:enable Metrics/AbcSize
__END__

$ cat DATA.lst | ./juancrucruz.rb

STEAK FAITH STOCK BLOOD BRICK BEANS FRANC
