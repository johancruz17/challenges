/*
  $ rustfmt vmelendez.rs
  $ rustc vmelendez.rs
  $
*/

fn collatz_sequence(x: i32) -> i32 {
  let mut xnext = x;
  let mut num = 0;
  while xnext != 1 {
    if xnext % 2 == 0 {
      xnext = xnext / 2;
    } else {
      xnext = 3 * xnext + 1;
    }
    num = num + 1;
  }

  return num;
}

fn main() -> std::io::Result<()> {
  let mut n = String::new();
  std::io::stdin().read_line(&mut n).unwrap();

  let input: Vec<i32> = {
    let mut a = String::new();
    std::io::stdin().read_line(&mut a).unwrap();
    a.split_whitespace()
      .map(|x| x.trim().parse::<i32>().unwrap())
      .collect()
  };

  let mut vec = Vec::new();
  for i in 0..input.len() {
    vec.push(collatz_sequence(input[i]));
  }

  let mut out_string = String::new();
  let vec_iter = vec.iter();
  for i in vec_iter {
    out_string += &i.to_string();
    out_string += &" ".to_owned();
  }

  println!("{}", out_string);
  Ok(())
}

/*
  $ cat .\DATA.lst | .\vmelendez.exe
  34 91 16 18 37 18 33 30 55 6 45 128 110 15 37 16 18 102 109 18 29 9 19 106 14
*/
