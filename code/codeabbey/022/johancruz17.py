#!/usr/bin/env python3

"""
$ pylint johan17.py.py
"""
from typing import List
import math
INPUT_1 = input()
ANSWER = ""
NUMBERS: List[int] = []


class Flags:
    def __init__(self, time: bool, exponent: int):
        self.time: bool = time
        self.exponent: int = exponent

    def iterate(self, r_a: float, var_a: float, var_b: float,
                var_c: float) -> float:
        # if it is zero it avoids several bucles:
        def is_minor(r_b: float, var_a: float, var_b: float,
                     var_c: float) -> bool:
            # if it is zero it avoids several bucles:
            if (math.floor(r_b / var_a) + math.floor(r_b / var_b)) < var_c:
                return True
            return False
        r_b = r_a - r_a / (10 ** self.exponent * var_c)
        while is_minor(r_b, var_a, var_b, var_c) and self.time:
            r_b += r_b / (10 ** (self.exponent + 1) * var_c)
        self.exponent += 1
        return float(r_b)

    def compare(self, r_a: float, r_b: float) -> str:
        # if it is zero it avoids several bucles
        def is_zero(before_answer: float, next_answer: float) -> bool:
            # if it is zero it avoids several bucles:
            return math.floor(before_answer) - math.floor(next_answer) == 0
        # zero in true means that with respect to the previous loop
        # it did not approach significantly
        zero = is_zero(r_a, r_b)
        if zero and self.time:
            self.time = not(zero) and self.time
            return str(math.floor(r_a))
        self.time = not(zero) and self.time
        return str(math.floor(r_b))


FLAG = Flags(True, 0)

for x_var in range(int(INPUT_1)):
    A, B, C = input().split()
    NUMBERS = NUMBERS + [int(A), int(B), int(C)]


for x_count in range(int(INPUT_1)):
    a: int = NUMBERS[x_count]
    b: int = NUMBERS[x_count + 1]
    c: int = NUMBERS[x_count + 2]
    x: float = a * b / (a + b)
    r: int = math.ceil(c * x)
    r_1: float = float(r)
    r_e_s = ""

    while((math.floor(r_1 / a) + math.floor(r_1 / b)) < c) and FLAG.time:
        r_1 += r_1 / (10 * c)

    FLAG.time = not math.floor(r_1) - r == 0
    if not FLAG.time:
        r_e_s = str(r)

    r_2: float = FLAG.iterate(r_1, a, b, c)
    r_e_s = FLAG.compare(r_1, r_2)
    r_3: float = FLAG.iterate(r_2, a, b, c)
    r_e_s = FLAG.compare(r_2, r_3)
    r_4: float = FLAG.iterate(r_3, a, b, c)
    r_e_s = FLAG.compare(r_3, r_4)
    r_5: float = FLAG.iterate(r_4, a, b, c)
    r_e_s = FLAG.compare(r_4, r_5)
    r_6: float = FLAG.iterate(r_5, a, b, c)
    r_e_s = FLAG.compare(r_5, r_6)
    r_7: float = FLAG.iterate(r_6, a, b, c)
    r_e_s = FLAG.compare(r_6, r_7)
    r_8: float = FLAG.iterate(r_7, a, b, c)
    r_e_s = FLAG.compare(r_7, r_8)
    r_9: float = FLAG.iterate(r_8, a, b, c)
    r_e_s = FLAG.compare(r_8, r_9)
    r_10: float = FLAG.iterate(r_9, a, b, c)
    r_e_s = FLAG.compare(r_9, r_10)
    r_11: int = int(r_e_s)

    while(math.floor(r_11 / a) + math.floor(r_11 / b)) < c:
        r_11 += 1

    r_e_s = str(r_11)
    ANSWER += r_e_s + " "
print(ANSWER)

# $ cat DATA.lst | python3 johan17.py
# 263758430 136094900976 758118502080 237548096 8180425 5840250 379877502
# 5572 8748 197944 167105106 1500701652 211802745 44271163200 50042551356
# 193536936 251860 182630 349935036 154222
