#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.24 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args.map {|x| x.to_i}

args.each do |chr|
  if chr.popcount % 2 == 0
    chr = chr ^ 128 if chr > 127
    print chr.chr
  end
end

puts

# $ ./richardalmanza.cr
# 8cLWf1TcI Q9EF YtRdnWx  M3QwU6 d0fJp6Q klg4Khj rjcRjScf0u8VCqTZ.
