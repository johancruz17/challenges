/*
$ eslint slayfer1112.ts && prettier --check slayfer1112.ts
$ tsc \
  --strict \
  --noImplicitAny \
  --noImplicitThis \
  --noUnusedLocals \
  --noUnusedParameters \
  --noImplicitReturns \
  --noFallthroughCasesInSwitch \
  slayfer1112.ts
*/

function assoc(arr: number[], index: number, rep: number): number[] {
  return arr.map((val, idx) => (idx === index ? rep : val));
}

function assocM(arr: number[][], index: number, rep: number[]): number[][] {
  return arr.map((val, idx) => (idx === index ? rep : val));
}

function makeLine(
  stringA: string,
  result: number[],
  len: number,
  index: number
): number[] {
  if (len > 0) {
    const val: string[] = stringA.split(' ');
    const numR = Number(val[index]);
    return makeLine(stringA, result.concat(numR), len - 1, index + 1);
  }
  return result;
}

function makeMatrix(
  matrix: string[],
  result: number[][],
  len: number,
  index: number
): number[][] {
  if (len > index) {
    const val: string = matrix[index];
    const res: number[] = makeLine(val, [], len, 0);
    return makeMatrix(matrix, result.concat([res]), len, index + 1);
  }
  return result;
}

function vecRemove(coll: number[], pos: number): number[] {
  return coll.slice(0, pos).concat(coll.slice(pos + 1, coll.length));
}

function finish(len: number, type: number): number {
  if (len === 0) {
    return 2;
  }
  if (type === 0) {
    return 1;
  }
  return 0;
}

function indexToMov(
  rowCol: number[],
  colRow: number[],
  type: number
): number[] {
  if (finish(rowCol.length, type) === 2) {
    return [0, 2, 0];
  }
  if (rowCol.indexOf(0) === 1 - 2) {
    if (type === 0) {
      const toMov: number = Math.max(...rowCol);
      const index: number = colRow.indexOf(toMov);
      process.stdout.write(`${index}`);
      return [index, 1, toMov];
    }
    const toMov: number = Math.min(...rowCol);
    const index: number = colRow.indexOf(toMov);
    process.stdout.write(`${index} `);
    return [index, 0, (1 - 2) * toMov];
  }
  const index: number = rowCol.indexOf(0);
  const res: number[] = vecRemove(rowCol, index);
  return indexToMov(res, colRow, type);
}

function getColumn(
  matrix: number[][],
  indexJ: number,
  counter: number,
  res: number[]
): number[] {
  if (counter < matrix.length) {
    const val: number = matrix[counter][indexJ];
    const resu: number[] = res.concat(val);
    return getColumn(matrix, indexJ, counter + 1, resu);
  }
  return res;
}

function mov(
  matrix: number[][],
  indexI: number,
  indexJ: number,
  type: number,
  score: number
): number {
  if (type !== 2) {
    if (type === 0) {
      process.stdout.write('');
      const rowCol: number[] = matrix[indexI];
      const [idx, typeP, scoreP]: number[] = indexToMov(rowCol, rowCol, type);
      const rowP: number[] = assoc(matrix[indexI], idx, 0);
      const matrixP: number[][] = assocM(matrix, indexI, rowP);
      return mov(matrixP, indexI, idx, typeP, score + scoreP);
    }
    process.stdout.write(' ');
    const rowCol: number[] = getColumn(matrix, indexJ, 0, []);
    const [index, typeP, scoreP]: number[] = indexToMov(rowCol, rowCol, type);
    const rowP: number[] = assoc(matrix[index], indexJ, 0);
    const matrixP: number[][] = assocM(matrix, index, rowP);
    return mov(matrixP, index, indexJ, typeP, score + scoreP);
  }
  process.stdout.write('\n');
  process.stdout.write(`score: ${score}`);
  return 1;
}

function solution(head: string, entry: string[]): number {
  const len = Number(head);
  const matrix: number[][] = makeMatrix(entry, [], len, 0);
  const indexI = 0;
  const indexJ = 0;
  const type = 0;
  const score = 0;
  const result: number = mov(matrix, indexI, indexJ, type, score);
  if (result) {
    return 1;
  }
  return 0;
}

function main(): number {
  process.stdin.resume();
  process.stdin.setEncoding('utf-8');
  process.stdin.on('data', (datEntry) => {
    const entry: string = datEntry.toString().trim();
    const entryArray: string[] = entry.trim().split('\n');
    const dat: string[] = entryArray.slice(1, entryArray.length);
    const result: number = solution(entryArray[0], dat);
    if (result) {
      process.stdout.write('\n');
      return 1;
    }
    process.stdout.write('\n');
    return 0;
  });
  return 0;
}

main();
/*
$ cat DATA.lst | node slayfer1112.js
28 6 1 27 15 18 21 0 4 7 5 13 4 11 13 5 7 18 2 5 21 17 3 0 22 25 4
17 10 16 7 11 18 21 14 2 22 11 20 20 17 4 25 29 5 28 4 2 29 15 2
13 11 25 27 24 24 8 13 9 5 2 10 24 4 19 28 4 3 23 25 20 28 26 7 21
6 27 11 21 13 6 14 27 7 14 13 7 11 11 21 2 0 21 24 16 16 21 21 26
6 10 18 1 21 14 14 5 6 15 23 1 1 11 15 3 2 7 16 26 3 13 7 7 20 9
9 12 8 26 23 16 20 24 13 1 6 19 0 27 12 10 10 8 25 26 29 14 16 20
5 4 21 9 15 25 20 26 0 28 25 19 19 16 9 20 14 11 17 5 25 9 26 23
15 29 7 28 2 25 8 0 2 22 14 24 0 22 3 8 12 20 22 3 28 2 18 7 6 8
9 27 25 3 10 19 12 21 27 9 4 26 12 17 14 16 22 24 29 28 14 15 4
20 24 22 9 1 3 10 2 6 9 2 6 28 21 6 8 21 4 10 24 2 15 20 13 15 7
6 18 20 8 3 4 29 8 22 27 17 29 6 19 27 16 4 2 24 19 26 11 19 14 7
25 15 12 3 18 8 20 22 13 16 8 27 23 20 1 0 26 4 23 28 18 22 6 13
16 18 1 3 26 20 19 11 8 1 19 25 5 17 25 16 26 27 2 16 29 13 19 14
3 7 27 20 6 23 20 17 7 1 5 10 19 4 6 9 24 4 13 0 6 18 4 8 14 4 29
21 10 22 23 24 6 14 23 5 0 15 24 18 27 29 10 23 23 2 11 20 21 15
11 17 18 5 22 22 28 14 17 19 13 13 8 19 9 7 24 23 0 4 9 0 16 1 2
20 7 0 11 14 12 7 21 3 23 11 29 29 18 26 24 3 14 18 20 19 27 26
1 19 3 5 20 14 27 10 17 23 7 4 15 19 23 6 12 13 27 23 1 8 11 16
21 22 26 10 7 12 14 1 25 25 24 5 16 25 26 11 10 28 8 5 19 9 18 14
15 17 28 24 15 26 2 14 5 26 14 0 29 8 7 19 5 18 17 1 29 9 7 2 19
3 16 9 11 22 25 24 18 0 27 4 12 0 23 21 26 29 14 9 3 29 11 10 29
19 22 7 28 29 0 8 17 15 20 28 28 12 13 17 23 12 6 6 11 18 19 8 2
21 15 10 13 2 2 19 21 13 18 4 10 13 28 18 0 17 26 26 13 27 20 6
15 15 28 21 5 27 4 14 8 2 26 19 18 27 21 8 28 25 12 24 21 25 7 22
1 13 14 23 13 28 22 27 3 24 7 3 0 14 26 7 23 8 26 18 10 15 27 1
11 4 8 18 18 15 26 28 16 17 6 16 0 13 23 9 8 10 1 7 29 12 5 23 12
2 16 19 24 1 12 18 25 0 24 12 21 23 8 17 1 9 10 25 18 16 4 1 20
2 17 26 22 18 9 5 5 15 19 28 1 14 25 2 27 16 28 1 0 7 8 15 22 14
24 28 11 5 28 11 27 8 15 12 0 10 14 25 3 2 1 4 22 8 8 24 16 6 25
12 18 23 11 3 17 13 26 1 22 21 17 7 19 0 17 24 26 6 17 27 24 11
2 9 17 22 15 0 10 26 9 11 6 25 23 29 6 3 27 6 22 17 2 23 29 25 9
29 20 21 9 13 22 29 2 12 11 24 1 5 27 27 28 10 20 12 26 5 12 11
3 18 23 10 21 29 16 11 25 21 1 25 13 18 29 22 4 4 20 13 25 10 22
9 28 23 22 12 12 22 19 17 9 24 5 3 3 21 19 29 13 3 20 0 5 6 3 20
10 5 4 6 0 20 29 3 9 14 10 1 15 5 29 27 10 6 24 17 17 12 4 23 9
28 17 25 0 11 5 22 16 23 19 12 10 7 15 28 27 12 16 15 9 10 16 8
5 16 3 15 1 29 12 16 15 13 24 29 17 16 1 12 3 28 12 9 0
score: 46098
*/
