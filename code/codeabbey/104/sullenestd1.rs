/*
$ cargo clippy
Checking code_104 v0.1.0 (/.../sullenestd1)
Finished dev [unoptimized + debuginfo] target(s) in 0.10s
$ cargo build
Compiling code_104 v0.1.0 (/.../sullenestd1)
Finished dev [unoptimized + debuginfo] target(s) in 0.21s
$ rustc sullenestd1.rs
*/

use std::io::{self, BufRead};

fn triangle_area(x1: f64, y1: f64, x2: f64, y2: f64, x3: f64, y3: f64) -> f64 {
  let det = ((x1 - x3) * (y2 - y1)) - ((x1 - x2) * (y3 - y1));
  det.abs() / 2.0
}

fn main() {
  let input = io::stdin();
  let lines = input.lock().lines();
  let mut n = 1;
  for x in lines {
    if n == 1 {
      n = 2;
    } else {
      let line = x.unwrap();
      let num: Vec<f64> = line
        .trim()
        .split(' ')
        .map(|x| x.parse().expect("Not a float!"))
        .collect();
      println!(
        "{}",
        triangle_area(num[0], num[1], num[2], num[3], num[4], num[5])
      )
    }
  }
}

/*
$ cat DATA.lst | ./sullenestd1
2058825
408448.5
1962020
8166048
4313036
11631530.5
11072049
4688396
3598616.5
11097630
4977228.5
2206271.5
7792157.5
9652407
*/
