% erlc -W john2104.erl
% erl -compile john2104.erl
% erlint:lint("john2104.erl").
% {ok,[]}

-module(john2104).
-export([start/0]).
-export([readfile/1]).
-export([domath/2]).
-export([sendmath/2]).
-export([loop/1]).
-export([digest/1]).
-import(lists,[nth/2]).
-export([returnnstring/1]).


loop(A) ->
  lists:foreach(fun(X) -> digest(X) end, A).


digest(A) ->
  Z = returnnstring(A),
  L = length(Z),
  sendmath(Z, L).


sendmath(_, L) when L == 1 ->
  ok;


sendmath(A, L) when L > 1 ->
  lists:foreach(fun(X) ->
    K = erlang:integer_to_list(X),
    Len = length(K),
    R = domath(K,Len),
    io:fwrite("~w ", [R])
  end, A).


domath(A,K) when K == 1 -> element(1,string:to_integer([nth(K, A)]));


domath(A,K) when K > 1 ->
  N = element(1,string:to_integer([nth(K, A)])),
  (N*K)+domath(A,K-1).


returnnstring(T) ->
  [ element(1, string:to_integer(Substr)) ||
  Substr <- string:tokens(T, " ")].


readfile(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  string:tokens(erlang:binary_to_list(Binary), "\r\n").


start() ->
  Arr = readfile("DATA.lst"),
  loop(Arr),
  io:fwrite("~n").


% erl -noshell -s john2104 start -s init stop
% 9 11 60
