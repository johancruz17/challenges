#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.3 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

OPTIONS = "RSP"

def o_val(option)
  (OPTIONS.index(option) || 0)
end

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split("\n") if fluid
args = args[0].split("\n") if !fluid

args = args[1..].map {|x| x.split}

args.each do |match|
  players = [[1, 0], [2, 0]]

  match.each do |round|
    next if round[0] == round[1]
    if o_val(round[0]) == (o_val(round[1]) - 1) % OPTIONS.size
      players[0][1] += 1
    else
      players[1][1] += 1
    end
  end

  players = players.sort_by {|x| x[1]}
  print "#{players[-1][0]} "
end
puts

# $ ./richardalmanza.cr
# 1 2 1 2 1 2 2 1 2 2 2 1 2 1 1 1 1 1 2 2 2 2
