/*
linter:
cppcheck \
  --error-exitcode=1 \
  slayfer1112.c \
&&  splint \
  -strict \
  -internalglobs \
  -modfilesys \
  -boundsread \
  slayfer1112.c
Checking slayfer1112.c ...
Splint 3.1.2 --- 05 Sep 2017

Finished checking --- no warnings

compilation:
gcc \
  -Wall \
  -Wextra \
  -Winit-self \
  -Wuninitialized \
  -Wmissing-declarations \
  -Winit-self \
  -ansi \
  -pedantic \
  -Werror \
  slayfer1112.c \
  -o slayfer1112 \
  -lm
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define MAX_SIZE 255

static int who_wins ( /*@in@*/ char game[])
  /*@modifies nothing@*/ {
  if (strcmp(game, "PR") == 0 ||
      strcmp(game, "RS") == 0 ||
      strcmp(game, "SP") == 0) { return 1; }
  else if (strcmp(game, "RP") == 0 ||
           strcmp(game, "SR") == 0 ||
           strcmp(game, "PS") == 0) { return 2; }
  else { return 0; }
}

/*Suppose that draw didn't exist for an entire game*/
static int rock_paper_scissors ( /*@in@*/ char game[][MAX_SIZE],
  /*@reldef@*/ int limit) /*@modifies nothing@*/ {
  int i;
  int results[] ={0, 0};
  for (i = 0; i < limit; i++) {
    if (who_wins(game[i]) == 1) { results[0] += 1; }
    if (who_wins(game[i]) == 2) { results[1] += 1; }
  }
  if (results[0] >= results[1]) { printf("1 "); }
  else { printf("2 "); }
  return 1;
}

int main(void) {
  static char games[MAX_SIZE][MAX_SIZE];
  int x, matches, returned = 0, i;
  int j = 0;
  char temp[MAX_SIZE];
  if (fgets(temp, MAX_SIZE, stdin) == 0) { return 0; }
  x = atoi(temp);
  while (j < x) {
    if (fgets(temp, MAX_SIZE, stdin) == 0) { return 0; }
    matches = (int) (((strlen(temp) + 1) / 3));
    for (i = 0; i < matches; i++) {
      games[i][0] = temp[(i*2) + i];
      games[i][1] = temp[(i*2) + i + 1];
      games[i][2] = '\0';
    }
    returned = rock_paper_scissors(games, matches);
    j++;
  }
  printf("\n");
  return returned;
}

/*
$ cat DATA.lst | ./slayfer1112
2 2 2 1 2 2 1 2 2 2 1 1 1 2 2 2 2 1 2 2 1 1 2 2 1 1 1
*/
