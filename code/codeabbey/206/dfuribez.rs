/*
$ clippy-driver dfuribez.rs -D warnings
$ rustc dfuribez.rs
*/

use std::io::{self, BufRead};

fn padd_string(string: String, padding: u32) -> String {
  let mut padded = string;
  for _ in 0..padding {
    padded += &padding.to_string();
  }

  padded
}

fn encode(text: String) {
  let allowed: String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567".to_string();
  let len_text = text.len();
  let padding = 5 - (len_text as u32 % 5);
  let mut binary = "".to_string();
  let text = padd_string(text, padding);

  for char in text.into_bytes() {
    let binary_raw = format!("0{:b}", char);
    binary += &format!("{:0>8}", binary_raw);
  }

  for index in 0..(binary.len() / 5) as u32 {
    let start = (index * 5) as usize;
    let end = (start + 5) as usize;
    let pos = isize::from_str_radix(&binary[start..end], 2).unwrap();
    print!("{}", allowed.as_bytes()[pos as usize] as char);
  }
  println!();
}

fn decode(text: String) {
  let allowed: String = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567".to_string();
  let mut binary = "".to_string();
  let mut decoded = Vec::new();

  for char in text.chars() {
    let binary_raw = format!("{:b}", allowed.find(char).unwrap());
    binary += &format!("{:0>5}", binary_raw);
  }

  for index in 0..(binary.len() / 8) as u32 {
    let start = (index * 8) as usize;
    let end = (start + 8) as usize;
    let pos = isize::from_str_radix(&binary[start..end], 2).unwrap() as u8;
    decoded.push(pos as char);
  }

  let padded = *decoded.last().unwrap() as u8 - 48;
  if padded >= 1 || padded <= 5 {
    for dec in &decoded[0..(decoded.len() as u32 - padded as u32) as usize] {
      print!("{}", dec);
    }
  }
  println!();
}

fn main() {
  let stdin = io::stdin();
  let lines = stdin.lock().lines();
  let mut first_line = true;

  for (index, line) in lines.enumerate() {
    let l = line.unwrap();
    if first_line {
      first_line = false;
    } else if index % 2 != 0 {
      encode(l);
    } else {
      decode(l);
    }
  }
}

/*
$ cat DATA.lst | ./dfuribez
MNXW23LVNZUXG5DTGU2TKNJV
littorals mid dizziest
ONUWK4TSMEQHA2LOOQQGC3DFOJ2GY6JR
crystallizing buzzword
MFUCA5DSNFTGYZJANVQWK3DTORZG63JAMVWWK3TEMVSCAZTMOVXXE2LOMU2DINBU
duet clambers prerogative flicks
OJSWO4TFONZWS33OGU2TKNJV
hornpipes
OVXG4YLNMVSDGMZT
disunites
OBQXEYLMNRQXQMRS
playbills taboo falter indigestibles
MZWHK23ZEBZGSYRAOJSW4ZDFPJ3G65LTNFXGOMRS
placentae ringleader gestured
MFXGK43UNBSXI2L2NFXGOMRS
unreasonable ethnologist substation painters
OVZWCYTMMUQGM2LTNBSXE3LBNYQHG3LVMRTWSZLSEBTWK6LTMVZHGMRS
rushes mosey progeny
MNXW4ZTSN5XHIYLUNFXW44ZANFXGG2LTNF3GK3DZEBWG643TMVZSAYLVMJ2XE3RAONQWY5TBM5STGMZT
nebula brainwashed raging epiglottises mobilizes
MFZGG2DJORSWG5DVOJSSA5DSOVZXG2LOM42DINBU
beeches pepsin initializing
NRQW2ZLOORZSAYTBOJSWIMRS
inaugurals
ONXXK3TEOBZG633GNFXGOIDTNRXXAMRS
ninja genies larynges incision millisecond
OV2HIZLSMFXGGZLTEBXW66TJNZTSA4DVOJ3GSZLXEB2HEYLHMVSGSYLOEBRW63LNMVZGGZLEGU2TKNJV
overpass interrogator fluffing urning
*/
