% erlc -W john2104.erl
% erl -compile john2104.erl
% erlint:lint("john2104.erl").
% {ok,[]}

-module(john2104).
-export([start/0]).
-export([readfile/1]).
-export([loop/4]).
-export([print/3]).
-export([digest/1]).
-import(lists,[nth/2]).
-export([returnnstring/1]).


digest(A) ->
  A1 = returnnstring(nth(1, A)),
  A2 = returnnstring(nth(2, A)),
  N = nth(2, A1),
  L = length(A2),
  R = array:new([{size, N},{fixed, false},{default, 0}]),
  loop(A2, L, 1, R).


loop(A, L, N, R) when N =< L ->
  W = nth(N,A) - 1,
  K = array:get(W, R) + 1,
  Rr = array:set(W, K, R),
  if N == L -> Ll = array:size(Rr),print(Ll, 0, Rr);
  true -> loop(A, L, N+1, Rr)
  end.

print(L, N, _) when N == L -> ok;

print(L, N, R) when N < L ->
  Num =  array:get(N, R),
  io:fwrite("~w ", [Num]),
  print(L, N+1, R).


returnnstring(T) ->
  [ element(1, string:to_integer(Substr)) ||
  Substr <- string:tokens(T, " ")].


readfile(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  string:tokens(erlang:binary_to_list(Binary), "\r\n").


start() ->
  Arr = readfile("DATA.lst"),
  digest(Arr),
  io:fwrite("~n").


% erl -noshell -s john2104 start -s init stop
% 5 2 3
