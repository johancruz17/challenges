let rec coorx posx ang step =
        let pi = 4.0 *. atan 1.0 in
        if  ang>0.0 && ang < 90.0
            then  posx +. step*.cos((90.0*.(pi/.180.0))-.(ang*.(pi/.180.0)))
            else coorx2 posx ang step
and coorx2 posx ang step=
        let pi = 4.0 *. atan 1.0 in
            if 90.0 <= ang && ang < 180.0
            then posx +. step*.cos((ang*.(pi/.180.0))-. (90.0*.(pi/.180.0)))
            else coorx3 posx ang step
and coorx3 posx ang step=
        let pi = 4.0 *. atan 1.0 in
        if 180.0 <= ang && ang < 270.0
            then posx -. step*.sin((ang*.(pi/.180.0)) -. (180.0*.(pi/.180.0)))
            else posx -. step*.cos((ang*.(pi/.180.0)) -. (270.0*.(pi/.180.0)));;

let rec coory posy ang step =
        let pi = 4.0 *. atan 1.0 in
        if  ang>0.0 && ang < 90.0
            then  posy +. step*.sin((90.0*.(pi/.180.0))-.(ang*.(pi/.180.0)))
            else coory2 posy ang step
and coory2 posy ang step=
        let pi = 4.0 *. atan 1.0 in
            if 90.0 <= ang && ang < 180.0
            then posy -. step*.sin((ang*.(pi/.180.0))-. (90.0*.(pi/.180.0)))
            else coory3 posy ang step
and coory3 posy ang step=
        let pi = 4.0 *. atan 1.0 in
        if 180.0 <= ang && ang < 270.0
            then posy -. step*.cos((ang*.(pi/.180.0)) -. (180.0*.(pi/.180.0)))
            else posy +. step*.sin((ang*.(pi/.180.0)) -. (270.0*.(pi/.180.0)));;

let inp= [[360 ; 1];[180 ; 30];[220 ; 21];[20 ; 282];[340 ; 79];[340 ; 291];[220 ; 3];[200 ; 323];[320 ; 276];[320 ; 354];[420 ; 285];[400 ; 339];[220 ;308];[140 ; 294];[100 ; 334];[440 ; 84]] in
let posx= ref 0.0 in
let posy= ref 0.0 in
    for i=0 to (List.length inp) -1     do
            let step=(List.nth (List.nth inp (i)) 0) in
            let ang=(List.nth (List.nth inp (i)) 1) in
            let posxn=ref (coorx !posx (float_of_int ang) (float_of_int step))  in
          let posyn=ref (coory !posy (float_of_int ang) (float_of_int step))  in
          posx := !posxn;
            posy := !posyn;
            if i= ((List.length inp) -1)  then
                begin
                print_float (floor((!posxn) +. 0.5));
        print_string " ";
        print_float (floor((!posyn) +. 0.5));
        print_string " ";
                end
    done;
