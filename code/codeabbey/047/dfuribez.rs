/*
$ rustfmt --check --config max_width=80,tab_spaces=2 dfuribez.rs
$ rustc dfuribez.rs
*/

use std::char;
use std::io::{self, BufRead};

fn cipher(shift: i32, message: String) {
  for i in message.chars() {
    if i == ' ' || i == '.' {
      print!("{}", i);
    } else {
      let mut shifted: i32 = { i as i32 - (shift + 65) };

      if shifted < 0 {
        shifted = 26 + shifted;
      }

      shifted = (shifted % 26) + 65;
      print!("{}", char::from_u32(shifted as u32).unwrap());
    }
  }
  println!("");
}

fn main() {
  let stdin = io::stdin();
  let lines = stdin.lock().lines();
  let mut n = 0;
  let mut shift: i32 = 0;

  for line in lines {
    let l = line.unwrap();
    if n != 0 {
      cipher(shift, l);
    } else {
      n = 1;
      let params: Vec<&str> = l.split_whitespace().collect();
      shift = params[1].parse().unwrap_or(0);
    }
  }
}

/*
$ cat DATA.lst | ./dfuribez
LET HIM THROW THE FIRST STONE FOUR SCORE AND SEVEN YEARS AGO THE DEAD \
BURY THEIR OWN DEAD.
AND FORGIVE US OUR DEBTS WHO WANTS TO LIVE FOREVER.
TO US IN OLDEN STORIES THAT ALL MEN ARE CREATED EQUAL MET A WOMAN AT \
THE WELL.
AS EASY AS LYING A DAY AT THE RACES GREENFIELDS ARE GONE NOW.
THE SECRET OF HEATHER ALE IN ANCIENT PERSIA THERE WAS A KING CARTHAGE \
MUST BE DESTROYED.
LOVEST THOU ME PETER.
*/
