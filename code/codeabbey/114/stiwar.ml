(*
el ejercicio #114 de codeabbey no permite el uso de
captura de datos desde el teclado, sino que a partir
de un entero dado en el dataset, generar dicho numero
sin capturarlo, en mi caso, la prueba la realice para
el numero 30. Razon por la cual, este script siempre 
devuelve 30 (la longitud del codigo generado en brainfuck 
no debe superar la longitud del numero dado en el dataset)
*)
let data = "++++++[>+<-]>[-<+++++>]<:";;
let leng = String.length data;;
let cel = Array.make 10 0;;
let c = ref 0;;
let p = ref 0;;
let k = ref 0;;
let j = ref 0;;
let t = ref 0;;
let flag = ref true;;
let rec decode x =
    match x with
    | '+' -> cel.(!p)<-cel.(!p) + 1;flag:=true;
    | '-' -> cel.(!p)<-cel.(!p) - 1;flag:=true;
    | '>' -> p := !p + 1;flag:=true;
    | '<' -> p := !p - 1;flag:=true;
    | ':' -> Printf.printf "%i y la longitud del brainfuck es de: %i" cel.(!p) (String.length data);flag:=true;    
    | '[' -> (                    
                j:=!c+1;
                t:=!c+1;
                while data.[!j]<>']' do            
                    j:=!j+1
                done;
                if cel.(!p)=0 then (
                    c:=!j+1;
                    flag:=false;                    
                )else(                    
                    flag:=false;
                    c:=!c+1;
                    decode data.[!c]                    
                );
             )    
    | ']' -> (            
                if cel.(!p)=0 then (                    
                 )else(
                     flag:=false;
                     c:=!t;
                    decode data.[!c];
                 )
             )    
    | _ -> failwith "se ingreso un caracter no valido\n"
;;
while !c<leng do    
    decode data.[!c];
    if(!flag)then(        
        c := !c + 1;
    )    
done;;
print_string "\n";;
