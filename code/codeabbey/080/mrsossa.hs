{-
 $ ghc  mrsossa.hs
   [1 of 1] Compiling Main ( mrsossa.hs )
   Linking code ...
 $ hlint mrsossa.hs
   No hints
-}

import System.IO
import Control.Monad

processfile :: Handle -> IO ()
processfile ifile =
  do
    iseof <- hIsEOF ifile
    Control.Monad.unless iseof $
      do
        line <- hGetLine ifile
        let vector_ints = map read $ words line :: [Float]
        let alan = head vector_ints / 100
        let bob = vector_ints!!1 / 100
        let result =  round ((alan / (alan + bob - (alan * bob))) * 100)
        print result
        processfile ifile

main =
  do
    ifile <- openFile "DATA.lst" ReadMode
    line <- hGetLine ifile
    processfile ifile
    hClose ifile

{-
 $ ./mrsossa
 48 46 94 72 89 72 71 73 58 50 55 52 87
 85 76 96 66 88 35 93 82 61 74 40
-}
