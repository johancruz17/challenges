/* $ npx eslint paolagiraldo.ts */

process.stdin.setEncoding('utf8');
process.stdin.on('readable', () => {
  const inputData = process.stdin.read();
  if (inputData !== null) {
    const rowsData = inputData.split('\n').slice(1);
    rowsData.forEach((row: string) => {
      const values = row.split(' ');
      const pos = Number(values[0]);
      const sub = values[1].slice(pos);
      const phrase = values[1].split(sub);
      const final = sub + phrase[0];
      console.log(final);
      return 'Output';
    });
  }
  return 'Data loaded';
});

/* $ tsc paolagiraldo.ts
$ cat DATA.lst | node paolagiraldo.js
plvrapuoswkumeiuiqjvrkyv
ewaiwqycfjcveriuoeayi
xshueqpypmpkugamiawlvtuei
kuyzvogfyalthtteyjegwxo
jbfxlbwfwflztjclo
rnoelaouvvwjyyunh
fxoozbuqglvrhtypefkjr
ynnzocjvoygwmeya
qywvxeqfjurhnmwe
 */
