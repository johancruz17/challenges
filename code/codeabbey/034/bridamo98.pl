#!/usr/bin/perl
# Linting:
# $ perlcritic --noprofile --brutal \
# --exclude 'NamingConventions::Capitalization' \
# --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
# --verbose 11 bridamo98.pl
# bridamo98.pl source OK
#
# Compile:
# $ perl -c bridamo98.pl
# bridamo98.pl syntax OK

package bridamo98;

use warnings FATAL => 'all';
use strict;
use 3.50;
use Readonly;
Readonly my $ERROR     => 0.000000001;
Readonly my $LOW_LIMIT => 0.0;
Readonly my $UP_LIMIT  => 100.0;
Readonly my $INDEX_A   => 0;
Readonly my $INDEX_B   => 1;
Readonly my $INDEX_C   => 2;
Readonly my $INDEX_D   => 3;
Readonly my $THREE     => 3;
Readonly my $FIFTY     => 50;

our ($VERSION) = 1;

my $SPACE = q{ };

sub function {
  my @data      = @_;
  my $x         = $data[0];
  my @constants = @{ $data[1] };
  my $A         = $constants[$INDEX_A];
  my $B         = $constants[$INDEX_B];
  my $C         = $constants[$INDEX_C];
  my $D         = $constants[$INDEX_D];
  my $f_in_x =
    $A * $x + $B * sqrt( $x**$THREE ) - $C * exp( -$x / $FIFTY ) - $D;
  return $f_in_x;
}

sub solve_problem {
  my @data        = @_;
  my $x_i         = $data[0];
  my $x_f         = $data[1];
  my @constants   = @{ $data[2] };
  my $A           = $constants[$INDEX_A];
  my $B           = $constants[$INDEX_B];
  my $C           = $constants[$INDEX_C];
  my $D           = $constants[$INDEX_D];
  my $middle      = ( $x_i + $x_f ) / 2.0;
  my $f_in_middle = function( $middle, \@constants );

  if ( $f_in_middle < $ERROR and $f_in_middle > -$ERROR ) {
    exit 1 if !print $middle;
  }
  else {
    if ( $f_in_middle < 0.0 ) {
      solve_problem( $middle, $x_f, \@constants );
    }
    else {
      solve_problem( $x_i, $middle, \@constants );
    }
  }
  return;
}

sub solve_all_problems {
  my @data               = @_;
  my @constants          = @{ $data[0] };
  my $number_of_problems = $data[1];
  for ( 0 .. $number_of_problems - 1 ) {
    solve_problem( $LOW_LIMIT, $UP_LIMIT, $constants[$_] );
    exit 1 if !print $SPACE;
  }
  return;
}

sub get_data {
  my @data               = @_;
  my $number_of_problems = $data[0];
  my @constants          = ();
  for ( 0 .. $number_of_problems - 1 ) {
    my @line = split $SPACE, <>;
    $constants[$_]->[$INDEX_A] = $line[$INDEX_A];
    $constants[$_]->[$INDEX_B] = $line[$INDEX_B];
    $constants[$_]->[$INDEX_C] = $line[$INDEX_C];
    $constants[$_]->[$INDEX_D] = $line[$INDEX_D];
  }
  return @constants;
}

sub main {
  my $number_of_problems = int <>;
  my @constants          = get_data($number_of_problems);
  solve_all_problems( \@constants, $number_of_problems );
  exit 1 if !print "\n";
  return;
}

main();

# $ cat DATA.lst | perl bridamo98.pl
# 27.1078965048218 73.7565417271981 94.2212245738574
# 7.1299443990938 93.9740099380288 70.4947276596613
# 71.4036141972429 60.9402058212254 24.1785185454319
