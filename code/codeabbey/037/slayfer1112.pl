# Linting:
# $ perlcritic --noprofile --brutal \
# --exclude 'NamingConventions::Capitalization' \
# --exclude 'Perl::Critic::Policy::CodeLayout::RequireTidyCode' \
# --verbose 11 slayfer1112.pl
# slayfer1112.pl source OK
#
# Compile:
# $ perl -c slayfer1112.pl
# slayfer1112.pl syntax OK

package slayfer1112;

use strict;
use warnings FATAL => 'all';
our ($VERSION) = 1;

use Readonly;
use POSIX;

sub data_in {
  my @data = ();
  while ( my $line = (<>) ) {
    push @data, $line;
  }
  return @data;
}

sub solution {
  my @args = @_;
  my $P    = $args[0];
  Readonly my $R => $args[1] / 100 / 12;
  my $L      = $args[2];
  my $answer = ceil( ( $P * $R ) / ( 1 - ( 1 / ( ( 1 + $R )**$L ) ) ) );
  exit 1 if !print $answer;
  return 'The mathematics are magic';
}

sub main {
  my @vals  = data_in();
  my $SPACE = q{ };
  my @body  = split $SPACE, $vals[0];
  solution(@body);
  exit 1 if !print "\n";
  return 'This thing make me think to buy a house';
}

main();
1;

# $ cat DATA.lst | perl slayfer1112.pl
# 22879
