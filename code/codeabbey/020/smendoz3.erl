% erlc -w smendoz3.erl
% erlint:lint("smendoz3.erl").
% {ok,[]}

-module(smendoz3).
-import(lists,[nth/2]).
-export([readfile/1]).
-export([start/0]).
-export([read/1]).
-export([function/1]).
-export([conditional/1]).
-export([is_vowel/1]).
-export([countvow/1]).

read(A) ->
  lists:foreach(fun(X) -> function(X) end, A).

function(A) ->
  VowString = conditional(A),
  countvow(VowString).

countvow([]) -> ok;
countvow(A) ->
  C = length(A),
  io:fwrite("~p ", [C]).

is_vowel($a) -> true;
is_vowel($e) -> true;
is_vowel($i) -> true;
is_vowel($o) -> true;
is_vowel($u) -> true;
is_vowel($y) -> true;
is_vowel(_) -> false.

conditional(String) -> [C || C <- String, is_vowel(C)].

readfile(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  string:tokens(erlang:binary_to_list(Binary), "\r\n").

start() ->
  Data = readfile("DATA.lst"),
  read(Data),
  io:fwrite("~n").

%  $ erl -noshell -s smendoz3 start -s init stop
%    8 5 11 14 16 10 9 10 16 16 12 7 8 13 9 4 10 10 7 9
