# info: some checks were skipped because they're not compatible with
# your version of Elixir (1.10.4).

# You can deactivate these checks by adding this to the
# `checks` list in your config:

#     {Credo.Check.Refactor.MapInto, false},
#     {Credo.Check.Warning.LazyLogging, false},
# Please report incorrect results: https://github.com/rrrene/credo/issues
# Analysis took 0.05 seconds (0.01s to load, 0.04s running
# 54 checks on 1 file)
# 8 mods/funs, found no issues.

# iex(1)> c("rechavar.exs")

defmodule Smoothing do
@moduledoc "
This is module solves Codeabbey exercise number 057 (Smoothing the weather)
"

  def main do
    get_values(File.read("DATA.lst"))
  end

  def get_values(file) do
    file = elem(file, 1)
    file = String.split(file, "\n")
    [_, array] = [Enum.at(file, 0), Enum.at(file, 1)]
    array = String.split(array, " ")
    float_array = Enum.map(array, &String.to_float/1)
    smoothing(float_array)
  end

  def smoothing(list) do
    [h|t] = list
    new_array = [h]
    smoothed_values = get_smooth(new_array, t, 1, Enum.count(t))
    Enum.each smoothed_values, fn item ->
      IO.puts "#{item} "
    end
  end

  def append_values(list1, list2) do
    list1 ++ list2
  end

  def get_smooth(list, list1, x, y) when x == 1 and y > 2 do
    [h|t] = list1
    list = append_values(list, [(h + Enum.at(t, 0) + Enum.at(list, 0)) / 3])
    get_smooth(list, list1, Enum.count(list), Enum.count(list1))
  end

  def  get_smooth(list, list1, x, y) when x > 1 and y > 2 do
    [h|t] = list1
    list = append_values(list, [(h + Enum.at(t, 0) + Enum.at(t, 1)) / 3])
    get_smooth(list, t, Enum.count(list), Enum.count(t))
  end

  def get_smooth(list, list1, x, y) when x > 1 and y == 2 do
    list ++ [List.last(list1)]
  end
end

# iex(2) > Smoothing.main
# 32.6 33.0 34.6 39.166666666666664 41.46666666666667 43.699999999999996 44.1
