#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 2.74 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

def f_to_i_possible (num)
  if num - num.to_i == 0.0
    num.to_i
  else
    num.round 10
  end
end
args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args[1..].map {|x| x.to_f}

print "#{f_to_i_possible args[0]} "
(1...args.size - 1).each do |x|
  avg = args[x - 1 .. x + 1].sum() / 3
  print "#{f_to_i_possible avg} "
end
print "#{f_to_i_possible args[-1]} "

puts

# $ ./richardalmanza.cr
# 32.6 33 34.6 39.1666666667 41.4666666667 43.7 44.1
