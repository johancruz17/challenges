/*
$ eslint slayfer1112.ts
$ tsc slayfer1112.ts
*/
function solution(entry: string[]): number {
  const entryF: number[] = entry.map(parseFloat);
  const maximum: number = Math.max(...entryF);
  const minimum: number = Math.min(...entryF);
  const result: number[] = [maximum, minimum];
  process.stdout.write(result.join(' '));
  process.stdout.write('\n');
  return 0;
}

function main(): number {
  process.stdin.resume();
  process.stdin.setEncoding('utf-8');
  process.stdin.on('data', (datEntry) => {
    const entry: string = datEntry.toString().trim();
    const entryArray: string[] = entry.trim().split('\n');
    const arr: string[] = entryArray[0].split(' ');
    solution(arr);
    return 0;
  });
  return 0;
}

main();
/*
$ cat DATA.lst | node slayfer1112.js
79968 -79467
*/
