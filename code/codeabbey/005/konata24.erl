% $ erl -compile konata24.erl
% $ erl -noshell -s erlint konata24 -s init stop
% ok

-module(konata24).

-import(lists, [nth/2]).

-export([start/0]).

-compile(konata24).

readfile(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  string:tokens(erlang:binary_to_list(Binary), "\r\n").

inputs(Array) ->
  [H | T] = Array,
  {Inputs, _} = string:to_integer(H),
  Values = for(1, Inputs,
    fun (X) ->
      Q = string:tokens(nth(X, T), " "),
      {A, _} = string:to_integer(nth(1, Q)),
      {B, _} = string:to_integer(nth(2, Q)),
      {C, _} = string:to_integer(nth(3, Q)),
      [{A, B, C}]
    end),
  {Inputs, Values}.

for(N, N, F) -> F(N);
for(I, N, F) -> F(I) ++ for(I + 1, N, F).

minimum3({A, B, C}) ->
  if A < B ->
    if A < C -> [A];
      true -> [C]
    end;
    B < C ->
    if B < A -> [B];
      true -> [A]
    end;
    true ->
    if C < B -> [C];
      true -> [B]
    end
  end.

sam(F, List) -> [F(X) || X <- List].

start() ->
  Data = readfile("DATA.lst"),
  {Inputs, Values} = inputs(Data),
  Solution = for(1, Inputs,
    fun (X) ->
      {A, B, C} = nth(X, Values), minimum3({A, B, C})
    end),
  E = fun (X) -> io:fwrite("~w ", [X]) end,
  sam(E, Solution),
  io:format("~n").

% $ erl -noshell -s konata24 -s init stop
% 3005639 -1214416 -8221224 -1342713 -5074760 -7986998 1094659 -2922519
% -191243 -4475707 -7084482 -5463765 -9300288 -974211 1592950 -8737149
% 350441 -9119450 -9207798 -3099517 1173483 -7453182 -6444877 -7381581
% -7376969 -8121188 2229253 -1692942 -2642475
