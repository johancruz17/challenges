#! /usr/bin/crystal

# $ ameba slayfer1112.cr
# Inspecting 1 file.
# .
# Finished in 8.48 milliseconds
# $ crystal build slayfer1112.cr

def data_entry()
  data = File.read("DATA.lst")
  values = [] of Array(Float64)
  data.each_line do |x|
    inter = [] of Float64
    (x.split).each do |y|
      y = y.is_a?(String) ? y.try &.to_f : y
      inter << y
    end
    values << inter
  end
  values[1..]
end

def solution(array)
  dist = array[0]
  runner1 = array[1]
  runner2 = array[2]
  sol = ((dist / (runner1+runner2) ) * runner1).round(8)
  mod = sol.divmod(sol.round(0))
  sol = sol.to_i if mod[1] == 0
  print "#{sol} "
end

data = data_entry()
data.each do |x|
  solution(x)
end
puts

# $ ./slayfer1112
# 17.58974359 33.61538462 18.29032258 184.95918367 18.10869565 39.27272727
# 82.28571429 6.85714286 4.8372093 7.56756757 5.84375 200.80645161 44.8
# 17.88888889 32.19512195 81.33333333 94.26315789 5.66666667 29.4 85.25
# 29.42857143 4.75609756 5.64705882 36.63157895 10.26666667 187.82051282
