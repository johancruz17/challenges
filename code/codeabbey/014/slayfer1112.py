# $ mypy --strict slayfer1112.py
# Success: no issues found in 1 source file

from typing import List

X: int = int(input())
B: int = 0
D: int = 0

while B == 0:
    Y: List[str] = input().split(' ')
    if Y[0] == '*':
        D = X * int(Y[1])
    elif Y[0] == '%':
        D = X % int(Y[1])
    else:
        D = X + int(Y[1])
    X = D
    if Y[0] == '%':
        B = 1
print(X)

# $ cat DATA.lst | python3 slayfer1112.py
# 2412
