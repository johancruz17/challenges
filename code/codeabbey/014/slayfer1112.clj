;; $ clj-kondo --lint slayfer1112.clj
;; linting took 177ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))]
    [data]))

(defn solution [data len x counter]
  (if (< counter (- len 1))
    (let [res (eval (read-string (str "(" (data counter) "N " x ")")))]
      (solution data len res (inc counter)))
    (let [text (str "(mod " x " " (data counter) "N)")
          replace (str/replace text #"%" "")
          res (eval (read-string replace))]
      (print (int res)))))

(defn main []
  (let [[data] (get-data)]
    (solution data (count data) (bigint (data 0)) 1)
    (println)))

(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; 2412
