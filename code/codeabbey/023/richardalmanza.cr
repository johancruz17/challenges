#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 4.49 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args[...-1].map {|x| x.to_i}

swap_counter = 0
checksum = 0

args.each_with_index do |x, y|
  if y + 1 != args.size
    if x > args[y + 1]
      swap_counter += 1
      temp = x
      args[y] = args[y + 1]
      args[y + 1] = temp
    end
  end
  checksum = ((checksum + args[y]) * 113) % 10_000_007
end

puts "#{swap_counter} #{checksum}"
# $ ./richardalmanza.cr
# 41 1962385
