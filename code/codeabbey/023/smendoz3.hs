-- $ ghc -o smendoz3 smendoz3.hs
--   [1 of 1] Compiling Main ( smendoz3.hs, smendoz3.o )
--   Linking code ...
-- $ hlint smendoz3.hs
--   No hints

main = do
  n <- getLine
  let a = show (sortS (init (checkString n)) 0)
  let b = show (arrayCheckSum (reverse (sortA (init (checkString n)) [])))
  print (a ++ " " ++ b)

checkString :: String -> [Integer]
checkString str = map read $ words str :: [Integer]

sortS :: [Integer] -> Int -> Int
sortS [x] swap = swap
sortS (x:y:xs) swap = if x > y then sortAux else sortS (y:xs) swap
  where sortAux = sortS (max x y:xs) swap + 1

sortA :: [Integer] -> [Integer] -> [Integer]
sortA [x] result = x:result
sortA (x:y:xs) result = if x > y then sortArray else sortA (y:xs) (x:result)
  where sortArray = sortA (max x y:xs) (min x y:result)

arrayCheckSum :: [Integer] -> Integer
arrayCheckSum [] = 0
arrayCheckSum [x] = mod (x * 113) 10000007
arrayCheckSum (x:xs) = arrayCheckSum $ head xs + mod (x*113) 10000007 : tail xs

-- $ ./smendoz3
--   "34 6862553"
