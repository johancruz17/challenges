#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defun degrees-to-radians (degrees)
  (* pi (/ degrees 180.0))
)

(defun calc-height(distance a-angle b-angle height)
  (setq height
    (round
      (/
        distance
        (-
          (/
            1.0
            (tan (degrees-to-radians a-angle))
          )
          (/
            1.0
            (tan (degrees-to-radians b-angle))
          )
        )
      )
    )
  )
  (format t "~a " height)
)

(defun solve(size-input distances a-angles b-angles height)
  (loop for x from 0 to (- size-input 1)
    do(calc-height
      (aref distances x)
      (aref a-angles x)
      (aref b-angles x)
      height
    )
  )
)

(defun get-input-lines(x number distances a-angles b-angles)
  (setq number (read))
  (setf (aref distances x) number)
  (setq number (read))
  (setf (aref a-angles x) number)
  (setq number (read))
  (setf (aref b-angles x) number)
)

(defvar size-input (read))
(defparameter distances (make-array size-input))
(defparameter a-angles (make-array size-input))
(defparameter b-angles (make-array size-input))
(defvar number 0)
(defvar height 0)

(loop for x from 0 to (- size-input 1)
  do(get-input-lines x number distances a-angles b-angles)
)

(solve size-input distances a-angles b-angles height)

#|
  cat DATA.lst | clisp bridamo98.lsp
  609 974 846 1754 1258 1992 1380 1549 1185 1947
  1249 1309 1843 519 1148 1896 1375
|#
