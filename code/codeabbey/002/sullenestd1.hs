{-
$ hlint sullenestd1.hs
$ ghc -dynamic sullenestd1.hs
[1 of 1] Compiling Main (sullenestd1.hs, sullenestd1.o)
Linking sullenestd1 ...
-}

main = do
  n <- getLine
  x <- getLine
  let lst = map read(words x)::[Int]
  print (sum lst)

-- $cat DATA.lst | ./sullenestd1
-- 27098
