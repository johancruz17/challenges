#!/usr/bin/env python3

"""
$ pylint cleancamera.py.py
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
"""

from typing import List


def sum_list(list_data: List[int]) -> int:
    sum_data = 0
    for i in list_data:
        sum_data = sum_data + i
    return sum_data


def divide_array() -> None:
    """
    get data file
    """
    file_open = open('DATA.lst', 'r')
    array_data = file_open.read()
    data_convert = [array_data]
    for line in data_convert:
        type_data = line.split()
        data_int = list(map(int, type_data))
    print(sum_list(data_int))


divide_array()

# $ python cleancamera.py
# 10000
