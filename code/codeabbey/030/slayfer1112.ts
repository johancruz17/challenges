/*
$ eslint slayfer1112.ts && prettier --check slayfer1112.ts
$ tsc \
  --strict \
  --noImplicitAny \
  --noImplicitThis \
  --noUnusedLocals \
  --noUnusedParameters \
  --noImplicitReturns \
  --noFallthroughCasesInSwitch \
  slayfer1112.ts
*/

function solution(entry: string[]): number {
  const text: string[] = entry;
  const len: number = text.length - 1;
  const reversed: string[] = text.map((value, idx) => {
    if (value) {
      return text[len - idx];
    }
    return value;
  });
  process.stdout.write(`${reversed.join('')}`);
  return 1;
}

function main(): number {
  process.stdin.resume();
  process.stdin.setEncoding('utf-8');
  process.stdin.on('data', (datEntry) => {
    const entry: string = datEntry.toString().trim();
    const entryArray: string[] = entry.trim().split('\n');
    const dat: string[] = entryArray.slice(0, entryArray.length);
    dat.map((value) => solution(value.split('')));
    process.stdout.write('\n');
    return 1;
  });
  return 0;
}

main();
/*
$ cat DATA.lst | node slayfer1112.js
aococ erehw ffo yhw flehs nwolc eraf
*/
