% $ erl -compile slayfer1112.erl
% $ erl -noshell -s erlint slayfer1112 -s init stop
% ok

-module(slayfer1112).

-import(lists, [nth/2]).

-export([start/0]).

-compile(slayfer1112).

% Read file and convert in binary list
readfile(FileName) ->
  {ok, Binary} = file:read_file(FileName),
  string:tokens(erlang:binary_to_list(Binary), "\r\n").

% Take the number of inputs
inputs(Array) ->
  [H | T] = Array,
  {Inputs, _} = string:to_integer(H),
  Lower = for(1, Inputs,
  fun (X) -> [string:to_lower(nth(X, T))] end),
  {Inputs, Lower}.

% For loop with I<N
for(N, N, F) -> F(N);
for(I, N, F) -> F(I) ++ for(I + 1, N, F).

% For loop with I>N
for2(N, N, F) -> F(N);
for2(I, N, F) -> F(I) ++ for2(I - 1, N, F).

% Delete all chars that are not lowercase letters
only_letters(Line) ->
  N = string:len(Line),
  Letters = for(1, N,
    fun (X) ->
       {A, B} = {nth(X, Line) > 96, nth(X, Line) < 123},
       case val of
         _ when {A, B} == {true, true} -> [nth(X, Line)];
         _ -> []
       end
    end),
  [Letters].

% Make a invert String and eval if matching
invert(N, Words) ->
  Invert = for(1, N,
    fun (X) ->
     Line = nth(X, Words),
     L = string:len(Line),
     [for2(L, 1, fun (Y) -> [nth(Y, Line)] end)]
    end),
  Invert.

% Function that evals if a line is palindrome
is_palindrome(Line, Inv_line) ->
  case Line of
    _ when Line == Inv_line -> "Y";
    _ -> "N"
  end.

% Print the result
result(F, List) -> [F(X) || X <- List].

% Run the program
start() ->
  Data = readfile("DATA.lst"),
  {N, Words} = inputs(Data),
  Words_l = for(1, N,
    fun (X) -> only_letters(nth(X, Words)) end),
  Invert = invert(N, Words_l),
  Palindrome = for(1, N,
       fun (X) ->
          [is_palindrome(nth(X, Words_l), nth(X, Invert))]
       end),
  Print = fun (X) -> io:fwrite("~s ", [X]) end,
  result(Print, Palindrome),
  io:fwrite("~n").

% erl -noshell -s slayfer1112 -s init stop
% N Y Y Y Y Y N Y N Y N N N N N
