--[[
$ luacheck mrsossa.lua
Checking mrsossa.lua                              OK
Total: 0 warnings / 0 errors in 1 file
]]

local data = io.lines("DATA.lst")
local c,n,k
for line in data do
    c = line
end
local i = 0
for num in string.gmatch(c, "[^%s]+") do
    if i==0 then
        n = tonumber(num)
        i = i +1
    else
        k = tonumber(num)
    end
end
local res = 0
while i <= n do
    res = (res + k) % i
    i = i + 1
end
print(res+1)


--[[
$ lua mrsossa.lua
54
]]
