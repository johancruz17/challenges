'''
$ mypy roguerre.py
Success: no issues found in 1 source file
'''
from typing import Dict, List
import sys

# parameters for the pseudo-random generator
PARAM_A = 445
PARAM_C = 700001
PARAM_M = 2097152


# linear congruential generator to generate
# a sequence of pseudo-random integers
def lin_con_gen(
        param_a: int,
        param_c: int,
        param_m: int,
        x_init: int,
        count: int) -> List[int]:
    sequence: List[int] = []
    x_next: int = x_init
    for _ in range(count):
        x_next = (param_a * x_next + param_c) % param_m
        sequence.append(x_next)
    return sequence


# transforms a value of some random sequence
# so that it fits in the range 1 to `n_nodes`
# (this being the number of nodes in the graph)
def transform_seq_val(
        num: int,
        n_lim: int) -> int:
    return num % n_lim + 1


# auxiliary function to build a transformed
# suitable random sequence
def transformed_seq(
        random_seq: List[int],
        n_nodes: int) -> List[int]:
    return [transform_seq_val(val, n_nodes) for val in random_seq]


# auxiliary function to build an empty graph
# of `n_nodes` nodes
def init_graph(
        n_nodes: int) -> Dict[int, Dict[int, int]]:
    return {key: {} for key in range(1, n_nodes + 1)}


# auxiliary function to build and edge in
# the graph with a given weight
def build_edge(
        graph: Dict[int, Dict[int, int]],
        from_node: int,
        to_node: int,
        weight: int) -> None:
    # can't be an edge to itself
    if from_node == to_node:
        return
    # can't be an existent edge
    if to_node in graph[from_node]:
        return
    # insert edge towards `to_node`
    graph[from_node][to_node] = weight
    # now the other way around (undirected graph)
    graph[to_node][from_node] = weight


# auxiliary function to build the edges in
# the `graph` passed (this is an in-place
# operation)
def build_edges(
        graph: Dict[int, Dict[int, int]],
        rand_seq: List[int]) -> None:
    for node_i in graph:
        # extracting nodes and weights
        node_1: int = rand_seq.pop(0)
        weight_1: int = rand_seq.pop(0)
        node_2: int = rand_seq.pop(0)
        weight_2: int = rand_seq.pop(0)
        # inserting edges
        build_edge(graph, node_i, node_1, weight_1)
        build_edge(graph, node_i, node_2, weight_2)


# auxiliary function to build the graph
# with a given `seed` and `n_nodes`
def build_graph(
        seed: int,
        n_nodes: int) -> Dict[int, Dict[int, int]]:
    # requiring 4 random values per node
    random_seq: List[int] = lin_con_gen(
        PARAM_A,
        PARAM_C,
        PARAM_M,
        seed,
        n_nodes * 4
    )
    # make the sequence suitable
    transformed_vals: List[int] = transformed_seq(random_seq, n_nodes)
    # set initial graph
    graph: Dict[int, Dict[int, int]] = init_graph(n_nodes)
    # set graph edges
    build_edges(graph, transformed_vals)
    return graph


# auxiliary function to compute the sum of
# weights for incident edges for each node
# in the graph
def get_sums(
        graph: Dict[int, Dict[int, int]]) -> List[int]:
    sums: List[int] = []
    for node in graph:
        sum_weights: int = 0
        for neighbor in graph[node]:
            sum_weights += graph[node][neighbor]
        sums.append(sum_weights)
    return sums


if __name__ == '__main__':
    line: str = sys.stdin.read().strip()
    N_NODES_INP: int
    SEED_INP: int
    N_NODES_INP, SEED_INP = [int(value) for value in line.split()]
    end_graph: Dict[int, Dict[int, int]] = build_graph(SEED_INP, N_NODES_INP)
    w_sums: List[int] = get_sums(end_graph)
    print(' '.join([str(sum) for sum in w_sums]))
# cat DATA.lst | python roguerre.py
# 41 35 20 43 16 60 19 25 40 53 32 34 12 37 36 27 31 1 16
