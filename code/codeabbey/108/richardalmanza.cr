#! /usr/bin/crystal

# $ ameba richardalmanza.cr #Linting
# Inspecting 1 file.
# .
# Finished in 1.44 milliseconds
# 1 inspected, 0 failures.
# $ crystal build richardalmanza.cr --release

args = ARGV
fluid = args == [] of String

args = File.read("DATA.lst").split if fluid
args = args[0].split if !fluid

args = args[1..].map {|x| x.to_i}

(0...args.size).step(2).each do |x|
  print "#{args[x] * (args[x + 1] - 1)} "
end

puts

# $ ./richardalmanza.cr
# 20 51 27 102 32 57 15 11 17
