/*
linter:
cppcheck \
  --error-exitcode=1 \
  slayfer1112.c \
&&  splint \
  -strict \
  -internalglobs \
  -modfilesys \
  -boundsread \
  slayfer1112.c
Checking slayfer1112.c ...
Splint 3.1.2 --- 05 Sep 2017

Finished checking --- no warnings

compilation:
gcc \
  -Wall \
  -Wextra \
  -Winit-self \
  -Wuninitialized \
  -Wmissing-declarations \
  -Winit-self \
  -ansi \
  -pedantic \
  -Werror \
  slayfer1112.c \
  -o slayfer1112 \
  -lm
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define MAX_SIZE 255

static int short_form(char result[]) /*@*/ {
  int x = (int) strlen(result), i, counter = 1, temp = 0;
  for (i = 0; i < x; i++) {
    if (result[i] == result[i + 1])
    { counter++; }
    else {
      temp = printf("%d%c", counter, result[i]);
      counter = 1;
    }
  }
  printf(" ");
  return temp;
}

static int neighboor_exist (int neigh[], int x, int y,
  char m[][MAX_SIZE], int prevI, int prevJ)
  /*@*/ {
  char state = m[neigh[0]][neigh[1]];
  if (neigh[0] >= 0 && neigh[0] < x && neigh[1] >= 0 && neigh[1] < y) {
    if (state == '1' && (neigh[0] != prevI || neigh[1] != prevJ))
    { return 1; }
    else { return 0; }
  }
  else { return 0; }
}

static int node_neighboors (int posI, int posJ, int x, int y, char path[],
  char m[][MAX_SIZE], int prevI, int prevJ) /*@*/ {
  int posA[2], posB[2], posC[2], posD[2], temp = 0;
  char n_path[MAX_SIZE + 3];
  strncpy(n_path, path, (size_t) MAX_SIZE);
  posA[0] = posI, posA[1] = posJ + 1;
  posB[0] = posI + 1, posB[1] = posJ;
  posC[0] = posI, posC[1] = posJ - 1;
  posD[0] = posI - 1, posD[1] = posJ;
  if (neighboor_exist(posA, x, y, m, prevI, prevJ) == 1) {
    strncat(n_path, "R", (size_t) 2);
    temp = node_neighboors(posA[0], posA[1], x, y, n_path, m, posI, posJ);
    strncpy(n_path, path , (size_t) MAX_SIZE);
  } if (neighboor_exist(posB, x, y, m, prevI, prevJ) == 1) {
    strncat(n_path, "D", (size_t) 2);
    temp = node_neighboors(posB[0], posB[1], x, y, n_path, m, posI, posJ);
    strncpy(n_path, path , (size_t) MAX_SIZE);
  } if (neighboor_exist(posC, x, y, m, prevI, prevJ) == 1) {
    strncat(n_path, "L", (size_t) 2);
    temp = node_neighboors(posC[0], posC[1], x, y, n_path, m, posI, posJ);
    strncpy(n_path, path , (size_t) MAX_SIZE);
  } if (neighboor_exist(posD, x, y, m, prevI, prevJ) == 1) {
    strncat(n_path, "U", (size_t) 2);
    temp = node_neighboors(posD[0], posD[1], x, y, n_path, m, posI, posJ);
    strncpy(n_path, path , (size_t) MAX_SIZE);
  } if (posI == 0 && posJ == 0) { temp = short_form(n_path); }
  return temp;
}

static int path_finder (char cas, int x,
  int y, char matriz[][MAX_SIZE]) /*@*/ {
  int posI, posJ, temp;
  static char path[MAX_SIZE];
  switch (cas) {
  case 'B':
    posI = x - 1, posJ = 0;
    break;
  case 'C':
    posI = x - 1, posJ = y - 1;
    break;
  default:
    posI = 0, posJ = y - 1;
    break;
  }
  temp = node_neighboors(posI, posJ, x, y, path, matriz, 0, 0);
  return temp;
}

int main(void) {
  static char matriz[MAX_SIZE][MAX_SIZE];
  int x, y, i, scan;

  scan = scanf("%d %d\n", &y, &x);

  for (i = 0; i < x; i++)
  { scan = scanf("%s\n", matriz[i]); }

  scan = path_finder('A', x, y, matriz);
  scan = path_finder('B', x, y, matriz);
  scan = path_finder('C', x, y, matriz);
  printf("\n");

  return scan;
}

/*
$ cat DATA.lst | ./slayfer1112
2D2L2D2R2D2L2D2L2D8L2D4L2D2L6U2L2U2L4U4L2U4L
8U2R2D2R2U2R2D6R4U2L2U2L4U4L2U4L
2U2L4U4L2U6L2D4L2D2L6U2L2U2L4U4L2U4L
*/
