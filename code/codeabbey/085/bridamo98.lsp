#|
  $ sblint -v bridamo98.lsp
    [INFO] Lint file bridamo98.lsp
  $ clisp -c bridamo98.lsp
    Compiling file <file-location>/bridamo98.lsp ...
    Wrote file <file-location>/bridamo98.fas
    0 errors, 0 warnings
    Bye.
|#

(defstruct
  (star (:conc-name dr-))
  name
  x
  y
)

(defun deg-to-rad (degrees)
  (* pi (/ degrees 180.0))
)

(defun result-y(x y angle)
  (+
    (*
      (sin (deg-to-rad angle))
      x
    )
    (*
      (cos (deg-to-rad angle))
      y
    )
  )
)

(defun result-x(x y angle)
  (+
    (*
      (cos (deg-to-rad angle))
      x
    )
    (*
      (sin (deg-to-rad angle))
      (*
        y
        -1.0
      )
    )
  )
)

(defun update-arrays(i angle aux-x aux-y aux-name stars)
  (setf aux-x (result-x (dr-x (aref stars i)) (dr-y (aref stars i)) angle))
  (setf aux-y (result-y (dr-x (aref stars i)) (dr-y (aref stars i)) angle))
  (setf aux-name (dr-name (aref stars i)))
  (setf (aref stars i) (make-star :name aux-name
    :x aux-x
    :y aux-y
  ))
)

(defun make-rotation(size-input angle aux-name aux-x aux-y stars)
  (loop for i from 0 to (- size-input 1)
    do(update-arrays i angle aux-x aux-y aux-name stars)
  )
)

(defun order-condition(a b)
  (if (= (dr-y a) (dr-y b))
    (return-from order-condition (< (dr-x a) (dr-x b)))
    (return-from order-condition (< (dr-y a) (dr-y b)))
  )
)

(defun order-stars(stars)
  (sort stars 'order-condition)
)

(defun get-row(x aux-x aux-y aux-name stars)
  (setq aux-name (string-capitalize (read)))
  (setq aux-x (read))
  (setq aux-y (read))
  (setf (aref stars x) (make-star :name aux-name
    :x aux-x
    :y aux-y
  ))
)

(defun get-input-line (size-input aux-x aux-y aux-name stars)
  (loop for x from 0 to (- size-input 1)
    do(get-row x aux-x aux-y aux-name stars)
  )
)

(defun print-solution(size-input stars)
  (loop for x from 0 to (- size-input 1)
    do(format t "~a " (dr-name (aref stars x)))
  )
)

(defvar size-input (read))
(defvar angle (read))
(defparameter stars (make-array size-input))
(defvar aux-name "")
(defvar aux-x 0)
(defvar aux-y 0)
(defvar aux-star nil)

(get-input-line size-input aux-x aux-y aux-name stars)
(make-rotation size-input angle aux-name aux-x aux-y stars)
(order-stars stars)
(print-solution size-input stars)

#|
  cat DATA.lst | clisp bridamo98.lsp
  Mizar Nembus Media Diadem Kastra Alcyone Altair Vega Procyon Deneb
  Kochab Gemma Algol Capella Zosma Rigel Aldebaran Sirius Thabit
  Jabbah Bellatrix Yildun Unuk Electra Alcor Betelgeuse Pherkad
|#
