;; $ clj-kondo --lint slayfer1112.clj
;; linting took 177ms, errors: 0, warnings: 0

(ns slayfer1112-047
  (:gen-class)
  (:require [clojure.string :as str]))

(defn get-data []
  (let [data (str/split-lines (slurp *in*))
        head (str/split (data 0) #" ")
        body (subvec data 1 (alength (to-array-2d data)))]
    [head
     body]))

(defn new-root-sqrt [r x n i]
  (if (< i n)
    (let [d (/ x r)]
      (new-root-sqrt (/ (+ r d) 2) x n (inc i)))
    r))

(defn solution [data]
  (let [x (read-string (data 0))
        n (read-string (data 1))
        r (new-root-sqrt 1 x n 0)]
    (print (str (double r) " "))))

(defn main []
  (let [[_ body] (get-data)]
    (doseq [x body]
      (let [arr (str/split x #" ")]
        (solution arr)))
    (println)))
(main)

;; $ cat DATA.lst | clj slayfer1112.clj
;; 219.2680589727784 1649.559110569358 24.48936170212766 126.3111453836021
;; 8.366600265340756 87.6818422209526 7.071067811865475 159.2930151371662
;; 6.782329983125268 70.12851739897322 694.4357809978496
