#!/usr/bin/env python3
'''
$ pylint danimelo.py

------------------------------------
Your code has been rated at 10.00/10

$ mypy danimelo.py
Success: no issues found in 1 source file

$
'''
import hashlib


def to_sha256(string: str) -> str:

    """ plain text goes into a sha256 """
    # Unicode-objects must be encoded before hashing
    string_hash = hashlib.sha256(string.encode('utf-8'))
    return string_hash.hexdigest()


def main() -> None:

    """ Bring the data to code """
    with open("DATA.lst") as file:
        data = file.read()
    print(to_sha256(data))


if __name__ == "__main__":

    main()
#
# $ ./danimelo.py DATA.lst
# 7f83b1657ff1fc53b92dc18148a1d65dfc2d4b1fa3d677284addd200126d9069
# $
