#!/usr/bin/env ruby

# Prepare user pages
preprocess do
  user_page_items = user_page
  user_page_items.each_value do |name|
    @items.create(name[:content], name[:attributes], name[:identifier])
  end
end

# General compiler
compile '/**/*.slim' do
  filter :slim
  layout @item.fetch(:layout, '/default.slim')
  filter :relativize_paths, type: :html
end

# Gitstats and Gitinspector compiler
compile '/git-analyzers/**/**/*' do
  write item.identifier.to_s
end

# Gallery compiler
compile '/**/gallery/*' do
  write item.identifier.to_s
end

# Scripts compiler
compile '/**/scripts/*.js' do
  write item.identifier.to_s
end

# Router
route '/**/*.slim' do
  if item.identifier =~ '/index.*'
    '/index.html'
  else
    item.identifier.without_ext + '/index.html'
  end
end

# Slim default layout
layout '/**/*.slim', :slim
