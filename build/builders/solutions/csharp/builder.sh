source "${srcGeneric}"

function compile {
  local solution="${1}"

  mcs "${solution}"
}

function lint {
  local solution="${1}"
  local lizard_max_warns='0'
  local lizard_max_func_length='30'
  local lizard_max_ccn='10'

      mono \
        "${binGendarme}/gendarme.exe" \
        --severity all \
        --set self-test \
        "${solution}" \
  &&  lizard \
        --ignore_warnings "${lizard_max_warns}" \
        --length "${lizard_max_func_length}" \
        --CCN "${lizard_max_ccn}" \
        "${solution}"
}

function build {
      env_prepare_ephemeral_vars \
  &&  env_prepare_python_packages \
  &&  generic_set_utf_8 \
  &&  generic_get_solution \
  &&  compile "root/src/${solutionFileName}" \
  &&  lint "root/src/${solutionFileName%.*}.exe"
}

build || exit 1
echo > "${out}"
