source "${srcGeneric}"

function set_mix_proj {
  local proj_name="${1}"
  local credo='{:credo, "~> 1.4"}'

      mix new "${proj_name}" \
  &&  sed -i \
        "s|# {:dep_from_hexpm, \"~> 0.3.0\"}|${credo}|g" \
        "${proj_name}/mix.exs"
}

function lint {
  local solution="${1}"
  local proj_name='elixir_test_project'
  local HOME='.'

      set_mix_proj "${proj_name}" \
  &&  pushd "${proj_name}" || return 1 \
  &&  yes | mix local.hex || true \
  &&  mix deps.get \
  &&  mix credo \
        --strict \
        --files-included \
        "../${solution}" \
  &&  popd || return 1
}

function compile {
  local solution="${1}"

  elixirc \
    --warnings-as-errors \
    "${solution}"
}

function build {
      generic_set_utf_8 \
  &&  generic_get_solution \
  &&  lint "root/src/${solutionFileName}" \
  &&  compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
