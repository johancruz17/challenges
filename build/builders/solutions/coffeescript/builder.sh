source "${srcGeneric}"

function lint {
  local solution="${1}"

  coffeelint \
    -f "${srcCoffeelintConfig}" \
    "${solution}"
}

function compile {
  local solution="${1}"

  coffee \
    --compile \
    "${solution}"
}

function build {
      env_prepare_ephemeral_vars \
  &&  env_prepare_node_modules \
  &&  generic_set_utf_8 \
  &&  generic_get_solution \
  &&  lint "root/src/${solutionFileName}" \
  &&  compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
