source "${srcGeneric}"

function lint {
  local solution="${1}"
  local code

      code="a<-lintr::lint('${solution}'); print(a); if(length(a) == 0) quit(status=0) else quit(status=1)" \
  &&  R \
        -e \
        "${code}"
}

function build {
      generic_set_utf_8 \
  &&  generic_get_solution \
  &&  lint "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
