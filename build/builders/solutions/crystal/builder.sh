source "${srcGeneric}"

function lint {
  local solution="${1}"

  ameba \
    --all \
    --fail-level Convention \
    "${solution}"
}

function compile {
  local solution="${1}"

  crystal build \
    --error-trace \
    --error-on-warnings \
    --threads 1 \
    --no-codegen \
    "${solution}"
}

function build {
      generic_set_utf_8 \
  &&  generic_get_solution \
  &&  lint "root/src/${solutionFileName}" \
  &&  compile "root/src/${solutionFileName}"
}

build || exit 1
echo > "${out}"
