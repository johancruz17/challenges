## Version 1.4.1
## language: en

Feature: OS Command Injection
  TOE:
    Cloud-AV
  Category:
    Input Validation
  Location:
    http://10.10.10.2:8080/scan
  CWE:
    CWE-78: Improper Neutralization of Special Elements used in an OS Command
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Find vulnerabilities and obtain root privileges
  Recommendation:
    Sanitize inputs that may contain special characters from the Shell syntax

  Background:
  Hacker's software:
    | <Software name>      | <Version>     |
    | Kali Linux           | 5.4.0         |
    | Firefox              | 68.6.0esr     |
    | Netcat               | v1.10-41.1+b1 |
  TOE information:
    Given that the target machine is running an online antivirus software

  Scenario: Normal use case
    Given that I login successfully to the server
    Then I see a text that seems like the output of a "ls -la" command
    """
    total 4756
    -rwxr-xr-x 1 scanner scanner 1113504 Oct 21  2018 bash
    -rwxr-xr-x 1 scanner scanner   34888 Oct 21  2018 bzip2
    -rwxr-xr-x 1 scanner scanner   35064 Oct 21  2018 cat
    -rw-rw-r-- 1 scanner scanner      68 Oct 21  2018 eicar
    -rw-rw-r-- 1 scanner scanner       5 Oct 21  2018 hello
    -rwxr-xr-x 1 scanner scanner   35312 Oct 21  2018 netcat
    -rwxr-xr-x 1 scanner scanner 3633560 Oct 21  2018 python
    """
    And an input box that asks for a file name to scan
    When I enter one of the files and scan it
    Then I am redirected to a page showing the results
    When I enter a random file name
    Then I am also redirected to the aforementioned output page

  Scenario: Static detection
    Given there is not access to the source code
    Then a static detection cannot be performed

  Scenario: Dynamic detection
    Given the normal use case of the AV scanner
    And under the supposition that the server executes something like this
    """
    ./cloud-av filename
    """
    When I try to concatenate a OS command using the following as input
    """
    filename; printf Hello
    """
    Then I get the output
    """
    Infected files: 0
    Data scanned: 0.00 MB
    Data read: 0.00 MB (ratio 0.00:1)
    Time: 9.794 sec (0 m 9 s)
    Hello
    """
    And I realize remote command execution is possible

  Scenario: Exploitation
    Given that I can concatenate commands to execute remotely
    And I know of a Bash one-liner to create a reverse shell from
    """
    http://pentestmonkey.net/cheat-sheet/shells/reverse-shell-cheat-sheet
    """
    When I set up a listener locally using "netcat"
    And concatenate the command to the filename in the input box
    Then I get a reverse shell for the user "scanner"
    And I list the files in the user's directory and their permissions
    """
    -rwsr-xr-x 1 root    scanner 8576 Oct 24  2018 update_cloudav
    -rw-rw-r-- 1 scanner scanner  393 Oct 24  2018 update_cloudav.c
    """
    When I notice the executable can be run as root
    Then I believe it can be used to escalate privileges
    And I move on to analyzing the source code
    """
    char *command = malloc(strlen(freshclam) + strlen(argv[1]) + 2);
    sprintf(command, "%s %s", freshclam, argv[1]);
    setgid(0);
    setuid(0);
    system(command);
    """
    And I notice that the script executes commands sent as arguments
    And I can use the same concatenation approach used previously
    When I set up another listener locally
    And concatenate another command to get a reverse shell
    Then I get a shell for the user "root"
    And the challenge is solved

  Scenario: Remediation
  Sanitize inputs that may contain special characters in the Shell syntax
    Given that the inputs are validated before constructing the command
    When I concantenate the command
    Then I get redirected to the output page

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.9/10 (Critical) - AV:A/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.5/10 (Critical) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    9.5/10 (Critical) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2020-04-13
