## Version 1.4.1
## language: en

Feature:
  TOE:
    Vulnerable Web Application
  Category:
    OS Command Injection
  Location:
    http://localhost:8080/vwa/CommandExecution/CommandExec-1.php
    username (field)
  CWE:
    CWE-78: Improper Neutralization of Special Elements used in an OS Command
    ('OS Command Injection')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Login as Admin user
  Recommendation:
    Replace characters given in a command execution

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows OS      | 10          |
    | Firefox         | 68.0.1      |
  Machine information:
    Given I am accesing the site
    """
    http://localhost:8080/vwa/CommandExecution/CommandExec-1.php
    """
    And A login form
    And The app is running on XAMPP v3.2.4

  Scenario: Normal use case
    Given A common login form
    When A user tries to login with a different user than Admin
    Then The app doesn't gives any response

  Scenario: Static detection
    When I watch the source code
    Then I can see a function inside the php code that validates the username
    """
    if(isset($_GET["username"])){
    echo shell_exec($_GET["username"]);
    """
    And As seen, the input isn't sanitized, so it receives any command
    And It executes the command given

  Scenario: Dynamic detection
    When I try to type this command 'ping www.google.com'
    Then The app shows the ping command response displayed
    And The command is sent via 'GET' method in the URL
    And [evidence](image1.png)

  Scenario: Exploitation
    When I'm trying to find the password to get access using the 'dir' command
    Then I get as a response the complete list of files and directories
    And [evidence](image2.png)
    When I use this command 'dir comex1'
    Then I can see there is a file named 'log1.txt'
    And I use this command to read the file 'more comex1\log1.txt'
    And i get this response 'password:ufoundmypassword'
    When I try to login using as username 'Admin'
    And As password 'ufoundmypassword'
    Then I get this response 'WELLDONE'

  Scenario: Remediation
    When I was looking at the code
    Then I modified the php code and used this
    """
    $target =$_GET["username"];
    $substitutions = array('&&' => '',';'  => '','/' => '','\\' => '' );
    $target = str_replace(array_keys($substitutions),$substitutions,$target);
    echo shell_exec($target);
    """
    And It replaces the characters that can be given in a command to open files
    And Or explore in directories
    When I type 'more comex1\log1.txt' or any exploration command
    And With the "'\'" character or other characters that work in linux
    Then The given function transforms it into an empty character
    And The vulnerability is patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    10/10 (Critical) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9/10 (Critical) - E:H/RL:W/RC:U
  Environmental: Unique and relevant attributes to a specific user environment
    9/10 (Critical) - CR:H/IR:H/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2019-08-08
