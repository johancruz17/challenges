## Version 1.4.1
## language: en

Feature:
  TOE:
    XSS Game
  Category:
    Cross-site scripting
  Location:
    https://xss-game.appspot.com/level2/frame
  CWE:
    CWE-312: https://cwe.mitre.org/data/definitions/312.html
    CWE-316: https://cwe.mitre.org/data/definitions/316.html
    CWE-314: https://cwe.mitre.org/data/definitions/314.html
    CWE-150: https://cwe.mitre.org/data/definitions/150.html
    CWE-79:  https://cwe.mitre.org/data/definitions/79.html
  Rule:
    REQ.177: https://fluidattacks.com/web/rules/177/
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Inject a script to pop up an alert() in the context of the application.
  Recommendation:
    Encode the text of the input to escape the special characters.

  Background:
    Hacker's software:
    | <Software name>     | <Version>       |
    | Windows 10          | 1809            |
    | Google Chrome       | 75.0.3770.100   |
  TOE information:
    Given I am accessing the site https://xss-game.appspot.com
    And [evidence](image2.png)
    And entered /level1/frame
    And [evidence](image1.png)
    And the server is running in Google App Engine

  Scenario: Normal use case
    When I post a new status
    Then the new post is automatically add

  Scenario: Static detection
    When I look at the code in the file index.html I notice that in line 43
    """
    43 var message = document.getElementById('post-content').value;
    """"
    Then I notice that the data in the text area is not filtered
    Then I can conclude there is incorrect data extraction

  Scenario: Dynamic detection
    When I post a status this is stored in the localstorage
    Then I try entering HTML tags are save correctly
    When looka the local storage the data are save with HTML tags
    And [evidence](image4.png)
    Then I can conclude the site is vulnerable to Cross-site scripting stored

  Scenario: Exploitation
    When you entered the following string in the post-form
    """
    <img src="not" onerror="alert('hello')"/>
    """
    Then I see that an alert comes up on the screen [evidence](image3.png)
    Then I can conclude that the site allows to post the post with html tags

  Scenario: Remediation
    To solve this vulnerability, use this function to escape the html characters
    """
    49 + function escape(text) {
    50 +         return text
    51 +             .replace(/&/g, "&amp;")
    52 +             .replace(/</g, "&lt;")
    53 +             .replace(/>/g, "&gt;")
    54 +             .replace(/"/g, "&quot;")
    55 +             .replace(/'/g, "&#039;");
    56 +   }
    """
    When implement the function to save the post
    """
    44 - DB.save(message, function() { displayPosts() } );
    44 + DB.save(escape(message), function() { displayPosts() } );
    """
    Then at the localstorage observe the following:
    """
    &lt;img src=&quot;not&quot; onerror=&quot;
    alert(&#039;hello&#039;)&quot;/&gt; [evidence](image5.png)
    """
    And in the post observe the following as plain text:
    """
    <img src="not" onerror="alert('hello')"/>
    """
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:H/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.3/10 (High) - E:H/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.6/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:N/MI:H/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-07-10
