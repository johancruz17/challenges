## Version 1.4.2
## language: en

Feature:
  TOE:
    DVNA
  Category:
    Code Execution
  Location:
    /app/bulkproducts - XML Body
  CWE:
    CWE-0611: Improper Restriction of XML External Entity Reference
    ('XXE') -variant-
      https://cwe.mitre.org/data/definitions/611.html
    CWE-0610: Externally Controlled Reference to a Resource in Another
    Sphere -class-
      https://cwe.mitre.org/data/definitions/610.html
    CWE-1015: Limit Access
      https://cwe.mitre.org/data/definitions/1015.html
  CAPEC:
    CAPEC-221: XML External Entities Blowup -detailed-
      http://capec.mitre.org/data/definitions/221.html
    CAPEC-278: Web Services Protocol Manipulation -standard-
      http://capec.mitre.org/data/definitions/278.html
    CAPEC-272: Protocol Manipulation -meta-
      http://capec.mitre.org/data/definitions/272.html
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Read files from the server
  Recommendation:
    Disable external entities

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running DVNA in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No access control
    Given I see the code at "dvna/routes/app.js"
    """
    233 module.exports.bulkProducts =  function(req, res) {
    234   if (req.files.products && req.files.products.mimetype=='text/xml'){
    235     var products = libxmljs.parseXmlString(req.files.products.data.toStr
    ing('utf8'), {noent:true,noblanks:true})
    236     products.root().childNodes().forEach( product => {
    237       var newProduct = new db.Product()
    238       newProduct.name = product.childNodes()[0].text()
    239       newProduct.code = product.childNodes()[1].text()
    240       newProduct.tags = product.childNodes()[2].text()
    241       newProduct.description = product.childNodes()[3].text()
    242       newProduct.save()
    243     })
    244     res.redirect('/app/products')
    245   }else{
    246     res.render('app/bulkproducts',{messages:{danger:'Invalid file'},lega
    cy:false})
    247   }
    248 }
    """
    Then I see it allows entities when parsing XML

  Scenario: Dynamic detection
  Checking default routes
    Given I go to "http://localhost:8000/app/bulkproducts"
    Then I upload a file with an inocuous external entity
    And get returned the value I sent as external entity in the list
    Then I know it's parsing external entities

  Scenario: Exploitation
  Exfiltrating /etc/passwd
    Given I send a new XXE payload which reads the server /etc/passwd
    Then I get the contents of /etc/passwd in the results list

  Scenario: Remediation
  Access control
    Given I patch the code like this
    """
    233 module.exports.bulkProducts =  function(req, res) {
    234   if (req.files.products && req.files.products.mimetype=='text/xml'){
    235     var products = libxmljs.parseXmlString(req.files.products.data.toStr
    ing('utf8'), {noent:false,noblanks:true})
    236     products.root().childNodes().forEach( product => {
    237       var newProduct = new db.Product()
    238       newProduct.name = product.childNodes()[0].text()
    239       newProduct.code = product.childNodes()[1].text()
    240       newProduct.tags = product.childNodes()[2].text()
    241       newProduct.description = product.childNodes()[3].text()
    242       newProduct.save()
    243     })
    244     res.redirect('/app/products')
    245   }else{
    246     res.render('app/bulkproducts',{messages:{danger:'Invalid file'},lega
    cy:false})
    247   }
    248 }
    """
    Then I external entities have no effect anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.6/10 (High) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.1/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-24
