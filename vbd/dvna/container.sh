#!/usr/bin/env bash

if [ $# -eq 0 ]
  then
    echo "Usage: container.sh port"
  else
    docker run --rm -d --name dvna -p "$1:9090" -t appsecco/dvna:sqlite
fi
