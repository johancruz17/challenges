## Version 1.4.1
## language: en

Feature:
  TOE:
    diwa
  Location:
    http://172.17.0.2/?page=downloads - approve,delete (fields)
  CWE:
    CWE-284: Improper Access Control
  Rule:
    REQ.097: Define control access model
  Goal:
    Approve or delete an upload
  Recommendation:
    Check user's privileges

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.08.01  |
    | Firefox         | 79.0        |
    | Docker          | 19.03.12-ce |
  TOE information:
    Given I am accessing the site at http://172.17.0.2/
    And it runs on a docker container

  Scenario: Normal use case
    Given a registered user uploads a file
    And that file needs to be accepted by an administrator
    When an administrator goes to
    """
    http://172.17.0.2/?page=downloads&review=1
    """
    Then he can see and approve or deleted uploaded files [evidences](app.png)

  Scenario: Static detection
    When I look at the code at "download.php"
    Then I can see that code does not check for user's privileges
    """
    01  <?php
    02
    03  $loggedIn = isLoggedIn();
    04  $isAdmin = (isset($_SESSION['user']['is_admin']) &&
          1 == $_SESSION['user']['is_admin']);
    05  $reviewMode = (isset($_GET['review']) && '1' == $_GET['review']);
    06
    07  // process review actions (approve / delete)
    08  try {
    09    $error = false;
    10    if(isset($_GET['approve']) && !empty($_GET['approve'])) {
    11      // approve file in DB
    12      $allowGuests = '';
    13      if(isset($_GET['guests']) && '1' === $_GET['guests']) {
    14        $allowGuests = ', allow_guests = 1';
    15      }
    16      ;
    17    if($model->approveDownload($_GET['approve'], (isset($pAllowGuests) &&
             '1' === $pAllowGuests))) {
    18      // redirect
    19      redirect('?page=downloads&review=1&approved=1');
    20    }
    ...
    """
    And I can conclude any user or guest can either validate or delete a file

  Scenario: Dynamic detection
    Given than now I know the system does not perform any type of validation
    When I go as a guest to (15 choosen randomly)
    """
    http://172.17.0.2/?page=downloads&review=1&approve=15
    """
    Then I can see [evidences](guest.png)
    And I can conclude that I can approve any file without being an admin

  Scenario: Exploitation
    Given that any guest can either approve or delete an uploaded file
    And after uploading 3 test files
    When I run "bruteforce.py" to approve first 100 files:
    """
    $ python bruteforce.py approve 100
    [+] file with index 3 has been approved
    [+] file with index 4 has been approved
    [+] file with index 5 has been approved
    """
    Then I can see that now those files are public [evidences](work.png)
    When I run "bruteforce.py" to delete all first 100 files
    """
    $ python bruteforce.py delete 100
    [-] file with index 3 has been deleted
    [-] file with index 4 has been deleted
    [-] file with index 5 has been deleted
    """
    Then I can see all files has been deleted [evidences](deleted.png)
    And I can conclude that I can either approve or delete an upload

  Scenario: Remediation
    Given I have patched the code by mofiying the lines 5, 10, 25
    """
    ...
    05  $reviewMode = (isset($_GET['review']) &&
        '1' == $_GET['review']  && $isAdmin);
    ...
    10  if(isset($_GET['approve']) && !empty($_GET['approve']) &&
        $isAdmin === true) {
    ...
    25  elseif(isset($_GET['delete']) && !empty($_GET['delete']) &&
        $isAdmin === true) {
    ...
    """
    And line 5 makes sure that only an admin can enter in review mode
    And lines 10, 25 makes sure that only an admin can delete or approve files
    When I reset the database and run again "bruteforce.py" to delete all files
    """
    $ python bruteforce.py delete 100
    $
    """
    Then I get nothing
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.4 (Medium) - 3.1/AV:N/AC:L/PR:L/UI:N/S:C/C:L/I:N/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.1 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.9 (Medium) - MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:L/MI:N/MA:L

  Scenario: Correlations
    No correlations have been found to this date {2020-08-05}
