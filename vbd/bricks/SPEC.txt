Name:   Bricks
Tech:   PHP
Site:   https://sechow.com/bricks/index.html
Repo:   https://svn.code.sf.net/p/owaspbricks/code/
Author: OWASP
From:   https://www.owasp.org/index.php/OWASP_Vulnerable_Web_Applications_Directory_Project#tab=Off-Line_apps