## Version 1.4.1
## language: en

Feature:
  TOE:
    OWASP Security Shepherd
  Category:
    SQL Injection
  Location:
    https://localhost/index.jsp - Customer Id (field)
  CWE:
    CWE-0089: SQL Injection
  Rule:
    REQ.173: Discard unsafe inputs
  Goal:
    Exploit the SQL injection flaw that escapes apostrophes and get the info
  Recommendation:
    Use better input sanitization and validation techniques

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali Linux      | 2019.1      |
    | VirtualBox      | 6.0.8       |
    | Firefox         | 60.4.0      |
    | Burp Suite      | 1.7.36      |
  TOE information:
    Given I am accessing the site https://localhost/index.jsp
    And the site's database environment is MySQL
    And the overall website is built with JSP technology
    And I entered one of its pages where there is a field for customer id

  Scenario: Normal use case
    When I access https://localhost/index.jsp
    Then I can see the field where I can input a customer id
    And I enter any name or number in the field
    And if the id doesn't match the page shows me the following message
    """
    There were no results found in your search
    """
    And I can't get any more information

  Scenario: Static detection
    When I inspect the page looking for the source code
    Then I recognize the JavaScript code that handles field input
    """
    51 $("#leForm").submit(function(){
    55 $("#userContent").text($("#aUserId").val());
    56 var theName = $("#aUserId").val();
    58   var ajaxCall = $.ajax({
    59   type: "POST",
    60   url: "8c3c35c30cdbbb73b7be3a4f8587aa9d880
               44dc43e248984a252c6e861f673d4",
    61   data: {
    62     aUserId: theName
    63   },
    """
    And I can see that it doesn't sanitize nor validate the input in any way
    And I also check for the server side code but it isn't accesible

  Scenario: Dynamic detection
    When I access https://localhost/index.jsp
    Then I begin testing the input field with some inputs to check for errors
    And I can see that the single quote has been properly escaped
    And I can also see that the character "\" produces a SQL syntax error
    And I conclude that based on this error there is the possibility for a SQLi

  Scenario: Exploitation
    When I access https://localhost/index.jsp
    Then I test some inputs on the customer id field
    And I know the "\" character produces an error so I base my attack on that
    And after investigation along with many attempts I enter "\' or "1"="1" #"
    And this SQL injection successfully shows me all the database information

  Scenario: Remediation
    When the web site is in development
    Then developers should properly validate all user input
    And it is good practice to validate on both frontend and backend
    And for the frontend an example for validation would be
    """
    if (!input_string.match(/^[0-9a-z]+$/))
        //For example if input isn't alphanumeric
        //Show error
    """
    Then the input should also be validated on the backend
    And for example in php a good way is whitelisting like this
    """
    function checkinput($input,$whitelist) {
        if (in_array($input,$whitelist)){
            return "OK";
        }
        else {
            return "Error";
        }
    }
    """
    And prepared statements for even more prevention against SQLi like this
    """
    $stmt = $conn->prepare("SELECT * FROM table WHERE (?)");
    $stmt->bind_param("s", $id);
    $id = "asd";
    $stmt->execute()
    """
    And this way the field is as safe as it can be from SQLi

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.2/10 (Medium) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    6.2/10 (Medium) - CR:M/IR:L/MAC:L/MUI:N/MC:H

  Scenario: Correlations
    No correlations have been found to this date 2019/06/19
