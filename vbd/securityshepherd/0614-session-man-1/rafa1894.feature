## Version 1.4.1
## language: en

Feature:
  TOE:
    OWASP Security Shepherd
  Category:
    Improper Session Management
  Location:
    https://localhost/index.jsp - checksum (header)
  CWE:
    CWE-0614: Sensitive Cookie in HTTPS Session Without 'Secure' Attribute
  Rule:
    REQ.029: Cookies with security attributes
  Goal:
    Get access to the administrator only section
  Recommendation:
    Use session cookies with proper security attributes and better encryption

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.2 LTS |
    | VirtualBox      | 6.0.8       |
    | Firefox         | 67.0.1      |
    | Burp Suite      | 1.7.36      |
  TOE information:
    Given I am accessing the site https://localhost/index.jsp
    And the site's database environment is MySQL
    And the overall website is built with JSP technology
    And I entered one of its pages where there is an administrator only button

  Scenario: Normal use case
    When I access https://localhost/index.jsp
    Then I can see an administrator only button
    And I click on the button
    And I can't access because I'm not an admin

  Scenario: Static detection
    When I inspect the page looking for the code source
    Then I recognize the Javascript code that creates the checksum cookie
    """
    40 document.cookie="checksum=dXNlclJvbGU9dXNlcg==";
    """
    And I notice that the encryption is simple base64
    Then I conclude it doesn't have proper security attributes
    And that its encryption is not strong enough

  Scenario: Dynamic detection
    When I access https://localhost/index.jsp
    Then I use Burp Suite to intercept the http traffic
    And I check all the header information where I can see the cookie
    Then I use a base64 decoder to decrypt the cookie
    And I successfully get the cookie in plaintext which is "userRole=user"

  Scenario: Exploitation
    When I access https://localhost/index.jsp
    Then I use Burp Suite to intercept the http traffic
    And I use a base64 decoder to decrypt the checksum cookie
    And I change the value from "userRole=user" to "userRole=administrator"
    Then I use a base64 encoder on this new value for the cookie
    And I insert it in the header to send the modified http request
    And I succesfully access the administrators only section

  Scenario: Remediation
    When the website is in its development stage
    Then best practice is to set cookies with HTTPOnly and Secure attributes
    And for example in php this can be set like this:
    """
    $name = 'checksum';
    $value = 'cookie';
    $exp= 0;
    $path = '/';
    $domain = 'localhost';
    $isSecure = true;
    $isHttpOnly = true;
    setcookie($name, $value, $exp, $path, $domain, $isSecure, $isHttpOnly);
    """
    And this is all on the backend but if the cookie is needed in JavaScript
    Then one can use better encryption like AES for it
    And this way the information of the cookie is completely secured

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    4.9/10 (Medium) - CR:L/IR:L/MAC:L/MUI:N/MC:L

  Scenario: Correlations
    No correlations have been found to this date 2019/06/13
