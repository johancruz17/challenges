## Version 1.4.1
## language: en

Feature: mitm-smtp
  TOE:
    bWAPP
  Category:
    MITM attacks
  Location:
    http://192.168.1.119/bWAPP/sm_mitm_2.php - action (field)
  CWE:
    CWE-319: Cleartext Transmission of Sensitive Information
  Rule:
    REQ.181: https://fluidattacks.com/web/rules/181/
  Goal:
    Sniff sensitive information
  Recommendation:
    Use secure protocols while sending sensitive information

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali linux      | 4.19.0      |
    | Ettercap        | 0.8.2       |
    | Wireshark       | 3.0.5       |
    | Nmap            | 4.5.3       |
    | ngrep           | V1.45       |
  TOE information:
    Given That I running bee-box
    When I enter into the web site
    Then The aplication is running on PHP 5.2.4

  Scenario: Normal use case
    Given The site
    When A user enters into it
    Then He can send a mail with his information

  Scenario: Static detection
    Given The source code
    When I see how the email is sent
    Then I can see that the application sends the message using SMTP
    When I analyse the protocol
    Then I see that the protocol is unencrypted
    And I can make a MITM attack to obtain sensitive information

  Scenario: Dynamic detection
    Given The "bee-box" machine
    When I use "ngrep" to see my SMTP traffic
    """
    $ sudo ngrep port 25
    ##
    T 195.130.132.10:25 -> 192.168.1.119:34370 [AS]
    ##
    T 195.130.132.10:25 -> 192.168.1.119:34370 [AP]
    421 albert.telenet-ops.be bizsmtp 190.28.111.82 relaying denied..
    ##
    T 192.168.1.119:34370 -> 195.130.132.10:25 [AP]
    QUIT..
    """
    Then I can see plain text traffic
    And I can make a MITM attack

  Scenario: Exploitation
    Given The site
    When I push the "secret" button
    Then I can intercept the SMTP traffic
    When I run "ettercap"
    Then I can make an ARP poisoning attack in the "bee-box" machine
    When I run the attack [evidence](image1.png)
    Then I run "wireshark" to see the "bee-box" machine traffic
    When I filter by SMTP traffic [evidence](image2.png)
    Then I can see the same traffic that I see inside the machine
    And I can make a MITM attack to the SMTP protocol

  Scenario: Remediation
    Given The machine
    When I update the protocol to SMTPS
    Then All the traffic travels encrypted
    And The vulnerability is patched

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      6.8/10 (Medium) - E:F/RL:T/RC:R/CR:H/IR:L/AR:L
    Environmental: Unique and relevant attributes to a specific user environment
      8.4/10 (High) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-10-09
