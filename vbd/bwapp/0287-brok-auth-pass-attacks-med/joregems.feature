## Version 1.4.1
## language: en

Feature:
  TOE:
    BWAPP
  Category:
    0284
  Location:
    http://localhost/bWAPP/ba_pwd_attacks_2.php - login (field)
    http://localhost/bWAPP/ba_pwd_attacks_2.php - password (field)
  CWE:
    CWE-284: Improper Access Control
  Rule:
    REQ.287 Generate alerts in resources
  Goal:
    Detect and exploit Broken Auth. - Password Attacks
  Recommendation:
    Use an anti-automation protection

  Background:
  Hacker's software:
  Hacker's software:
    | <Software name>    | <Version>         |
    | Ubuntu             | 18.1 x64          |
    | firefox quantum    | 65.0.1 (64-bit)   |
    |curl 7.61.0         |x86_64-pc-linux-gnu|
  TOE information:
    Given I have access to the app
    And the site http://localhost/bWAPP/ba_pwd_attacks_2.php
    And I am trying to access the page through the login form
    When I'm not authorized for that

  Scenario: Normal use case
  Grant access to the form given I know the credentials
    Given I know the login and password
    And I access http://localhost/bWAPP/ba_pwd_attacks_2.php
    When I put the correct login and password and hit the login button
    Then appears in green text:
    """
    Successful login!
    """

  Scenario: Alternative use case
  Not grant access given I don't know the login and the password
    Given I don't know the credentials
    Given I access http://localhost/bWAPP/ba_pwd_attacks_2.php
    And put any credentials aside the correct ones and hit the login button
    Then appears in red text:
    """
    Invalid credentials! Did you forgot your password?
    """

  Scenario: Static detection
  I check the php code seeking for clues that allow me a password attack
    Given I have privileged access to the code
    When I'm reading the code I notice, there are not alerts
    And there is not any anti-automation protection
    Then I can conclude there is not a generator alerts in resources

  Scenario: Dynamic detection
  I check the html code seeking for clues that allow me make an attack
    Given I logged in bWAPP application
    When I put any random text and hit the login button
    Then appears in red text
    """
    Invalid credentials! Did you forgot your password?
    """
    And I inspect the form and see a salt field
    Then I check the salt value
    """
    65        <input type="hidden" id="salt" name="salt" value="tgF1d@" />
    """
    And I put any random text and hit the login button
    Then appears in red text
    """
    Invalid credentials! Did you forgot your password?
    """
    And I check the salt value
    """
    65        <input type="hidden" id="salt" name="salt" value="ka9iX-" />
    """
    Then I can conclude the salt change each time I reload the page
    And I open the element inspector and select network console
    Then check the last post request
    And I copy it as curl giving right click on the request
    Then I paste it in terminal
    And get an html page
    When I open it as html I see the message
    """
    Incorrect salt!
    """
    Then I conclude, there is no way to make an straight force-brute attack
    And I reload the page and copy the get request as curl
    Then I copy it to a terminal and save it as a file
    And open it with a browser and check for the salt value
    Then I replace the salt value in my last curl post request
    And got after save it as a file
    And open it as a html file in the browser
    """
    Invalid credentials! Did you forgot your password?
    """
    Then I can conclude I can automatize this and make an attack from terminal

  Scenario: Dynamic exploitation
    Given I make an script based on bash and curl
    Then I make a dictionary for the attack (only to check vulnerability)
    """
    echo -e "kjk\nfasn\ndasdasd\ndasd\nbug\ndasjhdk\ndasd\ndasd\nbee">users_dic
    echo -e "khjk\nflasn\ndaasd\ndasd\nbug\ndasjhdk\ndasd\ndasd\nbee">pass_dic
    """
    And use my script
    """
    bash joregems.sh users_dic pass_dic da06nsdr0fuqk2hke6rkpojrlc
    """
    And the last argument is the cookie I copy it from the element inspector
    And from the storage tab the PHPSESSID variable

  Scenario: Remediation
    Given we the software does not generate the proper alerts
    And not have any automation software
    And I replace the lines 140 and 158 with
    """
        if($_SESSION["try"] <= 3)
        {
            if($_POST["login"] == $login && $_POST["password"] == $password)
            {
                $message = "<font color=\"green\">Successful login!</font>";
                $_SESSION["try"] = 0;
            }
            else
            {
                $message = "<font color=\"red\">Invalid credentials!";
                $message .= " Did you forgot your password?</font>";
            }

        }
        if($_SESSION["try"] > 3)
        {
            if (isset($_SESSION["captcha"])
            && ($_POST["captcha_user"] == $_SESSION["captcha"]))
            {
                if($_POST["login"] == $login && $_POST["password"] == $password)
                {
                    $message = "<font color=\"green\">Successful login!</font>";
                    $_SESSION["try"] = 0;
                }
                else
                {
                    $message = "<font color=\"red\">Invalid credentials!";
                    $message .= "  Did you forgot your password?</font>";
                }
            }
            else
            {
                $message = "<font color=\"red\">Incorrect CAPTCHA!</font>";
            }
        }
    }
    else
    {
    """
    And adding to the 168 line with
    """
    $_SESSION["try"] = $_SESSION["try"] +1;
    """
    And adding to the line 235
    """
        <?php if($_SESSION["try"] > 3): ?>
        <font color="red">so many attempts please resolve the chapta</font>
        <p><iframe src="captcha_box.php" scrolling="no" frameborder="0"
        height="70" width="350"></iframe></p>
        <p><label for="captcha_user">Re-enter CAPTCHA:</label><br />
        <input type="text" id="captcha_user" name="captcha_user" value=""
        autocomplete="off" /></p>
        <?php endif; ?>
    """
    When there is more than three tries failed
    Then there is a captcha when the resource is exceeded
    Then you need resolve first the captcha
    And this make you can't brute force with the same method
    Then I can conclude I fix the problem

  Scenario: Scoring
    Base: Attributes that are constants over time and organizations
    7.7/10 AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
    7.2/10 E:F/RL:T/RC:C
    Environmental: Unique and relevant attributes to a specific user environment
    7.0/10 CR:M/IR:H/AR:M/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    Given No correlations have been found to this date 2019-05-11
