Name:   WebGoat
Tech:   Java
Site:   https://github.com/WebGoat/WebGoat/wiki
Repo:   https://github.com/WebGoat/WebGoat
Author: OWASP
From:   https://www.owasp.org/index.php/OWASP_Vulnerable_Web_Applications_Directory_Project#tab=Off-Line_apps
