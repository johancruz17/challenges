#!/usr/bin/env bash

if [ $# -eq 0 ]
  then
    echo "Usage: container.sh port"
  else
    docker run --rm -d --name webgoat -p "$1:8080" -t webgoat/webgoat-8.0
fi
