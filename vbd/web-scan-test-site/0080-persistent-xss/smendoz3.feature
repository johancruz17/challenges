## Version 1.4.1
## language: en

Feature:
  TOE:
    Web Scanner Test Site
  Category:
    Cross Site Scripting
  Location:
    http://webscantest.com/crosstraining/dom.php - name (field)
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting')
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Inject a persistent script
  Recommendation:
    Escape the special characters

  Background:
  Hacker's software:
    | Name          | Version       |
    | Ubuntu        | 16.04         |
    | Firefox       | 72.0.02       |
    | nmap          | 7.01          |
  TOE information:
    Given I am accesing the webpage
    """
    http://webscantest.com/crosstraining/dom.php
    """
    When I get site IP address using command "nslookup webscantest.com"
    """
    Server:     127.0.1.1
    Address:    127.0.1.1#53

    Non-authoritative answer:
    Name:    webscantest.com
    Address: 69.164.223.208
    """
    Then I can see the IP Address information
    When I run "nmap -sC -sV -oA nmap 69.164.223.208"
    Then I can see the webpage information
    And is runing on Ubuntu 14.04.2 LTS
    And is running Apache/2.4.7 (Ubuntu)
    And is powered by PHP/5.5.9

  Scenario: Normal use case
  Page with a field of firstname to submit requests
    When I access the webpage and submit a name
    Then A message saying "Hello (name)!" and a link with a request appear

  Scenario: Static detection
    Given I can't access to the source code
    Then I can't perform a static detection

  Scenario: Dynamic detection
    When I write "<script>" in the parameter "First name"
    Then I see that the page only "Hello" and omit the rest of the page
    And I can conclude that the callback is not safe

  Scenario: Exploitation
    When I make the next request with Javascript code inside the parameter
    """
    <script>alert('XSS Injection')</script>
    """
    Then I see the alert of "XSS Injextion" I wrote
    And I conclude that the site allows to insert other JavaScript code
    And i conclude that I can execute that script

  Scenario: Remediation
    When I add the "htmlspecialchars" php function:
    """
    <?php
      echo htmlspecialchars($_GET["name"]);
    ?>
    """
    Then The page escape special characters as "<>"
    And I the problem should be solved

  Scenario: Scoring
  Severity scoring according to CVSSv3.1 standard
  Base: Attributes that are constants over time and organizations
    7.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.1/10 (High) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.1/10 (High) - CR:/IR:M/AR:M/

  Scenario: Correlations
    No correlations have been found to this date 2020-05-08
