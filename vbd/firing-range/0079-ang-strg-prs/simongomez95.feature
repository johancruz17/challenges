## Version 1.4.1
## language: en

Feature:
  TOE:
    Firing Range
  Category:
    Injection Flaws
  Location:
    firing-Range/angular/angular_form_parse/1.6.0 - Form
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting')
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Execute arbitrary JS
  Recommendation:
    Sanitize user input, don't use $parse

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am accessing Firing Range at
    """
    https://public-firing-range.appspot.com
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to https://firing-range/angular/angular_storage_parse/1.6.0
    Then I see a blank page

  Scenario: Static detection
  No input sanitization
    When I look at the page source
    """
    01  <!DOCTYPE html>
    02  <title>Angular localStorage Parse</title>
    03  <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular.js"
    ></script>
    04  <script>
    05    var irrelevantKey = 'irrelevant-key';
    06    if (!localStorage[irrelevantKey]) {
    07      localStorage[irrelevantKey] = '42';
    08    }
    09    angular.module('test', [])
    10        .controller('VulnerableController', ['$parse', function($parse) {
    11          $parse(localStorage.getItem(irrelevantKey))({});
    12        }]);
    13  </script>
    14  <div ng-app="test" ng-controller="VulnerableController"></div>
    """
    Then I see it's parsing the value in "localStorage.irrelevant-key"

  Scenario: Dynamic detection
  Angular fuzzing
    Given I save an Angular XSS Payload in localStorage using the ff console
    """
    localStorage.setItem('irrelevant-key', "constructor.constructor('alert(1)')(
    )")
    """
    Then I get an alert
    Then the page is vulnerable to Angular code injection

  Scenario: Exploitation
  There is no way to meaningfully exploit vulnerabilities in this ToE

  Scenario: Remediation
  Sanitize input
    Given I don't use $parse ever
    Then code can't be injected anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    3.7/10 (Low) - AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.5/10 (Low) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.5/10 (Low) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-18
