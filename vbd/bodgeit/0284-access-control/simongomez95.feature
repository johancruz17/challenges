## Version 1.4.1
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Broken Access Control
  Location:
    bodgeit/admin.jsp - Access
  CWE:
    CWE-0346: Origin Validation Error -base-
      https://cwe.mitre.org/data/definitions/346.html
    CWE-0284: Improper Access Control -class-
      https://cwe.mitre.org/data/definitions/284.html
    CWE-0264: Permissions, Privileges, and Access Controls -category-
      https://cwe.mitre.org/data/definitions/264.html
  CAPEC:
    CAPEC-077: Manipulating User-Controlled Variables -standard-
      http://capec.mitre.org/data/definitions/77.html
    CAPEC-022: Exploiting Trust in Client -meta-
      http://capec.mitre.org/data/definitions/22.html
  Rule:
    REQ.096 Definir privilegios requeridos de usuario:
  Goal:
    Access a page I'm not supposed to be able to see
  Recommendation:
    Always check if the user trying to access a resource is privileged enough

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/
    Then I can login and buy products as my own user

  Scenario: Static detection
  No user validation in admin section
    When I look at the code at "/bodgeit/root/admin.jsp"
    """
    ...
    08  PreparedStatement stmt = null;
    09  ResultSet rs = null;
    10  try {
    11    if (session == null || ! "admin@thebodgeitstore.com".equals(session.ge
    tAttribute("username"))) {
    12      conn.createStatement().execute("UPDATE Score SET status = 1 WHERE ta
    sk = 'HIDDEN_ADMIN'");
    13    }
    14
    15    stmt = conn.prepareStatement("SELECT * FROM Users");
    16    rs = stmt.executeQuery();
    17    out.println("<br/><center><table class=\"border\" width=\"80%\">");
    18    out.println("<tr><th>UserId</th><th>User</th><th>Role</th><th>BasketId
    </th></tr>");
    19    while (rs.next()) {
    20      out.println("<tr>");
    21      out.println("<td>" + rs.getInt("userid") + "</td><td>" + rs.getStrin
    g("name") +
    22          "</td><td>" + rs.getString("type") + "</td><td>" + rs.getInt("cu
    rrentbasketid") + "</td>");
    23      out.println("</tr>");
    24    }
    ...
    """
    Then I see it doesn't check for admin permisisons to render the admin page

  Scenario: Dynamic detection
  Crawl the site
    Given I use the Burp spider to crawl the site
    Then I find an interesting "admin.jsp" (evidence)[spider.png]

  Scenario: Exploitation
  Accessing sensible unprotected resources
    Given I know there's a "/bodgeit/admin.jsp"
    Then I go there with my browser
    Then I find a page with sensible info such as user list, basket contents

  Scenario: Remediation
  Only rendering admin page if user is admin
    Given I patch the code like this
    """
    ...
    08  PreparedStatement stmt = null;
    09  ResultSet rs = null;
    19  if ("ADMIN".equals(session.getAttribute("usertype"))) {
    10    try {
    11      if (session == null || ! "admin@thebodgeitstore.com".equals(session.
    getAttribute("username"))) {
    12        conn.createStatement().execute("UPDATE Score SET status = 1 WHERE
    task = 'HIDDEN_ADMIN'");
    13      }
    14
    15      stmt = conn.prepareStatement("SELECT * FROM Users");
    16      rs = stmt.executeQuery();
    17      out.println("<br/><center><table class=\"border\" width=\"80%\">");
    18      out.println("<tr><th>UserId</th><th>User</th><th>Role</th><th>Basket
    Id</th></tr>");
    19      while (rs.next()) {
    20        out.println("<tr>");
    21        out.println("<td>" + rs.getInt("userid") + "</td><td>" + rs.getStr
    ing("name") +
    22            "</td><td>" + rs.getString("type") + "</td><td>" + rs.getInt("
    currentbasketid") + "</td>");
    23        out.println("</tr>");
    24      }
    ...
    """
    Then the page only renders if the user attempting to access has ADMIN role

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.0/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.0/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-02-14
