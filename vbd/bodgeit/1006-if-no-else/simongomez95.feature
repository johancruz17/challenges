## Version 1.4.2
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Bad Coding Practices
  Location:
    bodgeit/password.jsp - code
  CWE:
    CWE-710: Improper Adherence to Coding Standards -class-
      https://cwe.mitre.org/data/definitions/710.html
    CWE-1006: Bad Coding Practices -category-
      https://cwe.mitre.org/data/definitions/1006.html
  CAPEC:
    No CAPEC attack patterns related to this vulnerability
  Rule:
    REQ.161: https://fluidattacks.com/web/en/rules/161/
  Goal:
    Better code quality
  Recommendation:
    Write an else for every if

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Wireshark             | 2.6.5     |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/search.jsp
    Then I can search for products

  Scenario: Static detection
  No precedent password validation
    Given I see the code at "bodgeit/root/password.jsp"
    """
    ...
    28  try {
    29      stmt.executeQuery("UPDATE Users set password= '" + password1 + "'
    where name = '" + username + "'");
    30
    31      okresult = "Your password has been changed";
    32
    33      if (request.getMethod().equals("GET")) {
    34          conn.createStatement().execute("UPDATE Score SET status = 1
    WHERE
    task = 'PASSWD_GET'");
    35      }
    36
    37  } catch (Exception e) {
    38      failresult = "System error.";
    39  } finally {
    40      stmt.close();
    41  }
    ...
    """
    Then I see it doesn't have an else for the if in line 33

  Scenario: Dynamic detection
  No dynamic detection

  Scenario: Exploitation
  No exploitation

  Scenario: Remediation
  Validate past passwords
    Given I write an else for the if
    """
    ...
    28  try {
    29      stmt.executeQuery("UPDATE Users set password= '" + password1 + "'
    where name = '" + username + "'");
    30
    31      okresult = "Your password has been changed";
    32
    33      if (request.getMethod().equals("GET")) {
    34          conn.createStatement().execute("UPDATE Score SET status = 1
    WHERE
    task = 'PASSWD_GET'");
    35      } else {Logger.log("No get method")}
    36
    37  } catch (Exception e) {
    38      failresult = "System error.";
    39  } finally {
    40      stmt.close();
    41  }
    ...
    """
    Then the code is rule-compliant

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    0.0/10 (None) - AV:P/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    0.0/10 (None) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    0.0/10 (None) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-22
