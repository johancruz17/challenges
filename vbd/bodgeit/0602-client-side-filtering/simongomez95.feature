## Version 1.4.1
## language: en

Feature:
  TOE:
    BodgeIT
  Category:
    Porous Defenses
  Location:
    bodgeit/advanced.jsp - query (parameter)
  CWE:
    CWE-0602: Client-Side Enforcement of Server-Side Security -base-
      https://cwe.mitre.org/data/definitions/602.html
    CWE-0693: Protection Mechanism Failure -class-
      https://cwe.mitre.org/data/definitions/693.html
    CWE-0254: 7PK - Security Features -category-
      https://cwe.mitre.org/data/definitions/254.html
  CAPEC:
    CAPEC-200: Removal of filters: Input filters, output filters, data
    masking -detailed-
      http://capec.mitre.org/data/definitions/200.html
    CAPEC-207: Removing Important Client Functionality -standard-
      http://capec.mitre.org/data/definitions/207.html
    CAPEC-022: Exploiting Trust in Client -meta-
      http://capec.mitre.org/data/definitions/22.html
  Rule:
    REQ.173: https://fluidattacks.com/web/en/rules/173/
  Goal:
    Send invalid input to the server
  Recommendation:
    Validate input server-side as well as client-side

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running BodgeIT in a docker container at
    """
    http://localhost:8000/bodgeit/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/bodgeit/password.jsp
    Then I can change my password

  Scenario: Static detection
  No input validation for unusual XSS payloads
    When I look at the code at "/thebodgeitstore/search/AdvancedSearch.java"
    """
    ...
    99  public String getQueryString(){
    100     String queryString = "";
    101     for (Map.Entry<String, String> entry : parameters.entrySet()){
    102         if(!"key".equals(entry.getKey())){
    103             queryString = queryString.concat(" ").concat(entry.getKey())
    .concat(":").concat(entry.getValue());
    104         }
    105
    106     }
    107     return queryString;
    108 }
    ...
    """
    Then I see it's not validating inputs

  Scenario: Dynamic detection
  Viewing page source
    Given I view the page source
    And find a JS snippet that sanitizes input against special characters
    """
    <SCRIPT>
        loadfile('./js/encryption.js');

        var key = "f418629d-bd7c-43";

        function validateForm(form){
            var query = document.getElementById('query');
            var q = document.getElementById('q');
            var val = encryptForm(key, form);
            if(val){
                q.value = val;
                query.submit();
            }
            return false;
        }

        function encryptForm(key, form){
            var params = form_to_params(form).replace(/</g, '&lt;').replace(/>/g
            , '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#39');
            if(params.length > 0)
                return Aes.Ctr.encrypt(params, key, 128);
            return false;
        }
    </SCRIPT>
    """
    Then I use the browser console to replace the encryptForm function
    And make it not sanitize the query before encrypting it
    """
    encryptForm = function(key, form){
            var params = form_to_params(form);
            if(params.length > 0)
                return Aes.Ctr.encrypt(params, key, 128);
            return false;
    """
    Then I send a XSS payload I know would be crippled by sanitization
    """
    <script>alert()</script>
    """
    And get an alert in the results screen

  Scenario: Exploitation
  Sending unvalidated input to the server
    Given I know inputs are only being validated in the client
    And I can modify the client code so they aren't anymore
    Then I can attempt a variety of attacks against the server
    And most probably be successful

  Scenario: Remediation
  Using prepared statements
    Given I patch the code as follows
    """
    import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;
    ...
    99  public String getQueryString(){
    100     String queryString = "";
    101     for (Map.Entry<String, String> entry : parameters.entrySet()){
    102         if(!"key".equals(entry.getKey())){
    103             queryString = queryString.concat(" ").concat(entry.getKey())
    .concat(":").concat(escapeHtml(entry.getValue()));
    104         }
    105
    106     }
    107     return queryString;
    108 }
    ...
    """
    Then inputs are also validated in the server

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    3.7/10 (Low) - AV:N/AC:H/PR:N/UI:N/S:U/C:N/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    3.5/10 (Low) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    3.5/10 (Low) - CR:M/IR:M/AR:M

  Scenario: Correlations
    0089-conq-aes-sqli
      Given I can send whatever input I want as query
      And the backend code inserts raw input into SQL queries
      Then I can inject SQL code into a search
