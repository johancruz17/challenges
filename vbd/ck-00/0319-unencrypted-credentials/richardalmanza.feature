## Version 1.4.1
## language: en

Feature: login admin in wordpress
  TOE:
    CK-00
  Category:
    Missing Encryption of Sensitive Data
  Location:
    http://ck/wp-login.php/
  CWE:
    CWE-319: Cleartext Transmission of Sensitive Information
  Rule:
    REQ.026: https://fluidattacks.com/web/rules/026/
  Goal:
    Privilege escalation to collect all the flags
  Recommendation:
    Use HTTPS instead of HTTP to encrypt user credentials

  Background:
  Hacker's software:
    | <Software name> | <Version>         |
    | Ubuntu (Bionic) | 18.04.4 LTS       |
    | Mitmweb         | 5.0.1             |
    | GNU bash        | 4.4.20(1)-release |
    | Wireshark       | 2.6.10            |
  User's software:
    | <Software name> | <Version>         |
    | Ubuntu (Bionic) | 18.04.2 LTS       |
    | Google Chrome   | 74.0.3729.169     |
  TOE information:
    Given running wordpress server

  Scenario: Normal use case
    Given the wordpress [page](wp2.png) in http://ck/
    Then I saw it uses http

  Scenario: Static detection
    Given there isn't any code
    Then there isn't a static detection either

  Scenario: Dynamic detection
    Given the information I got from checking the requests to wp
    Then I confirmed that it uses http, an unsecure protocol

  Scenario: Exploitation
    Given another pc I use to simulate an user
    Then from the hacker pc I used the next command to start capturing data
    """
    $ tshark -i enp3s0 -f 'host ck and port 80' -w Documents/vbd-hacking/wireshark-capture/ck-wp.pcap
    """
    And after it started sniffing the network I login from the user pc to wp
    When I finished, I stopped the tshark process
    And I Opened the ck-wp.pcap with wireshark
    Then I looked up for the credentials
    And I found [this](ws.png)

  Scenario: Remediation
  Use https to encrypt connection
    Given encrypted connection
    Then all sensitive information were encrypted with tls protocol
    And I couldn't find the login data

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.3/10 (Medium) - AV:A/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:L/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6/10 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.1/10 (Medium) - CR:L/IR:L/AR:L/

  Scenario: Correlations
    No correlations have been found to this date 2020-04-03
