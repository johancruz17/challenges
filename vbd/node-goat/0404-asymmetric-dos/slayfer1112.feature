## Version 1.4.1
## language: en

Feature:Asymmetric DoS
  TOE:
    node-goat
  Category:
    Asymmetric DoS
  Location:
    http://localhost:4000/profile
  CWE:
    CWE-404: https://cwe.mitre.org/data/definitions/404.html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Make an Asymmetric DoS attack.
  Recommendation:
    Validate and sanitize input in frontend and backend.

  Background:
  Hacker's software:
    | <Software name> |    <Version>    |
    | Ubuntu          |    18.04.04     |
    | Firefox         |     73.0.1      |
  TOE information:
    Given I am accessing the site http://localhost:4000
    And the server is running MongoDB version 2.1.18
    And Nodejs version 10.15.3

  Scenario: Normal use case
    Given I access to "http://localhost:4000/"
    And login in with a new user
    When I go to profile tab
    And I can update my personal information

  Scenario: Static detection
    Given The source code
    When I see in te route "/views" all html pages
    Then I found that all pages extends the "/views/layout.html"
    And I found that the layout bring the Name and Last Name line 74 to 75
    """
    74|   <li class="dropdown user-dropdown">
    75|     <a href="#" class="dropdown-toggle" data-toggle="dropdown">
    75|     <i class="fa fa-user"></i> {{firstName}} {{lastName}}
    75|     <b class="caret"></b></a>
    """
    When I see in "/views/benefits.html" that is the admin page
    Then I found that the admin page loads thename and last name of each user
    """
    47|   {% for user in users %}
    48|     <tr>
    49|       <td>{{user._id.toString()}}</td>
    50|       <td>{{user.firstName}}</td>
    51|       <td>{{user.lastName}}</td>
    """
    And I try to search the way to use that
    When I see in "/routes/profile.js"
    Then I found that firstName and lastName aren't sanitized line 40 to 48
    """
    40|   this.handleProfileUpdate = function (req, res, next) {
    41|
    42|     var firstName = req.body.firstName;
    43|     var lastName = req.body.lastName;
    44|     var ssn = req.body.ssn;
    45|     var dob = req.body.dob;
    46|     var address = req.body.address;
    47|     var bankAcc = req.body.bankAcc;
    48|     var bankRouting = req.body.bankRouting;
    """
    And I found another part in line 98 to 106
    """
     98|  profile.updateUser(
     99|    parseInt(userId),
    100|    firstName,
    101|    lastName,
    102|    ssn,
    103|    dob,
    104|    address,
    105|    bankAcc,
    106|    bankRouting,
    """
    When I see that i can inject the name and last name fields
    Then I know that the admin main page is "/views/benefits.html"
    And I can attack the admin session with this fields
    And I found a vulnerability

  Scenario: Dynamic detection
    Given I am in the profile tab
    When I try to use HTML on the input field
    Then I use a script that send an alert
    """
    <script>alert("THIS FIELD IS VULNERABLE")</script>
    """
    And I try the same thing in the lastName input field
    When I submit the changes i can see the alert [evidence](image1.png)

  Scenario: Exploitation
    Given The application doesn't sanitize the firstName and lastName input
    Given The script onlyexecute in tabs where we can see tha firstName or lastName
    Given I know that the admin account have a table with the users and his benefits
    And Knowing that we can attack the admin account
    When I submit a script in firstName or lastName
    """
    <script>document.body.innerHTML = ""</script>
    """
    Then I found that all tabs that show the firstName or lastName are empty/blank
    And I found that the admin dashboard are empty/blank
    And The vulnerability was exploit

  Scenario: Remediation
    Given I try to use 3 defenses between front and back
    Given The source code "/views/layout.html"
    Given The source code "/routes/profile.js"
    When I try to remediate the vulnerability i think in 3 levels of defense
    Then I use a dynamic validation in the field with a script
    And I save the script in the route "/js/script/validfield.js" line 1 to 17
    """
     1|   document.getElementById("firstName").oninput =
     1|   function() {validField(this)};
     2|   document.getElementById("lastName").oninput =
     2|   function() {validField(this)};
     3|
     4|   function validField(field) {
     5|     var regex = /((?![\s])([\W]))/;
     6|     try {
     7|       if (regex.test(field.value) == true){
     8|         field.value = field.value.slice(0,-1);
     9|         return false;
    10|       }
    11|       else {
    12|         return true;
    13|       }
    14|     } catch (e) {
    15|       return false;
    16|     }
    17|   }
    """
    Then I add the script in "/views/layout.html" line 129 to 130
    """
    129|    <!-- Valid inputs of name and last name -->
    130|    <script src="/js/script/validfield.js"></script>
    """
    And I have a dynamic validation to the field
    When I add a validation on the submit for the form
    Then I edit the code in "/routes/profile.js"
    """
    81|   var regex = /((?![\s])([\W]))/;
    82|   if (regex.test(firstName)==true || regex.test(lastName)==true) {
    83|     const firstNameSafeString = firstName
    84|     return res.render("profile", {
    85|       updateError: "Name or Last name does not comply with
    85|                     requirements for format specified",
    86|       firstNameSafeString,
    87|       lastName,
    88|       ssn,
    89|       dob,
    90|       address,
    91|       bankAcc,
    92|       bankRouting
    93|     });
    94|   }
    """
    And I have a submit validation in the form
    When I add a sanitization for the data update
    Then I edit the code in "/routes/profile.js" line 4 to 10
    """
     4|   function sanitize(s) {
     5|     var regex = /((?![\s])([\W]))/;
     6|     if (regex.test(s)==true) {
     7|       s="";
     8|     }
     9|     return s.trim()
    10|   }
    """
    And I use the function on the fields to update line 113 to 114
    """
    111|  profile.updateUser(
    112|    parseInt(userId),
    113|    sanitize(firstName),
    114|    sanitize(lastName),
    115|    ssn,
    116|    dob,
    117|    address,
    118|    bankAcc,
    119|    bankRouting,
    """
    When I add all 3 defense levels i try to attack with this field
    Then I can't make a DoS or use any script [evidence](image2.png)
    And I fix the vulnerability

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
      4.3/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:L
    Temporal: Attributes that measure the exploit's popularity and fixability
      4.2/10 (Medium) - E:H/RL:W/RC:C/CR:L/IR:L/AR:M
    Environmental: Unique and relevant attributes to a specific user environment
      4.2/10 (Medium) - MAV:N/MAC:L/MPR:L/MUI:N/MS:U/MC:N/MI:N/MA:L


  Scenario: Correlations
    No correlations have been found to this date 2020-03-10
