## Version 2.0
## language: en

Feature: Log injection
  TOE:
    Node-goat
  Category:
    Injection
  Location:
    http://localhost:4000/login
  CWE:
    CWE-93: Improper Neutralization of CRLF Sequences
      https://cwe.mitre.org/data/definitions/93.html
  Rule:
    REQ.080 Prevent log modification
      https://fluidattacks.com/web/rules/080/
  Goal:
    Inject customized logs
  Recommendation:
    Encode the inputs to proper context

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
  TOE information:
    Given I am accessing the site http://localhost:4000
    And the server is running MongoDB version 2.1.18
    And Nodejs version 10.15.3
    And Express version 4.13.4

  Scenario: Normal use case
    Given I am on the login page
    And I can see a form with username parameter and password parameter
    When I do not fill the parameter
    And I press the "Submit" button
    Then Appear the message
    """
    Invalid username
    """

  Scenario: Static detection
    Given I access to the source code
    When I check the source code
    Then I can see the related code
    """
    var userName = req.body.userName;
    console.log('Error: attempt to login with invalid user: ', userName);
    """
    When I analyze the code
    Then I realize the inputs are unsanitized
    And There is an ordinary output mechanism that allows CRLF injection

  Scenario: Dynamic detection
    Given I am on the main page
    And I can see logs of the website
    When I attempt to set the "username" parameter as
    """
    newuser2
    """
    Then Appears the log
    """
    Error: attempt to login with invalid user:  newuser2
    """
    When I use the console to make an HTTP request as
    """
    $ curl http://localhost:4000/login -X POST --data 'userName=newuser%0a&password=&_csrf='
    """
    Then Appears a new blackline into the logs
    And The logs are vulnerable against CRLF injection

  Scenario: Exploitation
    Given I can access to the login page
    And The website is vulnerable to CRLF injection
    When I inject the "username" parameter as
    """
    userName=newuser%0aError: new error&password=&_csrf=
    """
    Then I can see the injected log inside the logs
    """
    Error: attempt to login with invalid user:  newuser
    Error: new error
    """

  Scenario: Remediation
    Given The web site is vulnerable against CRLF injection
    And The code does not encode the inputs to proper context
    When The website needs to log an error
    Then The code must be replaced with
    """
    var ESAPI = require('node-esapi');
    console.log('Error: attempt to login with invalid user: %s',
    ESAPI.encoder().encodeForJavaScript(userName));
    """
    And The query could be shielded against CRLF injection

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      5.3/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      5.1/10 (Medium) - E:H/RL:O/RC:C
    Environmental: Unique and relevant attributes to a specific user environment
      4.4/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:N/MI:L/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-03-02
