## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuart - User input Code Execution
  Location:
    http://testphp.vulnweb.com - ID field
  Category:
    Web
  CWE:
    CWE-94: Improper Control of Generation of Code ('Code Injection')
      https://cwe.mitre.org/data/definitions/94.html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
    REQ.105: https://fluidattacks.com/web/rules/105/
  Goal:
    Inject and execute arbitrary code
  Recommendation:
    Do not perform code evaluation on user input.
    Sanitize user input.

  Background:
  Hacker's software:
    | <Software name>   |  <Version> |
    | Microsoft Windows |     10     |
    | Google Chrome     |  76.0.3809 |
  TOE information:
    Given I'm accessing the site through http://testphp.vulnweb.com
    When I'm signed up and logged in,
    Then I will start analysis.

  Scenario: Normal use case
    Given I got to the login page,
    And I access with default credentials,
    When I get redirected to a products page
    Then I can leave comments over them.

  Scenario: Static detection
    Given source code is not downloadable.
    But with another exploit we can download it,
    When we review the source
    Then we find
    """
    62: if ($_POST["phpaction"]==
    63: "printf(md5(acunetix_wvs_security_test));exit;//"){
    64:  eval($_POST["phpaction"]);
    65: }
    """
    And it is clear the script evaluates user input.

  Scenario: Dynamic detection
    Given the comment section has a form
    When inspecting the client-side source
    Then a hidden field appears
    And its name is "phpaction"
    And its value is valid PHP code [evidence](img1.png)
    Then it might be clear that valid PHP code could be inserted.

  Scenario: Exploitation
    Given we now know about the code injection vulnerability,
    When we forge a HTTP request with a body of, e.g.,
    """
    name=name&comment=comment&
    phpaction=printf%28md5%28acunetix_wvs_security_test%29%29%3Bexit%3B%2F%2F
    """
    And being the method of this request POST
    And its Content-Type application/x-www-form-urlencoded
    Then we can notice the presence of the hash [evidence](img2.png)
    And this hash is
    """
    63c19a6da79816b21429e5bb262daed8
    """
    And it is a MD5 of "acunetix_wvs_security_test"
    But if this constant is not defined,
    Then it is a hash of the name of this identifier as a literal string
    Then this means the input got evaluated.
    Given evaluation of arbitrary code might be the source of data leaks
    And of DoS
    When we inject code in conjunction with other exploits already in this repo
    Then we could take possession of the server and all its resources.

  Scenario: Remediation
    Given the string to evaluate comes from user input,
    When we get that input
    Then we could sanitize it.
    And avoid using code-evaluation functions.
    Then we could replace the lines 62-65 for:
    """
    62: if ($_POST["phpaction"]==
    63: "fa96dc7f56ff6bed54aa3c6b5daec0ef"){
    64:     if(isset(acunetix_wvs_security_test)) {
    65:         printf(md5(acunetix_wvs_security_test));
    66:     } else {
    67:         printf(md5("acunetix_wvs_security_test"));
    68:     }
    69:     exit;
    70: }
    """
    And here, we receive a hash instead of PHP code.
    Then the front end is not full of technical details,
    And no user input is directiy evaluated.

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.5/10 (High) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:L/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.9/10 (High) - E:F/RL:O/RC:R
  Environmental: Unique and relevant attributes to a specific user environment
    6.0/10 (Medium) - CR:M/IR:L/AR:H/MAV:N/MAC:L/MPR:N/MUI:N/MS:C/MC:L/MI:L

  Scenario: Correlations
    Given 0538-information-exposure determines the database structure
    When we inject PHP code which also injects SQL code,
    Then we could fetch any rows we want from the database
