## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuart
  Location:
    http://testphp.vulnweb.com - cookie (mycookie:3)
  CWE:
    CWE-0089: Improper Neutralization of Special Elements used in an SQL Command
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Detect and exploit vuln Insecure SQL query input

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 6.0.3 (64 bit) |
  TOE information:
    Given I am accessing the site testphp.vulnweb.com
    And Entered a php site which uses SQL requests

  Scenario: Normal use case
    Given I access http://testphp.vulnweb.com/listproducts.php?cat=1
    And I click on "posters"
    Then I can see this http://testphp.vulnweb.com/listproducts.php?cat=1

  Scenario: Dynamic detection:

    Given I access http://testphp.vulnweb.com/listproducts.php?cat=1
    Then I can write a "'" at the end of the url
    """
    http://testphp.vulnweb.com/listproducts.php?cat=1'
    """
    And I get an error message
    Then I can conclude that the site is vulnerable to a SQL injection

  Scenario: Exploitation:
    Given I access http://testphp.vulnweb.com/listproducts.php?cat=1
    And I proved number by number
    Then I can find that the table has 11 entries
    Then I can write the following SQL syntax in the url bar
    """
    http://testphp.vulnweb.com/listproducts.php?cat=1 union select 1,table_name,
    3,4,5,6,7,8,9,10,11 from information_schema.tables
    """
    Then I get the output
    And [evidence](evidence.png)
    And I can see which tables are contained in the database
    Then I can conclude that it's posible to access the database trough a SQLi

  Scenario: Remediation:
    When the query is programmed it should use parametrized queries
    And an extension like PDO
    Then the program can succesfully avoid most of the known SQLi

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.2/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:C/C:L/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    6.9 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.2 (Medium) - CR:M/IR:L/MC:L/MI:L/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-04-29
