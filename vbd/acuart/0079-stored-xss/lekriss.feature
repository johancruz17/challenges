## Version 1.4.1
## language: en

Feature:
  TOE:
    Acuart
  Location:
    http://testphp.vulnweb.com - cookie (mycookie:3)
  CWE:
    CWE-0079: Improper Neutralization of Input During Web Page Generation
  Rule:
    REQ.173 Discard unsafe inputs
  Goal:
    Detect and exploit vuln Insecure comments section
  Recommendation:
    Escape external input before executing it

  Background:
  Hacker's software:
    |<software name>       | <version>      |
    | Microsoft Windows 10 | 10.0.17763.437 |
    | Mozilla firefox      | 6.0.3 (64 bit) |
  TOE information:
    Given I am accessing the site testphp.vulnweb.com
    And Entered to site .../guestbook.php
    And there is a comments box

  Scenario: Normal use case
    Given I access testphp.vulnweb.com/guestbook.php
    And write "Anything" on the text box
    Then I can see the word anything stored as a comment

  Scenario: Static detection:
    Given I do not have access to the source coude
    Then I can not make static detection

  Scenario: Dynamic detection:
    Given I access testphp.vulnweb.com/guestbook.php
    Then I can write the following script in the comments box
    """
    <script>alert(Document.cookie)</script>
    """
    Then I get an alert window as output
    And I notice that my comment stills stored
    Then I conclude that it is possible to store malicious code on the page

  Scenario: Exploitation:
  Given I access testphp.vulnweb.com/guestbook.php
    Then I can write the following script in the search bar
    """
    <script>window.onload(alert("this is a stored xss"))</script>
    """
    Then I get an alert window with the message "this is a stored xss"
    And The script executes everytime I open that particular section
    Then I can conclude that is possible to store scripts at the server

  Scenario: Remediation:
    Given the method called by
    """
    51  <form action="" method="post" name="faddentry">
    """
    Then it should validate the data made from the input
    And use techniques to escape dangerous characters
    And that way, avoid malicious code to be incrusted

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    5.1 (Medium) - E:H/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    4.4 (Medium) - CR:L/MC:L/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2019-05-07
