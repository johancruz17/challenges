## Version 1.4.1
## language: en

Feature:
  TOE:
    Damn Small Vulnerable Web
  Category:
    Cross Site Request Forgery
  Location:
    http://localhost:65412/ - comment (field)
  CWE:
    CWE-352: Cross-Site Request Forgery (CSRF)
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Show a JavaScript alert
  Recommendation:
    Whitelisting allowed inputs

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Debian          | Buster      |
    | Firefox         | 68.9.0      |
  TOE information:
    Given I am accessing the site
    And a text box
    And The app is running on XAMPP 5.6.21

  Scenario: Normal use case
    Given I access to "http://localhost:65412/?comment="
    When I pass a value to the parameter "?comment=Hello everyone"
    Then I can see
    """
    Thank you for leaving the comment. Please click here here to see
    all comments
    """
    When I click in "here"
    Then I can see all comments
    """
    Comment(s):
    |id|comment     |       time              |
    |1 |Hey Everyone| Tue Oct 15 10:56:58 2019|
    """

  Scenario: Static detection
    Given I watch the source code
    Then I can see a function inside the Python code
    And the lines where the "comment" parameter is processed
    """
    cursor.execute("INSERT INTO comments VALUES(NULL, '%s', '%s')"
                   % (params["comment"], time.ctime()))
    """
    When I inspect the code
    Then I see the parameter "comment" is not filtered
    And I conclude that I could pass not just a number

  Scenario: Dynamic detection
    Given the parameter "comment"
    When I type "<h1>Hey</h1>"
    Then I can see a new comment
    """
    Comment(s):
    |id|comment     |       time              |
    |1 |Hey Everyone| Tue Oct 15 10:56:58 2019|
    |2 |Hey         | Tue Oct 15 10:57:04 2019|
    """
    Then I can conclude that I can use labels

  Scenario: Exploitation
    Given I can use labels
    And that the parameter "comment" is not filtered
    And I have this code for generating an alert
    """
    <button onclick="alert();">Click me</button>
    """
    When I put the code inside the parameter
    """
    http://localhost:65412/?comment=%3Cbutton%20onclick=%22alert();
    %22%3EClick%20me%3C/button%3E
    """
    Then I can see a new comment [evidence1](image1.png)
    When I click in his button
    Then I can show [evidence2](image2.png)

  Scenario: Remediation
    Given the next code snippet
    """
    cursor.execute ("INSERT IN VALUES comments (NULL, '% s', '% s')"
    % (params ["comment"], time.ctime ()))
    """
    Then it must validate the data entered in the parameter
    """
    params ["comment"]
    """
    And use a whitelist that only allows entries "[a-z] [A-Z] [0-9]"
    And in that way, prevent malicious code from being embedded

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constant over time and organizations
    4.3/10 (Na) - AV:N/AC:L/PR:N/UI:R/S:U/C:L/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fixability
    4.2/10 (Na) - E:H/RL:W
  Environmental: Unique and relevant Attributes to a specific user environment
    3.5/10 (Na) - CR:L/MAV:N/MUI:R/MS:U/MC:L
