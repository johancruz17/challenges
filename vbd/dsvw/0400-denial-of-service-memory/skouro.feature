## Version 1.4.1
## language: en

Feature:
  TOE:
    dsvw
  Category:
    Denial of Service (memory)
  Location:
    http://127.0.0.1:65412/ - size (field)
  CWE:
    CWE-400: https://cwe.mitre.org/data/definitions/400.html
    CWE-770 : https://cwe.mitre.org/data/definitions/770 .html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
  Goal:
    Verify if the site is vulnerable to denial of services (memory)
  Recommendation:
    Define a limit of use the resources of processing

  Background:
      Hacker's software:
    | <Software name>                  | <Version>     |
    | Ubuntu                           | 19.04         |
    | Google Chrome                    | 75.0.3770.100 |
  TOE information:
    Given I am accessing the site http://127.0.0.1:65412/
    And the server is running BaseHTTP version 0.3
    And Python version 2.7.16
    And SQLite version 3

  Scenario: Normal use case
    When I make the next request:
    """
    GET http://127.0.0.1:65412/?size=2000 HTTP/1.1
    """
    Then I get the required time to resize an image:
    """
    Time required (to 'resize image' to 2000x2000): 0.010000 seconds
    """

  Scenario: Static detection
    When I look at the code of file DSVW\dsvw.py
    """
    144 start, _ = time.time(), "<br>".join("#" * int(params["size"])
      for _ in range(int(params["size"])))
    """
    Then I conclude that no limit number of iterations

  Scenario: Dynamic detection
    When I make the next request and increment the size value successively
    """
    GET http://127.0.0.1:65412/?size=10000 HTTP/1.1
    """
    Then I get:
    """
    Time required (to 'resize image' to 10000x10000): 0.158107 seconds
    """
    When size value is 30000
    Then I get:
    """
    Time required (to 'resize image' to 30000x30000): 0.886528 seconds
    """
    When the value of size increment the time of response also
    Then I can conclude that size parameter is a possible attack vector

  Scenario: Exploitation
    When I make the next request:
    """
    GET http://127.0.0.1:65412/?size=999999 HTTP/1.1
    """
    Then the use of CPU and memory increment considerably
    And the host doesn't respond, the process of server is killed
    Then I can conclude that the vulnerability has been exploited

  Scenario: Remediation
    When execute a loop define a limit of iterations:
    """
      if int(params["size"]) < 10000:
        start, _ = time.time(), "<br>".join(
          "#" * int(params["size"]) for _ in range(int(params["size"])))
    """
    When I make the next request:
    """
    GET http://127.0.0.1:65412/?size=999999 HTTP/1.1
    """
    Then the site doesn't return anything
    Then I can confirm that the vulnerability was successfully patched

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:N/A:H/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.3/10 (High) - E:H/RL:W/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    5.6/10 (Medium) - MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:N/MI:N/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2019-07-23
