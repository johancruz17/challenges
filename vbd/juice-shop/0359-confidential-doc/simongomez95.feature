## Version 1.4.2
## language: en

Feature:
  TOE:
    Juice-Shop
  Category:
    Access Control
  Location:
    /ftp - Information Exposure
  CWE:
    CWE-200: Information Exposure
  Rule:
    REQ.096: https://fluidattacks.com/web/es/rules/096/
  Goal:
    Access unauthorized documents
  Recommendation:
    Don't expose files over web server

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running Juice-Shop in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No access control
    Given I see the code at "server.js"
    """
    115 /* /ftp directory browsing and file download */
    116 app.use('/ftp', serveIndex('ftp', { 'icons': true }))
    117 app.use('/ftp/:file', fileServer())
    """
    Then I see it allows access to a file index in "/ftp"

  Scenario: Dynamic detection
  Accessing unprotected index
    Given I access http://localhost:8000/#/about
    Then I see a link to the Terms and Conditions at
    """
    http://localhost:8000/ftp/legal.md?md_debug=true
    """
    Then I navigate to "http://localhost:8000/ftp/" to see if there's more
    And I find an index with some files

  Scenario: Exploitation
  Accessing confidential files
    Given I can see a list of files in the index
    Then I find an interesting one called "acquisitions.md"
    Then I download it and find confidential information
    """
    > This document is confidential! Do not distribute!

    Our company plans to acquire several competitors within the next year.
    This will have a significant stock market impact as we will elaborate in
    detail in the following paragraph:

    Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
    eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam
    voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
    clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit
    amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam
    nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat,
    sed diam voluptua. At vero eos et accusam et justo duo dolores et ea
    rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem
    ipsum dolor sit amet.

    Our shareholders will be excited. It's true. No fake news.
    """

  Scenario: Remediation
  Access control
    Given I patch the code like this
    """
    115 /* /ftp directory browsing and file download */
    116 app.use('/ftp/:file', fileServer())
    """
    Then the index isn't available anymore and only specific files are available

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.5/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    7.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    7.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-30
