## Version 1.4.2
## language: en

Feature:
  TOE:
    Juice-Shop
  Category:
    Error Handling
  Location:
    / - Error Handling
  CWE:
    CWE-285: Improper Authorization
  Rule:
    REQ.096: https://fluidattacks.com/web/es/rules/096/
  Goal:
    Remove all good reviews from the site
  Recommendation:
    Check for proper privileges before making transactions

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running Juice-Shop in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  Dev environment
    Given I see the code at "administration.component.ts"
    """
    ...
    69  deleteFeedback (id: number) {
    70    this.feedbackService.del(id).subscribe(() => {
    71      this.findAllFeedbacks()
    72    },(err) => {
    73      this.error = err
    74      console.log(this.error)
    75    })
    76  }
    ...
    """
    Then I see it doesn't care who is sending the request
    And just deletes the feedback without validating permissions

  Scenario: Dynamic detection
  Deleting customer feedbacks
    Given I have access to the unrestricted admin dashboard at
    """
    http://localhost:8000/#/administration
    """
    Then I click on the delete button of a feedback item
    And it gets deleted with no problems

  Scenario: Exploitation
  Tanking store reputation
    Given I can delete feedbacks
    Then I delete all good feedback (rating>3)
    Then the store gets a big hit on its reputation

  Scenario: Remediation
  Access control
    Given I implement a proper role system for users
    And I restrict access to the admin dashboard and functions for non-admins
    Then the vulnerability is fixed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    0285-admin-section
      Given I have unrestricted access to the admin section
      Then I can remove reviews
