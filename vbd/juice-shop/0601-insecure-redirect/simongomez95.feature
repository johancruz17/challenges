## Version 1.4.2
## language: en

Feature:
  TOE:
    Juice-Shop
  Category:
    Error Handling
  Location:
    / - Error Handling
  CWE:
    CWE-601: URL Redirection to Untrusted Site ('Open Redirect')
  Rule:
    REQ.173: https://fluidattacks.com/web/es/rules/173/
  Goal:
    Redirect user to typosquatted domain
  Recommendation:
    Clean old potentially dangerous links

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running Juice-Shop in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  Dev environment
    Given I see the code at "insecurity.js"
    """
    ...
    77  const redirectWhitelist = new Set([
    78    'https://github.com/bkimminich/juice-shop',
    79    'https://blockchain.info/address/1AbKfgvw9psQ41NbLi8kufDQTezwG8DRZm',
    80    'https://explorer.dash.org/address/Xr556RzuwX6hg5EGpkybbv5RanJoZN17kW'
    81    'https://gratipay.com/juice-shop',
    82    'http://shop.spreadshirt.com/juiceshop',
    83    'http://shop.spreadshirt.de/juiceshop',
    84    'https://www.stickeryou.com/products/owasp-juice-shop/794'
    85  ])
    ...
    """
    Then I see it whitelists a link to a site that doesn't exist anymore
    """
    'https://gratipay.com/juice-shop',
    """

  Scenario: Dynamic detection
  Finding unused code in the source
    Given I go to "http://localhost:8000/#/basket"
    Then I see multiple donation buttons
    Then I see they are hrefs to donation sites
    Then I open the page source code and ctrl+f for href
    And I find a link that isn't visible on the site
    """
    (l() (), t['ɵeld'](0, 0, null, null, 4, 'a', [
      ['href',
      '/redirect?to=https://gratipay.com/juice-shop']
    ], null, null, null, null, null)),
    """
    Then I go to
    """
    http://localhost:8000/redirect?to=https://gratipay.com/juice-
    shop
    """
    Then I get redirected to "https://gratipay.com/juice-shop"
    And it's not found

  Scenario: Exploitation
  Squatting the domain
    Given I buy the now inactive "gratipay.com" domain
    Then I can put a malicious payload (i.e. malware) there
    And send application users the link that redirects there
    Then I infect them

  Scenario: Remediation
  Access control
    Given I remove the dead link from the redirect whitelist
    """
    ...
    77  const redirectWhitelist = new Set([
    78    'https://github.com/bkimminich/juice-shop',
    79    'https://blockchain.info/address/1AbKfgvw9psQ41NbLi8kufDQTezwG8DRZm',
    80    'https://explorer.dash.org/address/Xr556RzuwX6hg5EGpkybbv5RanJoZN17kW'
    82    'http://shop.spreadshirt.com/juiceshop',
    83    'http://shop.spreadshirt.de/juiceshop',
    84    'https://www.stickeryou.com/products/owasp-juice-shop/794'
    85  ])
    ...
    """
    Then the application fobids the redirect when attempted

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:N/UI:R/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:F/RL:O/RC:C/
  Environmental: Unique and relevant attributes to a specific user environment
    6.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-30
