## Version 1.4.2
## language: en

Feature:
  TOE:
    Juice-Shop
  Category:
    Access Control
  Location:
    /#/administration - Access Control
  CWE:
    CWE-284: Improper Access Control
  Rule:
    REQ.096: https://fluidattacks.com/web/es/rules/096/
  Goal:
    Access administration section
  Recommendation:
    Implement proper access control

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | Burp Suite CE         | 1.7.36    |
  TOE information:
    Given I am running Juice-Shop in a docker container at
    """
    http://localhost:8000/
    """

  Scenario: Normal use case
  Normal site navigation
    Given I go to http://localhost:8000/
    Then I can navigate the site

  Scenario: Static detection
  No access control
    Given I see the code at "app.routing.ts"
    """
    35  const routes: Routes = [
    36    {
    37      path: 'administration',
    38      component: AdministrationComponent
    39    },
    """
    Then I see it allows access to the administration section to anyone

  Scenario: Dynamic detection
  Accessing unprotected endpoint
    Given I access http://localhost:8000/#/administration
    Then I enter a page where I can see user list, customer feedback
    And recycling requests

  Scenario: Exploitation
  Using leaked data
    Given I can see a list of users with their personal data
    Then I can use this data to better customize my targeted attacks

  Scenario: Remediation
  Access control
    Given I implement access control
    And don't display the admin page to non-admin users
    Then the vulnerability is fixed

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    6.5/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N/
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.0/10 (Medium) - E:F/RL:O/RC:C/
  En4.9onmental: Unique and relevant attributes to a specific user environment
    6.0/10 (Medium) - CR:M/IR:M/AR:M

  Scenario: Correlations
    No correlations have been found to this date 2019-01-30
