## Version 1.4.1
## language: en

Feature:
  TOE:
    SQLI-labs
  Category:
    SQL Injection -  GET-Blind-Boolean Based-Single Quotes
  Location:
    http://localhost/sqlilabs/Less-8/ - id(field)
  CWE:
    CWE-89: SQL Injection
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Verify if the site is vulnerable to SQL Injection
  Recommendation:
    Use Prepared Statements

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Microsoft Windows | 7               |
    | Chrome            | 78.0.3904.70    |
    | Wamp server       | 3.1.9 32 bit    |
  TOE information:
    Given I access the main page
    And the page is made with PHP
    And MySQL is the Database Management System
    And the website is running in localhost

  Scenario: Normal use case
    When I access the page
    Then a message is displayed at the center of the screen
    """
    Please input the ID as parameter with numeric value
    """
    When I enter the ID parameter as "?id=1"
    Then a new message appears with a login name and a password
    """
    You are in...........
    """

  Scenario: Static detection
    Given I access the backend code at
    """
    sqlilabs\Less-8\index.php
    """
    When I check the source code
    Then I find a query related to the id parameter
    """
    $sql="SELECT * FROM users WHERE id='$id' LIMIT 0,1";
    $result=mysqli_query($con, $sql);
    $row = mysqli_fetch_array($result, MYSQLI_BOTH);
    """
    And the variable $sql is storing a query string
    When there aren't prepared statements in the code
    And the query is stored as a single string
    Then the id parameter is vulnerable to SQL injection

  Scenario: Dynamic detection
    Given I access the website
    When I enter the id parameter of the URL using a single quote (')
    Then I can't see error messages in the screen
    And the welcome message "You are in..........." has been erased
    When I enter "1'" as the id in the browser
    Then there's no change in the main page
    When I inject a boolean expresion with single quotes as the id
    """
    id=1' and '1'='1
    """
    Then the welcome message is shown again
    And the site is vulnerable to SQL Injection

  Scenario: Exploitation
    Given I can use injections by adding comments at the end of the string
    When I test the website using boolean expresions
    Then I try to guess the value for the length of the database name
    """
    length(database())
    """
    When I assign "5" as the database length's value
    Then there's no message in the screen [evidence](img.png)
    And I keep guessing the right value for the length
    When I enter "8" as the length's value
    """
    ?id=1' and length(database())=8--+
    """
    Then the welcome message appears again [evidence](img2.png)
    And I find out that the database name has exactly 8 characters
    When I try to find the values for each letter of the database name
    Then I use a SQL substring function to get every character
    """
    substring(database(), <character index>, 1)
    """
    When I enter "s" as the value of the first character
    """
    ?id=1' and substring(database(),1,1)='s'--+
    """
    Then the welcome message appears again
    And I guessed the value of the first character
    When I make the same process for every character
    Then I find out the database name
    """
    security
    """
    And the website is vulnerable to Blind SQL Injection

  Scenario: Remediation
    When the application is executing queries in MySQL
    Then the query code must use prepared statements
    """
    $stmt = $mysqli->prepare("SELECT * FROM users WHERE id='$id' LIMIT 0,1")

    $id = $_POST['id']
    $stmt->bind_param("i", $id)

    $stmt->execute()
    """
    And the code can prevent SQL Injection

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.7/10 (High) - AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    7.1/10 (High) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.8/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L

  Scenario: Correlations
    No correlations have been found to this date 2019-11-28
