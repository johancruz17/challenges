## Version 1.4.1
## language: en

Feature:
  TOE:
    SQLI-labs
  Category:
    SQL Injection -  GET-Double Injection-Single Quotes
  Location:
    http://localhost/sqlilabs/Less-5/ - id(field)
  CWE:
    CWE-89: SQL Injection
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173 Discard unsafe inputs
      https://fluidattacks.com/web/rules/173/
  Goal:
    Verify if the site is vulnerable to SQL Injection
  Recommendation:
    Use Prepared Statements

  Background:
  Hacker's software:
    | <Software name>   | <Version>       |
    | Microsoft Windows | 7              |
    | Chrome            | 78.0.3904.70    |
    | Wamp server       | 3.1.9 32 bit    |
  TOE information:
    Given I access the main page
    And the page is made with PHP
    And MySQL is the Database Management System
    And the website is running in localhost

  Scenario: Normal use case
    When I access the page
    Then a message is displayed at the center of the screen
    """
    Please input the ID as parameter with numeric value
    """
    When I enter the ID parameter as "?id=1"
    Then a new message appears
    """
    You are in...........
    """

  Scenario: Static detection
    Given I access the backend code at
    """
    sqlilabs\Less-5\index.php
    """
    When I check the source code
    Then I find a query related to the id parameter
    """
    $sql="SELECT * FROM users WHERE id='$id' LIMIT 0,1";
    $result=mysqli_query($con, $sql);
    $row = mysqli_fetch_array($result, MYSQLI_BOTH);
    """
    And the variable $sql is storing a query string
    When there aren't prepared statements in the code
    And the query is stored as a single string
    Then the id parameter is vulnerable to SQL injection

  Scenario: Dynamic detection
    Given I access the website
    When I enter the id parameter of the URL using a single quote (')
    Then I get an error message from the database
    """
    You have an error in your SQL syntax; check the manual that corresponds
    to your MySQL server version for the right syntax to use near
    '''' LIMIT 0,1' at line 1
    """
    When the query's interpretation is broken by my input in the URL
    Then the site is vulnerable to SQL Injection

  Scenario: Exploitation
    When I test the website with a boolean injection
    """
    id=1' and '1'='1
    """
    Then the query isn't generating errors
    And a single quote injection should work on this website
    And I can use comments to ignore the remaining quote in the injection
    """
    id=1'--+
    """
    When I try to enter a union based injection into the query
    Then the website doesn't display new information
    And I need to make another type of injection
    When I try a SQL double injection
    Then I write my attack based on a main query
    """
    select count(*), floor(rand()*2)a from table_name group by a
    """
    And the query's result sometimes return a "duplicate entry" error
    And I can use the error to get confidential information
    When I want to get the database name and version
    Then I modify the basic query by using the DB information schema
    And It is necessary to concatenate the data inside the query
    """
    select count(*),concat((select database()),'--',floor(rand()*2))a
    from information_schema.tables group by a
    """
    When I'm going to inject the malicious query
    Then the basic query could return two columns
    And the injection might fail
    And I have to use the main query as a subquery for another selection
    When I rewrite the attack by selecting 1 column from the main query
    Then I enter the modified SQL injection
    """
    ?id = 1' and (select 1 from(select count(*),concat((select database()),
    '--',(select version()),'--',floor(rand()*2))a from
    information_schema.tables group by a)b)--+
    """
    And the subquery is returning more than 1 row
    And the duplicate error is not being displayed [evidence](img.png)
    When I reload the website more times
    Then a duplicate error is activated
    And I get sensible data [evidence](img2.png)
    And the website is vulnerable to SQL Double injection

  Scenario: Remediation
    When the application is executing queries in MySQL
    Then the query code must use prepared statements
    """
    $stmt = $mysqli->prepare("SELECT * FROM users WHERE id='$id' LIMIT 0,1")

    $id = $_POST['id']
    $stmt->bind_param("i", $id)

    $stmt->execute()
    """
    And the code can prevent SQL Injection

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.7/10 (High) - AV:N/AC:L/PR:L/UI:N/S:C/C:H/I:N/A:N
  Temporal: Attributes that measure the exploit's popularity and fiabilty
    7.1/10 (High) - E:F/RL:O/RC:C
  Environmental: Unique and relevant attributes to a specific user environment
    5.8/10 (Medium) - CR:L/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:H/MI:L

  Scenario: Correlations
    No correlations have been found to this date 2019-11-25
