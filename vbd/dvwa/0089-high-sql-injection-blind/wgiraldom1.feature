## Version 1.4.1
## language: en

Feature:
  TOE:
    Damn Vulnerable Web Application - Blind SQL Injection
  Location:
    http://127.0.0.1/dvwa/vulnerabilities/sqli_blind - ID field
  Category:
    Web
  CWE:
    CWE-89: Improper Neutralization of Special Elements used in a SQL Command
      https://cwe.mitre.org/data/definitions/89.html
  Rule:
    REQ.173: https://fluidattacks.com/web/rules/173/
    REQ.105: https://fluidattacks.com/web/rules/105/
  Goal:
    Fingerprint RDBMS and Web Server using blind SQL Injection
  Recommendation:
    Sanitize user input.
    Limit the amount of successive wrong queries made by an user.
    Prevent confirmation/error messages when possible.

  Background:
  Hacker's software:
    | <Software name>   |  <Version> |
    | Microsoft Windows |     10     |
    | Google Chrome     |  76.0.3809 |
    | Python            |    3.7.4   |
    | Apache Web Server |    2.4.1   |
    | PHP               |    7.1.1   |
    | MySQL             |   10.4.6   |
  TOE information:
    Given I'm accessing the site through http://localhost
    When the Web and MySQL Servers are running,
    Then I will start analysis.

  Scenario: Normal use case
    Given I got to the login page,
    And I access with default credentials,
    When I get into the SQL Injection (Blind) section.
    Then a page appears, with an input where you can write an user's ID.
    And it responds whether the user with said ID exists or not.

  Scenario: Static detection
    Given source code clearly is prone to SQL Injection,
    Given its core algorithm in "/vulnerabilities/sqli_blind/sources/high.php":
    """
    ...
    11: $getid  = "SELECT first_name, last_name FROM users WHERE
    12:   user_id = '$id' LIMIT 1;";
    13: $result = mysqli_query($GLOBALS["___mysqli_ston"],  $getid );
    14: // Get results
    15: $num = @mysqli_num_rows( $result ); // The '@'
    16:                          character suppresses errors
    ...
    """
    When an injected $id is concatenated with ' --'
    Then our SQL is still valid,
    And prone to be better exploited.

  Scenario: Dynamic detection
    Given the site exposes an input field where IDs could be queried
    When I put " 1' AND '1'='1 "
    Then I get a possitive answer
    """
    User ID exists in the database.
    """
    And this means the SQL input got executed.

  Scenario: Exploitation
    Given we need to get the amount of query params
    And we make successive requests using "UNION SELECT 1[,1]+"
    But we get failure responses until we hit the adequate amount (it is 2).
    When SQL Injection is blind
    Then we could use it as a yes/no Oracle
    And perform bruteforce/enumeration attacks. For example
    """
      id = 0' UNION SELECT TABLE_NAME, TABLE_SCHEMA FROM
        INFORMATION_SCHEMA.tables WHERE TABLE_SCHEMA LIKE '%A%
    """
    Then we could fetch if there are databases whose name has 'A'
    And it works [evidence](img1.png)

  Scenario: Extraction
    Given we are capable of everything SQLi empowers us with,
    When we assess the possibilities,
    Then we understand we are able not only to retrieve DB registers
    But also enumerate files from the system:
    """
      id = 0' UNION SELECT LOAD_FILE("file.txt") as f, 1 WHERE f NOT NULL
        AND '1'='1
    """
    And write files
    """
      id = 0' UNION SELECT '<?php echo', 'hello ?>' INTO OUTFILE 'test.php
    """

  Scenario: Remediation
    Given we know the type of the param we are expecting
    When we assert it is an integer greater than zero,
    Then we repair the code, and the vulnerability is gone
    """
    11: $id = intval($id);
    12: $getid  = "SELECT first_name, last_name FROM users
                    WHERE user_id = '$id' LIMIT 1;";
    """

  Scenario: Scoring
  Severity scoring according to CVSSv3 standard
  Base: Attributes that are constants over time and organizations
    7.3/10 (High) - AV:N/AC:L/PR:N/UI:N/S:U/C:L/I:L/A:L
  Temporal: Attributes that measure the exploit's popularity and fixability
    6.5/10 (Medium) - E:F/RL:O/RC:R
  Environmental: Unique and relevant attributes to a specific user environment
    6.8/10 (Medium) - CR:H/IR:L/AR:L/MAV:N/MAC:L/MPR:N/MUI:N/MS:U/MC:L/MI:L/MA:H

  Scenario: Correlations
    No correlations have been found to this date 2019-08-30
