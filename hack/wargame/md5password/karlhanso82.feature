# Version 2.0
## language: en

Feature: Training programming
  Site:
    http://wargame.kr
  Category:
    vulnerability
    hacking
  User:
    karlhanso82
  Goal:
    find md5 vulnerability given the challenge

 Background:
 Hacker's software:
   | <Software name> | <Version>    |
   | Windows OS      | 10           |
   | Chromium        | 74.0.3729.131|

 Machine information:
   Given the challenge URL
   """
   http://wargame.kr:8080/md5_password/
   """
   Then I open the url in chromium
   And I see an input with the name password
   Then I see a link with the name getsourced
   And a button login
   Then I enter some garbled text to the password input
   And I see a wrong message
   Then I move to the source code
   And I see it and therefore I conclude that it could be a md5 vulnerability

 Scenario: Fail:entering-sql-injection
   Given the challenge URL
   Then I see the source code
   """
   $key=auth_code("md5 password");
   $ps = mysql_real_escape_string($_POST['ps']);
   $row=@mysql_fetch_array(mysql_query("select * from admin_password where
   password='".md5($ps,true)."'"));
   """
   Then I see sql query then I try an OR and " or ""="
   And I got wrong message
   Then I decide to search for more information

 Scenario: Success:enter-the-password
   Given the challenge
   Then I enter the site
   And I see this particular line of code
   """
   $row=@mysql_fetch_array(mysql_query("select * from admin_password where
   password='".md5($ps,true)."'"));
   """
   Then I see that the method md5('pwd',true)
   And I see the php documentation page
   """
   md5 ( string $str [, bool $raw_output = FALSE ] ) : string
   If the optional raw_output is set to TRUE, then the md5 digest
   is instead returned in raw binary format with a length of 16.
   """
   Then that means if I enter the password the outcome would be binary
   And if the binary contains like OR sentence like
   Then I search for this password that  gives me this type of outcome
   And I find it 129581926211651571912466741651878684928
   Then this value gives this result
   """
   ?T0D??o#??'or'8.N=?
   """
   Then I introduce this especial number
   And it give me the flag to solve the challenge

