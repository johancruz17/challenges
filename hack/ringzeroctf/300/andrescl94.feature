## Version 2.0
## language: en

Feature: Let's-Play-Scrabble
  Site:
    https://ringzer0ctf.com
  Category:
    cryptography
  User:
    AndresCL94
  Goal:
    Get the flag

  Background:
  Hacker's software:
  | <Software name> | <Version>         |
  | Kali Linux      | 5.5.0-kali1-amd64 |
  | Python3         | 3.8.2             |
  | Crunch          | 3.6               |

  Machine information:
    Given the challenge URL
    """
    https://ringzer0ctf.com/challenges/300
    """
    Then I see a button to download a zip file
    When I extract the files
    Then I see a PDF that explains the challenge

  Scenario: Fail: Trial-And-Error
    Given the PDF file from the challenge
    And it contains the description of a custom encryption algorithm
    And it asks me to decrypt a ciphertext
    And it states that the plaintext contains the word "FLAGHYPHEN"
    And I do not have the seed to generate the key
    When I develop a script that implements the algorithm [cipher.py]
    And I try random keywords extracted from the PDF file as the seed
    Then I do not find the flag

  Scenario: Fail: Strings-Xxd
    Given that I do not have the seed
    When I use the command "strings" and "xxd" on the PDF
    Then I do not find anything from the binary/hex representation
    And I do not find the flag

  Scenario: Fail: Crunch
    Given that I do not have the seed
    And the word "scrabble" seems to be important throughout the challenge
    When I use the command "crunch" with that word
    Then I obtain a list of possible permutations of the word
    When I execute the script with that list of words
    Then I do not manage to crack the ciphertext
    And I do not get the flag

  Scenario: Sucess: Bruteforce
    Given that I can pass a list of words to the Python script
    And I downloaded the 10 million most common passwords
    """
    https://github.com/danielmiessler/SecLists/blob/master/Passwords/Common-
    Credentials/10-million-password-list-top-1000000.txt
    """
    When I run the script with this file
    Then I get a match with the keyword "crossword"
    And I find the flag
