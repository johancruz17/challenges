## Version 2.0
## language: en

Feature: CHALLENGE-Ringzer0ctf
  Site:
    Ringzer0ctf
  Category:
    CHALLENGE
  User:
    slayfer1112
  Goal:
    Found the flag decoding the message.

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 79.0.3945.130 |
    | NodeJS          | 13.6.0        |
  Machine information:
    Given I am accessing to the website
    Then I choose a challenge
    And I was redirect to a login form

  Scenario: Fail:decode-base64
    Given I'm on the challenge page
    And I see a message with the instructions
    """
    You have 3 seconds decrypt this message using the xoring key
    hidden into the xor key string. The length of the key is 10 characters.
    The message is encoded using base 64.
    Send the answer back using
    https://ringzer0ctf.com/challenges/16/[your_string]

    ----- BEGIN XOR KEY -----
    2g3c9sHDlZkNFWulwOaQthc4EobzKYVdKprLO1AO
    ----- END XOR KEY -----

    ----- BEGIN CRYPTED MESSAGE -----
    Rhs1YjM6BkgkKSYRUVopAhgyeiMaJSBtdzojMgZpGS0=
    ----- END CRYPTED MESSAGE -----
    """
    When I see the challenge
    Then I copy the crypted message
    And I try to use "atob()" in the console
    When I type "atob('Rhs1YjM6BkgkKSYRUVopAhgyeiMaJSBtdzojMgZpGS0=')"
    Then I receive the output "F5b3:H$)&QZ)2z#% mw:#2i-"
    And I try to use in the url
    """
    https://ringzer0ctf.com/challenges/16/F5b3:H$)&QZ)2z#% mw:#2i-
    """
    When I submit that
    Then I see an alert message that say
    """
    Wrong answer or too slow!
    """
    And I fail to get the password

  Scenario: Success:Decode-base64-and-XOR-with-node
    Given I'm on the challenge page
    Then I need to make the decode faster
    And Put in the url before 3 seconds
    When I think a way to make this I think that I can use javascript in node
    Then I think that I can use the module "puppeteer" for this challenge
    And I make a script that solves the challenge [evidence](slayfer1112.png)
    """
    The script login in the website, next take the XOR key and the message
    and decode the message with base64 and later eval the XOR key and try
    to find a legible result for the decode.

    You can run it with:

    node slayfer1112.js
    """
    When I get the flag
    Then I copy the flag and paste it in the submit flag form
    When I submit the form [evidence](img1)
    Then I receive a message that say "Good job! You got +6 points"
    And I found the flag
