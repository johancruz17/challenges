## Version 2.0
## language: en

Feature: level-2-modx
  Site:
    mod-x.co.uk
  User:
    ununicornio
  Goal:
    decrypt the file left by an intruder

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |

  Scenario: Fail:online-decoder
    Given I start the level
    And it prompts
    """
    Agent ununicornio...

    Good Work on your previous mission - you are ready for your next mission.
    We now have access to the terminals. We think the intruder is telling the
    truth - other changes have been made to the system.
    A file has been found on the system that is not our own.
    All we can think of is that this is some kind of installation file the
    hacker used to speed up his job while he was in our system. We have examined
    the file, but there appears to be a form of encryption used. I know this is
    your speciality, so we have assigned you the task of breaking the encryption
    Once broken, if you find the location of any other files put on the system
    include the full location (in standard Windows format) in your mission
    conclusion.
    Good luck ununicornio.
    """
    And give me a hint
    """
    Maybe research what ASCII is, and how shifts/shifting encryptions work...
    perhaps you could then write a program to help you :)
    """
    Then I recognize shifting or caesar encryption
    And guess it must be applied to the ascii code of each character
    Then I go use an online decyphering tool
    And one of the results it gives me resembles a windows registry entry
    """
    RTK_LOL_MN\SOTR\Microsoft\indos\urrentersion\Run]ontrol\\indos\\
    ebug\\ontrolee
    """
    But it looks like there's characters missing, but I'm on the right track
    Then I have to try something else

  Scenario: Success:own-program
    Given I know the encryption method used
    Then I implement my own shifting decypher in python
    And use the same key I found the interesting string before
    Then I get a full windows registry entry referencing a windows path
    And I submit it as my answer
    Then I finish the level
