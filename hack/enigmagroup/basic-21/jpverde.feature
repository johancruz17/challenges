## Version 2.0
## language: en

Feature: 21-Basic-enigmagroup
  Site:
    enigmagroup
  User:
    jpverde
  Goal:
    Bypass the login form with the correct password

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Windows OS      | 10          |
    | Firefox         | 68.0.1      |
  Machine information:
    Given A password form located in a web page
    """
    http://challenges.enigmagroup.org/basics/sql/1/
    """
    And A web page that only allows access if you type the correct password
    And The webpage says
    """
    Enter the password to continue, The username is admin
    """

  Scenario: Success:sql-injection
    Given I am in the webpage
    When I press the submit button
    Then I get redirected to another webpage with this message
    """
    Wrong password, your attempt has been logged
    """
    And I get sent back to the password field
    And I realize that when it says "logged" that means the password field
    And looks for a database
    When I inject in the password field a contnuation for the SQL query
    """
    ' OR '1'='1' --
    """
    And I add two dashes (--) to comment the rest of the query
    Then I managed to bypass the login
    And I get this message
    """
    Congratulations on beating the mission
    """
    And I completed the challenge
