#!/usr/bin/env python
"""
$ pylint kamadoatfluid.py
  No config file found, using default configuration
  --------------------------------------------------------------------
  Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ python kamadoatfluid.py
  test 4: hastads broadcast attack
    verification 1 passed
    verification 2 passed
    verification 3 passed
  enter this on wechall: 21987654321987654321
"""

# encryption equation for asymmetric RSA
#   c = m^e mod n, e = 3

# Theoretical basis for method 1
#   Given  m ^ e < n
#   Then   m = cubic_root(c)

# Theoretical basis for methods 2 and 3
#   Given  c = m^e mod n
#   And    m is an integer (objective)
#   And    c is an integer (encrypted message)
#   And    n is an integer (public key)
#   And    k is an integer (quotient)
#   And    r is an integer (remainder)
#   Then   one can find unique k, and r, such that:
#            m^e = k * n + r
#   Then   m^e = k * n + (m^e % n)
#   Then   m^e = k * n + c
#   Then   m = (k * n + c) ^ (1 / e)
#   Then   for k=0 we have the method 1
#   But    we are now free to assume that m ^ e >= n

# Theoretical basis for method 4
#   Hastad's broadcast attack
#   Given  you send the same 'm' (message) to 'e' (exponent) recipients
#   Then   RSA is not safe
#   Given  gcd(Ni, Nj) = 1 for all i, j
#   Given  C1 = c % N1
#          C2 = c % N2
#          C3 = c % N3
#   Then   compute that c using the chinese remainder theorem
#   Then   c = m^3 % (N1*N2*N3)
#   But    c = m^3, because RSA conditions

# encrypted message sent to Sunt
C1 = int('59058347096109371168795252122846264164451858386505'
         '37322471725138676007071599832161632287204549636092'
         '60109655976940145302365697467982898836206982239745'
         '95367916085717978321529423973042614191915468707322'
         '33209145507434569257351585794030657232714040035046'
         '28099840341741543996932391672527020041584756158229'
         '13969287')
# encrypted message sent to Qin
C2 = int('68832636863309521842738228912135910059247051689379'
         '75803056469379223409544614680809888088032770672596'
         '94079066453860069217495103780881443302426956054595'
         '23919847298779688078870911760407888507930842574615'
         '38315949143092080737002404840007500340125359757260'
         '59160406874335107804837931177495128902484330773504'
         '4171207')
# encrypted message sent to Ymenetn
C3 = int('31530233954435234871615474461082257860814574852516'
         '83234856675074524798052977806074112106256492130627'
         '50427168027196905639049286839942995639762010399790'
         '06456180637367868349438046345194799904021862696709'
         '80538508459392916192209232800445091886965624967182'
         '38906867327594511973716604476962464936810720924640'
         '92210096')

# public key of Sunt
N1 = int('67108337285130152841142557048979836392659321891857'
         '10122673233728187031921963006330842526100109456835'
         '00464495703173502003450692188680699372573345498334'
         '48872476614876540784326223874329257870403617101833'
         '44382160038707191626853980909371868024916680863506'
         '13878075086854448087474789148537740650154928465303'
         '46393353')
# public key of Qin
N2 = int('72291820644801851050330848110159409202048919534527'
         '95516412030276239664458797356005696155950726338627'
         '65908109136441404319664302394405891876079014454555'
         '06476844642551722261333276197428423259685681788297'
         '97850655364068824699237744054358642264643142596395'
         '74195573542234310174259613563843096211860508710039'
         '08824523')
# public key of Ymenetn
N3 = int('10478378898782706793002745278660882938206912722204'
         '56605149682021292026284194562544284204823809289927'
         '74984332897768761789989189306007432083874165500533'
         '67466485038289175085384708286124753709705489862890'
         '09607799580237494597589248806794509169565536014318'
         '23839124186749459819727500823704168760828991322166'
         '878406679')


# source of this function
# https://stackoverflow.com/questions/23621833/is-cube-root-integer
def find_cube_root(value):
    """ binary search for a full presition integer cubic root """
    low = 0
    hig = value
    while low < hig:
        med = (low+hig)//2
        if med**3 < value:
            low = med + 1
        else:
            hig = med
    return low


# source of this function
# https://rosettacode.org/wiki/Chinese_remainder_theorem#Python
def chinese_remainder(values, modulos):
    """ returns the chinese remainder value for the set of eq """
    cum = 0
    prod = reduce(lambda modulos, b: modulos*b, values)

    for n_i, a_i in zip(values, modulos):
        product = prod / n_i
        cum += a_i * mul_inv(product, n_i) * product
    return cum % prod


# source of this function
# https://rosettacode.org/wiki/Chinese_remainder_theorem#Python
def mul_inv(vala, valb):
    """ returns the modular inverse """
    valb0 = valb
    valx0, valx1 = 0, 1
    if valb == 1:
        return 1
    while vala > 1:
        valq = vala/valb
        vala, valb = valb, vala % valb
        valx0, valx1 = valx1 - valq * valx0, valx0
    if valx1 < 0:
        valx1 += valb0
    return valx1


def test_one():
    """ test one """
    print "test 1: assume m ^ e < n"
    valm1 = find_cube_root(C1)
    valm2 = find_cube_root(C2)
    valm3 = find_cube_root(C3)
    if valm1 == valm2 == valm3:
        print "  it worked"
    else:
        print "  it didn't worked"


def test_two():
    """ test two """
    print "test 2: pseudo brute force"
    valk = 0
    while True:
        valk += 1
        valm1 = find_cube_root(C1 + valk * N1)
        valm2 = find_cube_root(C2 + valk * N2)
        valm3 = find_cube_root(C3 + valk * N3)
        print valk
        print ' +' + str(valm1)
        print ' +' + str(valm2)
        print ' +' + str(valm3)
        print
        if valm1 == valm2:
            print "---"
            print valm1
            print "---"
            print valm2
            print "---"
            print valm3
            print "---"
            break


def test_three():
    """ test three """
    print "test 3: inverse equalization"
    valk1 = 1000000000
    valk2 = 1000000000
    valk3 = 1000000000
    while True:
        valm1 = find_cube_root(C1 + valk1 * N1)
        valm2 = find_cube_root(C2 + valk2 * N2)
        valm3 = find_cube_root(C3 + valk3 * N3)
        if valm1 > valm2 and valm1 > valm3:
            valk1 -= 1
        if valm2 > valm1 and valm2 > valm3:
            valk2 -= 1
        if valm3 > valm1 and valm3 > valm2:
            valk3 -= 1
        if valk1 < 0 or valk2 < 0 or valk3 < 0:
            break
        if valm1 == valm2 and valm2 == valm3:
            print "---"
            print valm1
            print "---"
            print valm2
            print "---"
            print valm3
            print "---"
            break


def test_four():
    """ test four """
    print "test 4: hastads broadcast attack"
    matn = [N1, N2, N3]
    mata = [C1, C2, C3]
    valc = chinese_remainder(matn, mata)
    valm = find_cube_root(valc)
    if C1 == (valm**3) % N1:
        print '  verification 1 passed'
    if C2 == (valm**3) % N2:
        print '  verification 2 passed'
    if C3 == (valm**3) % N3:
        print '  verification 3 passed'
    m_last20digits = valm % 100000000000000000000
    print 'enter this on wechall: ' + str(m_last20digits)


test_four()
