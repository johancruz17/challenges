package desencriptar;

import java.util.HashMap;

public class Desencriptar {
    private static final HashMap<String,String> listaCaracteres = new HashMap();
    public static void main(String[] args) {
    String text1 = "0123456789";
    String encr1 = "2A1B43172B012E093339";
    String text2 = "abcdefghijklmonpqrstuvwxyz";
    String encr2 = "270B41450E1011052F1C1618043E35371D1F15211A23000C3B30";
    String text3 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    String encr3 = "1E13253C292C31220D0F34383A3D02082D362432200A063F1914";
    String text4 = "_.:;?!";
    String encr4 = "260344072846";
    crearArray(text1,encr1);
    crearArray(text2,encr2);
    crearArray(text3,encr3);
    crearArray(text4,encr4);
    String textEncr="21052F151200271512413E35101A152F3511";
    desencriptar(textEncr);
    }

    private static void crearArray (String text,String encr){
    for(int i =1,j=2;i<=text.length();i++,j+=2){
        listaCaracteres.put(encr.substring(j-2,j),text.substring(i-1, i));
    }
    }
    private static void desencriptar(String textEncr){
        String resultado="";
        for(int i =2;i<=textEncr.length();i+=2){
            resultado += listaCaracteres.get(textEncr.substring(i-2, i));
        }
        System.out.println(resultado);
    }
}
