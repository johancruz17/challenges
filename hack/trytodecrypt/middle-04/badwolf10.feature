# Version 2.0
# language: en

Feature:  Challenge 4 (Middle) - Encoding, Crypto - Try To Decrypt
  Site:
    Try To Decrypt
  Category:
    Encoding, Crypto
  User:
    badwolf10
  Goal:
    Decrypt the given message


  Background:
  Hacker's software
    | <Software name>    |       <Version>      |
    | Windows            | 10.0.16299 (x64)     |
    | Mozilla Firefox    | 63.0                 |
    | Visual Studio Code | 1.29.1               |

  Scenario: Success: Decrypt message
    Given the challenge URL
    """
    https://www.trytodecrypt.com/decrypt.php?id=10#headline
    """
    And the text to decrypt
    """
    261129152E152B
    """
    And The online application to try encryption
    Then I start encrypting some random letters
    And I realize each letter is encrypted by a 2-symbol hexadecimal number
    And I notice after encryption each number is incremented
    And the increment is the module of the position in array by 3 e.g.
    """
    Assume we encrypt a b c d e f into 00 01 02 06 07 08 respectively (1-to-1)
    after the encryption add position % 3 to each encrypted number
      00 01 02 03 06 07 08 one-to-one encryption
    + 00 01 02 03 01 02 03 positional encryption
    ----------------------
      00 02 04 06 07 09 0B final encryption
    """
    Then I encrypyt the lower, upper case alphabet and numbers
    And I write a python script to undo the positional tweak
    And I obtain a dictionary of one-to-one encryption
    And I use the code to undo the positional encryption to message
    And the dictionary to decrypt each symbol
    Then I find the hidden message is 'm0n5t3r'
    And I solve the challenge
