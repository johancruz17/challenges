# Version 2.0
# language: en

Feature:  Challenge 3 (Middle) - Encoding, Crypto - Try To Decrypt
  Site:
    Try To Decrypt
  Category:
    Encoding, Crypto
  User:
    badwolf10
  Goal:
    Decrypt the given message


  Background:
  Hacker's software
    | <Software name>    |       <Version>      |
    | Ubuntu             | 16.04 LTS (x64)      |
    | Mozilla Firefox    | 63.0                 |
    | Visual Studio Code | 1.29.1               |

  Scenario: Success: Decrypt message
    Given the challenge URL
    """
    https://www.trytodecrypt.com/decrypt.php?id=9#headline
    """
    And the text to decrypt
    """
    6224F12C1C3FAA5AA54836B3C446D6415E74
    """
    And the online application to try encryption
    Then I start encrypting some random letters
    And I realize each letter is encrypted by a 3-digit hexadecimal number
    And the encrypted message is reversed from right to left
    Then I encrypt the alphabet, digits and special characters
    And find the corresponding encryption of each possible character
    Then I begin to replace each 3-character piece with the proper character
    And find the encrypted message is "fireball 123"
    Then I solve the challenge

