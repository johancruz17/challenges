## Version 2.0
## language: en

Feature: hard-01-decryption-trytodecrypt
  Code:
    hard-01
  Site:
    trytodecrypt
  Category:
    decryption
  User:
    danmur
  Goal:
    Decrypt message

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Firefox         | 68.0.1    |
    | Windows OS      | 10        |
    | CoffeeScript    | 2.4.1     |
  Machine information:
    Given the encrypted text
    """
    59656A6B6F9F656A67746767
    """
    And an input text field
    And the allowed characters
    """
    allowed characters: 0-9 a-z A-Z - _ . , ; : ? ! [space]
    """
    And two submit buttons
    """
    Encrypt
    This is the solution!
    """

  Scenario: Fail:testing-encryption
    Given the machine information
    When I try char 'a'
    And encrypt it
    Then I get '353F'
    But trying again
    Then I get a different result

  Scenario: Fail:experiments
    When trying various tests
    Then I notice that chars are encrypted as a byte
    And therefore two chars representing a letter
    And that for equal chars input a pattern appear
    """
    'aaaaaa'->'86909090909090'
    """
    And that first two characters determine the encoding of the letters

  Scenario: Success:decryption
    When I try '09azAZ-_.,;:?! '
    Then I get '494952536C6D868788898A8B8C8D8E8F'
    And I notice that encryption do not disorganize chars
    And that '0' match the key that determine encryption
    When building and running 'decrypt.coffee' algorithm
    Then I get the solution 'chim cheree'
