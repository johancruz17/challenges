## Version 2.0
## language: en

Feature: Reversing-247ctf
  Site:
    247ctf.com
  Category:
    Reversing
  User:
    SullenestDust1
  Goal:
    Capture the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>      |
    | Arch Linux      | 5.7.10-arch1-1 |
    | Chrome          | 86.0.4209.2    |

  Machine information:
    Given I am accessing the challenge site
    When reading the challenge statement, it says
    """
    You won't find the admin's secret password in this binary.
    We even encrypted it with a secure one-time-pad. Can you still recover
    the password?
    """
    Then I click the link to start the challenge
    And downloads the following file
    """
    c351ac9cd9e85ab7eb921dd362b51b246a401dba.zip
    """

  Scenario: Fail: Check secrets in the files
    Given The downloaded file
    When I extract it, I get the following file
    """
    encrypted_password
    """
    Then I check the file type with
    """
    exiftool
    ...
    """
    And I get the following output
    """
    ExifTool Version Number         : 12.00
    File Name                       : encrypted_password
    Directory                       : .
    File Size                       : 5.9 kB
    File Modification Date/Time     : 2020:07:28 17:39:30-05:00
    File Access Date/Time           : 2020:07:28 17:39:31-05:00
    File Inode Change Date/Time     : 2020:07:28 17:39:30-05:00
    File Permissions                : rwxrwxr-x
    File Type                       : ELF shared library
    File Type Extension             : so
    MIME Type                       : application/octet-stream
    CPU Architecture                : 64 bit
    CPU Byte Order                  : Little endian
    Object File Type                : Shared object file
    CPU Type                        : AMD x86-64
    """
    And I see that the file is an ELF file
    When I check the interesting strings in the file with
    """
    Strings
    """
    Then I get nothing interesting
    When I check the file with
    """
    https://hexed.it/
    """
    Then I found nothing useful

    Scenario: Success: Intercept function calls
    Given the ELF file
    """
    encrypted_password
    """
    When I check if it is a dynamic linked library with
    """
    file
    """
    Then I get the following output
    """
    $ file encrypted_password
    encrypted_password: ELF 64-bit LSB pie executable, x86-64, version 1
    (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for
    GNU/Linux 3.2.0, BuildID[sha1]=e66644a82644c5ac6ab7a507cc5c6e84432ee0ad,
    stripped
    """
    And confirm that the file is a dynamically linked library
    Given I have a dynamically linked file
    When I read about those files, I discover I can run them
    Then I run the file and get a prompt
    """
    $ ./encrypted_password
    Enter the secret password:
    """
    And enter common passwords
    """
    admin root 123456 admin1
    """
    And I always get no more output
    When I check how to analyze this kind of files in more detail
    Then I found a program named
    """
    ltrace
    """
    When I use it in the file, I get a lot of lines and one prompt
    """
    $ ltrace ./encrypted_password
    strlen("875e9409f9811ba8560beee6fb0c77d2"...)                          = 32
    strlen("875e9409f9811ba8560beee6fb0c77d2"...)                          = 32
    strlen("875e9409f9811ba8560beee6fb0c77d2"...)                          = 32
    strlen("875e9409f9811ba8560beee6fb0c77d2"...)                          = 32
    strlen("875e9409f9811ba8560beee6fb0c77d2"...)                          = 32
    strlen("875e9409f9811ba8560beee6fb0c77d2"...)                          = 32
    strlen("875e9409f9811ba8560beee6fb0c77d2"...)                          = 32
    strlen("875e9409f9811ba8560beee6fb0c77d2"...)                          = 32
    strlen("875e9409f9811ba8560beee6fb0c77d2"...)                          = 32
    strlen("875e9409f9811ba8560beee6fb0c77d2"...)                          = 32
    ...
    strlen("875e9409f9811ba8560beee6fb0c77d2"...)                          = 32
    strlen("875e9409f9811ba8560beee6fb0c77d2"...)                          = 32
    puts("Enter the secret password:"Enter the secret password:
    )                                       = 27
    fgets(
    """
    Then I input "123" and get
    """
    fgets(123
    "123\n", 33, 0x7f09097667e0)                               = 0x7ffd0422ece0
    strcmp("123\n", "141c85ccfb2ae19d8d8c224c4e403dce"...)                 = -2
    strlen("875e9409f9811ba8560beee6fb0c77d2"...)                          = 32
    strlen("875e9409f9811ba8560beee6fb0c77d2"...)                          = 32
    ...
    strlen("875e9409f9811ba8560beee6fb0c77d2"...)                          = 32
    strlen("875e9409f9811ba8560beee6fb0c77d2"...)                          = 32
    +++ exited (status 0) +++
    """
    And I consider two interesting strings
    """
    875e9409f9811ba8560beee6fb0c77d2 141c85ccfb2ae19d8d8c224c4e403dce
    """
    When I use the first one in the prompt produced by
    """
    ltrace
    """
    Then I get an output really similar to the one before
    And I use the second string and get [evidence](evidence.png)
    And I found the flag
