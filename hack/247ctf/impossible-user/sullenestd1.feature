## Version 2.0
## language: en

Feature: Cryptography-247ctf
  Site:
    247ctf.com
  Category:
    Cryptography
  User:
    Sullenestdust1
  Goal:
    Capture the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Arch Linux      | 5.7.8-arch1-1 |
    | Chrome          | 85.0.4183.15  |

  Machine information:
    Given I am accessing the challenge site
    When reading the challenge statement, it says
    """
    This encryption service will encrypt almost any plaintext. Can you abuse
    the implementation to actually encrypt every plaintext?
    """
    Then I click the link to start the challenge
    And redirects me to
    """
    https://a38d02422747c31e.247ctf.com/
    """

  Scenario: Fail: Try common usernames
    Given I am in the challenge page
    When I see the screen, it looks like a python file
    """
    ...
    flag_user = 'impossible_flag_user'

    class AESCipher():
        def __init__(self):
            self.key = aes_key
            self.cipher = AES.new(self.key, AES.MODE_ECB)
            self.pad = lambda s: s + (AES.block_size - len(s) % AES.block_si...
            self.unpad = lambda s: s[:-ord(s[len(s) - 1:])]

        def encrypt(self, plaintext):
            return self.cipher.encrypt(self.pad(plaintext)).encode('hex')
        def decrypt(self, encrypted):
            return self.unpad(self.cipher.decrypt(encrypted.decode('hex')))
    ...
    @app.route("/encrypt")
    def encrypt():
        try:
            user = request.args.get('user').decode('hex')
            if user == flag_user:
                return 'No cheating!'
            return AESCipher().encrypt(user)
        except:
            return 'Something went wrong!'
    @app.route("/get_flag")
    def get_flag():
        try:
            if AESCipher().decrypt(request.args.get('user')) == flag_user:
                return flag
            else:
                return 'Invalid user!'
        except:
            return 'Something went wrong!'
    ...
    """
    Then I notice some interesting methods
    """
    encrypt() and get_flag()
    """
    And also more important, I notice two lines in "get_flag()"
    """
    if AESCipher().decrypt(request.args.get('user')) == flag_user:
        return flag
    """
    And I conclude that i must use both methods to capture the flag
    Given Those two methods
    When I try to use "flag_user" as user, I get
    """
    https://a38d02422747c31e.247ctf.com/get_flag?user=flag_user
    Something went wrong!
    """
    Then I notice in the "encrypt()" method that the input should be in hex
    And I procede to convert the user to Hex with
    """
    https://codebeautify.org/string-hex-converter
    """
    Given the hex i got
    """
    696d706f737369626c655f666c61675f75736572
    """
    When I use it in place of the "user" parameter, I get
    """
    https://a38d02422747c31e.247ctf.com/encrypt?user=696d706f737369626c655f666c
    61675f75736572

    No cheating!
    """
    Then I try with common usernames
    """
    user, root, admin
    """
    Given The usernames in hex
    """
    75736572, 726f6f74, 61646d696e
    """
    When I try to use "encrypt" with them, i get some strings
    """
    https://a38d02422747c31e.247ctf.com/encrypt?user=75736572
    707ece4f0913868ec5df07d131b0822d

    https://a38d02422747c31e.247ctf.com/encrypt?user=726f6f74
    993c8d9882e0266995ed72b3716b2d8e

    https://a38d02422747c31e.247ctf.com/encrypt?user=61646d696e
    9ae9cef81d579d5af98d0f73197dd8b8
    """
    Then I use those strings with "get_flag" and obtain the following
    """
    https://a38d02422747c31e.247ctf.com/get_flag?user=707ece4f0913868ec5df07...
    Invalid user!

    https://a38d02422747c31e.247ctf.com/get_flag?user=993c8d9882e0266995ed72...
    Invalid user!

    https://a38d02422747c31e.247ctf.com/get_flag?user=9ae9cef81d579d5af98d0f...
    Invalid user!
    """
    And I conclude that I must get "flag_user" encrypted somehow

  Scenario: Success: Encrypt flag_user
    Given I want to encrypt "flag_user" but cannot do it directly
    When I check again the code, I notice the following line
    """
    self.cipher = AES.new(self.key, AES.MODE_ECB)
    """
    Then I can conclude that the encryption mode is ECB
    And That mode can easily be attacked
    Given The encryption mode is ECB
    Then I try to find the block size with different string sizes
    """
    https://a38d02422747c31e.247ctf.com/encrypt?user=AA
    3217c32e368e89920cea99583ae86775

    https://a38d02422747c31e.247ctf.com/encrypt?user=AAAA
    9a243224fc5d5f92770755acb36a067b

    #AA * 8
    https://a38d02422747c31e.247ctf.com/encrypt?user=AAAAAAAAAAAAAAAA
    c8e62bee30ee975081830b6e0ae1b0d7

    #AA * 16
    https://a38d02422747c31e.247ctf.com/encrypt?user=AAAAAAAAAAAAAAAAAAAAAAAAAA
    AAAAAA
    d4196601ecd309321037aaac6b9c76c52f28487624c5f1476e9fb265d2b47349

    #AA * 15
    https://a38d02422747c31e.247ctf.com/encrypt?user=AAAAAAAAAAAAAAAAAAAAAAAAAA
    AAAA
    5ccf4f293069356119727b54a94b6455
    """
    And I can confirm that the block size is 16 Bytes
    Given That the block size is 16 Bytes
    When I have a "flag_user" of size 20 Bytes
    Then I need at least 2 blocks of 16 Bytes to fit "flag_user"
    And To find out "flag_user" encrypted, I do the following
    """
    https://a38d02422747c31e.247ctf.com/encrypt?user=AAAAAAAAAAAAAAAAAAAAAAAAAA
    AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA696d706f737369626c655f666c61675f75736
    572

    https://a38d02422747c31e.247ctf.com/encrypt?user=CCCCCCCCCCCCCCCCCCCCCCCCCC
    CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC696d706f737369626c655f666c61675f75736
    572
    """
    And I get two strings
    """
    d4196601ecd309321037aaac6b9c76c5d4196601ecd309321037aaac6b9c76c5939454b054b
    7379b0709a270b894025c707ece4f0913868ec5df07d131b0822d

    9715ba717c61975196d018dd1376075f9715ba717c61975196d018dd1376075f939454b054b
    7379b0709a270b894025c707ece4f0913868ec5df07d131b0822d
    """
    When I compare the two strings, I found a substring present in both
    """
    939454b054b7379b0709a270b894025c707ece4f0913868ec5df07d131b0822d
    """
    Then To confirm that the substring is "flag_user" encrypted, I type
    """
    https://a38d02422747c31e.247ctf.com/get_flag?user=939454b054b7379b0709a270b
    894025c707ece4f0913868ec5df07d131b0822d
    """
    And get the following output
    """
    247CTF{ddd01e396dc1965c3fcf943f3968aa39}
    """
    And I have found the flag
