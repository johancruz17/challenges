## Version 2.0
## language: en

Feature: Network-247ctf
  Site:
    247ctf.com
  Category:
    Network
  User:
    SullenestDust1
  Goal:
    Capture the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>      |
    | Arch Linux      | 5.7.11-arch1-1 |
    | Chrome          | 86.0.4214.2    |
    | Wireshark       | 3.2.5          |

  Machine information:
    Given I am accessing the challenge site
    When reading the challenge statement, it says
    """
    Can you recover the private key we used to download the flag over a TLS
    encrypted connection?
    """
    Then I click the link to start the challenge
    And downloads the following file
    """
    bdad12487dbefaac303130233ff2fdb0300c6272.zip
    """

  Scenario: Fail: Check secrets in the file
    Given The downloaded file
    When I extract it, I get the following file
    """
    multiplications_tables.pcap
    """
    Then I try to see the file in hex with
    """
    https://hexed.it/
    """
    Then I found nothing interesting
    And check the file with
    """
    exiftool
    """
    And I get the following output
    """
    ExifTool Version Number         : 12.00
    File Name                       : multiplication_tables.pcap
    Directory                       : .
    File Size                       : 5.6 kB
    File Modification Date/Time     : 2019:10:05 01:34:18-05:00
    File Access Date/Time           : 2020:08:03 09:07:39-05:00
    File Inode Change Date/Time     : 2020:08:03 09:07:39-05:00
    File Permissions                : rw-rw-r--
    Error                           : Unknown file type
    """
    When I get that output
    Then I conclude that something must be done with the messages in
    """
    Wireshark
    """

    Scenario: Success: Decrypt encrypted messages
    Given The file
    """
    encrypted.pcap
    """
    When I open it with
    """
    Wireshark
    """
    Then I notice 15 messages between two addresses
    """
    192.168.10.11 192.168.10.159
    """
    And Looking to the protocols, I only see the following 2
    """
    TCP TLS
    """
    Given The messages
    When I read the "info" column, I notice some with interesting messages
    """
    Client Hello
    Server Hello, Certificate, Server Hello Done
    Application Data
    Application Data, Application Data, Aplication Data...
    """
    Then I Check the second one and found a Certificate
    And I Download it with the option
    """
    Export Packet Bytes
    """
    And I save it as
    """
    cert.crt
    """
    When I look the
    """
    Application Data, Application Data... message
    """
    Then I see some encypted messages
    And I conclude that, one of those should be the target to decrypt
    Given i have a certificate file
    """
    cert.crt
    """
    When I extract the public key from the certificate with
    """
    openssl x509 -inform DER -in cert.crt -pubkey -noout > key.pub
    """
    Then I check the key file, I can see a public key [evidence](evidence.png)
    Given I have a public key
    When I search for a way to find the private key with the public key
    Then I found
    """
    https://github.com/Ganapati/RsaCtfTool
    """
    And I use it to try to find the private key with the following command
    """
    python RsaCtfTool.py --publickey key.pub --private
    """
    And I get a private key [evidence](evidence-2.png)
    Given The found key
    When I Insert it in Wireshark
    """
    Preferences -> Protocols -> TLS -> RSA Key list
    """
    Then I can see an extra tab for TLS messages
    """
    Decrypted TLS (x bytes)
    """
    And I check the package with the encrypted messages
    Then I select the tab
    """
    Decrypted TLS (41 bytes)
    """
    And I found the flag (evidence)[evidence-3.png]
