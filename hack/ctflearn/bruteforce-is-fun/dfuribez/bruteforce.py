import hashlib


def main() -> None:
    for number in range(100_000):
        flag = f"ctflag{number:>05}"
        hex_md5 = hashlib.md5(bytearray(flag, "utf-8")).hexdigest()

        if hex_md5 == "e82a4b4a0386d5232d52337f36d2ab73":
            print(flag)


main()
