## Version 2.0
## language: en

Feature: The Simpsons - Cryptography - ctflearn
  Site:
    https://ctflearn.com/challenge/160
  User:
    dantivar
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> |   <Version>  |
    | Debian          |   stretch    |
    | Chrome          | 80.0.3987.122|
    | Python          |    3.5.3     |
    | jpegtopnm       |     10.0     |
    | ghex            |    3.18.3    |
  Machine information:
    Given A file "ItsKrumpingTime.jpg"

  Scenario: Fail: Comments on image
    Given The file I opened it with "jpegtopnm" looking for comments
    Then I see that there is no comments in the file

  Scenario: Success: ghex forensics
    Given The file I open it with "jpegtopnm" looking for comments
    Then I see that there is no comments in the file
    Then I open it with "ghex"
    And I search for "0xffd9" which is the end of file in "jpg" files
    And rigth after the end of the file I can see some kind of Python code
    Then I see a comment that says something about octal instead of decimal
    Given The Python variables in the code I transform them into decimal
    Then I transform them into Unicode and print them out
    And I get the output:
    """
    How much did Maggie originally cost?
    (Divided by 8, to the nearest integer, and then plus four)
    jrjerwhzkrexar
    """
    Then I google the question
    And perform the specified operations
    Then I set the key as "110" and transform it into Unicode
    And perform the operations that were in the code
    And I get a 3 characters word
    Then as I have a word as a key I try using Vigenere Cipher
    And I get the flag
