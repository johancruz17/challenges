## Version 2.0
## language: en

Feature: Taking LS - Forensics - ctflearn
  Site:
    https://www.ctflearn.com/
  Category:
    Forensics
  User:
    mmarulandc
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 19.10       |
    | Firefox         | 72.0.2      |
  Machine information:
    Given A link in the challenge page
    And the challenge has a zip file to download

  Scenario: Success: Analysing the content of the zip file
    When I unzip the file
    Then I list the elements inside the unzipped folder with "$ ls" command
    And I see a pdf
    When I open the pdf
    Then I realize the pdf has a password [evidence](evidence.png)
    And I think the password is inside the folder where is the pdf
    When I try with the command "$ ls -la" to list hidden files
    Then I realize there is a hidden folder named ".ThePassword"
    When I enter in that folder
    Then I see a file named "ThePassword.txt"
    And I open that file
    And I see the password of the pdf
    When I open the pdf again
    Then I enter the password
    And I see the flag [evidence](evidence2.png)
    When I put the value in flag format
    """
    CTFLearn{<FLAG>}
    """
    Then the challenge is solved
