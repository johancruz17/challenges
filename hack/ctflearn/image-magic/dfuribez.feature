## Version 2.0
## language: en

Feature:
  Site:
    ctflearn
  User:
    mr_once
  Goal:
    Get the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.08.01  |
    | Firefox         | 79.0        |
    | Python          | 3.8.5       |
    | Ristreto        | 0.10.0      |
  Machine information:
    Given I am downloading the image from
    """
    https://mega.nz/#!OKxByZyT!vaabCJRG5D9zAUp7drTekcA5pszu67r_TbQMtxEzqGE
    """
    And it is a ".jpg" file
    And the description of the challenge
    """
    It looks like someone messed up my picture! Can anyone reorganize
    the pixels? The python module PIL (Python Imaging Library) might be
    useful!
    https://mega.nz/#!OKxByZyT!vaabCJRG5D9zAUp7drTekcA5pszu67r_TbQMtxEzqGE
    Update: I think whoever messed up my image took every column of pixels
    and put them side by side. Update: I think the width of the image was
    304 before they messed with it.
    """

  Scenario: Fail:None
    Given The description of the challenge told me what to do
    Then there is no fail scenario

  Scenario: Success:Reshaping the image with numpy
    Given I read the image with "python" and "numpy"
    When I look for the size
    Then I can see it is
    """
    (1, 27968, 3)
    """
    And according to the description it should be
    """
    (304, 92, 3)
    """
    When I try to reshape the image using numpy's reshape method
    """
    im = im.reshape(304, 92, 3)
    """
    Then I get the image [evidence](flagrotated.png)
    When I rotate the image using "ristreto"
    Then I get flag [evidence](flag.png)
    And I solved the challenge
