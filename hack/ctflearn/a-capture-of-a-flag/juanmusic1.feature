## Version 2.0
## language: en

Feature: A CAPture of a flag - Forensics - ctflearn
  Site:
    https://www.ctflearn.com/
  Category:
    Forensics
  User:
    JuanMusic1
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.3 LTS |
    | Firefox         | 71.0        |
    | Wireshark       | 2.6.10      |
  Machine information:
    Given A link in the challenge page
    And the challenge has an PCAP file

  Scenario: Fail: search flag in plain text
    When I check PCAP file
    Then I can see many packets captured
    And I think the flag is in plain tex
    When I use the find in packet option in Wireshark
    Then I couldn't get the flag

  Scenario: Success: Analysing the PCAP packets
    When I see the registry of packets in Wireshark
    Then I search the GET and POST requests
    And I see a particular GET requests
    """
    [evidence](pcap.png)
    """
    When I see the msg parameter
    Then I think the value is encoded in base64
    And I try to decode with this tool
    """
    https://www.base64decode.org/
    """
    Then The value is decoded
    When I put the value in flag format
    """
    CTFLearn{<FLAG>}
    """
    Then The challenge is solved
