## Version 2.0
## language: en

Feature: enormous - miscellaneous - rankk
  Site:
    https://www.rankk.org/challenges/enormous.py
  User:
    JuanMusic1 (wechall)
  Goal:
    Find the flag in the page

  Background:
  Hacker's software:
    | Ubuntu      | 18.04.3 |
    | Firefox     | 70.0.1  |
  Machine information:
    Given A link in the challenge page
    When I access it
    Then I see a big red square
    """
      [evidence](start.png)
    """

  Scenario: Fail: Deleting the square
    Given The page with the big square
    When I delete the table tag in the html
    Then The page becomes empty

  Scenario: Success: Analysing the content of the css
    Given The page with the big square
    When I search in the source code of the page for the css content
    Then I pick the table tag
    And I disable the background color
    """
    [evidence](middle.png)
    """
    Then I see a message, but it is big
    When I pick the little red square
    Then I disable the padding
    And The message is now visible
    When I try enter the message as flag
    Then I solve the challenge
