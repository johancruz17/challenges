# language: en

Feature: Buried
  From site rankk.org
  From Level 1 Category
  With my user santiyepes

Background:
  Given I am running Linux 4.13.0-41-generic x86_64
  And Google Chrome 66.0.3359.181 (Build oficial) (64 bits)
  Given a Python site with a login form
  """
  URL: https://www.rankk.org/challenges/buried.py
  Message: You are looking for the needle in the haystack
  Objetive: Find the answer of the challenge
  Evidence: PDF file
  """

Scenario: Analyzing the challenge
The page shows a statement with a PDF file where the solution is found
  """
  https://www.rankk.org/challenges/files/chaos.pdf
  """
  Then I open the link that gives us the challenge
  And I find many phrases and words mixed together

Scenario: Looking for the needle in the haystack
The file shows a lot of scrambled words and phrases
  Then I download the PDF at the top and thus be able to edit it
  And a window opens asking where to save the file
  Then we change the extension for another to edit
  """
  File name: Chaos.doc
  """
  And I open the file
  Then I increase the zoom of the page
  And thus be able to move each block of words
  Then I find a text that tells me the possible solution
  """
  By the way, the password is impossibly_obscure.
  """
  Then I enter the password indicated by the text
  """
  impossibly_obscure
  """
  And I solve the challenge
