#!/usr/bin/env python3

import math
import pdb
import sys
import binascii

class rsa_pseudo_breaker():
    sys.setrecursionlimit(1000000) # long type,32bit OS 4B,64bit OS 8B(1bit for sign)
    plain_text = []

    def __init__(self):
        cipher_text = ["309117097659990665453", "125675338953457551017", "524099092120785248852",\
                   "772538252438953530955", "547462544172248492882", "028215860448757441963",\
                   "543018082275730030658", "585936545563088067075", "131807465077304821584"]

        # Modulus N (n)
        modulus_n = 783340156742833416191 #int(input())
        # Public exponential (e)
        pub_exp = 653                     #int(input())
        print("Public exponential (e):{0}".format(pub_exp))
        # Prime factor (p)
        prime_p = self.calculate_p(modulus_n)
        print("PRIME_P:{0}".format(prime_p))
        # Primer factor (q)
        prime_q = self.calculate_q(modulus_n, prime_p)
        print("PRIME_Q:{0}".format(prime_q))
        # phi(n): (p-1)*(q-1)
        phi = (prime_p-1)*(prime_q-1)
        print("phi(n):{0}".format(phi))
        # Decryption key (d)
        decrypt_key = self.modinv(pub_exp, phi)
        print("DECRYPT_KEY(d):{0}".format(decrypt_key))

        self.plain_text = self.decrypt(cipher_text, decrypt_key, modulus_n)
        print(self.plain_text)

        output = []
        output = self.dec_to_hex(self.plain_text)
        print(output)
        output = self.hex_to_ascii(output)
        print(output)

    def calculate_p(self, modulus_n):
        """
        Fn to calculate p prime factor of N, which is
        needed to calculate q.
        n = (p x q)
        Args:
            Constant MODULUS_N (int)

        Returns:
            P prime factor (int)
        """
        count = 0
        #pdb.set_trace()
        """
        Easiest way to check all odd numbers is to start
        just below the square root of N
        """
        possible_p = math.floor(math.sqrt(modulus_n))
        factor = possible_p

        while factor != 0:

            possible_p -= 1
            factor = modulus_n % possible_p
            count += 1

        print("MODULUS_N:{0} ||| possible_p:{1} ||| factor:{2}"\
                 .format(modulus_n, possible_p, factor))

        return possible_p


    def calculate_q(self, modulus_n, prime_p):
        """
        Fn to calculate the remaining prime factor q given the
        following formula:
                            q = n/p
        Args:
            Prime factor p  prime_p (int)
            Modulus         modulus_n (int)

        Returns:
            Prime factor q (int)

        """
        return int(modulus_n/prime_p)

    """
    To break the encyption, we need to find d, the value to decrypt the cipher text
    d can be calculated via this formula:
        d = e^-1  mod phi(n); where phi(n): (p-1)*(q-1)

    but in order to calculate it, we need to find a modular multiplicative inverse (e^-1)
    using the extended Euclidean algorithm
    https://en.wikibooks.org/wiki/Algorithm_Implementation/Mathematics/Extended_Euclidean_algorithm
    """
    def egcd(self, a, b):
        """
        Args:
            a : Public exponent 'e' (int)
            b : phi(n) (p-1)*(q-1)

        """
        if a == 0:
            return (b, 0, 1)
        else:
            g, x, y = self.egcd(b%a, a)
            return (g, y -(b // a) * x, x)

    def modinv(self, a, m):
        g, x, y = self.egcd(a, m)
        if g != 1:
            raise Exception('No modular inverse')
        return x%m

    def decrypt(self, cipher_text, decrypt_key, modulus_n):
        """
        In order to decrypt the cipher text, we need to apply the following formula
        m = c^d mod n;
                Where - m is the plain text message
                      - c is the cipher text
                      - d is the decryption key
                      - n is the modulus

        pow() it's used to calculate the modular exponentiation, due too the size of c^d
        Args:
            cipher_text[]       Array containing the cipher text
            decrypt_key(int)    decryption key calculated
            modulus_n (int)     the modulus already given

        Returns:
            plain_text[]        Array containig the "plain text"
        """
        plain_text = []
        for i, item in enumerate(cipher_text):
            decrypted = pow(int(item), decrypt_key, modulus_n)
            plain_text.append(decrypted)

        return plain_text

    def dec_to_hex(self, plain_text):
        """
        Fn to convert decimal to hexadecimal, this is just a requeriment
        of the challenge in order to get the password
        Args:
            plain_text[]        Array containing the plain_text
        Returns:
            output[]            Array containing each line converted to hex
        """
        output = []
        for i, item in enumerate(plain_text):
            hex_text = hex(int(item))
            output.append(str(hex_text[2:]))
        return output

    def hex_to_ascii(self, output):
        """
        Fn to convert hexadecimal to ascii, this is just a requeriment
        of the challenge in order to get the password
        Args:
            output[]            Array containing hex indexes
        Returns:
            output[]            Array containing each line converted to ascii

        """
        converted = []
        for i, item in enumerate(output):
            #ascio_text = binascii.unhexlify(item).decode('utf8')
            ascii_text = bytes.fromhex(str(item)).decode('utf-8')
            converted.append(ascii_text)

        print(converted)
        return converted

rsa_pseudo_breaker()
