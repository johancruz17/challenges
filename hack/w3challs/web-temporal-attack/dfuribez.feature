## Version 2.0
## language: en

Feature:
  Site:
    w3challs
  Category:
    Web
  User:
    mr_once
  Goal:
    Get the password

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Arch Linux      | 2020.08.01  |
    | Firefox         | 79.0        |
    | Python          | 3.8.5       |
  Machine information:
    Given I am accessing the site at
    """
    http://temporal.hax.w3challs.com/administration.php
    """
    And the source code is at
    """
    http://temporal.hax.w3challs.com/php_portal_administration.php
    """

  Scenario: Success:Understand the source code
    Given I am given the source code
    When I look at it I can see the function "authentication"
    """
    function authentication($your_password)
    {
      // TODO : change the password below choosing a more complex one \
          (! dictionnary word)
      // but it *must* contain only lowercase alphabetical characters
      $password = "unetulipe";
      if(!is_string($your_password) ||
          strlen($your_password) != strlen($password))
        return(0);
        $your_password = strtolower($your_password);
       for($i = 0; $i < strlen($password); $i++)
       {
          if($password[$i] != $your_password[$i])
            return(0);
          usleep(150000);
        }
      return(1);
    }
    """
    Then I can see the function checks the given password against the password
    And for every matching character makes a delay of 150 ms
    But first I need to get the length right

  Scenario: Success:Get password's length
    Given I need the password's length
    And since html's code limits the password's length to 9 characters
    And I code a script to get the password's length
    And the script for a given length iterates over all the letters
    And to that letter it adds "a" until the length is meet
    And makes the request to the page
    And if a request takes about 150ms thats the right length
    When I run the script
    """
    $ python temporal.py
    """
    Then I get
    """
    1.0
    1.0
    1.0
    1.0
    1.0
    1.0
    1.0
    1.0
    1.0
    151.0
    Found! password's length: 9
    """

  Scenario: Fail:Get the password
    Given I know the password's length
    And I code the function "find_password"
    And that functions iterates over all 9 character of the password
    And given the resposne time it tries to figure it out the right letter
    When small fluctuation of the response time occurred
    """
    password  time
    ...
    jkmnaziwj 1202.0
    jkmnaziwk 1203.5
    jkmnaziwl 1202.0
    ...
    """
    Then the password I get at the end was wrong

  Scenario: Success:Get rigth password
    Given that I know small fluctuations result in a bad password
    And I fixed the problem by making several requests and taking the average
    And adding a threshold of 50 to filter out the fluctuations
    When I run the script
    """
    $ python temporal.py
    """
    Then I get the right password [evidence](password.png)
    """
    a******** 1.0
    d******** 1.0
    e******** 1.0
    ...
    jkmnae*** 752.0
    jkmnag*** 751.5
    jkmnah*** 751.5
    jkmnaj*** 752.0
    ...
    jkmnazic* 1052.0
    jkmnazig* 1052.0
    ...
    jkmnaziwv 1202.0
    jkmnaziww 1202.0
    jkmnaziwx 1352.0
    jkmnaziwx
    """
    And I solve the challenge
