# language: en

Feature: Solve challenge 49
  From site W3Challs
  From Cryptography Category
  With my username Skhorn

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am using SageMath 8.2
  Given a web site stating there is a communication between two parties
  And that they using RSA to cipher their communications
  """
  URL: https://w3challs.com/challs/Crypto/rsa_server/index.php
  Message: RSA Server
  Details: The RSA Server of this challenge decrypt messages (base 10 numbers)
  with the private key corresponding to the attached public key, then it
  displays plaintext messages. After having successfully intercepted
  the beginning of a communication between Alice and the server,
  you know that the crypted message of Alice sent to the server is...
  Objective: Find the plaintext of Alice corresponding to the ciphertext
  mentioned.
  Evidence: - Web Site giving the details
            - Public values given, Public Module (N), Public Exponent (E)
            - Initital Ciphertext (C)
            - PHP form acting as Decryption Oracle
            - PHP form to send plaintext message to recieve Code
            - Initial Ciphertext cannot be decrypted by the Oracle
            - RSA Wikipedia link
  """

Scenario: RSA Possible Attacks
RSA is well known for not being easy to recover the decryption key (D), even
if both public module (N) and public exponent (E) are in plaintext, that
kind of operation is not feasible for N larger than 4096 bytes, although
in a challenge, a 322 digits module, posses quite a challenge.
That's why next scenarios will try to cover some fail attempts and the one that
succeded but just in this particular case.

Scenario: Chosen-Plaintext Attack - Failed Attempt
  Given an Oracle decryption function on the web site
  Then I think there is an easy way to recover the plaintext
  And just by generating multiple cipherthext
  And comparing with the one given:
  """
  # Example
  C1 = Ciphertext given, (327 bytes long)
  N = public module, (327 bytes long)
  E = public exponent, (89 bytes long)
  M = Plaintext message, variable length

  Cipher process: M^e mod N.

  This Chosen-Plaintext Attack or Bruteforce, would be like:

  # Pseudo code
  count_m = 0
  while flag
    # Compute cipher
    count_m += 1
    gen_c = (count_m ^ e) % n
    if c1 == c:
      return count_m

  Although it seems factible, the amount of computations would take longer
  than 24-h or more to find the matching plaintext message that generates
  the given ciphertext
  """

Scenario: Chosen-Ciphertext Attack - Failed Attempt
  Given the Oracle decryption function
  And reading about RSA Attacks
  Then I see the CCA Attack
  And it states:
  """
  The decryption oracle on the server, somehow allows
  anyone to decrypt any ciphertext given, unless the one of the challenge.

  This feature can be exploited by using pure math.
  e.g
  1. I have the ciphertext Alice sent to server.

    C = M^e mod N

  2. If I cannot decrypt the original, I migth be able to exploit
    by multyping it by a random value to the power of "e".

    C' = C*r^e mod N

  3. Then I send it to the oracle to be decrypted, which I should get:

    C'^d = (C * r^e)^d = M^e*d * r^e*d = M * r

    (M^e*d mod N) must equal (M^1 mod N)

  4. Then I can take that deciphered output and divide it by
    the random number I picked.

    M*r / r = M

  Thus I get the plain text message
  """
  Then I code a simple example
  And execute it, using shorter values to see if it works fine
  """
  # Private values
  D = 1019

  # Public values
  E = 79
  N = 3337

  # Plaintext message
  M = 8

  # Initital cipher = 8^79 mod 3337
  C = 2807

   # Random value
  r = 3

  # Computing C', 2807*3^79 mod 3337
  C' = 3022

  # Send it to decryption oracle
  Decryption oracle response: 24

  # Dividing response by random number
  M = 24/3
  M = 8
  """
  And this seems reliable
  Then I implement it to work on the challenge
  But while on it, three things happend
  """
  1. Computing pow(long, long, long), where long is more than
  **300 digits**, is no problem for python

  Trying to compute pow(long, 2^long, long) will take days, if the machine
  does not crash or get outs of memory.

  2^(89 digits), generates a number large enough it will even be slower to
  print on terminal.
  """
  Then I try to see if I can reduce the public exponent to shorter factors
  """
  # Public exponent
  E = 8882779616021621291269188171454909248033260109725256757089953784476
  2533291883538593265789 .. (89 digits long)
  """
  And I use sage function factor
  """
  # Factoring E, to reduce it
  factor(E)
  11 * 17^2 * 73 * 181 * 10104133397459 *
  209294518004101087894492419208731162385944068034119813043161654477873
  """
  And It seems more manageable this way, as I can rely on pow properties
  """
  # Pow properties example:
  (((E^11)^17)^17)^73^...
  """
  Then I start to compute it by multiplying each result with pow operation
  """
  # Sage output:
  prop = 3^11
  prop = prop^17
  prop = prop^17
  prop = prop^73
  prop = prop^181
  prop = prop^10104133397459
  MemoryError                           Traceback (most recent call last)
  <ipython-input-93-10ce678e9299> in <module>()
  ----> 1 prop = prop**Integer(10104133397459)
      ...
          2069         sig_off()
      ...
  MemoryError: failed to allocate 1074945105735868872 bytes
  """
  And although the attack seems reliable, is not computable by my machine
  And the last two values cannot be factored
  And I even try to compute 2^(half the size first large factor)
  And My machine crashes

Scenario: Boneh-Durfee attack
Boneh and Durfee attack relies on the fact that Decryption Key (D), is
smaller than N: D < N^0.292, although, in this case it is a supposition
given how large N is. This attack is performed using
Lenstra-Lenstra-Lovasz (LLL) Lattice basis reduction algorithm.
  Given the Boneh-Durfee paper
  """
  # Boneh-Durfee paper
  url: http://antoanthongtin.vn/Portals/0/UploadImages/kiennt2
  /KyYeu/DuLieuNuocNgoai/8.Advances%20in%20cryptology-Eurocrypt
  %201999-LNCS%201592/15920001.pdf
  """
  And a implementation on Sage
  """
  # Boneh-Durfee attack python implementation
  https://github.com/mimoo/RSA-and-LLL-attacks
  """
  And I look at the code to use my parameters
  And I stumble upon some values I need to tweak:
  """
  281     # the hypothesis on the private exponent
      (the theoretical maximum is 0.292)
  282     delta = .18 # this means that d < N^delta

  285     # Lattice (tweak those values)
  288     # you should tweak this (after a first run),
      (e.g. increment it until a solution is found)
  289     m = 4 # size of the lattice (bigger the better/slower)
  """
  Then I execute it
  """
  # Boneh-Durfee script output
  $ sage
  ...
  ...
  7 / 7  vectors are not helpful
  We do not have det < bound. Solutions might not be found.
  Try with highers m and t.
  size det(L) - size e^(m*n) =  3132
  === no solution was found ===
  === 0.327377080917 seconds ===
  """
  And not positive output
  Then I try to tweak those values
  """
  # Tweaking values
  d = 0.292
  m = 5..20
  """
  And I got no result
  But when *m* value was higher than 15, machine crashed after a few minutes

Scenario: Multiplication Propertie of RSA
  Given this attacks won't result because their computable complexity
  When I search a little more on RSA
  And I use the RSA Wikipedia article linked on the challenge
  Then I read something about RSA Multiplicative propertie
  And it states the following:
  """
  # RSA Multiplication Property
  # ≡ Equivalence
  Given public key values (e, N)
  and m1, m2, both plaintext of the user A, then

    (m1 * m2) ≡ (m1)^e * (m2)^e mod N

  proof:
    m1 ≡ m1^e mod N
    m2 ≡ m2^e mod N

  And:

  m1*m2 ≡ (m1*m2)^e ≡ m1^e * m2^e ≡ m1 * m2 mod N
  """
  And a little example will crear this
  """
  # Multiplicative propertie example
  # Public values
  e = 7
   n = 187
  # Decryption key
  d = 23
  # Plaintext messages
  m1 = 88
  m2 = 2
  # Ciphertext
  c1 = power_mod(m1, e, n)
  c1 = 11

  c2 = power_mod(m2, e, n)
  c2 = 128

  # Multiplicative propertie
  m1*m2 = 176
  (c1*c2)^e % n = 176

  So,
    m1*m2 ≡ (c1*c2)^e % n
      176 ≡ 176
  """
  But why is this important?
  And I make this last sample:
  """
  # Decryption
  Given Decryption key is d = 23
  What would happend if I decrypt the product of both ciphertext?

  (c1*c2)^e mod N = 176
  """
  And I see it's the same result of the plaintext multiplication
  Then I just divide it by one of the plaintext to get the other:
  """
  # Getting the plaintext from ciphertext product
  (c1*c2)^e mod N = 176

  176/m1 = 176/88 = 2 = m2
  176/m2 = 176/2  = 88 = m1
  """
  And that's how I will get the plaintext value of the challenge
  And it will be by abusing the Oracle
  Then I retrieve the values of N, E and C from the web site challenge
  """
  # Condensed values taken from the challenge
  # url: https://w3challs.com/challs/Crypto/rsa_server/index.php

  E = 888277961602162129126918817145490924803326010972525675708995378447625332
  91883538593265789

  N = 2046020743161249978251852063345860169633083290549609648194922
  765334935... +200 digits more

  C = 12912220688177891592251675983703832510071236468... +more digits
  """
  And I generate a random second message (m2):
  """
  # Random second message
  m2 = 332601097252567570899537844762533291883538593265789
  """
  Then I encrypt it using the public values (E, N)
  """
  # Encrypting second message
  C2 = (m2^e) % N
  C2 = 151195915089951938885806809533592626101425565598770415328... +digits
  """
  And I make the product of both ciphertext
  """
  # Product output
  C1 * C2 = 98228550829325546768065849126213973691638994... almost 652 digits
  """
  Then I use the oracle of the challenge
  """
  # Oracle function from challenge
  [RSA Server -- w3challs -- 1.0]

  Send me a command and its argument. Two choices are possible :
  1) cmd is "DECRYPT" arg is "crypted message"
  2) cmd is "CODE" arg is "Alice\'s message"

  cmd = "DECRYPT"
  arg = "195227502279246528828432710990507800533556730961195403105..."
  """
  And I get the following result
  """
  # Response
  Decrypted message: 234926919411274846178036562092505975752790275422342232532
  634617741440818402097751674000567098623706238585494424

  Notice I didn't need to make modular operation on the product, (c1*c2)^e % N
  It was just to demonstrate the propertie
  """
  Then I divide that value by the random second message
  And send that value as the Alice's code
  """
  # Getting code by sending plaintext message
  [RSA Server -- w3challs -- 1.0]

  Send me a command and its argument. Two choices are possible :
  1) cmd is "DECRYPT" arg is "crypted message"
  2) cmd is "CODE" arg is "Alice\'s message"

  cmd = "CODE"
  arg = "<plaintext> obtained"
  """
  And I get the password of the challenge
  And I complete the challenge
