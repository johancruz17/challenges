# $ pylint fgomezoso.py
#
# --------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)

"""
This program returns the 74th and 150th terms of the following series

0-1-1-2-3-5-8-13-...
"""
from __future__ import print_function


def series(num):

    """
    Returns the term "n" in the series
    """

    num_1 = 0
    num_2 = 1

    for _ in range(2, num):

        add = num_1 + num_2

        num_1 = num_2
        num_2 = add

    return add


def main():

    """
    Prints the terms in the format: Number1-Number2
    """

    print(series(74), "-", series(150), sep="")


main()

# $ python fgomezoso.py
# 806515533049393-6161314747715278029583501626149
