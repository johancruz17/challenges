# language: en

Feature: Solve Free Flag
  From site CTFS.ME
  From Criptography Category
  With my username Skhorn

Background:
  Given I am running Debian Stretch GNU/Linux kernel 4.9.0-6-amd64
  And I am using Firefox 52.7.3 (64-bit)
  Given a Java web site with a login form
  """
  URL: https://ctfs.me/challenges/CTFs.me#free-flag
  Message: Free flag
  Objective: Decrypt
  Evidence: Alphanumeric set of characters
  """

Scenario Outline: Cipher idetification
Alphanumeric set of characters with no aparent clue
  Given the set of characters
  """
  Q1RGU3tGcmVlX0ZsYWdfRm9yX0FsbH0=
  """
  When I look at it
  And I identify a characteristic character used to path theset
  """
  =
  """
  Then I perform this linux commands to decode it
  """
  $ touch freeflag | echo "Q1RGU3tGcmVlX0ZsYWdfRm9yX0FsbH0=" > freeflag
  $ base64 -d freeflag
  """
  And I look at the output generated
  Then I see in plain text the decoded string
  And use it as solution
