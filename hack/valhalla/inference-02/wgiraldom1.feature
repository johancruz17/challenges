## Version 2.0
## language: en

Feature: Inference attack # 2 - Valhalla
  Site:
    https://halls-of-valhalla.org
  Category:
    Inference Attack
  User:
    williamgiraldo
  Goal:
    Fetch Alice salary, but only with a reduced set of queries

  Background:
  Hacker's software:
    | <Software name>   |  <Version> |
    | Microsoft Windows |     10     |
    | Google Chrome     |  76.0.3809 |
    | PHP               |    7.1.1   |

  Machine information:
    Given I access the challenge URL
    And I get the challenge description
    """
    You may query the database to find some certain statistics such as min,
    max, average, etc. of various data, however queries must include at least
    four users in order to protect each user's privacy. In addition, if your
    queries exclude any users, they must exclude at least four employees
    in the aggregate function.

    Your goal is to determine Alice's salary.
    """
    And I see form controls to manage the SQL query (evidence)[img1.png].

  Scenario: Sucess: ID-name-lookup
    Given I can use Average, Minimum and Maximum
    When I start querying for min. and max. ID
    And I put in the WHERE clause 1=1
    Then I get 1 and 82, respectively.
    Then I use COUNT ID,
    And I put "id <= 10" and "id > 10"
    And I get 9 and 11 respectively.
    Then I use min. and max. ID
    But I put on the WHERE clause "id > 10"
    And "id <= 10"
    Then I get 11 and 10, respectively.
    Then I use min. Name
    And I get Alice. Now I will fetch the ID of Alice.
    When I use min. Name and "id <= 10"
    And I get "Bobby"
    Then I know ID of Alice is bigger than 10.
    Then I use the know IDs
    And query min. Name WHERE "id IN (1,82,9,10)"
    And Alice is there.
    But as I know Alice's ID is bigger than ten,
    Then she must have ID = 82. Now lets use a fact:
    """
      Let X be Alice's Salary, and A, B, C and D salaries of any other users.
      Let S1 = A + B + C + D
      Let S2 = A + B + C + D + X = S1 + X. So X = S2 - S1.
      So, if we know the sums S2 and S1, we know Alice's Salary.
    """
    And we know these sums:
    Given we query sum. Salary WHERE "id IN (1, 9, 10, 11)" {1}
    And sum. Salary WHERE "id IN (1, 9, 10, 11, 82)" {2}
    Then we substract {1} from {2}
    And get Alice's salary: 24562. Our challenge is complete.
    Then the page challenge exhibits a simpler method that makes us look dumb
    And it is in (evidence)[img2.png]
