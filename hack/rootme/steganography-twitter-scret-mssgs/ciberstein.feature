# language: en

Feature:
  Solve Twitter Secret Messages
  From root-me site
  Category Steganography

  Background:
    Given I am running Windows 7 (64bit)
    And I am using Google Chrome Version 70.0.3538.77 (Official Build) (64-bit)

  Scenario:
    Given the challenge URL
    """
    https://www.root-me.org/en/Challenges/Steganography/Twitter-Secret-Messages
    """
    Then I opened that URL with Google Chrome
    And I see the problem statement
    """
    We suspect that this tweet hides a rendezvous point. Help us to find it.

    "Ｃhｏose  a  jοｂ  yоu  lονｅ,  and  you  ｗіｌl
    ｎeｖｅｒ  have  tο  ｗｏrk  a  day  in  yοur  lіfｅ．"

    The validation password is the meeting place (in lower case).
    """
    Then I copied the encrypted message
    And I found a page that decrypts twitter messages
    """
    http://holloway.co.nz/steg/
    """

  Scenario: Successful solution
    Given the challenge url
    Then I open it with Google Chrome
    And I pasted the message in the decryption form
    Then I saw a decoded message
    """
    rendezvous at grand central terminal on friday.
    """
    Then I put as answer grand central terminal and the answer is correct.
    Then I solved the challenge.
