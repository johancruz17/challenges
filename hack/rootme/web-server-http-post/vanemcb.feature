Feature: Solve HTTP-POST
  From the Root Me site
  Category Web-Server
  With my username vanem_cb

Background:
  Given I am running Windows 10 (64 bits)
  And I am using Google Chrome Version 70.0.3538.77 (Build oficial) (64 bits)

Scenario: Normal use case
  Given the challenge URL
  """
  https://www.root-me.org/en/Challenges/Web-Server/HTTP-POST
  """
  Then I opened the URL with Google Chrome
  And I see the challenge statement
  """
  Find a way to beat the top score!
  """
  And I see the input field to submit the answer
  And I see a button
  """
  Start the challenge
  """
  When I click the button
  Then a chrome tab is open whith a new URL
  """
  http://challenge01.root-me.org/web-serveur/ch56
  """
  And I see a text
  """
  RandGame
  Human vs. Machine
  Here is my new game. It's not totally finished but
  I'm sure nobody can beat me! ;)
  Rules: click on the button to hope to generate a great score
  Score to beat: 999999
  """
  And I see a button after the text
  """
  Give a try!
  """
  When I click the button
  Then a new text appears
  """
  Hoo tooooo sad, you lost. Your score: 818895! I'm always the best :)
  """
  Then I don't write nothing in the input field to submit the answer
  And I don't solve the challenge
  But I conclude that I must modify something in the URL's source code

Scenario: Failed Attempt number 1
  Given a Chrome tab with a new URL by clicking the button
  """
  http://challenge01.root-me.org/web-serveur/ch56
  """
  Then I open Google Chrome developer tools
  And I see the URL's source code
  And I look for the lines of code that corresponds to the action of the button
  """
  <form action="" method="post" onsubmit="document.getElementsByName('score')
  [0].value = Math.floor(Math.random() * 1000001)">
  <input type="hidden" name="score" value="-1" />
  <input type="submit" name="generate" value="Give a try!">
  </form>
  """
  And I see an element type "hidden"
  Then I change that element to type "text"
  And I delete the value "-1"
  """
  <form action="" method="post" onsubmit="document.getElementsByName('score')
  [0].value = Math.floor(Math.random() * 1000001)">
  <input type="text" name="score" value/>
  <input type="submit" name="generate" value="Give a try!">
  </form>
  """
  Then appears an enable text box at the left side of the button
  And I write 1000000 in the text box
  And I click the button
  """
  Give a try!
  """
  But a new text appears
  """
  Hoo tooooo sad, you lost. Your score: 364788! I'm always the best :)
  """
  Then I don't write nothing in the input field to submit the answer
  And I don't solve the challenge
  But I conclude that I must modify the lines that compute the value of "score"

Scenario: Successful solution
  Given a Chrome tab with a new URL by clicking the button
  """
  http://challenge01.root-me.org/web-serveur/ch56
  """
  Then I open Google Chrome developer tools
  And I see the URL's source code
  And I look for the lines of code that corresponds to the action of the button
  """
  <form action="" method="post" onsubmit="document.getElementsByName('score')
  [0].value = Math.floor(Math.random() * 1000001)">
  """
  And I see function "Math.random()" multiplicated for the value "10000001"
  And I think that this multiplication could be the problem
  Then I delete the function and the multiplication
  """
  <form action="" method="post" onsubmit="document.getElementsByName('score')
  [0].value = Math.floor(1000001)">
  """
  And I click the button
  """
  Give a try!
  """
  And a new text appears
  """
  Wow, 1000001! How did you do that? :o
  Flag to validate the challenge: H7tp_h4s_N0_s3Cr37S_F0r_y0U
  """
  Then I write the flag in the input field to submit the answer
  And I solve the challenge
