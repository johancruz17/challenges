## Version 2.0
## language: en

Feature: rootme-steganography-yellow-dots

  Site:
    https://www.root-me.org
  Category:
    steganography
  User:
    paolagiraldo
  Goal:
    Find the validation password


  Background:
  Hacker's software:
    | <Software name> | <Version>       |
    | Ubuntu          | 19.10           |
    | Google Chrome   | 81.0.4044.138   |
    | ImageMagick     | 6.9.10-23       |
    |       | 3.34.1-1        |

  Machine information:
    Given I am accesing the site from my browser
    Then I read the challenge's statement
    """
    Statement
    You attend an interview for a forensic investigator job and they give you
    a challenge to solve as quickly as possible (having the Internet).
    They ask you to find the date of printing as well as the serial number of
    the printer in this document.
    You remain dubitative and accept the challenge.
    """
    Then I started the challenge
    And download the .png file [evidence](ch18.png)

  Scenario: Success: Blue channel
    Given The related ressource(s) in the challenge's site
    Then I used ImageMagick in the command line
    Then I run convert -channel RG -fx 0 ch18.png blue.png
    And I got a blue image from the original [evidence](blue.png)
    Then I started to examine the image with zoom in Eye of GNOME
    And I see a grid with black dots [evidence](dots.png)
    Then I searched on google about how to decoding these dots
    And I found the website
    """
    https://noticiasdeseguridadinformatica.wordpress.com/2015/05/14/documentos
    -que-imprime-contienen-su-identidad-su-impresora-espia-a-usted/
    """
    Then I read about the code and how decipher it
    Then I calculated the values from the grid with the instructive
    Then I got the values 5 11 27 7 14 30 29 92 6
    And  The challenge's instructions
    """
    The answer is in the form:
    hh:mm dd/mm/yyyy SSSSSSSS

    with
    - hh: the hour of the event
    - mm: the minutes of the event
    - dd: the day of the event
    - MM: the month of the event
    - yyyy: the year of the event
    - SSSSSSSS: the serial number
    """
    Then From the instructions to decipher the code I knew hh:11
    And mm: 05
    And dd: 27
    And MM: 7
    And yyyy:14
    And SSSSSSSS:3029926
    Then I put into the required format
    And I got 11:05 27/07/2014 06922930
    And I used this as validation password for the challenge
    And I got the message Well done you won 15 points
    And I caught the flag
