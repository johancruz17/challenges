## Version 2.0
## language: en

Feature: stegano-dot-and-next-line
  Site:
    https://www.root-me.org/es/Challenges/Esteganografia/Dot-and-next-line
  User:
    Sierris (wechall)
  Goal:
    Recover the flag from a jpg file

Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ImageMagick     | 6.9.10-23   |
    | Kali Linux      | 4.19.0      |
    | wxHexEditor     | 0.23        |
  Machine information:
    Given A ".zip" file
    Then I unzip the file
    And I get a JPG file "journal.jpg"

  Scenario: Fail: Watch the hex code in wxHexEditor
    Given A JPG file
    When I run "wxHexEditor" to see the hex code
    """
    $ wxHexEditor journal.jpg
    """
    Then I see the image hex code
    When I search for useful information
    Then I don't find anything
    And Watching the hex code did not work
    And I don't get the flag

  Scenario: Success: Analyse the image content
    Given A JPG file
    When I read the forum
    Then They talk about analyzing the image
    And I start analyzing the content
    Then I start forming phrases with letters next to dots
    When I use the letters below the dots I get "uetpah"
    When I use the letters before the dots I get "eresaremg"
    When I use the letters above the dots
    Then I form the phrase with the chosen letters [evidence](image1.jpg)
    And I get the flag
