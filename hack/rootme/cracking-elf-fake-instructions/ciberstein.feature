## Version 2.0
## language: en

Feature: Cracking-Root Me
  Site:
    www.root-me.org
  Category:
    Cracking
  User:
    ciberstein
  Goal:
    Find the fakes instructions.

  Background:
  Hacker's software:
    | <Software name> |    <Version>     |
    | Windows         | 10.0.17134 (x64) |
    | Chrome          | 70.0.3538.77     |
    | MinGW           | 0.602.22         |
    | WinRAR          | 5.61 (x64)       |
  Machine information:
    Given I am accessing the challenge site from my browser
    And the statement is displayed
    """
    Some fakes instructions.

    Compiled with gcc 4.3.2 on Xubuntu 8.04.
    Proc : 32bits x86.

    Start the challenge
    """
    Then I selected the challenge button to start
    And I download the RAR file
    And I have the WinRAR on my system
    Then I extract the file with WinRAR

  Scenario: Success:Debuging the program
    Given I need to access to the program
    And I have MinGW on my system
    Then I run GDB in the Windows command prompt
    And I loaded the program in gdb and disassembled main.
    Then I did notice a suspect function call through edx:
    """
    0x08048694 <main+320>:  mov    eax,DWORD PTR [ebp-0x94]
    0x0804869a <main+326>:  mov    DWORD PTR [esp+4],eax
    0x0804869e <main+330>:  lea    eax,[ebp-42]
    0x080486a1 <main+333>:  mov    DWORD PTR [esp],eax
    0x080486a4 <main+336>:  call   edx
    """
    Then I put a breakpoint on the call
    And I stepped in to have a look at the function and the data referenced
    """
    Dump of assembler code for function WPA:
    0x080486c4 <WPA+0>:     push   ebp
    0x080486c5 <WPA+1>:     mov    ebp,esp
    0x080486c7 <WPA+3>:     sub    esp,0x8
    0x080486ca <WPA+6>:     mov    eax,DWORD PTR [ebp+12]
    ;  _0cGjc5m_.5...
    0x080486cd <WPA+9>:     add    eax,0xb
    0x080486d0 <WPA+12>:    mov    BYTE PTR [eax],0xd
    ;"_0cGjc5m_.5\r..."
    0x080486d3 <WPA+15>:    mov    eax,DWORD PTR [ebp+12]
    0x080486d6 <WPA+18>:    add    eax,0xc
    0x080486d9 <WPA+21>:    mov    BYTE PTR [eax],0xa
    ; "_0cGjc5m_.5\r\n..."
    0x080486dc <WPA+24>:    mov    DWORD PTR [esp],0x804893c
    :  "Vérification de votre mot de passe.."
    0x080486e3 <WPA+31>:    call   0x804846c <puts@plt>
    0x080486e8 <WPA+36>:    mov    eax,DWORD PTR [ebp+12]
    0x080486eb <WPA+39>:    mov    DWORD PTR [esp+4],eax
    ;  "_0cGjc5m_.5\r\n�\2078CJ0�\2009" as first argument
    0x080486ef <WPA+43>:    mov    eax,DWORD PTR [ebp+8]
    0x080486f2 <WPA+46>:    mov    DWORD PTR [esp],eax
    ; my password as second argument
    0x080486f5 <WPA+49>:    call   0x804847c <strcmp@plt>
    ; string comparison here !
    0x080486fa <WPA+54>:    test   eax,eax
    ; here is the test
    0x080486fc <WPA+56>:    jne    0x804870f <WPA+75>
    0x080486fe <WPA+58>:    call   0x804872c <blowfish>
    0x08048703 <WPA+63>:    mov    DWORD PTR [esp],0x0
    0x0804870a <WPA+70>:    call   0x804848c <exit@plt>
    0x0804870f <WPA+75>:    call   0x8048803 <RS4>
    0x08048714 <WPA+80>:    mov    DWORD PTR [esp],0x8048964
    ;  "(!) L'authentification a échoué.\n Try again ! \r"
    0x0804871b <WPA+87>:    call   0x804846c <puts@plt>
    0x08048720 <WPA+92>:    mov    DWORD PTR [esp],0x1
    0x08048727 <WPA+99>:    call   0x804848c <exit@plt>
    """
    Then I found the password test, but the inner password
    """
    "_0cGjc5m_.5\r\n�\2078CJ0�\2009"
    """
    Then I saw is not easy to enter
    And I just override my password address with the inner password
    And i do just before the string comparison
    Then the test succeeded and displayed the solution
    """
    (gdb) cont
    Continuing.
    '+) Authentification réussie...
     U'r root!

     sh 3.0 # password: liberté!
    """
    Then I put as answer "liberté!" and the answer is correct.
    Then I solved the challenge.
