#pylint: disable=invalid-name
#pylint: disable=bad-indentation
#pylint: disable=superfluous-parens
# flake8: noqa

"""
Gets data from challenge pcap
"""

import pyshark

CNAME_TYPE = '5'
MX_TYPE = '15'
TXT_TYPE = '16'

cap = pyshark.FileCapture('/root/Downloads/ch21.pcap')
requests = []
out = ''

pcount = 0

for packet in cap: #Filters queries only
  if packet.dns.flags == '0x00000100':
    requests.append(packet)

qry = requests[2].dns.qry_name.replace('.jz-n-bs.local', '')
qry = qry.replace('.', '')[18:]
qry = qry[16:]
out += qry

for packet in requests[3:73]:
  qry = packet.dns.qry_name.replace('.jz-n-bs.local', '').replace('.', '')[18:]

  out += qry

print(out)
