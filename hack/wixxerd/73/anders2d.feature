## Version 2.0
## language: en

Feature: 73-programming-wixxerd
  Code:
    73
  Site:
    https://www.wixxerd.com
  Category:
    programming
  User:
    anders2d
  Goal:
    Decode the captcha that contains two random English words
    submit a response in 2 seconds

  Background:
  Hacker's software:
    | <Software name>   |  <Version>    |
    | Windows           | 10.0.1809     |
    | Chrome            | 76.0.3809.132 |

  Scenario: fail:try-to-search-capcha-in-source
    Given I can put the captcha manually
    When I try to search the captcha in source code
    Then the capcha is encrypt
    And I could not capture the flag

  Scenario: fail:try-to-decrypt-captcha
    Given I have access to HTML code
    When I search encrypt captcha
    Then I try to use symmetric key decryptography
    And I use the next code
    """
    var chkElement = document.getElementById("chk")
    var captcha = chkElement.value

    str1= {}
    for (each in captcha) {
        str1[each] = captcha.charCodeAt(each)
    }
    for (i=1; i<=50;i++){
        string = ''
        for (eachCode in str1 ) {
            if(str1[eachCode] != 32) {
                string += String.fromCharCode(str1[eachCode]-i)
            }else {
                string +="---"
            }
        }

        console.log(string+ " " + i)
    }
    """
    And I notice that the captcha is decrypt right sometimes (evidence)[img.png]
    But in other cases the capcha is no decrypt
    And I could not capture the flag

  Scenario: Success:try-decrypt-capcha-with-a-hint
    Given I have access to HTML code
    When I see the code and I found that exist the next HTML line
    """
    <input type="hidden" value="2" id="s" name="s">
    """
    Then after several attempts watching the S variable behavior
    Then I notice that if S variables is less than 7, the decrypt is easy
    Then I use the next code in chromer console
    """
    imgs = document.getElementsByTagName('img')

    isFail = false
    for (eachImg in imgs) {
        if(imgs[eachImg].src == "https://www.wixxerd.com/images/fail.jpg"){
            isFail = true
            break
        }
    }
    if(isFail){
        var chkElement = document.getElementById("chk")
        var capInput = document.getElementById("cap")
        var checkButton = document.getElementsByName("submit")[0]
        var crapcha = chkElement.value
        if(s.value <=7) {

        str1= {}
        for (each in crapcha) {
            str1[each] = crapcha.charCodeAt(each)
        }
        string = ""
        var i = s.value
            for (eachCode in str1 ) {
                if(str1[eachCode] != 32) {
                    string += String.fromCharCode(str1[eachCode]-i)
                }else {
                    string +=" "
                }
            }
            capInput.value = string
            checkButton.click()
        }else{
            location.reload(true)
        }
    }
    """
    Then after of 15 attempts the captcha is correct
    And I caught the flag
