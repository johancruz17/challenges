## Version 2.0
## language: en

Feature: 72-programming-wixxerd
  Code:
    72
  Site:
    https://www.wixxerd.com
  Category:
    programming
  User:
    anders2d
  Goal:
    Decode the captcha and submit a response in 2 seconds

  Background:
  Hacker's software:
    | <Software name>   |  <Version>    |
    | Windows           | 10.0.1809     |
    | Chrome            | 76.0.3809.132 |

  Scenario: fail:manual-capcha
    Given I can put the captcha manually
    When I write the capcha
    Then show the nex error
    """
    (Hint: This is NOT the way I am supposed to be implemented!)
    14 seconds is way to slow!
    """
    And I could not capture the flag

  Scenario: Success:view-source-code-and-run-javascript
    Given I have access to HTML code
    When I search the input field
    Then I notice that the capcha is in the source code (evidence)[img1.png]
    And I use the next JS code in the Chromer console once the page load
    """
    var chkElement = document.getElementById("chk")
    var capInput = document.getElementById("cap")
    var crapcha = chkElement.value
    var checkButton = document.getElementsByName("submit")[0]
    capInput.value = crapcha
    checkButton.click()
    """
    And After 3 attempts show the next message:
    """
    You got it!
    Time: 1 seconds
    The password is '<flag>'
    """
    And I caught the flag
