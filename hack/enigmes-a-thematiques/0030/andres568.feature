## Version 2.0
## language: en

Feature: 30-H4CK1NG-enigmes-a-thematiques
  Code:
    30
  Site:
    enigmes-a-thematiques
  Category:
    H4CK1NG
  User:
    andres568
  Goal:
    Found the password for authenticating

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Windows OS      | 10            |
    | Chrome          | 80.0.3987.100 |
  Machine information:
    Given I am accessing to the website
    And the website show a form for authenticating

  Scenario: Success:check-script
    Given I am on a blank page
    And I can see a "popup box" whit a form to enter a password
    When I do not fill the input and submit the form
    Then A new "popup box" appears with the message "Essaye encore !"
    When I press the "Aceptar" button
    Then I can see that the page only have a background
    When I inspect the page to read the html code
    And I realize that there is a script for the "popup box"
    Then I can see "password=='code_source'"
    And I refresh the website
    And I fill out the form with "code_source"
    Then A new "popup box" appears with the message
    """
    Mot de passe correct, tu peux maintenant valider l'épreuve !
    """
    When I set the password to pass the challenge
    Then I see the message "Tu as déjà résolu cette énigme !"
    And I could capture the flag
