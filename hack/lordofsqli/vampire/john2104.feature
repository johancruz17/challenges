## Version 2.0
## language: en

Feature: vampire-sql-injection-lordofsqli
  Site:
    https://los.rubiya.kr/gate.php
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | NixOS           | 19.09.2260          |
  | Chromium        | 78.0.3904.87        |

  Machine information:
    Given the challenge URL
    """
    https://los.rubiya.kr/chall/vampire_e3f1ef853da067db37f342f3a1881156.php
    """
    When I open the url with Chrome
    Then I see the PHP code that validates the user id
    And It shows the query made on the screen
    """
    select id from prob_vampire where id=''
    """
  Scenario: Fail:Sql-injection
    Given the PHP code
    And knowing that answer is correct when It returns:
    """
    VAMPIRE Clear!
    """
    When I try with the well known payload:
    """
    id='admin
    """
    Then I get nothing
    But I don't solve it

   Scenario: Success:Sqli-uppercase-technique
    Given I inspect the code
    And I see that they validate the admin string with a replace
    And change the id input to lowercase
    And the char single quote
    """
    if(preg_match('/\'/i', $_GET[id])) exit("No Hack ~_~");
    $_GET[id] = strtolower($_GET[id]);
    $_GET[id] = str_replace("admin","",$_GET[id]);
    """
    When I add admin to an admin string into the request
    """
    ?id=aadmindmin
    """
    Then I solve the challenge
