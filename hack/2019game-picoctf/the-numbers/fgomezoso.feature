## Version 2.0
## language: en

Feature: the numbers - crypto - picoctf
  Site:
    picoctf 2019
  Category:
    crypto
  User:
    fgomezoso
  Goal:
    Enter the flag in the format PICOCTF{}

   Background:
   Hacker's software:
     | <Software name> | <Version>    |
     | Windows OS      | 7            |
     | Chrome          | 78.0.3904.70 |
     | Python          | 3.8.0        |
  Machine information:
    Given I am accessing the challenge site via browser
    """
    https://2019game.picoctf.com/problems
    """
    And the challenge has a flag field
    And the challenge provides a picture with numbers
    """
    16 9 3 15 3 20 6 { 20 8 5 14 21 13 2 5 18 19 13 1 19 15 14 }
    """

  Scenario: Success:Making a script
    Given I know the flag format "PICOCTF{}"
    When I check the picture
    Then I can see there are brackets in between the numbers
    And I suspect that every number is related with an alphabet letter
    When I check the 16th position in the alphabet
    Then I find out that "P" is the letter number 16
    And It matches with the first letter of the flag format
    When I write the code "fgomezoso.py"
    And I use the numbers from the image as the input
    Then I find the flag
    """
    PICOCTF{<FLAG>}
    """
    And I solve the challenge
