## Version 2.0
## language: en

Feature: Empire1 - web - picoCTF
  Code:
    Empire1
  Site:
    2019game.picoctf.com
  Category:
    web
  User:
    lovecraft
  Goal:
    capture a flag

  Background:
  Hacker's software:
    | <Software name>    | <Version>           |
    | Ubuntu             | 18.04               |
    | FireFox            | 72.0.1(64 bits)     |
  Machine information:
    When I enter to the challenge I see a welcome message
    Then I see that I need to register in the webapp
    And after the registration I access to the only form "add Todo"
    And I assume the challenge consist in make something with this form

  Scenario: Failed: XSS
    When I try some basics XSS payloads in the text field
    Then I go to check the result in the "Your todos" options
    And the only result that I see is this
    """
      Very Urgent: <strong> <img src=x onclick="alert(1)"><strong>
    """

  Scenario: Failed: SQL injection
    When I insert a single quote in the field
    Then the web app crash itself
    And I deduce that the web app is vulnerable to sqli
    When I try differents types o injections like
    """
    ' select * from user
    ' select username from user
    'sel'%00 'ect' username from user
    """
    Then I see no result, only the error screen

  Scenario: Success: Blind SQLI and a lot research
    When I enter the next payload
    """
    '||(select username from user)||'
    """
    Then I see in the "Your Todo" option the next result
    """
    Very Urgent: danny.tunitis
    """
    When I change the query and assign my id like this
    """
    '||(select secret from user where id = 3)||'
    """
    Then I can see the flag in the "Your todo" option
    And now I can finish the challenge
