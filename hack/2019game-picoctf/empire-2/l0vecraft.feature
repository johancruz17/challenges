## Version 2.0
## language: en

Feature: Empire2 - web - picoCTF
  Code:
    Empire2
  Site:
    2019game.picoctf.com
  Category:
    web
  User:
    lovecraft
  Goal:
    capture a flag

  Background:
  Hacker's software:
    | <Software name>    | <Version>           |
    | Ubuntu             | 18.04               |
    | FireFox            | 72.0.1(64 bits)     |
    | BurpSuite Community| 2.1.07 (64 bits)    |
  Machine information:
    When I enter to the challenge I see a welcome message
    Then I see that I need to register in the webapp
    And after the registration I access to the only form "add Todo"
    And I assume the challenge consist in make something with this form

  Scenario: Failed: SQL injection
    When I insert a single quote in the field
    Then nothing happen
    When I try differents types o injections like
    """
    ' select * from user
    ' select username from user
    'sel'%00 'ect' username from user
    """
    Then I see the "To Do's" in my "To do's" page
    And I deduce that the web app now it is not vulnerable to SQLi

  Scenario: Success: Decode de cookie
    When I make any action in the web app I see a session cookie
    Then after search a lot, I notice the cookie session is from flask
    When I decode the cookie using the decoder in the web page
    """
    https://www.kirsle.net/wizards/flask-session.cgi
    """
    Then I can see the data of the decodification
    """
    {
    "_fresh": true,
    "_id": "<ID/>",
    "csrf_token": "<TOKEN/>",
    "dark_secret": "<FLAG/>",
    "user_id": "4"
    }
    """
    And I can see the flag and solve the challenge
