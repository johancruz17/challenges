## Version 2.0
## language: en

Feature: 1-Crypto - Cryptogaphy - cft-csirt
  Site:
    https://cc-csirt.policia.gov.co/servicios/ctf
  Category:
    Cryptogaphy
  User:
    Fluid
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04 LTS   |
    | Firefox         | 74.0        |
  Machine information:
    Given The challenge
    """
    HORWRUULQRODULQJRORJRGHSDUDQJDULFXWLULPLFXDUR
    """
    And A hint
    """
    La palabra que encuentras es, sin embargo, existen diversas
    formas de escribirla. Lograrás encontrar la forma correcta.
    """

  Scenario: Fail: Search the challenge in Google
    Given The challenge
    When I search the challenge in Google
    Then I found nothing
    And I couldn't get the flag

  Scenario: Fail: Decrypt the challenge
    When I check the challenge
    Then I try to use common ciphers
    And I try the caesar cipher on this page
    """
    https://www.dcode.fr/caesar-cipher
    """
    When I put the challenge string
    Then I get the next string
    """
    ELOTORRINOLARINGOLOGODEPARANGARICUTIRIMICUARO
    """
    When I try to put this string as flag
    Then I don't solve the challenge

  Scenario: Try other format for the string
    Given The next string
    When I convert to lowercase
    Then I try the new string as flag
    And I solve the challenge
