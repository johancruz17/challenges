## Version 2.0
## language: en

Feature: templed-crypto-hackthebox
  Site:
    https://www.hackthebox.eu
  Category:
    crypto
  User:
     martialwolf
  Goal:
    Find the flag on the given zip

  Background:
  Hacker's software:
    | <Software name> | <Version>           |
    | NixOS           | 19.09.2260          |
    | Chromium        | 78.0.3904.87        |

  Scenario: Success: ctf
    Given I downloaded the file called Templed.zip
    When I proceeded to decompress it
    Then it showed a PNG file called Scroll.png
    And I find within the file there is a message encoded [scroll.png]
    When I realized that the description refered to a temple encryption
    Then I decided to investigate websites deciphering those
    And I found the following web page
    """
    https://en.wikipedia.org/wiki/The_Ciphers_of_the_Monks
    """
    When I decoded it I got the following numbers
    """
    72 84 66 123 77 48 78 107 115 95 107 78 51 119 33 125
    """
    And I converted those numbers into ASCII letters
    Then I got the flag
