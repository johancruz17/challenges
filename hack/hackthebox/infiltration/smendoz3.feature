## Version 2.0
## language: en

Feature: Infiltration - OSINT - Hack the Box
  Site:
    https://www.hackthebox.eu
  Category:
    OSINT
  User:
    arguoK
  Goal:
    Find the flag

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 16.04       |
    | Firefox         | 72.0.2      |
  Machine information:
    Given A text
    """
    Can you find something to help you break into the company 'Evil Corp LLC'.
    Recon social media sites to see if you can find any useful information.
    """
    And the flag format
    """
    HTB{flag}
    """
    And challenge category OSINT

  Scenario: Fail: Wrong flag
    Given The information text
    And Category OSINT
    When I google "Evil Corp LLC"
    Then I see "LinkedIn", "Instagram" and "Apollo" in top 3 results
    When I enter "LinkedIn" site
    Then I see a flag format [evidence](img1.png)
    When I use a cipher/encode analyzer
    Then I find it's a Base64 encode [evidence](img2.png)
    And decoded say "You can do this, keep going!!!"
    And I don't find anything useful

  Scenario: Success: Flag in Instagram photo
    Given The information text
    And category OSINT
    Then I see "LinkedIn", "Instagram" and "Apollo" in top 3 results
    When I enter "Instagram" site
    Then I see a person profile named "Eryn Mcmahon"
    And It works as "Relational Factors Analyst" at "Evil Corp LLC"
    And have 10 photos
    When I look for each one of them
    Then I found only one with comments
    When I open the image
    Then I can see a credential of "E-Corp"
    When I zoom up the photo [evidence](img3.png)
    Then I can see the "HTB{flag}" format
    And I caught the flag
